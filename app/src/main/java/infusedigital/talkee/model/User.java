package infusedigital.talkee.model;

import android.content.SharedPreferences;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import infusedigital.talkee.activity.MyApplication;

/**
 * Created by Lincoln on 07/01/16.
 */
public class User implements Serializable {
    public int id;
    public String name;
    public String phonenum;
    public String emoticol;
    public String mood;
    public String photo_url;
    public String gcm_id;
    public String device_id;

    public void setUser(JSONObject obj) throws JSONException {
        if (!obj.isNull("userId"))
            id = obj.getInt("userId");

        if (!obj.isNull("name"))
            name = obj.getString("name");

        if (!obj.isNull("phonenum"))
            phonenum = obj.getString("phonenum");

        if (!obj.isNull("emoticol"))
            emoticol = obj.getString("emoticol");

        if (!obj.isNull("mood"))
            mood = obj.getString("mood");

        if (!obj.isNull("photo_url"))
            photo_url = obj.getString("photo_url");

        if (!obj.isNull("gcm_regid"))
            gcm_id = obj.getString("gcm_regid");

        if (!obj.isNull("deviceid"))
            device_id = obj.getString("deviceid");
    }

    public void initialize() {
        if (!TextUtils.isEmpty(name))
            name = "";

        if (!TextUtils.isEmpty(phonenum))
            phonenum = "";

        if (!TextUtils.isEmpty(photo_url))
            photo_url = "";

        if (!TextUtils.isEmpty(emoticol))
            emoticol = "";

        if (!TextUtils.isEmpty(mood))
            mood = "";

        if (!TextUtils.isEmpty(photo_url))
            photo_url = "";

        if (!TextUtils.isEmpty(gcm_id))
            gcm_id = "";

        if (!TextUtils.isEmpty(device_id))
            device_id = "";
    }

    /**
     * Set all details of Users
     *
     * @param preferences value of prefrences
     */
    public void setUserFromPreferences(SharedPreferences preferences) {
        id = Integer.parseInt(preferences.getString("userId", "0"));
        name = preferences.getString("name", null);
        emoticol = preferences.getString("color", "#750cc6");
        mood = preferences.getString("mood", "Available");
        photo_url = preferences.getString("photoUrl", null);
        phonenum = preferences.getString("phonenum", null);
        gcm_id = preferences.getString("gcm_regid", null);
        device_id = preferences.getString("deviceid", null);
    }

}
