package infusedigital.talkee.model;

/**
 * This class represents twitter in user profile.
 */
public class TwitterUser {
    public String name;

    public String email;

    public String description;

    public String pictureUrl;

    public String bannerUrl;

    public String language;

    public long id;
}
