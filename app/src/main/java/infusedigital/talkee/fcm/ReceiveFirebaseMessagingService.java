package infusedigital.talkee.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.LauncherActivity;
import infusedigital.talkee.activity.MessageScreen;
import infusedigital.talkee.utilities.AppConfig;

/**
 * This will use for the receiving data from the notification
 */
public class ReceiveFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    NotificationCompat.Builder notificationBuilder;
    private Notification notification;

    public ReceiveFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        System.out.println("++++++++++++ data:" + remoteMessage.getData().toString());
        System.out.println("+++++ remoteMessage:" + remoteMessage.getData().get("flag"));
        System.out.println("+++++ remoteMessage:" + remoteMessage.getData().get("sender_id"));
        System.out.println("+++++ remoteMessage:" + remoteMessage.getData().get("reciver_id"));
        System.out.println("+++++ ic_launcher:" + remoteMessage.getNotification().getIcon());
        System.out.println("+++++ remoteMessage:" + remoteMessage.getNotification().getIcon());
        System.out.println("+++++ onMessageReceived:" + remoteMessage.getFrom() + ":" + remoteMessage.getNotification().getTitle() + ":" + remoteMessage.getNotification().getBody());

//        System.out.println("+++++ AppConfig.isMessageScreen:" + AppConfig.isMessageScreen);
//        if (remoteMessage.getData().get("flag").equals("3") &&
//                AppConfig.isMessageScreen &&
//                MessageScreen.sender_id.equals(remoteMessage.getData().get("sender_id"))) {
//            new Handler(Looper.getMainLooper()).post(new Runnable() {
//                public void run() {
//                    MessageScreen.user_id = remoteMessage.getData().get("reciver_id");
//                    MessageScreen mMessageScreen = new MessageScreen();
//                    mMessageScreen.getAllMessages(false);
//                }
//            });
//        }
//        int requestCode = ("someString" + System.currentTimeMillis()).hashCode();

        Intent intent = new Intent(this, LauncherActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
//        PendingIntent pIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/" + remoteMessage.getNotification().getSound());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notification = new NotificationCompat.Builder(this)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(remoteMessage.getNotification().getTitle())
                    .setContentText(remoteMessage.getNotification().getBody())
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getNotification().getBody()))
                    .setContentIntent(pIntent)
                    .setSound(defaultSoundUri)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 1000})
                    .getNotification();
//            .setSmallIcon(R.drawable.ic_launcher)
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
        } else {
            notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(remoteMessage.getNotification().getTitle())
                    .setContentText(remoteMessage.getNotification().getBody())
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getNotification().getBody()))
                    .setContentIntent(pIntent)
                    .setSound(defaultSoundUri)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 1000})
                    .getNotification();
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
        }
        notification.priority = Notification.PRIORITY_MAX;

        SharedPreferences prefs = getSharedPreferences("UserInfo", MODE_PRIVATE);
        String loginState = prefs.getString("loginstate", "false");
        if (loginState.equals("true")) {
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            manager.notify(requestCode, notification);
            manager.notify(0, notification);

            if (AppConfig.sender_id != null && AppConfig.sender_id.equals(remoteMessage.getData().get("sender_id"))) {
                Intent mIntent = new Intent("talkee.response");
                mIntent.putExtra("response", "");
                sendBroadcast(mIntent);
            }
        }
    }
}