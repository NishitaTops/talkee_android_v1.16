package infusedigital.talkee.fcm;

import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * This will use for generating the device token while app will open
 */
public class FirebaseInstanceIDService extends FirebaseInstanceIdService {
    public FirebaseInstanceIDService() {
        System.out.println("+++++ FirebaseInstanceIDService");
    }

    @Override
    public void onTokenRefresh() {
        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        System.out.println("++++++ onTokenRefresh:" + deviceToken);
        SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        prefs1.getString("gcm_regid", deviceToken);
//        AppSettings.setDeviceToken(getApplicationContext(), deviceToken);
    }
}