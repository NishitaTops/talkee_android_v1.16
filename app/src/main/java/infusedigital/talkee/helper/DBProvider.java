package infusedigital.talkee.helper;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.utilities.Utility;

/**
 * Created by Super on 8/24/2016.
 */
public class DBProvider extends ContentProvider {

    static final String AUTHORITY = "infusedigital.talkee.provider.Messages";
    static final String URL = "content://" + AUTHORITY + "/messages";

    public static final Uri CONTENT_URI = Uri.parse(URL);

    public static final String GROUP_ID = "group_id";
    public static final String STATE = "state";
    public static final String FILE_PATH = "file_path";

    public static final String _ID = "_id";
    public static final String FRIEND_ID = "friend_id";
    public static final String MESSAGE_URL = "message_url";
    public static final String USER_ID = "user_id";
    public static final String FILENAME = "filename";
    public static final String CREATED = "created";

    private static HashMap<String, String> MESSAGES_PROJECTION_MAP;

    static final int MESSAGES = 1;
    static final int MESSAGES_ID = 2;

    static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "messages", MESSAGES);
        uriMatcher.addURI(AUTHORITY, "messages/#", MESSAGES_ID);
    }

    private static SQLiteDatabase db;
    static final String DATABASE_NAME = "Messages";
    static final String MESSAGES_TABLE_NAME = "messages";
    static final int DATABASE_VERSION = 1;

    //    static final String CREATE_DB_TABLE =
//            " CREATE TABLE " + MESSAGES_TABLE_NAME +
//                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
//                    " friend_id TEXT NOT NULL, " +
//                    " group_id TEXT NOT NULL, " +
//                    " state TEXT, " +
//                    " message_url TEXT, " +
//                    " file_path TEXT); ";
    static final String CREATE_DB_TABLE =
            " CREATE TABLE " + MESSAGES_TABLE_NAME +
                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " user_id TEXT, " +
                    " friend_id TEXT, " +
                    " message_url TEXT, " +
                    " filename TEXT, " +
                    " created TEXT); ";

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS" + MESSAGES_TABLE_NAME);
            onCreate(db);
        }
    }


    @Override
    public boolean onCreate() {
        Context context = getContext();
        DatabaseHelper dbHelper = new DatabaseHelper(context);

        db = dbHelper.getWritableDatabase();
        return (db == null) ? false : true;
    }

    /**
     * check record exist or not
     *
     * @param msgURL
     * @return
     */
    public static boolean isExists(String msgURL) {
        Cursor cursor = db.rawQuery("select 1 from " + MESSAGES_TABLE_NAME + " where " + MESSAGE_URL + "=?",
                new String[]{msgURL});
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }


    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(MESSAGES_TABLE_NAME);

        switch (uriMatcher.match(uri)) {
            case MESSAGES:
                qb.setProjectionMap(MESSAGES_PROJECTION_MAP);
                break;
            case MESSAGES_ID:
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

//        if (sortOrder == null || sortOrder == "") {
//            /**
//             * sort with cards name
//             */
//            sortOrder = FILE_PATH;
//        }

//        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, "");

        /**
         * register to watch a content URI for changes;
         */
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    /**
     * Get all audios of the user
     *
     * @param mContext
     * @param userId
     * @return
     */
    public static ArrayList<String> getAllAudioOfUser(Context mContext, String userId) {
        System.out.println("+++++++++++ all record of :" + userId);
        DatabaseHelper dbHelper = new DatabaseHelper(mContext);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        ArrayList<String> arrAudioList = new ArrayList<>();
        Cursor cur = db.rawQuery("SELECT * FROM " + MESSAGES_TABLE_NAME + " WHERE " + USER_ID + " = ? OR " + FRIEND_ID + " = ?", new String[]{userId, userId});
//        Cursor cur = db.rawQuery("SELECT * FROM " + MESSAGES_TABLE_NAME , null);
        if (cur.getCount() != 0) {
            cur.moveToFirst();
            do {
                String row_values = "";
                for (int i = 0; i < cur.getColumnCount(); i++) {
                    row_values = row_values + " || " + cur.getString(i);
                }
                System.out.println("+++++++++++++++++++++++++ getAllAudioOfUser:" + row_values);
                arrAudioList.add(cur.getString(3));
//                all data: || 3 || 677 || 642 || http://gettalkee.com/talkee_app/uploads/message/Talkee-hello.m4a || Talkee-hello.m4a || 2016-12-20 11:59:19
            } while (cur.moveToNext());
            return arrAudioList;
        }
        return arrAudioList;
    }

    /**
     * clear all data if older
     *
     * @param mContext
     */
    public static void clearAllOlderAudios(Context mContext) {
        DatabaseHelper dbHelper = new DatabaseHelper(mContext);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

//        Cursor cur = db.rawQuery("SELECT * FROM " + MESSAGES_TABLE_NAME + " WHERE " + USER_ID + " = ? OR " + FRIEND_ID + " = ?", new String[]{userId, userId});
        Cursor cur = db.rawQuery("SELECT * FROM " + MESSAGES_TABLE_NAME, null);
        if (cur.getCount() != 0) {
            cur.moveToFirst();
            do {
                String row_values = "";
                for (int i = 0; i < cur.getColumnCount(); i++) {
                    row_values = row_values + " || " + cur.getString(i);
                }
                System.out.println("+++++++++++++++++++++++++ all data:" + row_values);
//                all data: || 3 || 677 || 642 || http://gettalkee.com/talkee_app/uploads/message/Talkee-hello.m4a || Talkee-hello.m4a || 2016-12-20 11:59:19

                deleteIfOlder(mContext, cur);
            } while (cur.moveToNext());
        }
    }

    /**
     * delete older records
     *
     * @param mContext
     * @param cur
     */
    private static void deleteIfOlder(Context mContext, Cursor cur) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date startDate = dateFormat.parse(cur.getString(5));
            Date endDate = dateFormat.parse(dateFormat.format(new Date()));
            System.out.println("++++++++++++ is 1 day complete:" + Utility.dateDifference(startDate, endDate));
            if (Utility.dateDifference(startDate, endDate)) {
                // 1 day complete
                deleteRowAndFile(mContext, cur.getString(3));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * delete file and record
     *
     * @param mContext
     * @param url
     */
    public static void deleteRowAndFile(Context mContext, String url) {
        mContext.getContentResolver().delete(DBProvider.CONTENT_URI, DBProvider.MESSAGE_URL + "=?", new String[]{url});
        File audioFile = new File(MyApplication.getInstance().getMessageDir() +
                url.substring(url.lastIndexOf("/") + 1));
        if (audioFile.exists())
            audioFile.delete();
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case MESSAGES:
                return "vnd.android.cursor.dir/vnd.talkee.messages";

            case MESSAGES_ID:
                return "vnd.android.cursor.item/vnd.talkee.messages";

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID = db.insert(MESSAGES_TABLE_NAME, "", values);

        //if success
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case MESSAGES:
                count = db.delete(MESSAGES_TABLE_NAME, selection, selectionArgs);
                break;
            case MESSAGES_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete(MESSAGES_TABLE_NAME, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case MESSAGES:
                count = db.update(MESSAGES_TABLE_NAME, values, selection, selectionArgs);
                break;

            case MESSAGES_ID:
                count = db.update(MESSAGES_TABLE_NAME, values, _ID + " = " + uri.getPathSegments().get(1) +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }
}
