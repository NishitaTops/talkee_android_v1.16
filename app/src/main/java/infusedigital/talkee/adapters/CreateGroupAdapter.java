package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.datamodel.SearchUserData;
import infusedigital.talkee.model.User;
import infusedigital.talkee.view.UserBlankAvatar;

/**
 * Created by Super on 8/29/2016.
 */
public class CreateGroupAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private ArrayList<SearchUserData> friendList;

    private Activity mActivity;
    public static ArrayList<ViewHolder> viewHolderList;

    public CreateGroupAdapter(Activity activity, ArrayList<SearchUserData> friendsdata) {

        viewHolderList = new ArrayList();

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        friendList = friendsdata;
        mActivity = activity;
    }

    public void setData(ArrayList<SearchUserData> data){
        friendList = data;
    }

    public ArrayList<SearchUserData> getFriendData(){
        return friendList;
    }

    @Override
    public int getCount() {
        return friendList.size();
    }

    @Override
    public Object getItem(int position) {
        return viewHolderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.send_list_item, parent, false);
            holder = new ViewHolder();

            holder.friendPhotoView= (ImageView) convertView.findViewById(R.id.receiver_photo);
            holder.friendBlankPhoto = (UserBlankAvatar)convertView.findViewById(R.id.receiver_photo_blank);
            holder.friendNameView = (TextView) convertView.findViewById(R.id.receiver_name);
            holder.cancelView = (CircleImageView)convertView.findViewById(R.id.receiver_photo1);
//            holder.default_state = (RelativeLayout) convertView.findViewById(R.id.default_state);
//            holder.friendPhoneNumView = (TextView)convertView.findViewById(R.id.send_profile_phone);

            viewHolderList.add(holder);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.friendNameView.setSelected(true);
        holder.friendNameView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.friendNameView.setMarqueeRepeatLimit(-1);
        holder.friendNameView.setText(friendList.get(position).friend.name);

        holder.friendPhoneNumView.setText(friendList.get(position).friend.phonenum);

        holder.friendNameView.setTypeface(MyApplication.semiboldFont);

//        if (friendList.get(position).friend.mood.equals("Available")) {
//            CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
//            statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb93")));
//        } else if (friendList.get(position).friend.mood.equals("Away")) {
//            CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
//            statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#ffce0c")));
//        } else {
//            CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
//            statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#f52e62")));
//        }

        holder.cancelView.setVisibility(View.GONE);
        holder.friendBlankPhoto.setVisibility(View.GONE);
        holder.friendPhotoView.setVisibility(View.VISIBLE);

        if (friendList.get(position).isSelected) {
            holder.friendPhotoView.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb93")));
            holder.cancelView.setVisibility(View.VISIBLE);
            holder.friendNameView.setTextColor(Color.parseColor("#35eb93"));
            holder.friendPhoneNumView.setTextColor(Color.parseColor("#35eb93"));
        } else {
            holder.cancelView.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(friendList.get(position).friend.photo_url)){
                if (friendList.get(position).friend.photo_url.contains("http"))
                    Picasso.with(mActivity).load(friendList.get(position).friend.photo_url).placeholder(R.drawable.avatar_blank).fit().centerCrop().into(holder.friendPhotoView);
                else
                    Picasso.with(mActivity).load(Config.server + friendList.get(position).friend.photo_url).placeholder(R.drawable.avatar_blank).fit().centerCrop().into(holder.friendPhotoView);
            }else {
                holder.friendPhotoView.setVisibility(View.GONE);
                holder.friendBlankPhoto.setVisibility(View.VISIBLE);
                MyApplication.getInstance().setBlankAvatar(holder.friendBlankPhoto, friendList.get(position).friend.name, true);
            }

            holder.friendNameView.setTextColor(Color.parseColor("#000000"));
            holder.friendPhoneNumView.setTextColor(Color.parseColor("#3a3a3a"));
        }
        return convertView;
    }


    public static class ViewHolder {
        public ImageView friendPhotoView;
        public UserBlankAvatar friendBlankPhoto;
        public TextView friendNameView;
//        public RelativeLayout default_state;
        public TextView friendPhoneNumView;
        public CircleImageView cancelView;
    }


}
