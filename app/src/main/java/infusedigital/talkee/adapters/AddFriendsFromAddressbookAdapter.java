package infusedigital.talkee.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.AddFriendsFromContacts;
import infusedigital.talkee.activity.AddressBookActivity;
import infusedigital.talkee.datamodel.AddressBookData;
import infusedigital.talkee.utilities.AppConfig;

/**
 * Created by bluesky on 5/10/2016.
 */
public class AddFriendsFromAddressbookAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<AddressBookData> mItems;
    //    private AddressBookActivityNew mActivity;
    private AddressBookActivity mActivity;

//    public AddressBookListAdapter(Activity activity, ArrayList<AddressBookData> contactsdata) {
//
//        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mItems = contactsdata;
//        mActivity = (AddressBookActivityNew) activity;
//    }

    public AddFriendsFromAddressbookAdapter(Activity activity, ArrayList<AddressBookData> contactsdata) {

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = contactsdata;
        mActivity = (AddressBookActivity) activity;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

//        boolean isShow = true;
        if (convertView == null) {
//            if (mItems.get(position).getUserId() != null && mItems.get(position).getUserId().equals(AppConfig.userID)) {
//                convertView = mInflater.inflate(R.layout.addressbook_list_item_empty, parent, false);
//                isShow = false;
//            } else {
//                isShow = true;
            convertView = mInflater.inflate(R.layout.addressbook_list_item, parent, false);
            holder = new ViewHolder();
            holder.contact_photo = (ImageView) convertView.findViewById(R.id.contact_photo);
            holder.contact_name = (TextView) convertView.findViewById(R.id.contact_name);
            holder.contact_num = (TextView) convertView.findViewById(R.id.contact_number);
            holder.contact_invite = (TextView) convertView.findViewById(R.id.contact_invite);
            holder.llMain = (LinearLayout) convertView.findViewById(R.id.llMain);

            convertView.setTag(holder);
//            }
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if (isShow) {
//        System.out.println("++++++++ adapter:" + position + "---" + mItems.get(position).userId);
        if (mItems.get(position).userId != null && mItems.get(position).userId.length() > 0) { // common friend
            if (mItems.get(position).photo_url.length() > 0) {
                Picasso.with(mActivity)
                        .load(mItems.get(position).photo_url)
                        .placeholder(R.drawable.ic_appicon)
//                        .resize(50, 50)
                        .into(holder.contact_photo);
            } else {
                Picasso.with(mActivity)
                        .load(R.drawable.ic_appicon)
                        .placeholder(R.drawable.ic_appicon)
                        .into(holder.contact_photo);
            }

            holder.contact_name.setText(mItems.get(position).name);
            holder.contact_num.setText(mItems.get(position).phonenum);

        } else { // from contact
            if (mItems.get(position).photoUri != null) {
                holder.contact_photo.setImageURI(mItems.get(position).photoUri);
            } else {
                holder.contact_photo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.avatar_blank));
            }

            holder.contact_name.setText(mItems.get(position).name);
            holder.contact_num.setText(mItems.get(position).phonenum);
        }

        if (mItems.get(position).isSent) {
            holder.contact_invite.setText("Sent");
            if (mItems.get(position).getUserId() != null && mItems.get(position).getUserId().length() > 0) {
                holder.contact_invite.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                holder.contact_invite.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.drawable_btn_skip));
            } else {
                holder.contact_invite.setTextColor(mActivity.getResources().getColor(android.R.color.white));
                holder.contact_invite.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.register_back_selected));
            }
        } else {
//            System.out.println("++++++++++++++++ userid:" + position + "::" + mItems.get(position).getUserId() + ":::" + AppConfig.userID);
            if (mItems.get(position).getUserId() != null && mItems.get(position).getUserId().length() > 0) {
                if (mItems.get(position).getIsfollow().equals("0")) {
                    holder.contact_invite.setText("Add");
                } else if (mItems.get(position).getIsfollow().equals("1")) {
                    holder.contact_invite.setText("Friend");
                } else if (mItems.get(position).getIsfollow().equals("2")) {
                    holder.contact_invite.setText("Sent");
                } else if (mItems.get(position).getIsfollow().equals("3")) {
                    holder.contact_invite.setText("Accept");
                }
//                holder.contact_invite.setText("Add");
                holder.contact_invite.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                holder.contact_invite.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.drawable_btn_skip));
            } else {
                holder.contact_invite.setText("Invite");
                holder.contact_invite.setTextColor(mActivity.getResources().getColor(android.R.color.white));
                holder.contact_invite.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.register_back_selected));
            }
        }

        if (mItems.get(position).getUserId() != null && mItems.get(position).getUserId().equals(AppConfig.userID)) {
            holder.contact_invite.setVisibility(View.GONE);
//                holder.llMain.setVisibility(View.GONE);
        } else {
            holder.contact_invite.setVisibility(View.VISIBLE);
        }

        holder.contact_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddressBookData currentItem = mItems.get(position);
                if (!currentItem.isSent()) {
                    if ((currentItem.getUserId() == null || currentItem.getUserId().length() == 0) && !currentItem.isSent) {
//                        if (mActivity instanceof AddFriendsFromContacts) {
//                            new AddFriendsFromContacts().sendInvite(position);
//                        }
//                        if (mActivity instanceof AddressBookActivity) {
                        mActivity.sendInvite(position);
//                        }
                    } else {
//                        if (mActivity instanceof AddFriendsFromContacts) {
//                            if (!new AddFriendsFromContacts().isLoading)
//                                new AddFriendsFromContacts().sendRequest(currentItem.getUserId());
//                            else
//                                Toast.makeText(mActivity, "Please wait.", Toast.LENGTH_LONG).show();
//                        }
//                        if (mActivity instanceof AddressBookActivity) {
                        if (mItems.get(position).getIsfollow().equals("3")) {
                            mActivity.ActionOnRequest(currentItem.getUserId(), AppConfig.getUserId(mActivity));
                        } else if (mItems.get(position).getIsfollow().equals("0")) {
                            if (!new AddressBookActivity().isLoading)
                                mActivity.sendRequest(currentItem.getUserId(), AppConfig.getUserId(mActivity));
                            else
                                Toast.makeText(mActivity, "Please wait.", Toast.LENGTH_LONG).show();
                        }
//                        }
                    }
                }
            }
        });
//        }
        return convertView;
    }

    public void setData(ArrayList<AddressBookData> datas) {
        mItems = datas;
    }

    private static class ViewHolder {
        public ImageView contact_photo;
        public TextView contact_num, contact_name, contact_invite;
        LinearLayout llMain;
    }
}