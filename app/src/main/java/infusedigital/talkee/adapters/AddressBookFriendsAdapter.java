package infusedigital.talkee.adapters;

import android.app.Activity;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.AddFriendActivity;
import infusedigital.talkee.activity.AddressBookActivityNew;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.datamodel.AddFriend;
import infusedigital.talkee.datamodel.FriendsData;
import infusedigital.talkee.view.UserBlankAvatar;


/**
 * Created by Hiral on 21-June-16.
 */
public class AddressBookFriendsAdapter extends RecyclerView.Adapter<AddressBookFriendsAdapter.ViewHolder> {
    AddressBookActivityNew mActivity;
    private ArrayList<FriendsData> rowItems;
    LayoutInflater mInflater;

    public AddressBookFriendsAdapter(AddressBookActivityNew mActivity, ArrayList<FriendsData> items) {
        mInflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        this.mActivity = mActivity;
        this.rowItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {//send_list_item
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.addressbook_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final FriendsData mData = rowItems.get(position);
        if (mData.getProfile_url().length() > 0) {
            Picasso.with(mActivity)
                    .load(mData.getProfile_url())
                    .placeholder(R.drawable.ic_appicon)
                    .into(holder.receiver_photo);
        } else {
            holder.receiver_photo.setVisibility(View.GONE);
            holder.contact_photo_blank.setVisibility(View.VISIBLE);
            MyApplication.getInstance().setBlankAvatar(holder.contact_photo_blank, mData.getUsername(), true);
        }

        holder.receiver_username.setText(mData.getUsername());
        holder.receiver_name.setText(mData.getName() + " " + mData.getLast_name());

        holder.contact_req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.unFriendCall(mData.getUserid());
            }
        });

        holder.iv_contact_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.blockTheFriend(mData.getUserid());
            }
        });
    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView receiver_photo;
        public UserBlankAvatar contact_photo_blank;
        public TextView receiver_username;
        //        public RelativeLayout default_state;
        public TextView receiver_name, contact_req;
        public CircleImageView cancelView;
        LinearLayout llMain;
        ImageView iv_contact_block;

        public ViewHolder(View itemView) {
            super(itemView);
            llMain = (LinearLayout) itemView.findViewById(R.id.llMain);
            receiver_photo = (CircleImageView) itemView.findViewById(R.id.contact_photo);
            contact_photo_blank = (UserBlankAvatar) itemView.findViewById(R.id.contact_photo_blank);
            receiver_username = (TextView) itemView.findViewById(R.id.contact_name);
//            default_state = (RelativeLayout) itemView.findViewById(R.id.default_state);
//            cancelView = (CircleImageView) itemView.findViewById(R.id.receiver_photo1);
            receiver_name = (TextView) itemView.findViewById(R.id.contact_number);
            contact_req = (TextView) itemView.findViewById(R.id.contact_invite);
            contact_req.setText("UnFriend");
            iv_contact_block = (ImageView) itemView.findViewById(R.id.iv_contact_block);
            iv_contact_block.setVisibility(View.VISIBLE);
        }
    }
}
