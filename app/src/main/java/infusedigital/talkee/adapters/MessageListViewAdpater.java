package infusedigital.talkee.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.MessageScreen;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.datamodel.MessageData;
import infusedigital.talkee.view.UserBlankAvatar;

/**
 * Created by bluesky on 5/10/2016.
 */
public class MessageListViewAdpater extends BaseAdapter {

    private LayoutInflater mInflater;
    private ArrayList<MessageData> mItems;
    private MessageScreen mActivity;
    private TypedArray mEmojies;

    public MessageListViewAdpater( MessageScreen activity, ArrayList<MessageData> listDatas) {
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mActivity = activity;
        mItems = listDatas;
        mEmojies = mActivity.getResources().obtainTypedArray(R.array.emojies);
    }

    public void setData(ArrayList<MessageData> contactsData){
        mItems = contactsData;
        for (int i = 0; i<mItems.size(); i++) {
            Log.e("Super", mItems.get(i).messagePath);
        }
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).user.id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.messageview_child_item, parent, false);
            holder = new ViewHolder();

            holder.userPhotoView = (CircleImageView)convertView.findViewById(R.id.messageview_userphoto);
            holder.userPhotoViewBlank = (UserBlankAvatar)convertView.findViewById(R.id.messageview_userphoto_blank);
            holder.userNameView = (TextView)convertView.findViewById(R.id.username);
            holder.messageTimeView = (TextView)convertView.findViewById(R.id.messagetime);
            holder.playbackLayout = (RelativeLayout)convertView.findViewById(R.id.playbacklayout);
            holder.playBtn = (ImageView)convertView.findViewById(R.id.playbtn);
            holder.playProgressBar = (ProgressBar)convertView.findViewById(R.id.progress_bar);
            holder.playTotalTimeView = (TextView)convertView.findViewById(R.id.totalplaytime);

            holder.emojiView = (ImageView)convertView.findViewById(R.id.emojiview);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.userPhotoViewBlank.setVisibility(View.GONE);
        holder.userPhotoView.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(mItems.get(position).user.photo_url)){
            if (mItems.get(position).user.photo_url.contains("http")){
                Picasso.with(mActivity).load(mItems.get(position).user.photo_url).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.userPhotoView);
            }else {
                Picasso.with(mActivity).load(Config.server + mItems.get(position).user.photo_url).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.userPhotoView);
            }
        }else {
            Picasso.with(mActivity).load(R.drawable.ic_appicon).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.userPhotoView);
//            holder.userPhotoViewBlank.setVisibility(View.VISIBLE);
//            holder.userPhotoView.setVisibility(View.GONE);
//            MyApplication.getInstance().setBlankAvatar(holder.userPhotoViewBlank, mItems.get(position).user.name, true);
        }

        if (!TextUtils.isEmpty(mItems.get(position).user.name))
            holder.userNameView.setText(mItems.get(position).user.name.toUpperCase());

        holder.userNameView.setTypeface(MyApplication.semiboldFont);
        String lastTime = changeMicroSecsToDate(mItems.get(position).createdTime);

        if (!TextUtils.isEmpty(lastTime))
            holder.messageTimeView.setText(lastTime.toUpperCase());
        holder.messageTimeView.setTypeface(MyApplication.semiboldFont);

        if (!mItems.get(position).isemoji) {
            holder.emojiView.setVisibility(View.GONE);
            holder.playbackLayout.setVisibility(View.VISIBLE);

            if (mItems.get(position).userType.equals("friend")) {
                holder.playBtn.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.messageplay_black));
                holder.playbackLayout.setBackground(mActivity.getResources().getDrawable(R.drawable.messageview_friend_bg));
//            holder.playProgressBar.setBackground(mActivity.getResources().getDrawable(R.drawable.messageview_friend_progress_bg));
                holder.playProgressBar.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.friendmessage_progressbar));
                holder.playTotalTimeView.setTextColor(mActivity.getResources().getColor(R.color.friendmessagetotaltime));
            } else {
                holder.playBtn.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.messageplay_white));
                holder.playbackLayout.setBackground(mActivity.getResources().getDrawable(R.drawable.messageview_user_bg));
                holder.playProgressBar.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.usermessage_progressbar));
//            holder.playProgressBar.setBackground(mActivity.getResources().getDrawable(R.drawable.messageview_user_progress_bg));
                holder.playTotalTimeView.setTextColor(mActivity.getResources().getColor(R.color.usermessagetotltime));
            }

            String result;
            int time = Integer.parseInt(mItems.get(position).totalLength) / 1000;
            holder.playProgressBar.setProgress(mItems.get(position).progress);
            holder.playProgressBar.setMax(Integer.parseInt(mItems.get(position).totalLength));

            if (time == 0) {
                result = String.format("0:0%s", time);
                holder.playTotalTimeView.setVisibility(View.GONE);
                holder.playProgressBar.setVisibility(View.GONE);
            } else if (time < 10) {
                result = String.format("0:0%s", time);
                holder.playTotalTimeView.setVisibility(View.VISIBLE);
                holder.playProgressBar.setVisibility(View.VISIBLE);
            } else {
                result = String.format("0:%s", time);
                holder.playTotalTimeView.setVisibility(View.VISIBLE);
                holder.playProgressBar.setVisibility(View.VISIBLE);
            }

            holder.playTotalTimeView.setText(result);
            holder.playTotalTimeView.setTypeface(MyApplication.semiboldFont);

            holder.playBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mActivity.playMessage( mItems.get(position).createdTime, position);
                }
            });

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)holder.playProgressBar.getLayoutParams();
            params.width = mActivity.dip2px(mActivity.maxPlaybackWidthwithDP * time / 10);
            holder.playProgressBar.setLayoutParams(params);

        } else {
            //get position of emoji from message_url
            holder.emojiView.setVisibility(View.VISIBLE);
            holder.playbackLayout.setVisibility(View.GONE);

            int pos = getPositionfromString(mItems.get(position).messagePath);
            holder.emojiView.setImageResource(mEmojies.getResourceId(pos, -1));
        }
        if (position != 0)
            holder.messageTimeView.setVisibility(View.GONE);
        else
            holder.messageTimeView.setVisibility(View.VISIBLE);

        return convertView;
    }

    private int getPositionfromString(String messageurl){
        StringTokenizer tokens = new StringTokenizer(messageurl, ":");
        String currentTime = tokens.nextToken();
        String position = tokens.nextToken();
        Log.e("Super", "emoji position = " + position);
        return Integer.parseInt(position);
    }

    private String changeMicroSecsToDate(String microSecs){
        long microTime = Long.parseLong(microSecs);

        Date now = new Date(microTime);
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        String time = dateFormat.format(now);
        return time;
    }

    private static class ViewHolder {
        public CircleImageView userPhotoView;
        public UserBlankAvatar userPhotoViewBlank;
        public TextView userNameView, messageTimeView, playTotalTimeView;
        RelativeLayout playbackLayout;
        ImageView playBtn;
        ProgressBar playProgressBar;

        ImageView emojiView;
    }
}
