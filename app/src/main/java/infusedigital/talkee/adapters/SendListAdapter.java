package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.datamodel.SearchUserData;
import infusedigital.talkee.model.User;
import infusedigital.talkee.view.UserBlankAvatar;

/**
 * Created by bluesky on 5/15/2016.
 */
public class SendListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<SearchUserData> mItems;
    private Activity mActivity;
    public static ArrayList<ViewHolder> viewHolderList;

    public SendListAdapter(Activity activity, ArrayList<SearchUserData> friendsdata) {

        viewHolderList = new ArrayList();

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = friendsdata;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return viewHolderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.send_list_item, parent, false);
            holder = new ViewHolder();

            holder.receiver_photo = (ImageView) convertView.findViewById(R.id.receiver_photo);
            holder.receiver_photo_blank = (UserBlankAvatar) convertView.findViewById(R.id.receiver_photo_blank);
            holder.receiver_name = (TextView) convertView.findViewById(R.id.receiver_name);
//            holder.default_state = (RelativeLayout) convertView.findViewById(R.id.default_state);
            holder.cancelView = (CircleImageView) convertView.findViewById(R.id.receiver_photo1);
//            holder.receiver_num = (TextView)convertView.findViewById(R.id.send_profile_phone);

            viewHolderList.add(holder);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        holder.receiver_name.setSelected(true);
//        holder.receiver_name.setEllipsize(TextUtils.TruncateAt.MARQUEE);
//        holder.receiver_name.setMarqueeRepeatLimit(-1);

        if (mItems.get(position).group_id == -1) {
            holder.receiver_name.setText(mItems.get(position).friend.name);
            holder.receiver_num.setText(mItems.get(position).friend.phonenum);
        } else {
            holder.receiver_name.setText(getGroupName(mItems.get(position).groupMembers));
            holder.receiver_num.setText("Suggested Group");
        }

        holder.receiver_name.setTypeface(MyApplication.semiboldFont);

//        if (mItems.get(position).group_id == -1) {
//            if (mItems.get(position).friend.mood.equals("Available")) {
//                CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
//                statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb93")));
//            } else if (mItems.get(position).friend.mood.equals("Away")) {
//                CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
//                statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#ffce0c")));
//            } else {
//                CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
//                statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#f52e62")));
//            }
//        }

        holder.cancelView.setVisibility(View.GONE);
        holder.receiver_photo.setVisibility(View.VISIBLE);
        holder.receiver_photo_blank.setVisibility(View.GONE);

        if (mItems.get(position).isSelected) {
            holder.receiver_photo.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb93")));
            holder.cancelView.setVisibility(View.VISIBLE);
            holder.receiver_name.setTextColor(Color.parseColor("#35eb93"));
            holder.receiver_num.setTextColor(Color.parseColor("#35eb93"));
        } else {
            holder.cancelView.setVisibility(View.GONE);
            if (mItems.get(position).group_id == -1) {
                if (!TextUtils.isEmpty(mItems.get(position).friend.photo_url)) {
                    if (mItems.get(position).friend.photo_url.contains("http"))
                        Picasso.with(mActivity).load(mItems.get(position).friend.photo_url).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.receiver_photo);
                    else
                        Picasso.with(mActivity).load(Config.server + mItems.get(position).friend.photo_url).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.receiver_photo);
                } else {
                    Picasso.with(mActivity).load(R.drawable.ic_appicon).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.receiver_photo);
//                    holder.receiver_photo.setVisibility(View.GONE);
//                    holder.receiver_photo_blank.setVisibility(View.VISIBLE);
//                    MyApplication.getInstance().setBlankAvatar(holder.receiver_photo_blank, mItems.get(position).friend.name, true);
                }
            } else
                holder.receiver_photo.setImageDrawable(new ColorDrawable(Color.parseColor("#222222")));

            holder.receiver_name.setTextColor(Color.parseColor("#000000"));
            holder.receiver_num.setTextColor(Color.parseColor("#3a3a3a"));
        }
        return convertView;
    }

    public void setData(ArrayList<SearchUserData> data) {
        mItems = data;
    }

    private String getGroupName(ArrayList<User> members) {
        String result = "";
        int size = members.size();

        if (size == 0)
            return result;

        for (int i = 0; i < size; i++) {
            if (i == 0) {
                result = members.get(0).name;
            } else {
                result = result + ", " + members.get(i).name;
            }
        }

        Log.e("Super", "result = " + result);

//        if (size > 2){
//            result = members.get(0).name + ", " + members.get(1).name + String.valueOf(size - 2);
//        }else {
//            if (size == 1){
//                result = members.get(0).name;
//            }else
//                result = members.get(0).name + ", " + members.get(1).name;
//        }

        return result;
    }

    public static class ViewHolder {
        public ImageView receiver_photo;
        public UserBlankAvatar receiver_photo_blank;
        public TextView receiver_name;
        //        public RelativeLayout default_state;
        public TextView receiver_num;
        public CircleImageView cancelView;
    }

    public class CustomComparator implements Comparator<User> {
        @Override
        public int compare(User lhs, User rhs) {
            return lhs.name.compareTo(rhs.name);
        }
    }
}
