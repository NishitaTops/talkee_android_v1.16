package infusedigital.talkee.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.FriendRequestActivity;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.datamodel.FriendRequestsData;
import infusedigital.talkee.view.UserBlankAvatar;

/**
 * Created by bluesky on 5/10/2016.
 */
public class RequestFriendListAdapter extends ArrayAdapter {
    private LayoutInflater mInflater;
    //    private ArrayList<User> mItems;
    private ArrayList<FriendRequestsData> mItems;
    private FriendRequestActivity mActivity;

//    public RequestFriendListAdapter(FriendRequestActivity activity, ArrayList<User> listDatas) {
//        super(activity, 0, listDatas);
//
//        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mActivity = activity;
//        mItems = listDatas;
//
//        Log.e("Super", "mitmes = " + mItems.size());
//        for (int i = 0; i < mItems.size(); i++) {
//            Log.e("Super", "friend name = " + mItems.get(i).name);
//        }
//    }

    public RequestFriendListAdapter(FriendRequestActivity activity, ArrayList<FriendRequestsData> listDatas) {
        super(activity, 0, listDatas);

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mActivity = activity;
        mItems = listDatas;
    }

//    public void setData(ArrayList<User> contactsData) {
//        mItems = contactsData;
//    }

    public void setData(ArrayList<FriendRequestsData> contactsData) {
        mItems = contactsData;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.requst_friend_list_item, parent, false);
            holder = new ViewHolder();
            holder.friend_name = (TextView) convertView.findViewById(R.id.requested_name);
            holder.friend_photo = (ImageView) convertView.findViewById(R.id.requested_photo);
            holder.friend_photo_blank = (UserBlankAvatar) convertView.findViewById(R.id.requested_photo_blank);
            holder.recieveBtn = (ImageView) convertView.findViewById(R.id.request_receive_btn);
            holder.declineBtn = (ImageView) convertView.findViewById(R.id.request_decline_btn);
            holder.blockBtn = (ImageView) convertView.findViewById(R.id.request_block_btn);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.friend_name.setText(mItems.get(position).getUsername());
        holder.friend_name.setTypeface(MyApplication.regularFont);

        holder.recieveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                User data = mItems.get(position);
                FriendRequestsData data = mItems.get(position);
                mActivity.actionOnRequest(data, "1");
            }
        });

        holder.declineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                User data = mItems.get(position);
                FriendRequestsData data = mItems.get(position);
                mActivity.actionOnRequest(data, "2");
            }
        });

        holder.blockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(mActivity, "Coming soon.", Toast.LENGTH_LONG).show();
                FriendRequestsData data = mItems.get(position);
                mActivity.blockTheFriend(data.getUserid());
            }
        });

        holder.friend_photo.setVisibility(View.VISIBLE);
        holder.friend_photo_blank.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(mItems.get(position).getProfile_url())) {
            if (mItems.get(position).getProfile_url().contains("http"))
                Picasso.with(mActivity).load(mItems.get(position).getProfile_url()).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.friend_photo);
            else
                Picasso.with(mActivity).load(Config.server + mItems.get(position).getProfile_url()).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.friend_photo);
        } else {
            Picasso.with(mActivity).load(R.drawable.ic_appicon).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.friend_photo);
//            holder.friend_photo.setVisibility(View.GONE);
//            holder.friend_photo_blank.setVisibility(View.VISIBLE);
//            MyApplication.getInstance().setBlankAvatar(holder.friend_photo_blank, mItems.get(position).getUsername(), true);
        }

        return convertView;
    }

    private static class ViewHolder {
        public ImageView friend_photo;
        public UserBlankAvatar friend_photo_blank;
        public TextView friend_name;
        public ImageView recieveBtn, declineBtn, blockBtn;
    }
}
