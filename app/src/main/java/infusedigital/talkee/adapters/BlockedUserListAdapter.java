package infusedigital.talkee.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.BlockUsersActivity;
import infusedigital.talkee.activity.FriendRequestActivity;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.datamodel.AddFriend;
import infusedigital.talkee.datamodel.FriendRequestsData;
import infusedigital.talkee.view.UserBlankAvatar;

/**
 * Created by bluesky on 5/10/2016.
 */
public class BlockedUserListAdapter extends ArrayAdapter {
    private LayoutInflater mInflater;
    private ArrayList<AddFriend> mItems;
    private BlockUsersActivity mActivity;

    public BlockedUserListAdapter(BlockUsersActivity activity, ArrayList<AddFriend> listData) {
        super(activity, 0, listData);

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mActivity = activity;
        mItems = listData;
    }

    public void setData(ArrayList<AddFriend> contactsData) {
        mItems = contactsData;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.addressbook_list_item, parent, false);
            holder = new ViewHolder();
            holder.contact_name = (TextView) convertView.findViewById(R.id.contact_name);
            holder.contact_number = (TextView) convertView.findViewById(R.id.contact_number);
            holder.contact_invite = (TextView) convertView.findViewById(R.id.contact_invite);
            holder.contact_invite.setText("Unblock");
            holder.contact_photo = (ImageView) convertView.findViewById(R.id.contact_photo);
            holder.contact_photo_blank = (UserBlankAvatar) convertView.findViewById(R.id.contact_photo_blank);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.contact_name.setText(mItems.get(position).getUsername());
        holder.contact_name.setTypeface(MyApplication.regularFont);
        holder.contact_number.setText(mItems.get(position).getName() + " " + mItems.get(position).getLast_name());
        holder.contact_number.setTypeface(MyApplication.regularFont);

        holder.contact_photo.setVisibility(View.VISIBLE);
        holder.contact_photo_blank.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(mItems.get(position).getProfile_url())) {
            if (mItems.get(position).getProfile_url().contains("http"))
                Picasso.with(mActivity).load(mItems.get(position).getProfile_url()).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.contact_photo);
            else
                Picasso.with(mActivity).load(Config.server + mItems.get(position).getProfile_url()).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.contact_photo);
        } else {
            Picasso.with(mActivity).load(R.drawable.ic_appicon).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.contact_photo);
//            holder.contact_photo.setVisibility(View.GONE);
//            holder.contact_photo_blank.setVisibility(View.VISIBLE);
//            MyApplication.getInstance().setBlankAvatar(holder.contact_photo_blank, mItems.get(position).getUsername(), true);
        }

        holder.contact_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.unBlockTheFriend((mItems.get(position).getUserid()));
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        public ImageView contact_photo;
        public UserBlankAvatar contact_photo_blank;
        public TextView contact_name, contact_number, contact_invite;
    }
}
