package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.AddressBookActivity;
import infusedigital.talkee.datamodel.AddressBookData;

/**
 * Created by bluesky on 5/10/2016.
 */
public class AddressBookListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<AddressBookData> mItems;
    //    private AddressBookActivityNew mActivity;
    private AddressBookActivity mActivity;

//    public AddressBookListAdapter(Activity activity, ArrayList<AddressBookData> contactsdata) {
//
//        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mItems = contactsdata;
//        mActivity = (AddressBookActivityNew) activity;
//    }

    public AddressBookListAdapter(Activity activity, ArrayList<AddressBookData> contactsdata) {

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = contactsdata;
        mActivity = (AddressBookActivity) activity;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.addressbook_list_item, parent, false);
            holder = new ViewHolder();
            holder.contact_photo = (ImageView) convertView.findViewById(R.id.contact_photo);
            holder.contact_name = (TextView) convertView.findViewById(R.id.contact_name);
            holder.contact_num = (TextView) convertView.findViewById(R.id.contact_number);
            holder.contact_invite = (TextView) convertView.findViewById(R.id.contact_invite);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (mItems.get(position).photoUri != null) {
            holder.contact_photo.setImageURI(mItems.get(position).photoUri);
        } else {
            holder.contact_photo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.avatar_blank));
        }

        holder.contact_name.setText(mItems.get(position).name);
        holder.contact_num.setText(mItems.get(position).phonenum);

        if (mItems.get(position).isSent)
            holder.contact_invite.setText("SENT");
        else
            holder.contact_invite.setText("INVITE");

        holder.contact_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mItems.get(position).isSent)
                    mActivity.sendInvite(position);
            }
        });

        return convertView;
    }

    public void setData(ArrayList<AddressBookData> datas) {
        mItems = datas;
    }

    private static class ViewHolder {
        public ImageView contact_photo;
        public TextView contact_num, contact_name, contact_invite;
    }
}