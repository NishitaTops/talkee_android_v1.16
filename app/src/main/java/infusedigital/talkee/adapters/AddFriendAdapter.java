package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.AddFriendActivity;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.datamodel.AddFriend;
import infusedigital.talkee.datamodel.FriendsData;
import infusedigital.talkee.view.UserBlankAvatar;


/**
 * Created by Hiral on 21-June-16.
 */
public class AddFriendAdapter extends RecyclerView.Adapter<AddFriendAdapter.ViewHolder> {
    AddFriendActivity mActivity;
    private ArrayList<AddFriend> rowItems;
    LayoutInflater mInflater;

    public AddFriendAdapter(AddFriendActivity mActivity, ArrayList<AddFriend> items) {
        mInflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        this.mActivity = mActivity;
        this.rowItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {//send_list_item
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.addressbook_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final AddFriend mData = rowItems.get(position);
        if (mData.getProfile_url().length() > 0) {
            Picasso.with(mActivity)
                    .load(mData.getProfile_url())
                    .placeholder(R.drawable.ic_appicon)
                    .resize(50, 50)
                    .into(holder.receiver_photo);
        } else {
            Picasso.with(mActivity)
                    .load(R.drawable.ic_appicon)
                    .placeholder(R.drawable.ic_appicon)
                    .into(holder.receiver_photo);
//            holder.receiver_photo.setVisibility(View.GONE);
//            holder.receiver_photo_blank.setVisibility(View.VISIBLE);
//            MyApplication.getInstance().setBlankAvatar(holder.receiver_photo_blank, mData.getUsername(), true);
//            holder.receiver_photo_blank.setColor(mActivity.getResources().getColor(R.color.req_color));
        }

        holder.receiver_username.setText(mData.getUsername());
        holder.receiver_name.setText(mData.getName() + " " + mData.getLast_name());
//        System.out.println("++++++ pos:" + position + "::::" + mData.getIsfollow());
        if (mData.getIsfollow().equalsIgnoreCase("0")) { //No friend
            holder.contact_req.setText("Add");
        } else if (mData.getIsfollow().equalsIgnoreCase("1")) { //Already friend
            holder.contact_req.setText("Friend");
        } else if (mData.getIsfollow().equalsIgnoreCase("2")) { // requested
            holder.contact_req.setText("Sent");
        } else if (mData.getIsfollow().equalsIgnoreCase("3")) { // got request
            holder.contact_req.setText("Accept");
        }

        holder.contact_req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.contact_req.getText().toString().equalsIgnoreCase("Friend")) { //No friend
                    // Do nothing
                } else if (holder.contact_req.getText().toString().equalsIgnoreCase("Add")) { //Already friend
                    mActivity.sendRequest(mData.getUserid());
                } else if (holder.contact_req.getText().toString().equalsIgnoreCase("Sent")) { // requested
                    // Do nothing
                } else if (holder.contact_req.getText().toString().equalsIgnoreCase("Accept")) { // got request
                    mActivity.ActionOnRequest(mData.getUserid());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView receiver_photo;
        public UserBlankAvatar receiver_photo_blank;
        public TextView receiver_username;
        //        public RelativeLayout default_state;
        public TextView receiver_name, contact_req;
        public CircleImageView cancelView;
        LinearLayout llMain;

        public ViewHolder(View itemView) {
            super(itemView);
            llMain = (LinearLayout) itemView.findViewById(R.id.llMain);
            receiver_photo = (CircleImageView) itemView.findViewById(R.id.contact_photo);
            receiver_photo_blank = (UserBlankAvatar) itemView.findViewById(R.id.contact_photo_blank);
            receiver_username = (TextView) itemView.findViewById(R.id.contact_name);
//            default_state = (RelativeLayout) itemView.findViewById(R.id.default_state);
//            cancelView = (CircleImageView) itemView.findViewById(R.id.receiver_photo1);
            receiver_name = (TextView) itemView.findViewById(R.id.contact_number);
            contact_req = (TextView) itemView.findViewById(R.id.contact_invite);
        }
    }
}
