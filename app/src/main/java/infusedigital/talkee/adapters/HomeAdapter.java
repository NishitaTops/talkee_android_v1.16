package infusedigital.talkee.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.activity.TabViewActivity;
import infusedigital.talkee.datamodel.MessageThard;
import infusedigital.talkee.view.NotificationColorItemView;
import infusedigital.talkee.view.UserBlankAvatar;

public class HomeAdapter extends ArrayAdapter {
    private LayoutInflater mInflater;
    private ArrayList<MessageThard> mItems;
    private TabViewActivity mActivity;

    public HomeAdapter(TabViewActivity activity, ArrayList<MessageThard> notificationsdata) {
        super(activity, 0, notificationsdata);
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = notificationsdata;
        mActivity = activity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.notification_list_item, parent, false);

            holder = new ViewHolder();

            holder.notification_color = (NotificationColorItemView) convertView.findViewById(R.id.notification_color);
            holder.notification_count = (TextView) convertView.findViewById(R.id.notification_count);

            holder.notification_user_photo = (ImageView) convertView.findViewById(R.id.notification_user_photo);
            holder.blankAvatar = (UserBlankAvatar) convertView.findViewById(R.id.blankavatar);
            holder.notification_name = (TextView) convertView.findViewById(R.id.notification_userename);
            holder.default_state = (RelativeLayout) convertView.findViewById(R.id.default_status);
            holder.status_color = (CircleImageView) convertView.findViewById(R.id.status_color);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        final String type = mItems.get(position).getDataType();
        holder.blankAvatar.setVisibility(View.GONE);

        holder.notification_name.setTypeface(MyApplication.semiboldFont);

        if (mItems.get(position).getName().equalsIgnoreCase("Requests")) {
            holder.blankAvatar.setVisibility(View.VISIBLE);
            MyApplication.getInstance().setBlankAvatar(holder.blankAvatar, "", true);
            holder.notification_count.setText(mItems.get(position).getMsg_count());
            holder.notification_name.setText("Requests");
            holder.blankAvatar.setColor(mActivity.getResources().getColor(R.color.req_bg_color_new));
        } else if (mItems.get(position).getName().equalsIgnoreCase("ADD FRIENDS")) {
//            holder.blankAvatar.setVisibility(View.VISIBLE);
//            MyApplication.getInstance().setBlankAvatar(holder.blankAvatar, "", true);
            Picasso.with(mActivity)
                    .load(R.drawable.ic_add_friends)
                    .placeholder(R.drawable.ic_appicon)
                    .into(holder.notification_user_photo);
            holder.notification_count.setText("");
            holder.default_state.setVisibility(View.GONE);
            holder.notification_color.setVisibility(View.GONE);
            holder.notification_name.setText("ADD FRIENDS");
            holder.notification_name.setTextColor(mActivity.getResources().getColor(R.color.add_friends_text));
//            holder.blankAvatar.setColor(mActivity.getResources().getColor(R.color.req_bg_color_new));
//            ADD FRIEND
        } else {
            holder.notification_name.setText(mItems.get(position).getUsername());
//            System.out.println("+++++ mItems.get(position).getMsg_count():" + mItems.get(position).getMsg_count());
            if (Integer.parseInt(mItems.get(position).getMsg_count()) == 0) {
                holder.notification_count.setText("");
                holder.default_state.setVisibility(View.VISIBLE);
                holder.notification_color.setVisibility(View.GONE);
            } else {
                holder.notification_count.setText(mItems.get(position).getMsg_count());
            }

            if (mItems.get(position).getImage_url().length() > 0) {
                Picasso.with(mActivity)
                        .load(mItems.get(position).getImage_url())
                        .placeholder(R.drawable.ic_appicon)
                        .into(holder.notification_user_photo);
            } else {
//                holder.notification_user_photo.setVisibility(View.GONE);
//                holder.blankAvatar.setVisibility(View.VISIBLE);
//                MyApplication.getInstance().setBlankAvatar(holder.blankAvatar, mItems.get(position).getUsername(), true);
//                if (mItems.get(position).getEmoticol().length() > 0 && mItems.get(position).getEmoticol().length() == 7) {
//                    holder.blankAvatar.setColor(Color.parseColor(mItems.get(position).getEmoticol()));
//                } else {
//                    holder.blankAvatar.setColor(mActivity.getResources().getColor(R.color.colorPrimary));
//                }

//                Picasso.with(mActivity)
//                        .load("http://gettalkee.com/talkee_app/uploads/user/small/b073b05-1480740174960.74.png")
//                        .placeholder(R.drawable.ic_placeholder)
//                        .into(holder.notification_user_photo);
                Picasso.with(mActivity)
                        .load(R.drawable.ic_placeholder)
                        .placeholder(R.drawable.ic_appicon)
                        .into(holder.notification_user_photo);
                holder.notification_name.setText(mItems.get(position).getUsername());
            }

            if (mItems.get(position).getMood().equalsIgnoreCase("Available")) {
                holder.status_color.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb93")));
            } else if (mItems.get(position).getMood().equalsIgnoreCase("Away")) {
                holder.status_color.setImageDrawable(new ColorDrawable(Color.parseColor("#ffce0c")));
            } else {
                holder.status_color.setImageDrawable(new ColorDrawable(Color.parseColor("#f52e62")));
            }
        }

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mItems.get(position).getName().equalsIgnoreCase("Requests")) {
//                    Intent requestIntent = new Intent(mActivity, FriendRequestActivity.class);
////                    mActivity.startActivity(requestIntent);
//                    mActivity.startActivityForResult(requestIntent, 1);
//                } /*else if (Integer.parseInt(mItems.get(position).getMsg_count()) > 0) {
//
//                }*/
//            }
//        });

//        convertView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                if (!mItems.get(position).getName().equalsIgnoreCase("Requests")) {
//                    LayoutInflater li = LayoutInflater.from(mActivity);
//                    View promptsView = li.inflate(R.layout.block_popup, null);
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
//                    alertDialogBuilder.setView(promptsView);
//                    final AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    final TextView tvUsername = (TextView) promptsView.findViewById(R.id.tvUsername);
//                    final TextView tvUnFriend = (TextView) promptsView.findViewById(R.id.tvUnFriend);
//                    final TextView tvBlock = (TextView) promptsView.findViewById(R.id.tvBlock);
//                    final TextView tvCancel = (TextView) promptsView.findViewById(R.id.tvCancel);
//
//                    tvUsername.setText(mItems.get(position).getUsername());
//
//                    tvCancel.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            alertDialog.dismiss();
//                        }
//                    });
//
//                    tvUnFriend.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            alertDialog.dismiss();
//                            mActivity.unFriendCall(mItems.get(position).getUserId());
//                        }
//                    });
//
//                    tvBlock.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            alertDialog.dismiss();
//                            mActivity.blockTheFriend(mItems.get(position).getUserId());
//                        }
//                    });
//                    alertDialogBuilder.setCancelable(false);
//                    alertDialog.show();
//                }
//                return true;
//            }
//        });

        return convertView;
    }

    private static class ViewHolder {
        public ImageView notification_user_photo;
        public UserBlankAvatar blankAvatar;
        public NotificationColorItemView notification_color;
        public TextView notification_count;
        public TextView notification_name;
        public RelativeLayout default_state;
        public CircleImageView status_color;
    }

    public void setList(ArrayList<MessageThard> list) {
        mItems = list;
    }
}