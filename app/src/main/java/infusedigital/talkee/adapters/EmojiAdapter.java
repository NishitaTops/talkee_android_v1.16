package infusedigital.talkee.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Comparator;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.MessageScreen;
import infusedigital.talkee.model.User;

/**
 * Created by bluesky on 5/15/2016.
 */
public class EmojiAdapter extends BaseAdapter{
    private LayoutInflater mInflater;
    private TypedArray mItems;
    private MessageScreen mActivity;
    public static ArrayList<ViewHolder> viewHolderList;

    public EmojiAdapter(MessageScreen activity, TypedArray arrayData) {

        viewHolderList = new ArrayList();

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = arrayData;
        mActivity = activity;
        mActivity.setGridHeight();
    }

    @Override
    public int getCount() {
        return mItems.length();
    }

    @Override
    public Object getItem(int position) {
        return viewHolderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.emoji_list_item, parent, false);
            holder = new ViewHolder();

            holder.emoji_icon= (ImageView) convertView.findViewById(R.id.emoji_icon);
            viewHolderList.add(holder);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position > mItems.length() - 1)
            position = 0;

        holder.emoji_icon.setImageResource(mItems.getResourceId(position, -1));

        final int finalPosition = position;

        holder.emoji_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mActivity.sendEmoji(finalPosition);
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        public ImageView emoji_icon;
    }

    public class CustomComparator implements Comparator<User> {
        @Override
        public int compare(User lhs, User rhs) {
            return lhs.name.compareTo(rhs.name);
        }
    }
}
