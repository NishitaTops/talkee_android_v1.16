package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.activity.SendViewActivity;
import infusedigital.talkee.datamodel.FriendsData;
import infusedigital.talkee.datamodel.MsgFriendsData;
import infusedigital.talkee.view.UserBlankAvatar;


/**
 * Created by Hiral on 21-June-16.
 */
public class MsgFriendsListAdapter extends RecyclerView.Adapter<MsgFriendsListAdapter.ViewHolder> {
    SendViewActivity mActivity;
    public ArrayList<MsgFriendsData> rowItems;
    LayoutInflater mInflater;
    boolean isSelected = false;

    public MsgFriendsListAdapter(SendViewActivity mActivity, ArrayList<MsgFriendsData> items) {
        mInflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        this.mActivity = mActivity;
        this.rowItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.send_list_item, parent, false);
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.addressbook_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final MsgFriendsData mData = rowItems.get(position);
        if (mData.getPhoto_url().length() > 0) {
            Picasso.with(mActivity)
                    .load(mData.getPhoto_url())
                    .placeholder(R.drawable.ic_appicon)
                    .into(holder.receiver_photo);
        } else {
            Picasso.with(mActivity)
                    .load(R.drawable.ic_appicon)
                    .placeholder(R.drawable.ic_appicon)
                    .into(holder.receiver_photo);
//            holder.receiver_photo_blank.setVisibility(View.VISIBLE);
//            MyApplication.getInstance().setBlankAvatar(holder.receiver_photo_blank, mData.getUsername(), true);
//            holder.receiver_photo_blank.setColor(mActivity.getResources().getColor(R.color.req_color));
        }

        if (mData.isSelected()) {
            holder.cancelView.setVisibility(View.VISIBLE);
        } else {
            holder.cancelView.setVisibility(View.INVISIBLE);
        }

        holder.receiver_name.setText(mData.getName() + " " + mData.getLast_name());
        holder.receiver_phone.setText(mData.getUsername());

        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rowItems.get(position).isSelected()) {
                    rowItems.get(position).setSelected(false);
                } else {
                    rowItems.get(position).setSelected(true);
                }
                notifyDataSetChanged();
                mActivity.showButton();
            }
        });
    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView receiver_photo;
        public UserBlankAvatar receiver_photo_blank;
        public TextView receiver_name;
        //        public RelativeLayout default_state;
        public TextView receiver_phone, contact_invite;
        public CircleImageView cancelView;
        LinearLayout llMain;

        public ViewHolder(View itemView) {
            super(itemView);
            llMain = (LinearLayout) itemView.findViewById(R.id.llMain);
            receiver_photo = (ImageView) itemView.findViewById(R.id.contact_photo);
            receiver_photo_blank = (UserBlankAvatar) itemView.findViewById(R.id.contact_photo_blank);
            receiver_name = (TextView) itemView.findViewById(R.id.contact_name);
//            default_state = (RelativeLayout) itemView.findViewById(R.id.default_state);
            cancelView = (CircleImageView) itemView.findViewById(R.id.receiver_photo1);
            receiver_phone = (TextView) itemView.findViewById(R.id.contact_number);
            contact_invite = (TextView) itemView.findViewById(R.id.contact_invite);
            contact_invite.setVisibility(View.GONE);
        }
    }
}