package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.datamodel.NotificationListData;
import infusedigital.talkee.model.User;
import infusedigital.talkee.view.NotificationColorItemView;
import infusedigital.talkee.view.UserBlankAvatar;

/**
 * Created by bluesky on 5/10/2016.
 */
public class GridListAdapter extends ArrayAdapter {
    private LayoutInflater mInflater;
    private ArrayList<NotificationListData> mItems;
    private Activity mActivity;

    public GridListAdapter(Activity activity, ArrayList<NotificationListData> notificationsdata) {
        super(activity, 0, notificationsdata);
        mInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = notificationsdata;

        mActivity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.notification_list_item, parent, false);

            holder = new ViewHolder();

            holder.notification_color = (NotificationColorItemView) convertView.findViewById(R.id.notification_color);
            holder.notification_count = (TextView) convertView.findViewById(R.id.notification_count);

            holder.notification_user_photo = (ImageView) convertView.findViewById(R.id.notification_user_photo);
            holder.blankAvatar = (UserBlankAvatar) convertView.findViewById(R.id.blankavatar);
            holder.notification_name = (TextView) convertView.findViewById(R.id.notification_userename);
            holder.default_state = (RelativeLayout) convertView.findViewById(R.id.default_status);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.notification_count.setText(String.valueOf(mItems.get(position).notification_count));

//        if (TextUtils.isEmpty(mItems.get(position).friend.emoticol)) mItems.get(position).friend.emoticol = "#2edadc";
//
//        int color = Color.parseColor(mItems.get(position).friend.emoticol);
        int color;
        if (mItems.get(position).friend == null) {
            if (mItems.get(position).requestType == 1)
                color = Color.parseColor("#2edadc");
            else
                color = Color.parseColor("#35eb94");
        } else {
            if (TextUtils.isEmpty(mItems.get(position).friend.emoticol))
                mItems.get(position).friend.emoticol = "#2edadc";
            color = Color.parseColor(mItems.get(position).friend.emoticol);
        }

        holder.notification_color.setColor(color);

        if (mItems.get(position).requestType == 1) {
            holder.blankAvatar.setVisibility(View.GONE);
            holder.notification_user_photo.setImageDrawable(new ColorDrawable(color));
        } else if (mItems.get(position).requestType == 2) {
            holder.blankAvatar.setVisibility(View.GONE);
            holder.notification_user_photo.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb94")));
        } else {
            holder.blankAvatar.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mItems.get(position).friend.photo_url)) {
                if (mItems.get(position).friend.photo_url.contains("http")) {
                    Picasso.with(mActivity).load(mItems.get(position).friend.photo_url).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.notification_user_photo);
                } else {
                    Picasso.with(mActivity).load(Config.server + mItems.get(position).friend.photo_url).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.notification_user_photo);
                }
            } else {
                Picasso.with(mActivity).load(R.drawable.ic_appicon).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.notification_user_photo);
//                holder.notification_user_photo.setVisibility(View.GONE);
//                holder.blankAvatar.setVisibility(View.VISIBLE);
//                MyApplication.getInstance().setBlankAvatar(holder.blankAvatar, mItems.get(position).friend.name, true);
            }
        }

        if (mItems.get(position).requestType == 1) {
            holder.notification_name.setText("Request");
            holder.notification_name.setTextColor(color);
            holder.notification_name.setLines(1);
            holder.notification_name.setSingleLine(true);
        } else if (mItems.get(position).requestType == 0) {
            holder.notification_name.setText(mItems.get(position).friend.name);
            holder.notification_name.setLines(1);
            holder.notification_name.setSingleLine(true);
        } else {
            String name = getGroupName(mItems.get(position).groupMembers);
            holder.notification_name.setText(name);
            holder.notification_name.setLines(2);
            holder.notification_name.setSingleLine(false);
            holder.notification_name.setTextColor(Color.parseColor("#35eb94"));
        }

        holder.notification_name.setTypeface(MyApplication.semiboldFont);

        if (mItems.get(position).notification_count == 0) {
            holder.notification_count.setVisibility(View.GONE);
            holder.notification_color.setVisibility(View.GONE);

            holder.default_state.setVisibility(View.VISIBLE);

            if (mItems.get(position).group_id == -1) {
                if (mItems.get(position).friend.mood.equals("Available")) {
                    CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
                    statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb93")));
                } else if (mItems.get(position).friend.mood.equals("Away")) {
                    CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
                    statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#ffce0c")));
                } else {
                    CircleImageView statusColor = (CircleImageView) holder.default_state.findViewById(R.id.status_color);
                    statusColor.setImageDrawable(new ColorDrawable(Color.parseColor("#f52e62")));
                }
            } else {
                holder.default_state.setVisibility(View.GONE);
            }
        } else {
            if (mItems.get(position).requestType == 1) {
                holder.notification_count.setVisibility(View.VISIBLE);
                holder.notification_color.setVisibility(View.VISIBLE);
                holder.default_state.setVisibility(View.INVISIBLE);
            } else if (mItems.get(position).requestType == 2) {
                //this is group feature
                holder.notification_count.setVisibility(View.VISIBLE);
                holder.notification_color.setVisibility(View.VISIBLE);
                holder.default_state.setVisibility(View.INVISIBLE);
            } else {
                holder.notification_count.setVisibility(View.VISIBLE);
                holder.notification_color.setVisibility(View.VISIBLE);
                holder.default_state.setVisibility(View.INVISIBLE);
            }
        }

        return convertView;
    }

    private String getGroupName(ArrayList<User> members) {

        String result = "";
        int size = members.size();
        if (size == 0)
            return result;

        if (size > 2) {
            result = members.get(0).name + ", " + members.get(1).name + " + " + String.valueOf(size - 2);
        } else {
            if (size == 1) {
                result = members.get(0).name;
            } else
                result = members.get(0).name + ", " + members.get(1).name;
        }

        return result;
    }

    private static class ViewHolder {
        public ImageView notification_user_photo;
        public UserBlankAvatar blankAvatar;
        public NotificationColorItemView notification_color;
        public TextView notification_count;
        public TextView notification_name;
        public RelativeLayout default_state;
    }

    public void setList(ArrayList<NotificationListData> list) {
        mItems = list;
    }
}
