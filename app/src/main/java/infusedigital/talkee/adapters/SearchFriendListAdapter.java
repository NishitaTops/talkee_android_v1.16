package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.AddFriendActivity;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.datamodel.SearchFriendListData;
import infusedigital.talkee.view.UserBlankAvatar;

/**
 * Created by bluesky on 5/10/2016.
 */
public class SearchFriendListAdapter extends ArrayAdapter {
    private LayoutInflater mInflater;
    private ArrayList<SearchFriendListData> mItems;
    private AddFriendActivity mActivity;

    public SearchFriendListAdapter(Activity activity, ArrayList<SearchFriendListData> contactsdata) {
        super(activity, 0, contactsdata);
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = contactsdata;

        mActivity = (AddFriendActivity) activity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.add_friend_list_item, parent, false);
            holder = new ViewHolder();
            holder.friend_name = (TextView) convertView.findViewById(R.id.searched_name);

            holder.friend_photo = (ImageView) convertView.findViewById(R.id.searched_photo);
            holder.friend_photo_blank = (UserBlankAvatar) convertView.findViewById(R.id.searched_photo_blank);

            holder.friend_btn = (LinearLayout) convertView.findViewById(R.id.send_request_btn);
            holder.friendStatus = (TextView) convertView.findViewById(R.id.send_request_btn_txt);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.friend_name.setText(mItems.get(position).friend.name);

        int color;

        GradientDrawable rndrect = (GradientDrawable) mActivity.getResources().getDrawable(R.drawable.add_friend_btn);

        if (mItems.get(position).btn_friend) {
            holder.friendStatus.setText("Friend");
            color = Color.parseColor("#35eb93");
            rndrect.setStroke(1, color);
            rndrect.setColor(color);
            holder.friendStatus.setTextColor(Color.parseColor("#ffffff"));
        } else if (mItems.get(position).btn_sent) {
            holder.friendStatus.setText("Sent");
            color = Color.parseColor("#35eb93");
            rndrect.setStroke(1, color);
            rndrect.setColor(color);
            holder.friendStatus.setTextColor(Color.parseColor("#ffffff"));
        } else if (mItems.get(position).btn_receive) {
            holder.friendStatus.setText("Accept");
            color = Color.parseColor("#35eb93");
            rndrect.setStroke(1, color);
            rndrect.setColor(color);
            holder.friendStatus.setTextColor(Color.parseColor("#ffffff"));
        } else {
            holder.friendStatus.setText("Add");
            color = Color.parseColor("#35eb93");
            rndrect.setStroke(1, color);
            rndrect.setColor(Color.parseColor("#35eb93"));
            holder.friendStatus.setTextColor(Color.parseColor("#ffffff"));
        }

        holder.friend_btn.setBackgroundDrawable(rndrect);

        holder.friend_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItems.get(position).btn_friend) {

                } else if (mItems.get(position).btn_sent) {

                } else if (mItems.get(position).btn_receive) {
                    mActivity.sendAccept(holder.friend_btn, mItems.get(position));
                } else {
                    mActivity.sendRequest(holder.friend_btn, mItems.get(position));
                }
            }
        });

        holder.friend_photo.setVisibility(View.VISIBLE);
        holder.friend_photo_blank.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(mItems.get(position).friend.photo_url)) {
            if (mItems.get(position).friend.photo_url.contains("http"))
                Picasso.with(mActivity).load(mItems.get(position).friend.photo_url).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.friend_photo);
            else
                Picasso.with(mActivity).load(Config.server + mItems.get(position).friend.photo_url).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.friend_photo);
        } else {
            Picasso.with(mActivity).load(R.drawable.ic_appicon).placeholder(R.drawable.ic_appicon).fit().centerCrop().into(holder.friend_photo);
//            holder.friend_photo_blank.setVisibility(View.VISIBLE);
//            holder.friend_photo.setVisibility(View.GONE);
//            MyApplication.getInstance().setBlankAvatar(holder.friend_photo_blank, mItems.get(position).friend.name, false);
        }

        return convertView;
    }

    private static class ViewHolder {
        public ImageView friend_photo;
        public UserBlankAvatar friend_photo_blank;
        public TextView friend_name;
        public LinearLayout friend_btn;
        public TextView friendStatus;
    }
}
