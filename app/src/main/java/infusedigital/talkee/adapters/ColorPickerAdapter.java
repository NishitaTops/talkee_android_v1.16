package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;

/**
 * Created by bluesky on 5/15/2016.
 */
public class ColorPickerAdapter extends ArrayAdapter{
    private LayoutInflater mInflater;
    private ArrayList<String> mItems;
    private Activity mActivity;
    public static ArrayList viewHolderList;

    public ColorPickerAdapter(Activity activity, ArrayList<String> colorData) {
        super(activity, 0, colorData);

        viewHolderList = new ArrayList();

        mInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = colorData;
        mActivity = activity;
    }

    @Override
    public Object getItem(int position) {
        return viewHolderList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.colorpicker_dialog_item, parent, false);
            holder = new ViewHolder();

            holder.itemColorView = (CircleImageView) convertView.findViewById(R.id.colorpickerItem);

            viewHolderList.add(holder);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        int color = Color.parseColor(mItems.get(position));

        holder.itemColorView.setImageDrawable(new ColorDrawable(Color.parseColor(mItems.get(position))));
        return convertView;
    }

    public static class ViewHolder {
        public CircleImageView itemColorView;
    }
}
