package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.datamodel.FriendsData;
import infusedigital.talkee.view.UserBlankAvatar;


/**
 * Created by Hiral on 21-June-16.
 */
public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.ViewHolder> {
    Context context;
    private ArrayList<FriendsData> rowItems;
    LayoutInflater mInflater;

    public FriendsListAdapter(Context context, ArrayList<FriendsData> items) {
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.rowItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {//send_list_item
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.addressbook_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        FriendsData mData = rowItems.get(position);
        if (mData.getProfile_url().length() > 0) {
            Picasso.with(context)
                    .load(mData.getProfile_url())
                    .placeholder(R.drawable.ic_appicon)
                    .into(holder.receiver_photo);
        } else {
            Picasso.with(context)
                    .load(R.drawable.ic_appicon)
                    .placeholder(R.drawable.ic_appicon)
                    .into(holder.receiver_photo);
        }

        if (mData.isSelected()) {
            holder.llMain.setBackgroundColor(context.getResources().getColor(R.color.loginWithNumSel));
        } else {
            holder.llMain.setBackgroundColor(context.getResources().getColor(R.color.loginWithNum));
        }
        holder.receiver_name.setText(mData.getUsername());
        holder.receiver_phone.setText(mData.getName() + " " + mData.getLast_name());

        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rowItems.get(position).isSelected())
                    rowItems.get(position).setSelected(false);
                else
                    rowItems.get(position).setSelected(true);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView receiver_photo;
        public UserBlankAvatar receiver_photo_blank;
        public TextView receiver_name;
        //        public RelativeLayout default_state;
        public TextView receiver_phone, contact_req;
        public CircleImageView cancelView;
        LinearLayout llMain;

        public ViewHolder(View itemView) {
            super(itemView);
            llMain = (LinearLayout) itemView.findViewById(R.id.llMain);
            receiver_photo = (ImageView) itemView.findViewById(R.id.contact_photo);
//            receiver_photo_blank = (UserBlankAvatar) itemView.findViewById(R.id.receiver_photo_blank);
            receiver_name = (TextView) itemView.findViewById(R.id.contact_name);
//            default_state = (RelativeLayout) itemView.findViewById(R.id.default_state);
//            cancelView = (CircleImageView) itemView.findViewById(R.id.receiver_photo1);
            receiver_phone = (TextView) itemView.findViewById(R.id.contact_number);
            contact_req = (TextView) itemView.findViewById(R.id.contact_invite);
            contact_req.setText("Send");
        }
    }
}
