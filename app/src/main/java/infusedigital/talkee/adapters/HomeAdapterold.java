package infusedigital.talkee.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.FriendRequestActivity;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.datamodel.HomeListData;
import infusedigital.talkee.datamodel.Message;
import infusedigital.talkee.datamodel.UserSendMessage;
import infusedigital.talkee.view.NotificationColorItemView;
import infusedigital.talkee.view.UserBlankAvatar;

public class HomeAdapterold extends ArrayAdapter {
    private LayoutInflater mInflater;
    private ArrayList<HomeListData> mItems;
    private Activity mActivity;

    public HomeAdapterold(Activity activity, ArrayList<HomeListData> notificationsdata) {
        super(activity, 0, notificationsdata);
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = notificationsdata;
        mActivity = activity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.notification_list_item, parent, false);

            holder = new ViewHolder();

            holder.notification_color = (NotificationColorItemView) convertView.findViewById(R.id.notification_color);
            holder.notification_count = (TextView) convertView.findViewById(R.id.notification_count);

            holder.notification_user_photo = (ImageView) convertView.findViewById(R.id.notification_user_photo);
            holder.blankAvatar = (UserBlankAvatar) convertView.findViewById(R.id.blankavatar);
            holder.notification_name = (TextView) convertView.findViewById(R.id.notification_userename);
            holder.default_state = (RelativeLayout) convertView.findViewById(R.id.default_status);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final String type = mItems.get(position).getDataType();
        holder.blankAvatar.setVisibility(View.GONE);
        //0: message_thard, 1:user_send_msg
        if (type.equalsIgnoreCase("0")) {
            Message msgThread = mItems.get(position).getMessage_thard();
            holder.notification_name.setText(msgThread.getUsername());
            holder.notification_count.setText(msgThread.getMessagecount());

            if (msgThread.getImage().length() > 0) {
                Picasso.with(mActivity)
                        .load(msgThread.getImage())
                        .placeholder(R.drawable.ic_appicon)
                        .into(holder.notification_user_photo);
            } else {
                Picasso.with(mActivity)
                        .load(R.drawable.ic_appicon)
                        .placeholder(R.drawable.ic_appicon)
                        .into(holder.notification_user_photo);
            }

        } else if (type.equalsIgnoreCase("1")) {
            UserSendMessage userSendMsg = mItems.get(position).getUser_send_msg();
            holder.notification_count.setText("");
            holder.notification_color.setVisibility(View.GONE);
            holder.notification_name.setText(userSendMsg.getUsername());

            if (userSendMsg.getPhoto_url().length() > 0) {
                String url = userSendMsg.getPhoto_url();
                if (!url.contains("http"))
                    url = "http://topsdemo.co.in/webservices/talkee_app/v.1.1/" + url;
                Picasso.with(mActivity)
                        .load(url)
                        .placeholder(R.drawable.ic_appicon)
                        .into(holder.notification_user_photo);
            } else {
                Picasso.with(mActivity)
                        .load(R.drawable.ic_appicon)
                        .placeholder(R.drawable.ic_appicon)
                        .into(holder.notification_user_photo);
            }
        } else if (type.equalsIgnoreCase("2")) { // request
            holder.blankAvatar.setVisibility(View.VISIBLE);
            MyApplication.getInstance().setBlankAvatar(holder.blankAvatar, "Requests", true);
            holder.notification_count.setText(mItems.get(position).getUser_requests());
            holder.notification_name.setText("Requests");
            holder.blankAvatar.setColor(mActivity.getResources().getColor(R.color.req_color));
//            holder.notification_count.setBackgroundColor(mActivity.getResources().getColor(R.color.friend_message_playing_back));
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equalsIgnoreCase("2")) {
                    Intent requestIntent = new Intent(mActivity, FriendRequestActivity.class);
                    mActivity.startActivity(requestIntent);
                }
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        public ImageView notification_user_photo;
        public UserBlankAvatar blankAvatar;
        public NotificationColorItemView notification_color;
        public TextView notification_count;
        public TextView notification_name;
        public RelativeLayout default_state;
    }

    public void setList(ArrayList<HomeListData> list) {
        mItems = list;
    }
}