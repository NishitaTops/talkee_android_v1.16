package infusedigital.talkee.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.params.CoreProtocolPNames;
import infusedigital.talkee.activity.MyApplication;

/**
 * Created by bluesky on 5/16/2016.
 */
public class UploadFileAsync extends AsyncTask<String, Void, String> {

    int serverResponseCode;
    String upLoadServerUri;
    String sourceFileUri; //// = "http://192.168.1.138/talkee/uploader.php?";
    boolean isProfilePhoto = false;
    SharedPreferences prefs = null;
    Context mContext;

    HttpURLConnection conn = null;
    DataInputStream inStream = null;

    public UploadFileAsync(Context context, String serverUri, String path, boolean isProfilePhoto) {
        mContext = context;
        upLoadServerUri = serverUri;
        sourceFileUri = path;
        this.isProfilePhoto = isProfilePhoto;

        Log.e("Super", "uploadfileasync = " + serverUri + " : " + path);
//        UploadFile();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
//            String sourceFileUri = Environment.getExternalStorageDirectory().getPath() + "/Pictures/1463129489115.jpg";
            String fileName = sourceFileUri;
            Log.d ("aaaaa", sourceFileUri);
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File sourceFile = new File(sourceFileUri);

            if (sourceFile.isFile()) {
                try {
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);

                    URL url = new URL(upLoadServerUri);
Log.d ("*****", upLoadServerUri + ":" + sourceFileUri);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("ENCTYPE",
                            "multipart/form-data");
                    conn.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty("bill", fileName);
                    dos = new DataOutputStream(conn.getOutputStream());

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"bill\";filename=\""
                            + fileName  + "\"" + lineEnd);

                    dos.writeBytes(lineEnd);

                    // create a buffer of maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {

                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math
                                .min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0,
                                bufferSize);

                    }

                    // send multipart form data necesssary after file
                    // data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens
                            + lineEnd);

                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();

                    if (serverResponseCode == 201 || serverResponseCode == 200){
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line+"\n");
                        }
                        br.close();
                       Log.e("Super", "server upload response = " +  sb.toString());

                        if (isProfilePhoto){
                            prefs = mContext.getSharedPreferences("UserInfo",mContext.MODE_PRIVATE);
                            String photoUrl = sb.toString().substring(1);
                            MyApplication.currentUser.photo_url = photoUrl;
                            prefs.edit().putString("photoUrl", MyApplication.currentUser.photo_url).commit();
                            Log.e("Super", "photo url -= " + MyApplication.currentUser.photo_url);
                        }
                    }

//                    if (serverResponseCode == 200) {
                        Log.d ("aaaa", "a+" + ":"+serverResponseMessage);
//                    }
                    Log.i("Upload file to server", fileName + " File is written");

                    fileInputStream.close();
                    dos.flush();
                    dos.close();


                } catch ( Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
