package infusedigital.talkee.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tops on 12/27/2016.
 */
public class IncomingSms extends BroadcastReceiver {

    public Pattern otpSize = Pattern.compile("(|^)\\d{4}");

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
//            if (bundle != null) {
//                final Object[] pdusObj = (Object[]) bundle.get("pdus");
//                for (int i = 0; i < pdusObj.length; i++) {
//                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
//                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
//                    String senderNum = phoneNumber;
//                    String message = currentMessage.getDisplayMessageBody();
//                    System.out.println("+++++++++++++ data:" + phoneNumber);
//                    System.out.println("+++++++++++++ data:" + senderNum);
//                    System.out.println("+++++++++++++ data:" + message);
//                    if (message != null) {
//                        Matcher otp = otpSize.matcher(message);
//                        if (otp.find()) {
//                            System.out.println("+++++++++++++ final otp:" + otp.group(0));
//                        }
//                    }
//                    try {
//                        if (senderNum.equals("+447481339104")) {
//                            MainActivity Sms = new MainActivity();
//                            Sms.recivedSms(message);
//                        }
//                    } catch (Exception e) {
//                    }
//                }
//            }

            SmsMessage[] msgs = null;

            String str = "";
            if (bundle != null) {
                //---retrieve the SMS message received---
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for (int i = 0; i < msgs.length; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String address = "SMS from " + msgs[i].getOriginatingAddress();
                    String msgBody = msgs[i].getMessageBody().toString();
                    StringTokenizer tokens = new StringTokenizer(msgBody, ":");
                    String number = tokens.nextToken();
                    String senderNumber = msgs[i].getOriginatingAddress();
                    String msgText = tokens.nextToken();
                    System.out.println("Address--" + address + "\n" + "messageBody---" + msgBody);

                    if (msgBody != null) {
                        Matcher m = otpSize.matcher(msgText);
                        if (m.find()) {
                            System.out.println("++++++++++ otp:" + m.group(0));

                            if (senderNumber.equals("+447481339104")) {
                                Bundle extras = intent.getExtras();
                                Intent mIntent = new Intent("talkee.otpreader");
                                mIntent.putExtra("otp", m.group(0));
                                context.sendBroadcast(mIntent);
//                                MainActivity Sms = new MainActivity();
//                                Sms.recivedSms(m.group(0));
                            }
                        }
//                    else
//                    {
//                       Toast.makeText(context,"Something went Wrong",Toast.LENGTH_SHORT).show();
//                    }
                    }
                    System.out.println("String Message---" + msgBody + "\n" + msgText);
                }
            }
        } catch (Exception e) {

        }
    }
}
