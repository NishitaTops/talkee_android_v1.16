package infusedigital.talkee.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

import java.util.ArrayList;

import infusedigital.talkee.datamodel.AddressBookData;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Tops on 10/17/2016.
 */

public class AppConfig {
    public static String OTP = "";
    public static JSONObject USER_DETAILS = new JSONObject();
    public static String inserted_type = "1";
    public static String social_id = "";
    public static String social_name = "";
    public static String social_username = "";
    public static String social_profile_image = "";
    public static boolean isEditProf = false;
    public static int deviceWidth = 0;
    public static boolean isRefresh = false;
    public static boolean isAddFriend = false;
    public static boolean isMessageScreen = false;
    public static String param_session = "session_state";
    public static boolean isLogin;
    public static ArrayList<String> arrAllContacts = new ArrayList<String>();
    public static ArrayList<AddressBookData> addressBookDatas = new ArrayList<>();
    public static ArrayList<AddressBookData> margeAddressBookData = new ArrayList<>();
    public static String userID = "";
    public static String sender_id;

    /**
     * get user current id
     *
     * @param mContext
     * @return
     */
    public static String getUserId(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("UserInfo", MODE_PRIVATE);
        return prefs.getString("userId", "null");
    }
}
