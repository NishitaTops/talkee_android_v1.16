package infusedigital.talkee.utilities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.MainActivity;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.activity.TabViewActivity;
import infusedigital.talkee.helper.DBProvider;

/**
 * Created by bluesky on 5/16/2016.
 */
public class Utility {

    //    strAvatar.substring(strAvatar.lastIndexOf("/") + 1) // get file name from the URL with extension.
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Calculate the width of progressbar
     *
     * @param duration
     * @return
     */
    public static int getProgressSize(Context mContext, String duration) {
//        System.out.println("++++++++++++++++ duration:" + duration + ":total:" + AppConfig.deviceWidth);
//        AppConfig.deviceWidth - 116
//        hdpi - 250 | xhdpi - 270 | xxhdpi - 420 | xxxhdpi - 550
        int spacing = 250;
        switch (mContext.getResources().getString(R.string.mbool)) {
            case "hdpi":
                spacing = 230;
                break;
            case "mdpi":
                spacing = 230;
                break;
            case "xhdpi":
                spacing = 270;
                break;
            case "xxhdpi":
                spacing = 420;
                break;
            case "xxxhdpi":
                spacing = 550;
                break;
            default:
                spacing = 230;
                break;
        }
        int size = AppConfig.deviceWidth - spacing;
        switch (duration) {
            case "0:01":
                return size * 1 / 10;
            case "0:02":
                return size * 2 / 10;
            case "0:03":
                return size * 3 / 10;
            case "0:04":
                return size * 4 / 10;
            case "0:05":
                return size * 5 / 10;
            case "0:06":
                return size * 6 / 10;
            case "0:07":
                return size * 7 / 10;
            case "0:08":
                return size * 8 / 10;
            case "0:09":
                return size * 9 / 10;
            case "0:10":
                return size * 10 / 10;
            case "0:11":
                return size * 11 / 10;
            case "0:00":
                return 50;
        }
        return 50;
    }

    public static boolean setGridViewHeightBasedOnItems(GridView gridView, int column) {

        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();
            if (numberOfItems % column == 0) {
                numberOfItems = numberOfItems / column;
            } else {
                numberOfItems = (numberOfItems / column) + 1;
            }
            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(0, null, gridView);
                item.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            int totalDividersHeight = 0;
            int totalPadding = gridView.getPaddingTop() + gridView.getPaddingBottom();
            ViewGroup.LayoutParams params = gridView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + (totalPadding * 3);
            gridView.setLayoutParams(params);
            gridView.requestLayout();
            return true;
        } else {
            return false;
        }
    }

    public static boolean fileExist(String uri) {
        if (!new File(uri).exists()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check date difference if 24 hours completed or not
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static boolean dateDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

//        System.out.println("startDate : " + startDate);
//        System.out.println("endDate : " + endDate);
//        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);
//        System.out.println("++++++++++++++ different:" + different);
        if (elapsedDays == 0) {// 24 not completed
            if (elapsedHours > 23 && elapsedMinutes > 58) {
                // delete
                return true;
            } else {
                return false;
            }
        } else {// 24 completed
            return true;
        }

//        return true;
    }

    public static void sessionExpireDialog(final Context mContext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Please Login again!")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (!AppConfig.getUserId(mContext).equals(null)) {
                            ArrayList<String> mArrayList = DBProvider.getAllAudioOfUser(mContext, AppConfig.getUserId(mContext));
                            for (int i = 0; i < mArrayList.size(); i++) {
                                DBProvider.deleteRowAndFile(mContext, mArrayList.get(i));
                            }
                        }

                        SharedPreferences prefs = mContext.getSharedPreferences("UserInfo", mContext.MODE_PRIVATE);
                        prefs.edit().putString("loginstate", "false").commit();
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.clear();
                        editor.apply();
                        AppConfig.social_profile_image = "";
                        AppConfig.inserted_type = "1";
                        AppConfig.social_id = "";
                        AppConfig.social_name = "";
                        AppConfig.social_username = "";
                        AppConfig.social_profile_image = "";

                        Intent nextIntent = new Intent(mContext, MainActivity.class);
                        nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(nextIntent);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Clear all pref data on start
     *
     * @param mContext
     */
    public static void clearState(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("UserInfo", mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
        AppConfig.social_profile_image = "";
        AppConfig.inserted_type = "1";
        AppConfig.social_id = "";
        AppConfig.social_name = "";
        AppConfig.social_username = "";
        AppConfig.social_profile_image = "";
    }
}
