package infusedigital.talkee.utilities;

/**
 * Created by Tops on 10/14/2016.
 */

public class WSMethods {
    //    public static String BASE_URL = "http://topsdemo.co.in/webservices/talkee_app/v.1.3/ws/";
    public static String BASE_URL_AUDIO = "http://gettalkee.com/talkee_app/"; // LIVE SERVER
    //        public static String BASE_URL = "http://gettalkee.com/talkee_app/ws/"; // LIVE APP
    public static String BASE_URL = "http://54.243.196.147/ws/"; // LIVE SERVER
    public static String CHECK_NUMBER_POST = "check_user_number";
    public static String REGISTRATION_POST = "user_registration";
    public static String LOGIN_POST = "user_login";
    public static String LOGOUT_POST = "logout";
    public static String SOCIAL_REGISTRATION_POST = "user_social_registration";
    public static String SOCIAL_VERIFICATION_POST = "social_varification";
    //Session
    public static String SETTINGS_POST = "user_setting";
    public static String NOTIFICATION_POST = "user_notification";
    public static String HOME_THREAD_LIST_POST = "home_thread_list";
    public static String CHANGE_COLORCODE_POST = "change_colorcode";
    public static String SEND_MESSAGE_POST = "send_message";
    public static String LIST_OF_FRIENDS_POST = "list_of_friends"; // no
    public static String SEARCH_FRIEND_POST = "search_friend"; // no
    public static String LIST_REQUESTS_POST = "show_request_received"; // no
    public static String ADD_FRIENDS_LIST_POST = "search_users";// no // already friend = 1, notfriend = 0, requested = 2, requestreceived = 3
    public static String SEND_FRIEND_REQ_POST = "send_friend_request";// already friend = 1, notfriend = 0, requested = 2, requestreceived = 3
    public static String ACCEPT_DEC_REQ_POST = "request_accept_reject";// already friend = 1, notfriend = 0, requested = 2, requestreceived = 3
    public static String BLOCK_UNBLOCK_POST = "block_unblock";
    public static String BLOCK_USER_LIST_POST = "block_user_list"; //no
    public static String UNFRIEND_POST = "unfriend";
    public static String UPDATE_READ_STATUS_POST = "update_read_status";
    public static String FRIENDS_MESSAGE_LIST_POST = "friends_message_list";
    public static String CHECK_USER_NAME_POST = "check_user_name";
    public static String SYNC_CONT_NUMBER_POST = "sync_cont_number/id/";
}