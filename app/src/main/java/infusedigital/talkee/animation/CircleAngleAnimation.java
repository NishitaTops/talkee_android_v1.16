package infusedigital.talkee.animation;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.util.Log;
import android.view.View;

import infusedigital.talkee.activity.TabViewActivity;

/**
 * Created by bluesky on 5/15/2016.
 */
public class CircleAngleAnimation {

    private Circle circle;

    private float oldAngle;
    private float newAngle;
    public static ValueAnimator progressAnim;

    public static void recordingProgress(final Circle_Play circle, long duration) {
        progressAnim = ValueAnimator.ofInt(0, 360).setDuration(duration);
        progressAnim.start();

        progressAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int degree = (int) animation.getAnimatedValue();
                circle.setAngle(degree);
                circle.invalidate();
            }
        });

        progressAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                try {
                    TabViewActivity.getInstance().record_stop();

                    if (TabViewActivity.myAudioRecorder == null)
                        return;

                    TabViewActivity.myAudioRecorder.stop();
                    TabViewActivity.myAudioRecorder.release();
                    TabViewActivity.myAudioRecorder = null;
                    TabViewActivity.recordLayout.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d("animation", "animation end");
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public static void cancelProgressAnimation() {
        if (progressAnim != null && progressAnim.isRunning()) {
            progressAnim.cancel();
            TabViewActivity.recordLayout.setVisibility(View.GONE);
        }
    }

    public static void cancelProgressAnim() {
        if (progressAnim != null && progressAnim.isRunning()) {
            progressAnim.cancel();
        }
    }

}
