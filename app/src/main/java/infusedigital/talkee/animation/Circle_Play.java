package infusedigital.talkee.animation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import infusedigital.talkee.R;

/**
 * Created by bluesky on 5/15/2016.
 */
public class Circle_Play extends View {
    private static final int START_ANGLE_POINT = -90;
    private float mDensity;

    private final Paint paint;
    private RectF rect_in;
    float mRadius;

    private float angle;

    public Circle_Play(Context context, AttributeSet attrs) {
        super(context, attrs);

        mDensity = getResources().getDisplayMetrics().density;

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(0);
        //Circle color
        paint.setColor(getResources().getColor(R.color.rec_play_stroke));

        angle = 0;
        mRadius = dip2px(92) / 2;
    }

    public void setColor(int color){
        paint.setColor(color);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        rect_in = new RectF(0, 0, getWidth(), getHeight());
        canvas.drawArc(rect_in, START_ANGLE_POINT, angle, true, paint);
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    private int dip2px (float dpValue){
        return (int)(dpValue *mDensity + 0.5f);
    }
}
