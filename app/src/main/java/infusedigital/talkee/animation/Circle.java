package infusedigital.talkee.animation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import infusedigital.talkee.R;

/**
 * Created by bluesky on 5/15/2016.
 */
public class Circle extends View {
    private static final int START_ANGLE_POINT = -90;
    private float mDensity;

    private final Paint paint;
    private final RectF rect_in, rect_out;

    private float angle;

    public Circle(Context context, AttributeSet attrs) {
        super(context, attrs);

        final int strokeWidth = 10;
        mDensity = getResources().getDisplayMetrics().density;

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);

        //Circle color
        paint.setColor(getResources().getColor(R.color.rec_circle_color));

        int width = getWidth();
        rect_in = new RectF(dip2px(15), dip2px(15), width - dip2px(30), width - dip2px(30));
        rect_out = new RectF(dip2px(5), dip2px(5), width - dip2px(10), width - dip2px(10));

        angle = 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(rect_in, START_ANGLE_POINT, angle, false, paint);
        canvas.drawArc(rect_out, START_ANGLE_POINT, angle, false, paint);
    }

    private int dip2px (float dpValue){
        return (int)(dpValue *mDensity + 0.5f);
    }
}
