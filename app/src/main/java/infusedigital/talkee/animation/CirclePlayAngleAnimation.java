package infusedigital.talkee.animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.util.Log;
import android.widget.RelativeLayout;

import infusedigital.talkee.R;

/**
 * Created by bluesky on 5/15/2016.
 */
public class CirclePlayAngleAnimation {
    public static ValueAnimator progressAnim;

    public static void cancelProgress() {
        if (progressAnim.isRunning()) {
            progressAnim.removeAllListeners();
            progressAnim.cancel();
            progressAnim = null;
        }
    }

    public static void resumeProgress() {
        if (progressAnim != null) {
            progressAnim.resume();
        }
    }

    public static void pauseProgress() {
        if (progressAnim != null && progressAnim.isRunning()) {
            progressAnim.pause();
        }
    }

    public static void recordingProgress(final Circle_Play circle, long duration) {

        progressAnim = ValueAnimator.ofInt(0, 360).setDuration(duration);
        progressAnim.start();

        progressAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int degree = (int) animation.getAnimatedValue();
                circle.setAngle(degree);
                circle.invalidate();
            }
        });

        progressAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                circle.setAngle(0);
                circle.invalidate();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

}
