package infusedigital.talkee.twitterSignIn;

import com.twitter.sdk.android.core.Callback;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;


// example users/show service endpoint
public interface CustomService {
    @GET("/1.1/followers/list.json")
    void show(@Query("user_id") long id, Callback<Response> cb);
}
