package infusedigital.talkee.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.adapters.MsgFriendsListAdapter;
import infusedigital.talkee.adapters.SendListAdapter;
import infusedigital.talkee.animation.CirclePlayAngleAnimation;
import infusedigital.talkee.animation.Circle_Play;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.MsgFriendsData;
import infusedigital.talkee.datamodel.SearchUserData;
import infusedigital.talkee.helper.DBProvider;
import infusedigital.talkee.utilities.AlertDialogManager;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.UploadFileAsync;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;

public class SendViewActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    //    ListView send_grid;
    private Tracker mTracker;

    private ArrayList<SearchUserData> friendList = new ArrayList<>();
    private ArrayList<SearchUserData> listData = new ArrayList<>();
    private SendListAdapter adapter;
    private MsgFriendsListAdapter adapterFriends;

    ImageView back_btn;

    SharedPreferences prefs = null;

    static int sendview = 0;

    private ProgressDialog loadingDialog;
    AlertDialogManager alert = new AlertDialogManager();

    ArrayList checked_position;
    //    RelativeLayout play_btn;
    ImageView playbtn, pausebtn;

    public TextView send_btn;

    ArrayList gcm_list;

    HashMap<String, String> itemData = new HashMap<String, String>();

    private EditText searchbox;
    private ImageView searchIcon, searchCancel;

    boolean play_status;

    private RelativeLayout sendViewTitle;
    private CircleImageView playBtnView;
    int[] themeColorArray, alphaThemeColorArray;
    private int currentTheme;
    Context context;
    private String userId;
    RecyclerView recycler_view_friends;
    private LinearLayoutManager linearLayoutManager;
    private boolean isLoading = true;
    //    private ArrayList<FriendsData> arrAllFriendsList;
    private ArrayList<MsgFriendsData> arrAllSearchedFriendsList;
    private TextView tvEmptyView;
    private MediaPlayer mpPlay;
    private boolean isFirst = true, isFirstLoad = true;
    private long mDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_send_view);
        context = SendViewActivity.this;

        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        userId = prefs.getString("userId", "null");

        themeColorArray = getResources().getIntArray(R.array.themecolor);
        alphaThemeColorArray = getResources().getIntArray(R.array.alphathemecolor);

        sendViewTitle = (RelativeLayout) findViewById(R.id.sendviewTitle);
        playBtnView = (CircleImageView) findViewById(R.id.send_play_btn);

        currentTheme = prefs.getInt("currentThemeNum", 0);
        final int themecolor = themeColorArray[currentTheme];
        sendViewTitle.setBackgroundColor(themecolor);
        playBtnView.setImageDrawable(new ColorDrawable(themecolor));

        changeStatusBarColor(Color.parseColor("#e4215d"));

        checked_position = new ArrayList();
        play_status = false;

        searchbox = (EditText) findViewById(R.id.send_txt_box);
        playbtn = (ImageView) findViewById(R.id.real_play_btn);
        pausebtn = (ImageView) findViewById(R.id.real_pause_btn);
        searchIcon = (ImageView) findViewById(R.id.send_search_icon);
        searchCancel = (ImageView) findViewById(R.id.send_search_cancel_btn);
        send_btn = (TextView) findViewById(R.id.send_confirm_btn);
        recycler_view_friends = (RecyclerView) findViewById(R.id.recycler_view_friends);
        tvEmptyView = (TextView) findViewById(R.id.tvEmptyView);
        linearLayoutManager = new LinearLayoutManager(this);
        recycler_view_friends.setLayoutManager(linearLayoutManager);

//        recycler_view_friends.addOnScrollListener(new RecyclerViewScrollListener() {
//
//            @Override
//            public void onScrollUp() {
//
//            }
//
//            @Override
//            public void onScrollDown() {
//
//            }
//
//            @Override
//            public void onLoadMore() {
//                if (hasNext) {
//                    if (!isLoading) {
//                        isLoading = true;
//                        page++;
////                        adapterFriends.showLoading(true);
////                        getFriendsList();
//                        searchFriendsAPICall("");
//                    }
//                } else {
////                    adapter.showLoading(false);
//                }
//            }
//        });

//        send_grid = (ListView) findViewById(R.id.send_grid);

//        send_grid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
//        adapter = new SendListAdapter(SendViewActivity.this, friendList);
//        send_grid.setAdapter(adapter);
//        adapter.setData(friendList);
//        send_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
////                ViewHolder vh = (ViewHolder)adapter.getItem(position);
////                CircleImageView receiverPhoto = (CircleImageView)view.findViewById(R.id.receiver_photo);
////                TextView receiverName = (TextView)view.findViewById(R.id.receiver_name);
//
//                SearchUserData data = listData.get(position);
//
//                if (data.isSelected) {
//                    send_grid.setItemChecked(position, false);
//                    data.isSelected = false;
//                } else {
//                    send_grid.setItemChecked(position, true);
//                    data.isSelected = true;
//                }
//
//                adapter.notifyDataSetChanged();
//
//                if (checkSendMessage()) {
//                    send_btn.setBackgroundColor(themecolor);
//                    send_btn.setVisibility(View.VISIBLE);
//                } else {
//                    send_btn.setVisibility(View.GONE);
//                }
//
////                boolean bHash = false;
////
////                TextView name, phone;
////                name = (TextView) view.findViewById(R.id.receiver_name);
////                phone = (TextView)view.findViewById(R.id.send_profile_phone);
////                if (checked_position.contains(position)) {
////                    send_grid.setItemChecked(position, false);
////
////                    int start = data.name.length() - 1;
////                    String plus = data.name.substring(start);
////
////                    deselectIcon.setVisibility(View.GONE);
////
////                    for ( String key : itemData.keySet() ) {
////                        if (key.equals(String.valueOf (position))) {
////                            data.photo_url = itemData.get(key);
////                            break;
////                        }
////                    }
////
////                    for (int i = 0; i < checked_position.size(); i++) {
////                        int value = (int)checked_position.get(i);
////                        if (value == position)
////                            checked_position.remove(i);
////                    }
////                } else {
////                    send_grid.setItemChecked(position, true);
////                    checked_position.add(position);
////                    itemData.put(String.valueOf(position), data.photo_url);
////                    int start = data.name.length() - 1;
////                    String plus = data.name.substring(start);
////
////                    if (plus.equals("+")) {
////                        data.name = data.name.substring(0, start - 1);
////                        Log.d("plus", data.name);
////                    }
////
////                    data.photo_url = "tmp";//Uri.parse("R.drawable.avatar_blank").toString();
////
//////                    deselectIcon.setVisibility(View.VISIBLE);
////                }
//
////                adapter.notifyDataSetChanged ();
//
////                if (checked_position.size() > 0) {
//////                    send_btn.setBackgroundColor(Color.parseColor("#750cc7"));
////                    send_btn.setBackgroundColor(themecolor);
//////                    send_btn.setTextColor(Color.parseColor("#ffffff"));
//////                    name.setTextColor(Color.parseColor("#35eb93"));
//////                    phone.setTextColor(Color.parseColor("#35eb93"));
////                    x.setVisibility(View.VISIBLE);
////                    send_btn.setVisibility(View.VISIBLE);
////                } else {
////                    send_btn.setBackgroundColor(Color.parseColor("#000000"));
////                    send_btn.setTextColor(Color.parseColor("#ffffff"));
//////                    name.setTextColor(Color.parseColor("#000000"));
//////                    phone.setTextColor(Color.parseColor("#3a3a3a"));
////                    x.setVisibility(View.GONE);
////                    send_btn.setVisibility(View.GONE);
////                }
//            }
//        });


//        InputMethodManager inputMethodmanager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//        inputMethodmanager.hideSoftInputFromInputMethod(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);

        gcm_list = new ArrayList();

        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if the recorded file is valid
                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
                boolean valid = false;

                MediaPlayer mp = new MediaPlayer(); //.create(SendViewActivity.this, Uri.parse(path));
                try {
                    mp.setDataSource(path);
                    mp.prepare();
                    int duration = mp.getDuration();
                    mp.release();
                    if (duration < 2) {
                        alert.showAlertDialog(SendViewActivity.this,
                                "Recording Error",
                                "Please record again. You must record more than 1 sec.", false);
                    } else {
                        valid = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                if (valid) {
//                    for (int i = 0; i < listData.size(); i++) {
//                        if (listData.get(i).isSelected) {
//                            SearchUserData data = listData.get(i);
//                            send_voice_message(data);
//                        }
//                    }
//                }
                if (valid) {
                    ArrayList<String> userIds = new ArrayList<String>();
                    for (int i = 0; i < adapterFriends.rowItems.size(); i++) {
                        if (adapterFriends.rowItems.get(i).isSelected())
                            userIds.add(adapterFriends.rowItems.get(i).getFriends_id());
                    }
                    System.out.println("+++++ userIds:" + userIds);
                    sendAudioMessage(userIds);
                }
            }
        });

        hideSoftKeyboard();

        mpPlay = new MediaPlayer();
        mpPlay.setOnCompletionListener(this);

        pausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("++++++ pausebtn clicked.");
                if (mpPlay != null) {
                    mDuration = mpPlay.getDuration();
                    mpPlay.pause();
                    playbtn.setVisibility(View.VISIBLE);
                    pausebtn.setVisibility(View.GONE);
                    CirclePlayAngleAnimation.progressAnim.pause();
                }
            }
        });

        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("++++++ playbtn clicked.");
                if (mpPlay != null && isFirst) {
                    System.out.println("++++++ playbtn clicked in if.");
                    //check if the recorded file is valid
                    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
                    boolean valid = false;
//                MediaPlayer mpPlay = new MediaPlayer();
                    try {
                        mpPlay.setDataSource(path);
                        mpPlay.prepare();
                        int duration = mpPlay.getDuration();
                        System.out.println("++++++++++++++++ duration:" + duration);
                        if (duration < 2) {
                            alert.showAlertDialog(SendViewActivity.this,
                                    "Recording Error",
                                    "Please record again. You must record more than 1 sec.", false);
                        } else {
                            valid = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (valid) {
//                        playSound("record");
                        playSound("playnew");
                        mDuration = mpPlay.getDuration();
                        playAudio();
                    }
                } else if (!isFirst) {
                    System.out.println("++++++ playbtn clicked in else.");
                    playAudio();
                    playbtn.setVisibility(View.GONE);
                    pausebtn.setVisibility(View.VISIBLE);
                }
            }
        });

//        play_btn = (RelativeLayout) findViewById(R.id.play_message_btn);
//        play_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if (mpPlay != null && mpPlay.isPlaying()) {
////                    playbtn.setVisibility(View.VISIBLE);
//////                    pausebtn.setVisibility(View.GONE);
////                    mpPlay.pause();
////                } else if (mpPlay != null) {
////                    mpPlay.start();
////                    playbtn.setVisibility(View.GONE);
////                    pausebtn.setVisibility(View.VISIBLE);
////                } else if (mpPlay != null && !mpPlay.isPlaying()) {
//                //check if the recorded file is valid
//                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.3gp";
//                boolean valid = false;
//
////                MediaPlayer mpPlay = new MediaPlayer();
//                try {
//                    mpPlay.setDataSource(path);
//                    mpPlay.prepare();
//                    int duration = mpPlay.getDuration();
////                    mp.release();
//                    if (duration < 2) {
//                        alert.showAlertDialog(SendViewActivity.this,
//                                "Recording Error",
//                                "Please record again. You must record more than 1 sec.", false);
//                    } else {
//                        valid = true;
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                if (valid) {
//                    try {
//                        playSound("play");
//
//                        mpPlay.start();
//                        long duration = mpPlay.getDuration();
//
//                        playRecordedAudio(duration);
//                        mpPlay.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                            @Override
//                            public void onCompletion(MediaPlayer mp) {
//                                mp.stop();
//                                mp.release();
////                                mp = null;
//                                playbtn.setVisibility(View.VISIBLE);
////                                    pausebtn.setVisibility(View.GONE);
//                            }
//                        });
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
////                }
//            }
//        });

        back_btn = (ImageView) findViewById(R.id.sendview_back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mTracker = MyApplication.getInstance().getDefaultTracker();
//        getFriendsList();
        searchFriendsAPICall(""); //TODO open for Milestone 3
    }

    /**
     * Play Audio
     */
    private void playAudio() {
        try {
            mpPlay.start();
//            pausebtn.setVisibility(View.VISIBLE);
//            playbtn.setVisibility(View.GONE);
            if (isFirst) {
                playRecordedAudio(mDuration);
                isFirst = false;
            } else {
                CirclePlayAngleAnimation.progressAnim.resume();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        mpPlay.stop();
        isFirst = true;
        mpPlay.release();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    /**
     * send
     *
     * @param userIds
     */
    private void sendAudioMessage(ArrayList<String> userIds) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("reciver_id", userIds.toString());
        try {
            params.put("audio", new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.m4a"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostSendAudioMessageCallComplete, WSMethods.BASE_URL + WSMethods.SEND_MESSAGE_POST, params);
    }

    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSendAudioMessageCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    onBackPressed();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

//    private void getUserList() {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/group.php")
//                .setParameter("act", "getgroup")
//                .setParameter("user_id", String.valueOf(MyApplication.currentUser.id));
//
//        final String access_url = builder.toString();
//
//        startLoading();
//
//        Log.e("Super", "get voice message = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        endLoading();
//
//                        try {
//                            if (!response.getString("status").equals("SUCCESS")) {
//                                return;
//                            }
//                            friendList.clear();
//                            JSONArray groupuserdata, groupiddata;
//
//                            groupuserdata = response.getJSONArray("groupusers");
//                            groupiddata = response.getJSONArray("groupidinfo");
//
//                            for (int i = 0; i < groupiddata.length(); i++) {
//                                //get group_id
//                                int group_id = groupiddata.getInt(i);
//
//                                //get group members
//                                JSONArray groupusers = groupuserdata.getJSONArray(i);
//                                ArrayList<User> groupUserList = new ArrayList<>();
//
//                                for (int j = 0; j < groupusers.length(); j++) {
//                                    JSONObject jsonObject = groupusers.getJSONObject(j);
//                                    User groupuser = new User();
//                                    groupuser.setUser(jsonObject);
//                                    groupUserList.add(groupuser);
//
//                                }
//
//                                friendList.add(new SearchUserData(null, false, group_id, groupUserList));
//                            }
//
//                            getFriends();
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

//    private void getFriends() {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
//                .setParameter("act", "getfriends")
//                .setParameter("user_id", String.valueOf(MyApplication.currentUser.id));
//        String access_url = builder.toString();
//
//        startLoading();
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        endLoading();
//                        try {
//                            if (!response.getString("status").equals("SUCCESS")) {
//                                return;
//                            }
//
//                            friendList.clear();
//
//                            JSONArray jsonData = response.getJSONArray("friend");
//
//                            for (int i = 0; i < jsonData.length(); i++) {
//
//                                JSONObject jresponse = jsonData.getJSONObject(i);
//
//                                User friend = new User();
//                                friend.setUser(jresponse);
//
//                                friendList.add(new SearchUserData(friend, false, -1, null));
//                            }
//
//                            listData = friendList;
//                            adapter.setData(friendList);
//                            adapter.notifyDataSetChanged();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

    @Override
    protected void onResume() {
        super.onResume();
//        getUserList();
//        getFriends();
        hideSoftKeyboard();

        searchbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                callSearch(searchbox.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchbox.setText("");
            }
        });

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSearch(searchbox.getText().toString());
            }
        });

    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    /**
     * API for get friends and search friends
     *
     * @param query
     */
    public void callSearch(String query) {
        if (TextUtils.isEmpty(query) || query.toString().trim().length() == 0) {
//            adapter.setData(friendList);
//            listData = friendList;
//            adapter.notifyDataSetChanged();

//            if (arrAllFriendsList.size() > 0) {
//                adapterFriends = new FriendsListAdapter(context, arrAllFriendsList);
//                recycler_view_friends.setAdapter(adapterFriends);
//                adapterFriends.notifyDataSetChanged();
//            }
            searchFriendsAPICall("");
            return;
        } else {
            if (query.length() >= 2)
                searchFriendsAPICall(query);
        }

//        ArrayList newList = new ArrayList();
//
//        for (int i = 0; i < friendList.size(); i++) {
//            SearchUserData data = friendList.get(i);
//
//            if (data.group_id == -1) {
//                if (data.friend.name.contains(query)) {
//                    newList.add(data);
//                }
//            } else {
//                if (getGroupName(data.groupMembers).contains(query))
//                    newList.add(data);
//            }
//        }
//
//        adapter.setData(newList);
//
//        listData = newList;
//        adapter.notifyDataSetChanged();
    }

    /**
     * search friend api call here
     *
     * @param query
     */
    private void searchFriendsAPICall(String query) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("username", query);
        if (isFirstLoad)
            startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostSearchFriendsCallComplete, WSMethods.BASE_URL + WSMethods.SEARCH_FRIEND_POST, params);
    }

    /**
     * API parsing of search friends
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSearchFriendsCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                if (isFirstLoad) {
                    isFirstLoad = false;
                    endLoading();
                }
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
//                hasNext = Boolean.parseBoolean(jobj.getString("IS_NEXT"));
                if (flag.equalsIgnoreCase("true")) {
                    JSONArray arrFriendsList = jobj.getJSONArray("USER_DETAILS");
                    arrAllSearchedFriendsList = new ArrayList<>();
                    for (int i = 0; i < arrFriendsList.length(); i++) {
                        JSONObject obj = arrFriendsList.getJSONObject(i);
                        arrAllSearchedFriendsList.add(new MsgFriendsData(obj.getString("friends_id"),
                                obj.getString("name"),
                                obj.getString("last_name"),
                                obj.getString("username"),
                                obj.optString("phonenum"),
                                obj.getString("photo_url"),
                                false));
                    }
                    if (arrAllSearchedFriendsList.size() > 0) {
                        adapterFriends = new MsgFriendsListAdapter(SendViewActivity.this, arrAllSearchedFriendsList);
                        recycler_view_friends.setAdapter(adapterFriends);
                        tvEmptyView.setVisibility(View.GONE);
                    }
                } else {
                    recycler_view_friends.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                    if (jobj.optString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

//    private String getGroupName(ArrayList<User> members) {
//        String result = "";
//        int size = members.size();
//
//        if (size == 0)
//            return result;
//
//        for (int i = 0; i < size; i++) {
//            if (i == 0) {
//                result = members.get(0).name;
//            } else {
//                result = result + ", " + members.get(i).name;
//            }
//        }
//
//        return result;
//    }

    private void playRecordedAudio(long duration) {
        Circle_Play circle = (Circle_Play) findViewById(R.id.play_sound_circle);

        circle.setColor(getResources().getColor(R.color.loginBtnColorSel));

        CirclePlayAngleAnimation.recordingProgress(circle, duration);

        playbtn.setVisibility(View.GONE);
        pausebtn.setVisibility(View.VISIBLE);
    }

    private void send_voice_message(SearchUserData data) {
        //analytics
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Message")
                .build());

        Log.e("Super", "send voice message");
        if (data.group_id == -1) {
            Log.e("Super", "send voice to user");
            String serverUri = "http://gettalkee.com/talkee/send_message.php?";

            Log.e("Super", "send_voice_message");

            new UploadFileAsync(this, serverUri + "regId=" + data.friend.gcm_id + "&userId=" + MyApplication.currentUser.id + "&friendId=" +
                    data.friend.id + "&requestType=2",
                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.m4a", false).execute("");
        } else {
            Log.e("Super", "send voice to group");
            String serverUri = "http://gettalkee.com/talkee/group.php?";
            new UploadFileAsync(this, serverUri + "act=" + "sendmessage" + "&user_id=" + MyApplication.currentUser.id + "&group_id=" + data.group_id,
                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.m4a", false).execute("");
        }

        new CopyTask(Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.m4a", data).execute();

        sendview = 1;
    }

    /**
     * Show button on selection
     */
    public void showButton() {
        boolean isSelected = false;
        for (int i = 0; i < adapterFriends.rowItems.size(); i++) {
            if (adapterFriends.rowItems.get(i).isSelected()) {
                isSelected = true;
            }
        }
        if (isSelected) {
            send_btn.setVisibility(View.VISIBLE);
//            recycler_view_friends.setPadding(0, 0, 0, 100);
        } else {
            send_btn.setVisibility(View.GONE);
//            recycler_view_friends.setPadding(0, 0, 0, 10);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.stop();
        isFirst = true;
        mpPlay.release();
        mpPlay = new MediaPlayer();
        mpPlay.setOnCompletionListener(this);
        playbtn.setVisibility(View.VISIBLE);
        pausebtn.setVisibility(View.GONE);
    }

    class CopyTask extends AsyncTask<Void, Void, Boolean> {
        private String src_path;
        private SearchUserData data;
        //        private String friendId;
        private String filename;
//        private int group_id;

        public CopyTask(String path, SearchUserData userData) {
            this.src_path = path;
            data = userData;
        }

        @Override
        protected void onPreExecute() {
            startLoading();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            filename = String.valueOf(System.currentTimeMillis());
            File src = new File(src_path);

            String directory;

            if (data.group_id == -1) {
                directory = MyApplication.getInstance().getMessageDir() + data.friend.id;
            } else {
                directory = MyApplication.getInstance().getGroupMessageDir() + data.group_id + File.separator + MyApplication.currentUser.id;
            }

            Log.e("Super", "copy message" + directory);
//            String friendDirectory = getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator + friendId;
            File file = new File(directory);
//           Log.e("Super", "frienddirectory = " + friendDirectory);

            if (!file.exists())
                file.mkdir();

//            String destpath = getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator + friendId +
//                    File.separator + filename + ".3gp";
            String destpath = directory + File.separator + filename + ".m4a";

            File dest = new File(destpath);
            boolean result = CopyFile(src, dest);
            return result;
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);

            endLoading();

            if (aVoid) {
                updateDB(data, filename);
                finish();
            }
        }
    }

    private boolean CopyFile(File src, File dest) {
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void updateDB(SearchUserData data, String filename) {
        ContentValues values = new ContentValues();
        String friendID = "";
        if (data.group_id == -1)
            friendID = String.valueOf(data.friend.id);
        else
            friendID = String.valueOf(MyApplication.currentUser.id);

        values.put(DBProvider.FRIEND_ID, friendID);
        values.put(DBProvider.MESSAGE_URL, "");
        values.put(DBProvider.STATE, "send");
        values.put(DBProvider.GROUP_ID, data.group_id);
        values.put(DBProvider.FILE_PATH, filename);

        getContentResolver().insert(DBProvider.CONTENT_URI, values);
    }

    // Playing sound
    public void playSound(String name) {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + getPackageName() + "/raw/" + name);

            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkSendMessage() {
        int checkednum = 0;

        if (friendList != null && friendList.size() > 0) {
            for (int i = 0; i < friendList.size(); i++) {
                if (friendList.get(i).isSelected)
                    checkednum++;
            }
        }

        if (checkednum > 0)
            return true;
        else
            return false;
    }
}
