package infusedigital.talkee.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import infusedigital.talkee.R;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.gcm.GcmIntentService;
import infusedigital.talkee.utilities.AlertDialogManager;
import infusedigital.talkee.utilities.Iso2Phone;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
//    private static final String TWITTER_KEY = "lVJOWL2CckrqTGobWMJBazjHE";
//    private static final String TWITTER_SECRET = "zrlWyEoH7fw66CVp94ZyPPolldHq9kyVvHbeI9MDHfsKEhoapl";

    private Button register_btn;
    private TextView login_btn;
    private String TAG = MainActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private SharedPreferences prefs = null;

    private String deviceId;

    private String phonenum, type;
    public static String gcm_reg_id = "";

    private ProgressDialog loadingDialog;
    AlertDialogManager alert = new AlertDialogManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        System.out.println("+++++++++++++++++++++++++ token:" + FirebaseInstanceId.getInstance().getToken());
        prefs.edit().putString("gcm_regid", FirebaseInstanceId.getInstance().getToken()).commit();
        login_btn = (TextView) findViewById(R.id.login_btn);

        //TODO for send to server
   /*     ArrayList<String> contacts = new ArrayList<>();
        contacts.add("90127437120");
        contacts.add("90127437120");
        contacts.add("90127437120");
        contacts.add("90127437120");
        contacts.add("90127437120");

        String[] data = new String[contacts.size()];
        for (int i = 0; i < contacts.size(); i++) {
            data[i] = contacts.get(i);
        }
        JSONArray json = new JSONArray(Arrays.asList(data));
        System.out.println("+++++++++++ json:" + json);*/


//        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        String countryCode = tm.getSimCountryIso();
//        System.out.println("++++++++ countryCode:" + countryCode);


        // TODO getting phone number
//        TelephonyManager tm = (TelephonyManager) getApplicationContext()
//                .getSystemService(Context.TELEPHONY_SERVICE);
//        String phNo = tm.getLine1Number();
//        String country = tm.getSimCountryIso();

//        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
//        int countryCode = phoneUtil.getCountryCodeForRegion(Locale.getDefault().getCountry());
//        System.out.println("++++++++ countryCode:" + countryCode);

//        TelephonyManager telMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
//        String simContryiso = telMgr.getSimCountryIso();
//        String indicative = Iso2Phone.getPhone(simContryiso);
//        System.out.println("++++++++ countryCode:" + indicative);

        register_btn = (Button) findViewById(R.id.register_btn);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            TextView tvVersion = (TextView) findViewById(R.id.tvVersion);
            tvVersion.setText("Version:" + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (checkPlayServices()) {
            registerGCM();
        }

        getHashKey(MainActivity.this);
        requestMultiplePermissions();

        login_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent nextIntent = new Intent(getApplicationContext(), LoginViewActivity.class);
                startActivity(nextIntent);
                finish();
//                gcm_reg_id = prefs.getString("gcm_regid", "null");
//                if (deviceId == null || gcm_reg_id == null || gcm_reg_id.equals("null")) {
//                    Toast.makeText(getApplicationContext(), "This device not allowed all permissions for working app properly.", Toast.LENGTH_LONG).show();
//                    return;
//                }
//
//                if (phonenum.equals("null") || TextUtils.isEmpty(phonenum)) {
//                    Intent nextIntent = new Intent(getApplicationContext(), LoginViewActivity.class);
//                    startActivity(nextIntent);
//                    finish();
//                } else {
//                    URIBuilder builder = new URIBuilder();
//
//                    Log.d("aaaa", " gcm_regid = " + gcm_reg_id);
//                    builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/login.php")
//                            .setParameter("act", "login")
//                            .setParameter("phonenum", phonenum)
//                            .setParameter("deviceid", deviceId)
//                            .setParameter("gcm_regid", gcm_reg_id)
//                            .setParameter("type", type);
//
//                    String access_url = builder.toString();
//                    Log.e("Super", "login accent userl su = " + access_url);
//
//                    startLoading();
//
//                    JsonObjectRequest req = new JsonObjectRequest(access_url,
//                            new Response.Listener<JSONObject>() {
//                                @Override
//                                public void onResponse(JSONObject response) {
//                                    try {
//                                        endLoading();
//
//                                        String status = response.getString("status");
//
//                                        if (!status.equals("SUCCESS")) {
//                                            Toast.makeText(MainActivity.this, "You can't login", Toast.LENGTH_SHORT).show();
//                                            return;
//                                        }
//
//                                        JSONObject data = response.getJSONObject("data");
//                                        MyApplication.currentUser.setUser(data);
//
//                                        prefs.edit().putString("userId", String.valueOf(MyApplication.currentUser.id)).commit();
//                                        prefs.edit().putString("photoUrl", MyApplication.currentUser.photo_url).commit();
//                                        prefs.edit().putString("name", MyApplication.currentUser.name).commit();
//                                        prefs.edit().putString("color", MyApplication.currentUser.emoticol).commit();
//                                        prefs.edit().putString("mood", MyApplication.currentUser.mood).commit();
//                                        prefs.edit().putString("phonenum", MyApplication.currentUser.phonenum).commit();
//                                        prefs.edit().putString("gcm_regid", MyApplication.currentUser.gcm_id).commit();
//                                        prefs.edit().putString("deviceid", MyApplication.currentUser.device_id).commit();
//                                        prefs.edit().putString("loginstate", "true").commit();
//
//                                        Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
//                                        startActivity(nextIntent);
//                                        finish();
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//
//                                }
//                            }, new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            endLoading();
//                        }
//                    });
//
//                    MyApplication.getInstance().addToRequestQueue(req);
//                }
            }
        });

        register_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//
//                gcm_reg_id = prefs.getString("gcm_regid", "null");
//                if (deviceId == null || gcm_reg_id == null || gcm_reg_id.equals("null")) {
//                    Toast.makeText(getApplicationContext(), "This device not allowed all permissions for working app properly.", Toast.LENGTH_LONG).show();
//                    return;
//                }
//
//                if (register_btn.isSelected())
//                    register_btn.setTextColor(getResources().getColor(R.color.loginBtnColorS));
//                else
//                    register_btn.setTextColor(getResources().getColor(R.color.loginBtnColorSel));
                Intent nextIntent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(nextIntent);
                finish();
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    String token = intent.getStringExtra("token");
                    prefs.edit().putString("gcm_regid", token).commit();
//                    Toast.makeText(getApplicationContext(), "GCM registration token: " + token, Toast.LENGTH_LONG).show();

                } else if (intent.getAction().equals(Config.SENT_TOKEN_TO_SERVER)) {
                    // gcm registration id is stored in our server's MySQL

//                    Toast.makeText(getApplicationContext(), "GCM registration token is stored in server!", Toast.LENGTH_LONG).show();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

//                    Toast.makeText(getApplicationContext(), "Push notification is received!", Toast.LENGTH_LONG).show();
                }
            }
        };

//        getKeyHash();

//        ContentResolver cr = getContentResolver(); //Activity/Application android.content.Context
//        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
//        if (cursor.moveToFirst()) {
//            ArrayList<String> alContacts = new ArrayList<String>();
//            do {
//                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
//
//                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
//                    while (pCur.moveToNext()) {
//                        String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                        alContacts.add(contactNumber);
//                        break;
//                    }
//                    pCur.close();
//                }
//            } while (cursor.moveToNext());
//
//            if (alContacts.size() > 0) {
//                JSONObject obj_json = new JSONObject();
//                JSONArray obj_arr = new JSONArray();
//                for (int i = 0; i < alContacts.size(); i++) {
//                    try {
//                        JSONObject obj = new JSONObject();
//                        obj.put("phonenum", getOnlyDigits(alContacts.get(i)));
//                        System.out.println("+++++++++++++++++ contact:" + i + ":::" + alContacts.get(i) + "::::" + /*alContacts.get(i).replaceAll("[\\\\-\\\\+\\\\.\\\\^:,\\ \\(\\)\\-]", "") +*/ ":::" + getOnlyDigits(alContacts.get(i)));
//                        obj_arr.put(obj);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
////                System.out.println("+++++++++++++++ final array:" + obj_arr);
//                try {
//                    obj_json.put("sync", obj_arr);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                System.out.println("+++++++++++++ final arr:" + obj_json.toString());
////                params.put("sync", obj_json.toString());
//            }
//        }
    }

//    public static String getOnlyDigits(String s) {
//        Pattern pattern = Pattern.compile("[^0-9]");
//        Matcher matcher = pattern.matcher(s);
//        String number = matcher.replaceAll("");
//        return number;
//    }

    private void getKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Toast.makeText(this, Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_LONG).show();
                Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public String getHashKey(Context context) {

        PackageInfo info;
        try {
            info = context.getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(com.loopj.android.http.Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                System.out.println("+++++++++++++++++++++ something:" + something);
//                Util.printLog("hash key : " + something);
                return something;
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
        return "";
    }

    // starting the service to register with GCM
    private void registerGCM() {
        Intent intent = new Intent(this, GcmIntentService.class);
        intent.putExtra("key", "register");
        startService(intent);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed!");
                Toast.makeText(getApplicationContext(), "This device is not supported. Google Play Services not installed!", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        phonenum = prefs.getString("phonenum", "null");
        type = prefs.getString("type", "null");
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    private void requestMultiplePermissions() {
        String cameraPermission = android.Manifest.permission.CAMERA;
        String recordAudioPermission = android.Manifest.permission.RECORD_AUDIO;
        String wakeLockPermission = android.Manifest.permission.WAKE_LOCK;
        String writeExternalStoragePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStoragePermission = Manifest.permission.READ_EXTERNAL_STORAGE;
        String readPhoneStatePermission = Manifest.permission.READ_PHONE_STATE;
        String readContactsPermission = Manifest.permission.READ_CONTACTS;
        String writeContactsPermission = Manifest.permission.WRITE_CONTACTS;
        String sendSmsPermission = Manifest.permission.SEND_SMS;
        String internetPermission = Manifest.permission.INTERNET;
        String accessNetWorkPermission = Manifest.permission.ACCESS_NETWORK_STATE;

        int hasCameraPermission = ContextCompat.checkSelfPermission(MainActivity.this, cameraPermission);
        int hasRecordAudioPermission = ContextCompat.checkSelfPermission(MainActivity.this, recordAudioPermission);
        int hasWakeLockPermission = ContextCompat.checkSelfPermission(MainActivity.this, wakeLockPermission);
        int hasWriteExternalPermission = ContextCompat.checkSelfPermission(MainActivity.this, writeExternalStoragePermission);
        int hasReadExternalPermission = ContextCompat.checkSelfPermission(MainActivity.this, readExternalStoragePermission);
        int hasReadPhoneStatePermission = ContextCompat.checkSelfPermission(MainActivity.this, readPhoneStatePermission);
        int hasReadContactsPermission = ContextCompat.checkSelfPermission(MainActivity.this, readContactsPermission);
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(MainActivity.this, writeContactsPermission);
        int hasSendSmsPermission = ContextCompat.checkSelfPermission(MainActivity.this, sendSmsPermission);
        int hasInternetPermission = ContextCompat.checkSelfPermission(MainActivity.this, internetPermission);
        int hasAccessNetworkPermission = ContextCompat.checkSelfPermission(MainActivity.this, accessNetWorkPermission);

        List<String> permissions = new ArrayList<String>();
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(cameraPermission);
        }
        if (hasRecordAudioPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(recordAudioPermission);
        }
        if (hasWakeLockPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(wakeLockPermission);
        }
        if (hasWriteExternalPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(writeExternalStoragePermission);
        }
        if (hasReadExternalPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(readExternalStoragePermission);
        }
        if (hasReadPhoneStatePermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(readPhoneStatePermission);
        }
        if (hasReadContactsPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(readContactsPermission);
        }
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(writeContactsPermission);
        }
        if (hasSendSmsPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(sendSmsPermission);
        }

        if (hasInternetPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(internetPermission);
        }

        if (hasAccessNetworkPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(accessNetWorkPermission);
        }

        if (!permissions.isEmpty()) {
            String[] params = permissions.toArray(new String[permissions.size()]);
            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(params, 1);
            }
        } else {
            initData();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length == permissions.length) {

                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        return;
                }
                initData();
            } else {
                Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initData() {
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = tm.getDeviceId();
        Log.e("deviceId = %s", deviceId);
        prefs.edit().putString("deviceid", deviceId).commit();
    }
}