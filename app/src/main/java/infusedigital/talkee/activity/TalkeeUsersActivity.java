package infusedigital.talkee.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import infusedigital.talkee.R;
import infusedigital.talkee.adapters.AddFriendAdapter;
import infusedigital.talkee.adapters.AddFriendAdapterTalkeeUsers;
import infusedigital.talkee.adapters.AddressBookListAdapter;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.AddFriend;
import infusedigital.talkee.datamodel.AddressBookData;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;
import infusedigital.talkee.view.RecyclerViewScrollListener;

public class TalkeeUsersActivity extends AppCompatActivity {

    private ImageView backBtn;
    private TextView talkee_users_title, tvEmptyView;
    private RelativeLayout toolbarlayout;

    private ArrayList<AddressBookData> addressBookDatas;

    public static final String SMS_SENT_ACTION = "com.andriodgifts.gift.SMS_SENT_ACTION";
    public static final String SMS_DELIVERED_ACTION = "com.andriodgifts.gift.SMS_DELIVERED_ACTION";
    private RecyclerView recycler_view_TalkeeUsers;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar mProgressBar;
    private TalkeeUsersActivity context;
    int page = 1;
    private boolean isLoading;
    private boolean isFirst = true;
    private ProgressDialog loadingDialog;
    private EditText searchbox;
    private ImageView searchIcon, searchCancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_talkee_friends);

        context = TalkeeUsersActivity.this;
        backBtn = (ImageView) findViewById(R.id.backbtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        arrAllFriendsList = new ArrayList<>();
        talkee_users_title = (TextView) findViewById(R.id.talkee_users_title);
        tvEmptyView = (TextView) findViewById(R.id.tvEmptyView);
        talkee_users_title.setTypeface(MyApplication.boldFont);
        tvEmptyView.setTypeface(MyApplication.boldFont);

        toolbarlayout = (RelativeLayout) findViewById(R.id.toolbarlayout);
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        int currentTheme = prefs.getInt("currentThemeNum", 0);
        int[] themeColorArray = getResources().getIntArray(R.array.themecolor);
        int themecolor = themeColorArray[currentTheme];
//        toolbarlayout.setBackgroundColor(themecolor);

        mProgressBar = (ProgressBar) findViewById(R.id.mProgressBarLoadMore);
        recycler_view_TalkeeUsers = (RecyclerView) findViewById(R.id.recycler_view_TalkeeUsers);
        linearLayoutManager = new LinearLayoutManager(this);
        recycler_view_TalkeeUsers.setLayoutManager(linearLayoutManager);

        recycler_view_TalkeeUsers.addOnScrollListener(new RecyclerViewScrollListener() {

            @Override
            public void onScrollUp() {
                System.out.println("+++++++++++ onScrollUp");
            }

            @Override
            public void onScrollDown() {
                System.out.println("+++++++++++ onScrollDown");
                if (hasNext) {
                    if (!isLoading) {
                        isLoading = true;
                        page++;
                        mProgressBar.setVisibility(View.VISIBLE);
                        getFriendsList(query);
                    }
                } else {
                    System.out.println("++++++++++++++++++ onLoadMore onScrollDown else");
                }
            }

            @Override
            public void onLoadMore() {
                System.out.println("++++++++++++++++++ onLoadMore" + hasNext + "::::" + isLoading);
                if (hasNext) {
                    if (!isLoading) {
                        isLoading = true;
                        page++;
                        mProgressBar.setVisibility(View.VISIBLE);
                        getFriendsList(query);
                    }
                } else {
                    System.out.println("++++++++++++++++++ onLoadMore else");
                }
            }
        });

        searchbox = (EditText) findViewById(R.id.send_txt_box_talkee);

        searchbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                query = searchbox.getText().toString().trim();
                System.out.println("+++++++++++ text change:" + query);
                if (query.length() != 0) {
//                    if (!isLoading)
                    page = 1;
                    getFriendsList(query);
                } else if (searchbox.getText().toString().length() == 0) {
                    page = 1;
                    getFriendsList("");
//                    servicesView.setVisibility(View.VISIBLE);
//                    recycler_view_friends.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                query = searchbox.getText().toString().trim();
//                if (query.length() != 0) {
////                    if (!isLoading)
//                    getFriendsList(query);
//                } else if (searchbox.getText().toString().length() == 0) {
//                    servicesView.setVisibility(View.VISIBLE);
//                    recycler_view_friends.setVisibility(View.GONE);
//                }
            }
        });

        searchIcon = (ImageView) findViewById(R.id.send_search_icon_talkee);
        searchCancel = (ImageView) findViewById(R.id.send_search_cancel_btn_talkee);

        searchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 1;
                searchbox.setText("");
                System.out.println("+++++++++++ cross click");
//                getFriendsList("");
            }
        });

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query = searchbox.getText().toString();
                if (query.length() != 0) {
                    page = 1;
                    System.out.println("+++++++++++ search icon click");
                    getFriendsList(query);
                }
            }
        });

        getFriendsList(query);
    }

    /**
     * Get friends list
     */
    private void getFriendsList(String query) {

        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("username", query);
        params.put("paging", page);
        if (isFirst)
            startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostGetFriendsCallComplete, WSMethods.BASE_URL + WSMethods.ADD_FRIENDS_LIST_POST, params);
    }

    private boolean hasNext;
    private ArrayList<AddFriend> arrAllFriendsList;
    private AddFriendAdapterTalkeeUsers adapterFriends;
    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostGetFriendsCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");

                if (mProgressBar.getVisibility() == View.VISIBLE)
                    mProgressBar.setVisibility(View.GONE);

                if (jobj.getString("IS_NEXT").equalsIgnoreCase("true")) {
                    hasNext = true;
                } else {
                    hasNext = false;
                }

                if (flag.equalsIgnoreCase("true")) {

                    JSONArray arrFriendsList = jobj.getJSONArray("USER_DETAILS");
                    if (page == 1)
                        arrAllFriendsList.clear();
                    for (int i = 0; i < arrFriendsList.length(); i++) {
                        JSONObject obj = arrFriendsList.getJSONObject(i);
                        arrAllFriendsList.add(new AddFriend(obj.getString("userid"),
                                obj.getString("name"),
                                obj.getString("last_name"),
                                obj.getString("username"),
                                obj.getString("profile_url"),
                                obj.getString("isfollow")));
                    }
                    if (arrAllFriendsList.size() > 0) {
                        if (adapterFriends != null) {
                            adapterFriends.notifyDataSetChanged();
                        } else {
                            adapterFriends = new AddFriendAdapterTalkeeUsers(TalkeeUsersActivity.this, arrAllFriendsList);
                            recycler_view_TalkeeUsers.setAdapter(adapterFriends);
                        }
                    }
                } else {
                    if (page == 1)
                        arrAllFriendsList.clear();

                    if (jobj.optString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }
                }
                System.out.println("+++++++++++ status:" + hasNext);
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.in_slide_left_right, R.anim.fade_out);
    }

    /**
     * Get friends list
     */
    public void sendRequest(String friendId) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
//        params.put("user_id", "240");
        params.put("reciver_id", friendId);
//        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostSendFriendReqCallComplete, WSMethods.BASE_URL + WSMethods.SEND_FRIEND_REQ_POST, params);
    }

    private String query = "";
    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSendFriendReqCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
//                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    page = 1;
                    getFriendsList(query);
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Accept/Decline request received
     *
     * @param sender_id
     */
    public void ActionOnRequest(final String sender_id) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("requestflag", "1");
        params.put("sender_id", sender_id);
        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostSendFriendReqCallComplete, WSMethods.BASE_URL + WSMethods.ACCEPT_DEC_REQ_POST, params);
    }

    private void startLoading() {
        isFirst = false;
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }
}