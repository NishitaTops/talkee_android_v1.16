package infusedigital.talkee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class LauncherActivity extends AppCompatActivity {
    SharedPreferences prefs = null;
    PackageInfo pInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_launcher);

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {

//        boolean isFirst = true;
//        if (isFirst)
//            throw new RuntimeException("This is a crash");
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        SharedPreferences prefs1 = getSharedPreferences("UpdateInfo", MODE_PRIVATE);
        String updateApp = prefs1.getString("isUpdatedApp", "");

        prefs = getSharedPreferences("UserInfo", MODE_PRIVATE);
        String loginState = prefs.getString("loginstate", "false");
//        String updateApp = prefs.getString("isUpdatedApp", "");
//        System.out.println("+++++++++++++ data:" + updateApp.equals("false") + "::" + loginState.equals("true") + "::::" + pInfo.versionName);
        if ((updateApp.equals("") || updateApp.length() == 0) && loginState.equals("true") && pInfo.versionName.equalsIgnoreCase("1.14")) {
            System.out.println("+++++++ in if");
            prefs.edit().putString("loginstate", "false").commit();
            prefs1.edit().putString("isUpdatedApp", "true").commit();
        } else {
            prefs1.edit().putString("isUpdatedApp", "true").commit();
        }

        if (loginState.equals("true")) {
            MyApplication.currentUser.setUserFromPreferences(prefs);
            Intent tabIntent = new Intent(LauncherActivity.this, TabViewActivity.class);
            tabIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(tabIntent);
        } else {
            Intent mainIntent = new Intent(LauncherActivity.this, MainActivity.class);
            startActivity(mainIntent);
        }
        finish();
//            }
//        }, 2500);
    }
}
