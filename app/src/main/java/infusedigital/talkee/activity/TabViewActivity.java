package infusedigital.talkee.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.transition.Fade;
import android.transition.Transition;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.animation.CircleAngleAnimation;
import infusedigital.talkee.animation.Circle_Play;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.AddressBookData;
import infusedigital.talkee.datamodel.NotificationListData;
import infusedigital.talkee.fragment.TabMessageFragment;
import infusedigital.talkee.gcm.NotificationUtils;
import infusedigital.talkee.model.User;
import infusedigital.talkee.utilities.AlertDialogManager;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;
import infusedigital.talkee.view.ThemeColorView;

/**
 * Created by gstream on 5/23/2016.
 */

public class TabViewActivity extends BaseActivity /*implements SensorEventListener*/ {
    //    private MyPagerAdapter adapter;
    private Toolbar toolbar;
    public static TabViewActivity instance;

    public RelativeLayout rec_btn;

    static public RelativeLayout recordLayout;
    NotificationUtils notificationUtils;
    SharedPreferences prefs = null;

    String gcm_reg_id = "";
    String userId;

    static public MediaRecorder myAudioRecorder;
    private String outputFile = null;

    boolean isRecordCanceled = false;

    private ImageView stackBtn, pickerbtn, groupBtn;
    private ImageView stackBtn1, recordicon1, groupBtn1;


    int[] themeColorArray, alphaThemeColorArray, recordIconBgColorArray;
    private int currentTheme;
    ImageView recordIconView, recordIconView1;
    CircleImageView recordIconBg;

    public ArrayList<User> friendRequestData;
    static public ArrayList<NotificationListData> notificationList;
    public NotificationListData friendInfo = null;

    //    RelativeLayout expandView;
    private ProgressDialog loadingDialog;
    private int colorInt = 0, themecolor;
    private boolean isRecording;
    RelativeLayout relTopToolBarExpanded, relTopToolBar;
    private Bundle mBundle;
    private SwipeRefreshLayout swipeContainer;
    AlertDialogManager alert = new AlertDialogManager();
    private MediaPlayer mp;
    int duration = 0;
    private boolean isValid;
    private CollapsingToolbarLayout collapsingToolbar;
    private SensorManager mSensorManager;
    private Sensor mSensor;

//    public String getHashKey(Context context) {
//
//        PackageInfo info;
//        try {
//            info = context.getPackageManager().getPackageInfo("com.gingerapp", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md;
//                md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String something = new String(com.loopj.android.http.Base64.encode(md.digest(), 0));
//                //String something = new String(Base64.encodeBytes(md.digest()));
//                System.out.println("+++++++++++++++++++++ something:" + something);
////                Util.printLog("hash key : " + something);
//                return something;
//            }
//        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("name not found", e1.toString());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("no such an algorithm", e.toString());
//        } catch (Exception e) {
//            Log.e("exception", e.toString());
//        }
//        return "";
//    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View contentView = getLayoutInflater().inflate(R.layout.activity_tab_view, mFrameContent);

//        Transition fade = new Fade();
//        fade.excludeTarget(android.R.id.statusBarBackground, true);
//        fade.excludeTarget(android.R.id.navigationBarBackground, true);
//        getWindow().setExitTransition(fade);
//        getWindow().setEnterTransition(fade);

//        Picasso.with(getActivity())
//                .load(filterDiscoveryList.get(position).getProfileImageUrl())
//                .placeholder(getResources().getDrawable(R.drawable.default_logo_discover))
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
//                .error(getResources().getDrawable(R.drawable.default_logo_discover))
//                .into(holder.ivProfile);

        themeColorArray = getResources().getIntArray(R.array.themecolor);
        alphaThemeColorArray = getResources().getIntArray(R.array.alphathemecolor);
        recordIconBgColorArray = getResources().getIntArray(R.array.recordiconbg);

        SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        String userPhone = prefs1.getString("phonenum", "");
        System.out.println("++++++++++++++ current user phone:" + userPhone);
//        getHashKey(TabViewActivity.this);
//        setProximitySensor();

        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
        if (new File(path).exists()) {
            new File(path).delete();
            System.out.println("+++++++++++ on create file deleted");
        }

        System.out.println("++++++++++++ mbool:" + getResources().getString(R.string.mbool));

//        System.out.println("+++++++++++++++ 2 numbers compare:" + PhoneNumberUtils.compare("98325647582", "198325647582"));
//        PackageInfo info;
//        try {
//            info = getPackageManager().getPackageInfo(
//                    "infusedigital.talkee", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md;
//                md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String hash_key = new String(Base64.encode(md.digest(), 0));
//                System.out.println("+++++++++++ hash_key:" + hash_key);
//            }
//
//        } catch (PackageManager.NameNotFoundException e1) {
//        } catch (NoSuchAlgorithmException e) {
//        } catch (Exception e) {
//        }

        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        //set current theme color
        toolbar = (Toolbar) contentView.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams) toolbar.getLayoutParams();
        layoutParams.height = (int) getResources().getDimension(R.dimen.toolbar_size);
        toolbar.setLayoutParams(layoutParams);
        toolbar.requestLayout();

        currentTheme = prefs.getInt("currentThemeNum", 0);
        if (currentTheme >= themeColorArray.length)
            currentTheme = 0;
        themecolor = themeColorArray[currentTheme];

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setBackgroundColor(themecolor);

        relTopToolBar = (RelativeLayout) findViewById(R.id.relTopToolBar);
        relTopToolBarExpanded = (RelativeLayout) findViewById(R.id.relTopToolBarExpanded);
        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (!isRecording) {
                    if (verticalOffset == -toolbar.getHeight() + toolbar.getHeight()) {
//                        System.out.println("+++++++ exp");
                        toolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                        relTopToolBarExpanded.setBackgroundColor(themecolor);
                        relTopToolBarExpanded.setVisibility(View.VISIBLE);
                        toolbar.setVisibility(View.GONE);
                        relTopToolBar.setVisibility(View.GONE);
                        collapsingToolbar.setBackgroundColor(themecolor);
                    } else {
                        toolbar.setVisibility(View.VISIBLE);
                        collapsingToolbar.setBackgroundColor(getResources().getColor(android.R.color.white));
                        toolbar.setBackgroundColor(getResources().getColor(android.R.color.white));
//                        System.out.println("++++++ col");
                        relTopToolBarExpanded.setVisibility(View.GONE);
                        relTopToolBar.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

//        int themecolor = themeColorArray[currentTheme];
        toolbar.setBackgroundColor(themecolor);
        recordIconView = (ImageView) contentView.findViewById(R.id.recordicon);
        recordIconView1 = (ImageView) contentView.findViewById(R.id.recordicon1);
        recordIconBg = (CircleImageView) contentView.findViewById(R.id.recordiconbg);
        recordIconBg.setImageDrawable(new ColorDrawable(themecolor));
        int recordiconbgcolor = recordIconBgColorArray[currentTheme];
        recordIconBg.setImageDrawable(new ColorDrawable(recordiconbgcolor));
//        applyRecordIcon(currentTheme);

        main.setSelected(true);
//        channel.setSelected(false);
        addfriend.setSelected(false);
        set.setSelected(false);

        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        userId = prefs.getString("userId", "null");
        gcm_reg_id = prefs.getString("gcm_regid", "null");
        if (gcm_reg_id.equalsIgnoreCase("null"))
            gcm_reg_id = FirebaseInstanceId.getInstance().getToken();

        notificationUtils = new NotificationUtils(this);
        recordLayout = (RelativeLayout) findViewById(R.id.record_layout);

        changeStatusBarColor(getResources().getColor(R.color.statusbar));

        rec_btn = (RelativeLayout) findViewById(R.id.recBtn);
        stackBtn = (ImageView) findViewById(R.id.stackbtn);
        stackBtn1 = (ImageView) findViewById(R.id.stackbtn1);
        groupBtn = (ImageView) findViewById(R.id.groupbtn);
        groupBtn1 = (ImageView) findViewById(R.id.groupbtn1);
        groupBtn1.setColorFilter(getResources().getColor(android.R.color.black));

        groupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Coming soon.", Toast.LENGTH_LONG).show();
//                Intent chatIntent = new Intent(TabViewActivity.this, CreateGroup.class);
//                startActivity(chatIntent);
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        groupBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Coming soon.", Toast.LENGTH_LONG).show();
//                Intent chatIntent = new Intent(TabViewActivity.this, CreateGroup.class);
//                startActivity(chatIntent);
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
        }
        stackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                System.out.println("+++++ clicked");
                if (!isRecording) {
                    if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        drawerLayout.closeDrawers();
                    } else {
                        drawerLayout.openDrawer(Gravity.LEFT);
                    }
                }
            }
        });

        stackBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRecording) {
                    if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        drawerLayout.closeDrawers();
                    } else {
                        drawerLayout.openDrawer(Gravity.LEFT);
                    }
                }
            }
        });

        rec_btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startProgress();
                        record_start();
                        break;

                    case MotionEvent.ACTION_UP:
                        record_stop();
                        break;

                    case MotionEvent.ACTION_CANCEL:
                        record_stop();
                        break;

//                    case MotionEvent.ACTION_MOVE:
//                        System.out.println("++++++++++++ ACTION_MOVE");
//                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//                            System.out.println("++++++++++++ ACTION_MOVE apply if");
//                            record_stop();
//                        } else {
////                            System.out.println("++++++++++++ ACTION_MOVE apply else");
////                            startProgress();
////                            record_start();
//                        }
//                        break;
                }
                return true;
            }
        });

        recordIconView1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
                        if (new File(path).exists()) {
                            boolean op = new File(path).delete();
                            System.out.println("+++++++++++ on click file deleted");
                        }
                        startProgress();
                        recordIconView1.setImageDrawable(getResources().getDrawable(R.drawable.recordgreen));
                        record_start();
                        break;
                    case MotionEvent.ACTION_UP:
                        recordIconView1.setImageDrawable(getResources().getDrawable(R.drawable.recordblack));
                        record_stop();
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        recordIconView1.setImageDrawable(getResources().getDrawable(R.drawable.recordblack));
                        record_stop();
                        break;
                }
                return true;
            }
        });

        pickerbtn = (ImageView) findViewById(R.id.pickerbtn);
        pickerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorTheme();
            }
        });

        TabMessageFragment.mAdapter = null;
//        expandView = (RelativeLayout) findViewById(R.id.expandview);
//        expandView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startExpandMainActivity();
//            }
//        });

        notificationList = new ArrayList<>();
        friendRequestData = new ArrayList<>();
        instance = this;

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                AppConfig.isRefresh = true;
//                System.out.println("++++++++ pull refresh");
//                TabMessageFragment.mAdapter = null;
//                Fragment mFragment = new TabMessageFragment();
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.replace(R.id.message_fragment, mFragment);
//                ft.commit();

//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                TabMessageFragment mTabMessageFragment = new TabMessageFragment();
//                fragmentTransaction.add(R.id.llFragement, mTabMessageFragment, "TabMessageFragment");
//                fragmentTransaction.commit();

                refreshActivity();

//                System.out.println("++++++++ pull refresh done replace");
                if (swipeContainer.isRefreshing())
                    swipeContainer.setRefreshing(false);
            }
        });

        if (AppConfig.addressBookDatas.size() == 0)
            new GetContactsTask().execute();

//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        TabMessageFragment mTabMessageFragment = new TabMessageFragment();
//        fragmentTransaction.add(R.id.llFragement, mTabMessageFragment, "TabMessageFragment");
//        fragmentTransaction.commit();

    }

//    private void setProximitySensor() {
//        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
//        /*mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        mAudioManager.setMode(AudioManager.STREAM_MUSIC);*/
//
//        if (mSensor == null) {
//            Toast.makeText(getApplicationContext(), "No Proximity Sensor!", Toast.LENGTH_SHORT).show();
//        }
//    }

    private void startProgress() {
        recordLayout.setVisibility(View.VISIBLE);
        isRecordCanceled = false;
        Circle_Play circle = (Circle_Play) findViewById(R.id.circle);

        changeStatusBarColor(Color.parseColor("#e4215d"));
        circle.setColor(Color.parseColor("#ffffff"));

        CircleAngleAnimation.recordingProgress(circle, 10500);
        //recorded output file
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.m4a";
    }

    private void refreshActivity() {
        Activity a = TabViewActivity.this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            a.recreate();
        } else {
            final Intent intent = a.getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            a.finish();
            a.overridePendingTransition(0, 0);
            a.startActivity(intent);
            a.overridePendingTransition(0, 0);
        }
    }

    /**
     * Refresh while pull down
     */
    public static void refreshFragment() {
        AppConfig.isRefresh = false;
        TabMessageFragment.mAdapter.notifyDataSetChanged();
//        Fragment mFragment = new TabMessageFragment();
//        FragmentTransaction ft = getInstance().getFragmentManager().beginTransaction();
//        ft.replace(R.id.message_fragment, mFragment);
//        ft.commit();
    }

    public static TabViewActivity getInstance() {
        return instance;
    }

    private void colorTheme() {
        final Dialog colorPicker = new Dialog(this);
        colorPicker.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Window window = colorPicker.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM | Gravity.FILL_HORIZONTAL;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        colorPicker.requestWindowFeature(Window.FEATURE_NO_TITLE);
        colorPicker.setContentView(R.layout.theme_color_picker);

        LinearLayout parent = (LinearLayout) colorPicker.findViewById(R.id.themecolorsview);

        for (int i = 0; i < parent.getChildCount(); i++) {
            ThemeColorView colorView = (ThemeColorView) parent.getChildAt(i);
            colorView.setColor(themeColorArray[i], alphaThemeColorArray[i]);

            if (currentTheme == i) {
                colorView.setSelectState(true);
                colorView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        colorPicker.dismiss();
                    }
                });
            } else {
                colorView.setSelectState(false);
                final int finalI = i;
                colorView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        colorPicker.dismiss();
                        //save current theme
//                        prefs.edit().putInt("currentThemeNum", finalI).commit();
                        currentTheme = finalI;
                        colorInt = finalI;
                        //change current activity theme
//                        toolbar.setBackgroundColor(themeColorArray[finalI]);
                        toolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                        recordIconBg.setImageDrawable(new ColorDrawable(recordIconBgColorArray[finalI]));
                        relTopToolBarExpanded.setBackgroundColor(themeColorArray[finalI]);
                        themecolor = themeColorArray[finalI];

//                        System.out.println("++++++ colorInt " + colorInt + ":::" + finalI);
                        saveColor(colorInt);
//                        applyRecordIcon(currentTheme);
                    }
                });
            }
            colorView.invalidate();
        }
        colorPicker.show();
    }

    /**
     * save color API
     *
     * @param colorPos
     */
    private void saveColor(int colorPos) {
        String color = "#27dbd9";
        if (colorPos == 0)
            color = "#750cc6";
        else if (colorPos == 1)
            color = "#8dc70c";
        else if (colorPos == 2)
            color = "#00d3bd";
        else if (colorPos == 3)
            color = "#0c74c7";
        else if (colorPos == 4)
            color = "#e48c11";
        else if (colorPos == 5)
            color = "#e4c611";

        RequestParams params = new RequestParams();
        params.put("user_id", userId);
        params.put("colourcode", color);
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostColorChangeCallComplete, WSMethods.BASE_URL + WSMethods.CHANGE_COLORCODE_POST, params);
    }

    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostColorChangeCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    prefs.edit().putInt("currentThemeNum", colorInt).commit();
                    if (currentTheme >= themeColorArray.length)
                        currentTheme = 0;
                    themecolor = themeColorArray[currentTheme];
                    relTopToolBarExpanded.setBackgroundColor(themecolor);
//                    refreshActivity();
//                    onCreate(mBundle);
//                    finish();
//                    Intent mIntent = new Intent(TabViewActivity.this, TabViewActivity.class);
//                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                    startActivity(mIntent);
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

//    private void applyRecordIcon(int i){
//        switch (i) {
//            case 0:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon2));
//                break;
//            case 1:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon_black));
//                break;
//            case 2:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon_green));
//                break;
//            case 3:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon_lightblue));
//                break;
//            case 4:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon_navy));
//                break;
//            case 5:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon_orange));
//                break;
//            case 6:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon_pink));
//                break;
//            case 7:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon_teal));
//                break;
//            case 8:
//                recordIconView.setImageDrawable(getResources().getDrawable(R.drawable.record_icon_yellow));
//                break;
//
//            default:
//                break;
//        }
//    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    /**
     * Starts recording audio
     */
    private void record_start() {
        isRecording = true;
        groupBtn.setClickable(false);
        pickerbtn.setClickable(false);

//        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
//        if (new File(path).exists()) {
//            new File(path).delete();
//            System.out.println("+++++++++++ on start record file deleted");
//        }

        playSound("record");
        Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (myAudioRecorder != null) {
                    try {
                        myAudioRecorder.stop();
                        myAudioRecorder.release();
                        myAudioRecorder = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                myAudioRecorder = new MediaRecorder();
                myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
                myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                myAudioRecorder.setOutputFile(outputFile);

                try {
                    myAudioRecorder.prepare();
                    myAudioRecorder.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        mHandler.sendEmptyMessageDelayed(0, 300);
    }

    /**
     * Audio recoding stop
     */
    public void record_stop() {
        isRecording = false;
        System.out.println("++++++ stop called");
        CircleAngleAnimation.cancelProgressAnimation();

        if (isRecordCanceled)
            return;

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
                boolean valid = false;
                try {
                    MediaPlayer mpPlay = new MediaPlayer();
                    mpPlay.setDataSource(path);
                    mpPlay.prepare();
                    int duration = mpPlay.getDuration();
                    System.out.println("++++++++++++++++ duration:" + duration);
                    if (duration < 1000) {
                        alert.showAlertDialog(TabViewActivity.this,
                                "Recording Error",
                                "Please record again. You must record more than 1 sec.", false);
                    } else {
                        valid = true;
                    }
                    if (valid) {
                        Intent nextIntent = new Intent(getApplicationContext(), SendViewActivity.class);
                        //        startActivity(nextIntent);
                        startActivityForResult(nextIntent, 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);

//        Intent nextIntent = new Intent(getApplicationContext(), SendViewActivity.class);
////        startActivity(nextIntent);
//        startActivityForResult(nextIntent, 1);
        isRecordCanceled = true;

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                try {
                    if (myAudioRecorder == null)
                        return;

                    myAudioRecorder.stop();
                    myAudioRecorder.release();
                    myAudioRecorder = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.sendEmptyMessageDelayed(1, 200);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_FASTEST);

        if (notificationList != null && notificationList.size() > 0) {
            notificationList.clear();
        }

        recordLayout.setVisibility(View.GONE);

        if (friendRequestData != null && friendRequestData.size() > 0) {
            friendRequestData.clear();
        }

        if (SendViewActivity.sendview == 1) {
            SendViewActivity.sendview = 0;
        }

        changeStatusBarColor(Color.parseColor("#750cc6"));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {

                } else {
                    Toast.makeText(getApplicationContext(), "Record audio permission denied. You couldnt send your voice message", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("+++++++++++++++++++++++++++++ onActivityResult:" + requestCode + ":" + resultCode);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
                if (new File(path).exists()) {
                    new File(path).delete();
                    System.out.println("+++++++++++ on onActivityResult file deleted");
                }

                finish();
                startActivity(getIntent());
//                Toast.makeText(context, "back to home.", Toast.LENGTH_LONG).show();
//                Fragment mFragment = new TabMessageFragment();
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.replace(R.id.message_fragment, mFragment);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // Playing sound

    public void playSound(String name) {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/" + name);

            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

//    @Override
//    public void onSensorChanged(SensorEvent sensorEvent) {
////        Log.e("distance", "++++++++++++++++++++ " + String.valueOf(sensorEvent.values[0]));
////        Log.e("MaximumRange", "++++++++++++++++++++ " + String.valueOf(mSensor.getMaximumRange()));
//
//        if (sensorEvent.values[0] < mSensor.getMaximumRange()) {
////            mAudioManager.setSpeakerphoneOn(false);
//            Log.e("distance", "++++++++++++++++++++ " + "setSpeakerphoneOn : false");
//        } else {
////            mAudioManager.setSpeakerphoneOn(true);
//            Log.e("distance", "++++++++++++++++++++ " + "setSpeakerphoneOn : true");
//        }
//    }
//
//    @Override
//    public void onAccuracyChanged(Sensor sensor, int i) {
//
//    }

//    /**
//     * Call of unBlock req
//     */
//    public void blockTheFriend(String friend_id) {
//        RequestParams params = new RequestParams();
//        params.put("user_id", AppConfig.getUserId(context));
//        params.put("friend_id", friend_id);
//        params.put("block_flag", "1");
//        startLoading();
//        new MyLoopJPost(context, "", onLoopJPostBlockFriendCallComplete, WSMethods.BASE_URL + WSMethods.BLOCK_UNBLOCK_POST, params);
//    }
//
//    /**
//     * API parsing of unBlock req
//     */
//    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostBlockFriendCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {
//
//        @Override
//        public void response(String result) {
//            try {
//                endLoading();
//                JSONObject jobj = new JSONObject(result);
//                String flag = jobj.getString("FLAG");
//                if (flag.equalsIgnoreCase("true")) {
//                    finish();
//                    startActivity(getIntent());
//                } else {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    };
//
//    /**
//     * Call of unFriend req
//     */
//    public void unFriendCall(String friend_id) {
//        RequestParams params = new RequestParams();
//        params.put("user_id", AppConfig.getUserId(context));
//        params.put("friend_id", friend_id);
//        startLoading();
//        new MyLoopJPost(context, "", onLoopJPostUnFriendCallComplete, WSMethods.BASE_URL + WSMethods.UNFRIEND_POST, params);
//    }
//
//    /**
//     * API parsing of unFriend req
//     */
//    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostUnFriendCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {
//
//        @Override
//        public void response(String result) {
//            try {
//                endLoading();
//                JSONObject jobj = new JSONObject(result);
//                String flag = jobj.getString("FLAG");
//                if (flag.equalsIgnoreCase("true")) {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                    finish();
//                    startActivity(getIntent());
//                } else {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    };

    /**
     * Featch all the contacts in backgr
     */
    class GetContactsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getAddressBookInfos();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void getAddressBookInfos() {
        AppConfig.addressBookDatas.clear();

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String PHOTO_URI = ContactsContract.CommonDataKinds.Phone.PHOTO_URI;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String PHONE_TYPE = ContactsContract.CommonDataKinds.Phone.TYPE;
        int PHONE_TYPEMOBILE = ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Uri photo_uri;
//                Bitmap photoBmp;
                String phone = null;
                String name = "";

                String id = cursor.getString(cursor.getColumnIndex(_ID));
                name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                String url = cursor.getString(cursor.getColumnIndex(PHOTO_URI));
                if (url != null)
                    photo_uri = Uri.parse(url);
                else
                    photo_uri = null;

                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER))) > 0) {
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, new String[]{PHONE_NUMBER},
                            PHONE_CONTACT_ID + " = ? AND " + PHONE_TYPE + " = " + PHONE_TYPEMOBILE, new String[]{id}, null);

                    while (phoneCursor.moveToNext()) {
                        phone = phoneCursor.getString(phoneCursor.getColumnIndex(PHONE_NUMBER));
                        if (phone.trim().length() > 0)
                            phone = getOnlyDigits(phone);
                        System.out.println("++++++++ phone in register code activity:" + name + "::" + phone + ":::" + getOnlyDigits(phone));
                        AppConfig.arrAllContacts.add(getOnlyDigits(phone));
                    }

//                    if ((phone != null || phone.equals("null")) && phone.trim().length() > 0) {
//                        addressBookDatas.add(new AddressBookData(photo_uri, name, getOnlyDigits(phone), false));
//                    } else {
                    AppConfig.addressBookDatas.add(new AddressBookData(photo_uri, name, phone, false));
//                    }
//                    System.out.println("++++++++ phone:" + name + "::" + phone);

                    phoneCursor.close();
                }

                //TODO for sord array as alphabetical order
                Collections.sort(AppConfig.addressBookDatas, new Comparator<AddressBookData>() {
                    public int compare(AddressBookData p1, AddressBookData p2) {
                        return p1.getName().compareTo(p2.getName());
                    }
                });
            }
        }
    }

    /**
     * Get all numbers
     *
     * @param s
     * @return
     */
    public static String getOnlyDigits(String s) {
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }
}