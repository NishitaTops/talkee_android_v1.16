package infusedigital.talkee.activity;

import android.graphics.Typeface;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.File;

import infusedigital.talkee.R;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.helper.MyPreferenceManager;
import infusedigital.talkee.model.User;
import infusedigital.talkee.view.UserBlankAvatar;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Super on 8/24/2016.
 */
public class MyApplication extends MultiDexApplication {

    public static final String TAG = MyApplication.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private Tracker mTracker;
    private FirebaseAnalytics firebaseAnalytics;


    public static MyApplication instance;
    public static User currentUser;
    private MyPreferenceManager pref;
    public static Typeface semiboldFont, lightFont, regularFont, boldFont;
    int[] userBlankColors;

    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());
        if (!FirebaseApp.getApps(this).isEmpty()) {
            FacebookSdk.sdkInitialize(getApplicationContext());
        }
        AppEventsLogger.activateApp(this);
//        Fabric.with(getApplicationContext(), new Crashlytics());

        //TODO client's Fabric
        TwitterAuthConfig authConfig = new TwitterAuthConfig("lVJOWL2CckrqTGobWMJBazjHE", "zrlWyEoH7fw66CVp94ZyPPolldHq9kyVvHbeI9MDHfsKEhoapl");
        //TODO our Fabric
//        TwitterAuthConfig authConfig = new TwitterAuthConfig("FyqRZJKxt3nlvJiyv0XtVBoUD", "x8NQZ5t3gvHKbuHEgoRwG8ssd4UZXQkIR28S3JC7FiyF0nl9ut");
        Fabric.with(getApplicationContext(), new Twitter(authConfig), new Crashlytics());

        // By suggestion from client
//        Fabric fabric = new Fabric.Builder(this)
//                .kits(new Crashlytics())
//                .debuggable(true)
//                .build();
//        fabric.with(getApplicationContext(), new Crashlytics());

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setMinimumSessionDuration(100000);

        initFolder();
        currentUser = new User();
//        TwitterAuthConfig authConfig = new TwitterAuthConfig("lVJOWL2CckrqTGobWMJBazjHE", "zrlWyEoH7fw66CVp94ZyPPolldHq9kyVvHbeI9MDHfsKEhoapl");
//        Fabric.with(this, new Twitter(authConfig));
        semiboldFont = Typeface.createFromAsset(getAssets(), "fonts/myriadpro-semibold.otf");
        lightFont = Typeface.createFromAsset(getAssets(), "fonts/myriadpro-light.otf");
        regularFont = Typeface.createFromAsset(getAssets(), "fonts/myriadpro-regular.otf");
        boldFont = Typeface.createFromAsset(getAssets(), "fonts/myriadpro-bold.otf");
        userBlankColors = getResources().getIntArray(R.array.userblankavatar);

        instance = this;
    }

    private void initFolder() {
        String root = getExternalFilesDir(null).getAbsolutePath();

        File tmpDir = new File(root + File.separator + Config.MESSAGE_DIR);
        if (!tmpDir.exists()) {
            tmpDir.mkdir();
        }

        File groupDir = new File(root + File.separator + Config.GROUP_DIR);
        if (!groupDir.exists())
            groupDir.mkdir();
    }

    public void setBlankAvatar(UserBlankAvatar avatar, String name, boolean isCircle) {
        if (TextUtils.isEmpty(name))
            return;

        char firstChar = name.charAt(0);
        boolean isDigit = ((firstChar >= '0') && (firstChar <= '9'));

        if (isDigit) {
            //this is black bg
            avatar.setColor(userBlankColors[userBlankColors.length - 1]);
            avatar.setName("#");
        } else {
            //get first char position with alphabet
            String upperName = name.toUpperCase();

            char first = upperName.charAt(0);

            int position = first - 'A';

            if (position < 0 || position > userBlankColors.length - 1) {
                position = userBlankColors.length - 1;
                first = '#';
            }

            avatar.setIsCircle(isCircle);
            avatar.setName(String.valueOf(first));
            avatar.setColor(userBlankColors[position]);
        }

    }

    public String getMessageDir() {
        String messageUrl = getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator;
        return messageUrl;
    }

    public String getGroupMessageDir() {
        String messageUrl = getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.GROUP_DIR + File.separator;
        return messageUrl;
    }

    public MyPreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new MyPreferenceManager(this);
        }

        return pref;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setShouldCache(false);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.app_tracker);
            mTracker.enableExceptionReporting(true);
            mTracker.enableAutoActivityTracking(true);
        }

        return mTracker;
    }

    public FirebaseAnalytics getFirebaseAnalytics() {
        return firebaseAnalytics;
    }
}