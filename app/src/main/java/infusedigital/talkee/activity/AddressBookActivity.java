package infusedigital.talkee.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import infusedigital.talkee.R;
import infusedigital.talkee.adapters.AddFriendsFromAddressbookAdapter;
import infusedigital.talkee.adapters.AddFriendsFromContactAdapter;
import infusedigital.talkee.adapters.AddressBookListAdapter;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.AddressBookData;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;

public class AddressBookActivity extends AppCompatActivity {

    private ImageView backBtn;
    //    private AddressBookListAdapter adapter;
    private AddFriendsFromAddressbookAdapter adapter;
    private ListView contactListView;
    private TextView addressbookTitle, tvEmptyView;
    private RelativeLayout toolbarlayout;

//    private ArrayList<AddressBookData> addressBookDatas;

    public static final String SMS_SENT_ACTION = "com.andriodgifts.gift.SMS_SENT_ACTION";
    public static final String SMS_DELIVERED_ACTION = "com.andriodgifts.gift.SMS_DELIVERED_ACTION";
    public boolean isSimAvailable = true;
    ProgressBar mProgressBar;
    Context mContext;
    //    private ArrayList<AddressBookData> margeAddressBookData;
    public boolean isLoading;
    String mfriendId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_address_book);
        mContext = AddressBookActivity.this;

        backBtn = (ImageView) findViewById(R.id.addressbook_backbtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addressbookTitle = (TextView) findViewById(R.id.addressbook_title);
        tvEmptyView = (TextView) findViewById(R.id.tvEmptyView);
        addressbookTitle.setTypeface(MyApplication.boldFont);
        tvEmptyView.setTypeface(MyApplication.boldFont);

        toolbarlayout = (RelativeLayout) findViewById(R.id.toolbarlayout);
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        int currentTheme = prefs.getInt("currentThemeNum", 0);
        int[] themeColorArray = getResources().getIntArray(R.array.themecolor);
        int themecolor = themeColorArray[currentTheme];
//        toolbarlayout.setBackgroundColor(themecolor);

        contactListView = (ListView) findViewById(R.id.contactlistview);
        contactListView.setDivider(null);
//        addressBookDatas = new ArrayList<>();
//        adapter = new AddressBookListAdapter(AddressBookActivity.this, addressBookDatas);
//        margeAddressBookData = new ArrayList<>();
        adapter = new AddFriendsFromAddressbookAdapter(AddressBookActivity.this, AppConfig.margeAddressBookData);
        contactListView.setAdapter(adapter);
        mProgressBar = (ProgressBar) findViewById(R.id.mProgressBar);

//        new GetContactsTask().execute();
        if (AppConfig.arrAllContacts.size() > 0 && AppConfig.addressBookDatas.size() > 0) {
            mProgressBar.setVisibility(View.VISIBLE);
            syncContacts();
        } else {
            new GetContactsTask().execute();
        }

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = null;
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        message = "Message Sent Successfully !";
                        Log.e("Super", "Message result = " + message);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        message = "Error.";
                        Log.e("Super", "Message result = " + message);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        message = "Error: No service.";
                        Log.e("Super", "Message result = " + message);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        message = "Error: Null PDU.";
                        Log.e("Super", "Message result = " + message);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        message = "Error: Radio off.";
                        Log.e("Super", "Message result = " + message);
                        break;

                }
            }
        }, new IntentFilter(SMS_SENT_ACTION));

        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                isSimAvailable = false;
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                break;
            case TelephonyManager.SIM_STATE_READY:
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                isSimAvailable = false;
                break;
        }

        System.out.println("+++++++++++ sim available:" + isSimAvailable);
    }

    /**
     * Sync contact api call
     */
    private void syncContacts() {
        System.out.println("++++++++++++ sync api call");
        RequestParams params = new RequestParams();

        if (AppConfig.arrAllContacts.size() > 0 && AppConfig.addressBookDatas.size() > 0)
            params.put("sync", getJsonContacts(AppConfig.arrAllContacts));
        else
            params.put("sync", getJsonContacts(AppConfig.arrAllContacts));
//        params.put("sync", "{\"sync\":[{\"phonenum\":\"9408784196\"}]}");

//        startLoading();
        new MyLoopJPost(mContext, "", onLoopJPostSyncContactsCallComplete, WSMethods.BASE_URL + WSMethods.SYNC_CONT_NUMBER_POST + AppConfig.getUserId(mContext), params);
    }

    /**
     * get all contacts in json for sync
     *
     * @return
     */
    public String getJsonContacts(ArrayList<String> arrAllContacts) {
        if (arrAllContacts.size() > 0) {
            Set<String> hs = new HashSet<>();
            hs.addAll(arrAllContacts);
            arrAllContacts.clear();
            arrAllContacts.addAll(hs);

            JSONObject obj_json = new JSONObject();
            JSONArray obj_arr = new JSONArray();
            for (int i = 0; i < arrAllContacts.size(); i++) {
                try {
                    if (getOnlyDigits(arrAllContacts.get(i)).length() >= 5) {
                        JSONObject obj = new JSONObject();
                        obj.put("phonenum", getOnlyDigits(arrAllContacts.get(i)));
                        System.out.println("+++++++++++++++ send to api:" + getOnlyDigits(arrAllContacts.get(i)));
//                    System.out.println("+++++++++++++++++ contact:" + i + ":::" + arrAllContacts.get(i) + ":::" + arrAllContacts.get(i).replaceAll("[\\\\-\\\\+\\\\.\\\\^:,\\ \\(\\)\\-]", ""));
                        obj_arr.put(obj);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println("+++++++++++++++ final array:" + obj_arr);
            try {
                obj_json.put("sync", obj_arr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//                params.put("sync", obj_json.toString());
            return obj_json.toString();
        }
        return "";
    }


    /**
     * Get all numbers
     *
     * @param s
     * @return
     */
    public static String getOnlyDigits(String s) {
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }

    /**
     * API parsing of sync api call
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSyncContactsCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
//                endLoading();
                System.out.println("+++++++++++ response:" + result);
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    JSONArray USER_DETAILS = jobj.getJSONArray("USER_DETAILS");
//                    goToHomeScreen();
                    ArrayList<String> contacts = new ArrayList<>();
                    contacts.clear();
                    AppConfig.margeAddressBookData.clear();
                    if (USER_DETAILS != null)
                        for (int j = 0; j < USER_DETAILS.length(); j++) {
                            JSONObject aData = USER_DETAILS.getJSONObject(j);
                            System.out.println("+++++++++ add from api:" + Uri.parse("") + ":::" + aData.getString("name") + ":::" + aData.optString("phonenum") + ":::" + false + ":::" + aData.getString("userId") + ":::" + aData.getString("username") + ":::" + aData.getString("photo_url"));
                            AddressBookData abData = new AddressBookData();
                            abData.setPhotoUri(Uri.parse(""));
                            abData.setName(aData.getString("name"));
                            abData.setPhonenum(aData.optString("phonenum"));
                            abData.setUserId(aData.getString("userId"));
                            abData.setUsername(aData.getString("username"));
                            abData.setPhoto_url(aData.getString("photo_url"));
                            abData.setIsfollow(aData.getString("isfollow"));

                            if (aData.getString("isfollow").equals("0") || aData.getString("isfollow").equals("1")
                                    || aData.getString("isfollow").equals("3"))
                                abData.setSent(false);
                            else
                                abData.setSent(true);

                            //TODO add only for add and sent status
                            if (aData.getString("isfollow").equals("0") || aData.getString("isfollow").equals("2"))
                                AppConfig.margeAddressBookData.add(abData);

                            if (aData.optString("phonenum").length() > 10) {
                                contacts.add(aData.optString("phonenum").substring(aData.optString("phonenum").length() - 11));
                                contacts.add(aData.optString("phonenum").substring(aData.optString("phonenum").length() - 10));
                                contacts.add(aData.optString("phonenum"));
                            } else {
                                contacts.add(aData.optString("phonenum"));
                            }
//                        margeAddressBookData.add(new AddressBookData(Uri.parse(""), aData.getString("name"), aData.getString("phonenum"), false, aData.getString("userId"), aData.getString("username"), aData.getString("photo_url")));
                        }

                    SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
                    String userPhone = prefs1.getString("phonenum", "");

                    for (int i = 0; i < AppConfig.addressBookDatas.size(); i++) {
                        boolean isAdd = false;
                        AddressBookData data = AppConfig.addressBookDatas.get(i);
                        System.out.println("++++++++++++++++++ final " + i + ":" + data.getPhonenum());
                        if (data.getPhonenum() != null && !contacts.contains(data.getPhonenum())) {
                            System.out.println("++++++++++++++++++ added");
                            if (userPhone.length() == 0) {
                                isAdd = true;
                            } else if (userPhone.length() > 0 && !userPhone.contains(data.getPhonenum())) {
                                isAdd = true;
                            } else {
                                isAdd = false;
                            }
                            if (isAdd) {
                                contacts.add(data.getPhonenum());
                                AppConfig.margeAddressBookData.add(new AddressBookData(data.getPhotoUri(), data.getName(), data.getPhonenum(), false, "", "", "", "0"));
                            }
                        } else if (data.getPhonenum() != null && contacts.contains(data.getPhonenum())) {
                            System.out.println("+++++++++++ contains:" + data.getPhonenum() + "::" + data.getName());
                            for (int j = 0; j < AppConfig.margeAddressBookData.size(); j++) {
//                                if (AppConfig.margeAddressBookData.get(j).getPhonenum().substring(AppConfig.margeAddressBookData.get(j).getPhonenum().length() - data.getPhonenum().length()).equals(data.getPhonenum())) {

                                if (AppConfig.margeAddressBookData.get(j).getPhonenum().equals(data.getPhonenum())) {
                                    AppConfig.margeAddressBookData.get(j).setName(data.getName());
                                } else if (AppConfig.margeAddressBookData.get(j).getPhonenum().length() > data.getPhonenum().length()) {
                                    if (AppConfig.margeAddressBookData.get(j).getPhonenum().substring(AppConfig.margeAddressBookData.get(j).getPhonenum().length() - data.getPhonenum().length()).equals(data.getPhonenum()))
                                        AppConfig.margeAddressBookData.get(j).setName(data.getName());
                                }
                            }
                        }
                    }

//                    for (int i = 0; i < AppConfig.addressBookDatas.size(); i++) {
//                        AddressBookData data = AppConfig.addressBookDatas.get(i);
////                        System.out.println("++++++++++++++++++ final " + i + ":" + data.getPhonenum());
//                        if (!contacts.contains(data.getPhonenum())) {
////                            System.out.println("++++++++++++++++++ added");
//                            contacts.add(data.getPhonenum());
//                            AppConfig.margeAddressBookData.add(new AddressBookData(data.getPhotoUri(), data.getName(), data.getPhonenum(), false, "", "", "", "0"));
//                        }
//                    }

//                    margeAddressBookData = removeDuplicates(AppConfig.addressBookDatas);
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                }

                if (mProgressBar != null && mProgressBar.getVisibility() == View.VISIBLE)
                    mProgressBar.setVisibility(View.GONE);

                System.out.println("++++++++++++ arrAllContacts synced:" + AppConfig.arrAllContacts.size());
                System.out.println("++++++++++++ addressBookDatas:" + AppConfig.addressBookDatas.size());
                if (AppConfig.margeAddressBookData.size() > 0) {
                    adapter.setData(AppConfig.margeAddressBookData);
                    adapter.notifyDataSetChanged();
                    tvEmptyView.setVisibility(View.GONE);
                } else {
                    tvEmptyView.setVisibility(View.VISIBLE);
                    contactListView.setEmptyView(tvEmptyView);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    class GetContactsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getAddressBookInfos();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressBar.setVisibility(View.GONE);
            if (AppConfig.addressBookDatas.size() > 0) {
                adapter.setData(AppConfig.addressBookDatas);
                adapter.notifyDataSetChanged();
                tvEmptyView.setVisibility(View.GONE);
            } else {
                tvEmptyView.setVisibility(View.VISIBLE);
                contactListView.setEmptyView(tvEmptyView);
            }
        }
    }

    public void sendInvite(int position) {
        System.out.println("++++++++++++++ position:" + position);
        if (!isSimAvailable) {
            Toast.makeText(mContext, "Sim in not available!", Toast.LENGTH_LONG).show();
            return;
        }

        String phoneNum = AppConfig.margeAddressBookData.get(position).phonenum;
        //Check if the phoneNumber is empty
        if (phoneNum.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Enter a Phone Number", Toast.LENGTH_SHORT).show();
        } else {
            sendSMS(phoneNum, position);//, smsBody);
        }

//        addressBookDatas.get(position).isSent = true;
//        adapter.setData(AppConfig.addressBookDatas);
//        adapter.notifyDataSetChanged();
    }

    private void sendSMS(String phoneNumber, int position) {//}, String smsMessage){
        System.out.println("++++++++++++++ phoneNumber:" + phoneNumber);
        SmsManager sms = SmsManager.getDefault();
        String detaileBody = getResources().getString(R.string.invitestring).toString();
        String urlBody = "";
//        String urlBody = getResources().getString(R.string.appurl).toString();

        List<String> messages = sms.divideMessage(detaileBody);
        for (String message : messages) {
            System.out.println("++++++++++++++ sent1:");
            sms.sendTextMessage(phoneNumber, null, message, PendingIntent.getBroadcast(
                    this, 0, new Intent(SMS_SENT_ACTION), 0), PendingIntent.getBroadcast(this, 0, new Intent(SMS_DELIVERED_ACTION), 0));
        }
        System.out.println("++++++++++++++ sent2:");

//        sms.sendTextMessage(phoneNumber, null, urlBody, PendingIntent.getBroadcast(
//                this, 0, new Intent(SMS_SENT_ACTION), 0), PendingIntent.getBroadcast(this, 0, new Intent(SMS_DELIVERED_ACTION), 0));
        System.out.println("++++++++++++++ sent3:");
        AppConfig.margeAddressBookData.get(position).isSent = true;
        adapter.setData(AppConfig.margeAddressBookData);
        adapter.notifyDataSetChanged();
    }

    private void getAddressBookInfos() {
        if (AppConfig.addressBookDatas != null)
            AppConfig.addressBookDatas.clear();

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String PHOTO_URI = ContactsContract.CommonDataKinds.Phone.PHOTO_URI;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String PHONE_TYPE = ContactsContract.CommonDataKinds.Phone.TYPE;
        int PHONE_TYPEMOBILE = ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Uri photo_uri;
//                Bitmap photoBmp;
                String phone = null;
                String name = "";

                String id = cursor.getString(cursor.getColumnIndex(_ID));
                name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                String url = cursor.getString(cursor.getColumnIndex(PHOTO_URI));
                if (url != null)
                    photo_uri = Uri.parse(url);
                else
                    photo_uri = null;

                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER))) > 0) {
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, new String[]{PHONE_NUMBER},
                            PHONE_CONTACT_ID + " = ? AND " + PHONE_TYPE + " = " + PHONE_TYPEMOBILE, new String[]{id}, null);

                    while (phoneCursor.moveToNext()) {
                        phone = phoneCursor.getString(phoneCursor.getColumnIndex(PHONE_NUMBER));
                    }

                    AppConfig.addressBookDatas.add(new AddressBookData(photo_uri, name, phone, false));

                    phoneCursor.close();
                }

                //TODO for sord array as alphabetical order
                Collections.sort(AppConfig.addressBookDatas, new Comparator<AddressBookData>() {
                    public int compare(AddressBookData p1, AddressBookData p2) {
                        return p1.getName().compareTo(p2.getName());
                    }
                });

//                if (photo_uri != null){
//                    try{
//                        photoBmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(),Uri.parse(photo_uri));
//                    }catch (FileNotFoundException e){
//                        e.printStackTrace();
//                    }catch (IOException e){
//                        e.printStackTrace();
//                    }
//                }
            }

//            if (addressBookDatas.size() > 0) {
//                adapter.setData(addressBookDatas);
//                adapter.notifyDataSetChanged();
//                tvEmptyView.setVisibility(View.GONE);
//            } else {
//                tvEmptyView.setVisibility(View.VISIBLE);
//                contactListView.setEmptyView(tvEmptyView);
//            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.in_slide_left_right, R.anim.fade_out);
    }

    /**
     * Get friends list
     */
    public void sendRequest(String friendId, String userId) {
        mfriendId = friendId;
        RequestParams params = new RequestParams();
        params.put("user_id", userId);
//        params.put("user_id", "240");
        params.put("reciver_id", friendId);
//        startLoading();
        isLoading = true;
        new MyLoopJPost(mContext, "", onLoopJPostSendFriendReqCallComplete, WSMethods.BASE_URL + WSMethods.SEND_FRIEND_REQ_POST, params);
    }

    private String query = "";
    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSendFriendReqCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
//                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    for (int i = 0; i < AppConfig.margeAddressBookData.size(); i++) {
                        if (AppConfig.margeAddressBookData.get(i).getUserId().equals(mfriendId))
                            AppConfig.margeAddressBookData.get(i).setSent(true);
                    }
                    adapter.notifyDataSetChanged();
//                    page = 1;
//                    getFriendsList(query);
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(mContext);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Accept/Decline request received
     *
     * @param sender_id
     */
    public void ActionOnRequest(final String sender_id, String userId) {
        RequestParams params = new RequestParams();
        params.put("user_id", userId);
        params.put("requestflag", "1");
        params.put("sender_id", sender_id);
        isLoading = true;
        new MyLoopJPost(mContext, "", onLoopJPostSendFriendReqCallComplete, WSMethods.BASE_URL + WSMethods.ACCEPT_DEC_REQ_POST, params);
    }
}