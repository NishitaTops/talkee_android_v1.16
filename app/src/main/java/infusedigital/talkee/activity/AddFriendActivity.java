package infusedigital.talkee.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Fade;
import android.transition.Transition;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import cz.msebera.android.httpclient.client.utils.URIBuilder;
import infusedigital.talkee.R;
import infusedigital.talkee.adapters.AddFriendAdapter;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.AddFriend;
import infusedigital.talkee.datamodel.SearchFriendListData;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;
import infusedigital.talkee.view.RecyclerViewScrollListener;

public class AddFriendActivity extends BaseActivity {

    ListView search_list;
    ArrayList searchList;

    SharedPreferences prefs = null;

    private ProgressDialog loadingDialog;

    private ImageView stackBtn;
    private TextView tvEmptyView;

    private RelativeLayout addFriendTitle;
    private TextView addFriendTitleText;
    int[] themeColorArray, alphaThemeColorArray;

    private int currentTheme;

    EditText searchbox;
    private ImageView searchIcon, searchCancel;
    LinearLayout servicesView;
    LinearLayout llTalkeeFriends, addressBook, shareInvitation;//, slackItem, connectedDrive, thermostatView;

    RecyclerView recycler_view_friends;
    private LinearLayoutManager linearLayoutManager;
    boolean hasNext;
    int page = 1;
    private boolean isLoading = true;
    private ArrayList<AddFriend> arrAllFriendsList;
    private AddFriendAdapter adapterFriends;
    String query = "";
    boolean isFirst = true;
    private ProgressBar mProgressBar;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View contentView = getLayoutInflater().inflate(R.layout.activity_add_friend, mFrameContent);
//        setContentView(R.layout.activity_add_friend);

//        Transition fade = new Fade();
//        fade.excludeTarget(android.R.id.statusBarBackground, true);
//        fade.excludeTarget(android.R.id.navigationBarBackground, true);
//        getWindow().setExitTransition(fade);
//        getWindow().setEnterTransition(fade);

        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        themeColorArray = getResources().getIntArray(R.array.themecolor);
        alphaThemeColorArray = getResources().getIntArray(R.array.alphathemecolor);

        tvEmptyView = (TextView) contentView.findViewById(R.id.tvEmptyView);
        addFriendTitle = (RelativeLayout) contentView.findViewById(R.id.addfriendTitle);
        addFriendTitleText = (TextView) contentView.findViewById(R.id.addfriend_title);
        addFriendTitleText.setTypeface(MyApplication.boldFont);

        currentTheme = prefs.getInt("currentThemeNum", 0);
        int themecolor = themeColorArray[currentTheme];
//        addFriendTitle.setBackgroundColor(themecolor);

        main.setSelected(false);
//        channel.setSelected(false);
        addfriend.setSelected(true);
        addressbook.setSelected(false);
        set.setSelected(false);

        search_list = (ListView) findViewById(R.id.search_friend_list);
        search_list.setDivider(null);
        arrAllFriendsList = new ArrayList<>();

        searchList = new ArrayList();

        mProgressBar = (ProgressBar) contentView.findViewById(R.id.mProgressBar);
        recycler_view_friends = (RecyclerView) contentView.findViewById(R.id.recycler_view_friends_list);
        linearLayoutManager = new LinearLayoutManager(this);
        recycler_view_friends.setLayoutManager(linearLayoutManager);

        changeStatusBarColor(getResources().getColor(R.color.statusbar));

        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
        }

        stackBtn = (ImageView) findViewById(R.id.addfriend_stackbtn);

        stackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

        searchIcon = (ImageView) findViewById(R.id.send_search_icon);
        searchCancel = (ImageView) findViewById(R.id.send_search_cancel_btn);

        searchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchbox.setText("");
//                if (searchbox.getText().toString().length() >= 2) {
//                    query = searchbox.getText().toString();
//                    if (!isLoading)
//                        getFriendsList(query);
//                }
                servicesView.setVisibility(View.VISIBLE);
                recycler_view_friends.setVisibility(View.GONE);
            }
        });

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query = searchbox.getText().toString();
                if (query.length() != 0) {
//                    if (!isLoading)
                    page = 1;
                    getFriendsList(query);
                }
            }
        });

        searchbox = (EditText) findViewById(R.id.send_txt_box);

        searchbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                query = searchbox.getText().toString().trim();
                if (query.length() != 0) {
//                    if (!isLoading)
                    page = 1;
                    getFriendsList(query);
                } else if (searchbox.getText().toString().length() == 0) {
                    servicesView.setVisibility(View.VISIBLE);
                    recycler_view_friends.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                query = searchbox.getText().toString().trim();
//                if (query.length() != 0) {
////                    if (!isLoading)
//                    getFriendsList(query);
//                } else if (searchbox.getText().toString().length() == 0) {
//                    servicesView.setVisibility(View.VISIBLE);
//                    recycler_view_friends.setVisibility(View.GONE);
//                }
            }
        });

        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                mFrameContent.setTranslationX(slideOffset * drawerView.getWidth());
                drawerLayout.bringChildToFront(drawerView);
                drawerLayout.requestLayout();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                searchbox.setFocusable(false);
                hideSoftKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                searchbox.setFocusable(true);
                searchbox.setFocusableInTouchMode(true);
                searchbox.requestFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(searchbox, InputMethodManager.SHOW_IMPLICIT);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        servicesView = (LinearLayout) findViewById(R.id.serviceitems);
        search_list.setVisibility(View.GONE);

        llTalkeeFriends = (LinearLayout) findViewById(R.id.llTalkeeFriends);
        llTalkeeFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addressIntent = new Intent(AddFriendActivity.this, TalkeeUsersActivity.class);
                startActivity(addressIntent);
                overridePendingTransition(R.anim.in_slide_right_left, R.anim.out_slide_right_left);
            }
        });
        addressBook = (LinearLayout) findViewById(R.id.addressbook);
        addressBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddressBookActivity();
            }
        });
        shareInvitation = (LinearLayout) findViewById(R.id.shareinvitation);
        shareInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actShareInvitation();
            }
        });
//        slackItem = (LinearLayout)findViewById(R.id.slackteam);
//        connectedDrive = (LinearLayout)findViewById(R.id.connecteddrive);
//        thermostatView = (LinearLayout)findViewById(R.id.thermostat);

        recycler_view_friends.addOnScrollListener(new RecyclerViewScrollListener() {

            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                if (hasNext) {
                    if (!isLoading) {
                        isLoading = true;
                        page++;
//                        adapterFriends.showLoading(true);
                        mProgressBar.setVisibility(View.VISIBLE);
                        getFriendsList(query);
                    }
                } else {
//                    adapter.showLoading(false);
                }
            }
        });
//        getFriendsList(query);
    }

    /**
     * Share the application with content message
     */
    private void actShareInvitation() {
        String detaileBody = getResources().getString(R.string.invitestring).toString();
//        String urlBody = getResources().getString(R.string.appurl).toString();
        String text = detaileBody /*+ " " + urlBody*/;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.sharevia)));
    }

    /**
     * Start addressBook activity for show contacts
     */
    private void startAddressBookActivity() {
        Intent addressIntent = new Intent(this, AddressBookActivity.class);
        startActivity(addressIntent);
        overridePendingTransition(R.anim.in_slide_right_left, R.anim.out_slide_right_left);
    }

    /**
     * Hide the keyboard while open activity
     */
    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

//    public void callSearch(String query) {
//        if (TextUtils.isEmpty(query)) {
//            searchList.clear();
//            search_list.setAdapter(new SearchFriendListAdapter(AddFriendActivity.this, searchList));
//
//            servicesView.setVisibility(View.VISIBLE);
//            search_list.setVisibility(View.GONE);
//
//            return;
//        }
//
//        servicesView.setVisibility(View.GONE);
//        search_list.setVisibility(View.VISIBLE);
//
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
//                .setParameter("act", "searchfriends")
//                .setParameter("keyword", query)
//                .setParameter("user_id", String.valueOf(MyApplication.currentUser.id));
//
//        String access_url = builder.toString();
//
//        Log.e("Super", "add friend = " + access_url);
////                startLoading();
//
//        searchList.clear();
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (!response.getString("status").equals("SUCCESS"))
//                                return;
//
//                            JSONArray searchedData = response.getJSONArray("data");
//
//                            for (int i = 0; i < searchedData.length(); i++) {
//                                JSONObject userData = searchedData.getJSONObject(i);
//                                User friend = new User();
//                                friend.setUser(userData);
//
//                                String becomefriend = userData.getString("becomefriend");
//                                String bsent = userData.getString("sent");
//                                String breceive = userData.getString("receive");
//
//                                boolean bf = false, bs = false, br = false;
//
//                                if (becomefriend.equals("1")) bf = true;
//                                if (bsent.equals("1")) bs = true;
//                                if (breceive.equals("1")) br = true;
//
//                                searchList.add(new SearchFriendListData(friend, bf, bs, br));
//                            }
//
//                            search_list.setAdapter(new SearchFriendListAdapter(AddFriendActivity.this, searchList));
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

    public void sendRequest(final LinearLayout friendBtn, SearchFriendListData data) {
        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/send_message.php")
                .setParameter("regId", data.friend.gcm_id)
                .setParameter("userId", String.valueOf(MyApplication.currentUser.id))
                .setParameter("friendId", String.valueOf(data.friend.id))
                .setParameter("requestType", "0");

        String access_url = builder.toString();

        Log.e("Super", "send friend request = " + access_url);

        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(getApplicationContext(), "Friend Request Sent", Toast.LENGTH_LONG).show();
                            TextView txt = (TextView) friendBtn.findViewById(R.id.send_request_btn_txt);
                            txt.setText("Sent");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

    public void sendAccept(final LinearLayout friendBtn, SearchFriendListData data) {
        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/send_message.php")
                .setParameter("regId", data.friend.gcm_id)
                .setParameter("userId", String.valueOf(MyApplication.currentUser.id))
                .setParameter("friendId", String.valueOf(data.friend.id))
                .setParameter("requestType", "1");

        String access_url = builder.toString();
        Log.e("Super", "friend accept = " + access_url);

        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(getApplicationContext(), "New Friend Added", Toast.LENGTH_LONG).show();
                            TextView txt = (TextView) friendBtn.findViewById(R.id.send_request_btn_txt);

                            txt.setText("Friend");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    private void startLoading() {
        isFirst = false;
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    /**
     * Get friends list
     */
    private void getFriendsList(String query) {
        servicesView.setVisibility(View.GONE);
        recycler_view_friends.setVisibility(View.VISIBLE);

        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("username", query);
        params.put("paging", page);
//        if (isFirst)
//            startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostGetFriendsCallComplete, WSMethods.BASE_URL + WSMethods.ADD_FRIENDS_LIST_POST, params);
    }

    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostGetFriendsCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
//                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");

                if (mProgressBar.getVisibility() == View.VISIBLE)
                    mProgressBar.setVisibility(View.GONE);

                if (flag.equalsIgnoreCase("true")) {
                    if (jobj.getString("IS_NEXT").equalsIgnoreCase("true")) {
                        hasNext = true;
                    } else {
                        hasNext = false;
                    }
                    JSONArray arrFriendsList = jobj.getJSONArray("USER_DETAILS");
                    if (page == 1)
                        arrAllFriendsList.clear();
                    for (int i = 0; i < arrFriendsList.length(); i++) {
                        JSONObject obj = arrFriendsList.getJSONObject(i);
                        arrAllFriendsList.add(new AddFriend(obj.getString("userid"),
                                obj.getString("name"),
                                obj.getString("last_name"),
                                obj.getString("username"),
                                obj.getString("profile_url"),
                                obj.getString("isfollow")));
                    }
                    if (arrAllFriendsList.size() > 0) {
                        if (adapterFriends != null) {
                            adapterFriends.notifyDataSetChanged();
                        } else {
                            adapterFriends = new AddFriendAdapter(AddFriendActivity.this, arrAllFriendsList);
                            recycler_view_friends.setAdapter(adapterFriends);
                        }
                    }
                } else {
                    if (page == 1)
                        arrAllFriendsList.clear();

                    if (jobj.optString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }

//                    if (!isLoading) {
//                        String message = jobj.getString("MESSAGE");
//                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Get friends list
     */
    public void sendRequest(String friendId) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
//        params.put("user_id", "240");
        params.put("reciver_id", friendId);
//        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostSendFriendReqCallComplete, WSMethods.BASE_URL + WSMethods.SEND_FRIEND_REQ_POST, params);
    }

    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSendFriendReqCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
//                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    page = 1;
                    getFriendsList(query);
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Accept/Decline request received
     *
     * @param sender_id
     */
    public void ActionOnRequest(final String sender_id) {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//        alertDialogBuilder.setTitle("Request");
//        alertDialogBuilder.setMessage("Are you want to accept the friend request?");
//        alertDialogBuilder
//                .setCancelable(false)
//                .setPositiveButton("Accept",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
        RequestParams params = new RequestParams();
//        params.put("user_id", userId);
//                                params.put("user_id", "240");
        params.put("user_id", AppConfig.getUserId(context));
        params.put("requestflag", "1");
        params.put("sender_id", sender_id);
        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostSendFriendReqCallComplete, WSMethods.BASE_URL + WSMethods.ACCEPT_DEC_REQ_POST, params);
//                            }
//                        })
//                .setNegativeButton("Decline",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
//                            }
//                        });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        // show it
//        alertDialog.show();
    }

//    /**
//     * Get friends list
//     */
//    private void getFriendsList() {
//        RequestParams params = new RequestParams();
////        params.put("user_id", userId);
//        params.put("user_id", "240");
//        params.put("paging", page);
//        startLoading();
//        isLoading = true;
//        new MyLoopJPost(context, "", onLoopJPostSendAudioMessageCallComplete, WSMethods.BASE_URL + WSMethods.LIST_OF_FRIENDS_POST, params);
//    }
//
//    /**
//     * API parsing of check status of number
//     */
//    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSendAudioMessageCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {
//
//        @Override
//        public void response(String result) {
//            try {
//                endLoading();
//                JSONObject jobj = new JSONObject(result);
//                String flag = jobj.getString("FLAG");
//                hasNext = Boolean.parseBoolean(jobj.getString("IS_NEXT"));
//                if (flag.equalsIgnoreCase("true")) {
//                    JSONArray arrFriendsList = jobj.getJSONArray("USER_DETAILS");
//                    arrAllFriendsList = new ArrayList<>();
//                    for (int i = 0; i < arrFriendsList.length(); i++) {
//                        JSONObject obj = arrFriendsList.getJSONObject(i);
//                        arrAllFriendsList.add(new FriendsData(obj.getString("userid"),
//                                obj.getString("name"),
//                                obj.getString("last_name"),
//                                obj.getString("username"),
//                                obj.getString("profile_url"),
//                                false));
//                    }
//                    if (arrAllFriendsList.size() > 0) {
//                        adapterFriends = new FriendsListAdapter(context, arrAllFriendsList);
//                        recycler_view_friends.setAdapter(adapterFriends);
//                    }
//                } else {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                }
//                isLoading = false;
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    };
}