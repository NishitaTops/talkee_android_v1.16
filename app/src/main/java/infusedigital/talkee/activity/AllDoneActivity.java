package infusedigital.talkee.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.Base64;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import infusedigital.talkee.R;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.WSMethods;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AllDoneActivity extends AppCompatActivity {
    private SharedPreferences prefs, prefs1;
    String phoneNum, userName, avatarPath, emotiColor, deviceId, regId, firstName, lastName;
    private ProgressDialog loadingDialog;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_all_done);
        context = AllDoneActivity.this;

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        }

        prefs = getApplicationContext().getSharedPreferences("RegisterUserInfo", MODE_PRIVATE);
        prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        phoneNum = prefs.getString("phonenum", "null");
        userName = prefs.getString("registerUserName", "null");
        firstName = prefs.getString("registerFirstName", "null");
        lastName = prefs.getString("registerLastName", "null");
        avatarPath = prefs.getString("avatarPath", "null");
        emotiColor = prefs.getString("registerColor", "#2edadc");
        deviceId = prefs1.getString("deviceid", "null");
        regId = prefs1.getString("gcm_regid", "null");
        if (regId.equalsIgnoreCase("null"))
            regId = FirebaseInstanceId.getInstance().getToken();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppConfig.inserted_type.equals("1")) {
//                    register();
//                    System.out.println("++++ phone n:" + phoneNum);
//                    Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
//                    nextIntent.putExtra("phone", phoneNum);
//                    nextIntent.putExtra("isLogin", false);
//                    startActivity(nextIntent);
//                    finish();
                    //TODO send sms after enter number
//                    sendSMS();
                    //TODO register call here
                    registerByNumber();
                } else {
                    registerSocial();
                }
            }
        }, 2000);
    }

    private void registerByNumber() {
        RequestParams params = new RequestParams();
        params.put("mobile_number", phoneNum);
        params.put("user_name", userName);
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        try {
            if (avatarPath != null) {
                if (avatarPath.length() > 0) {
                    System.out.println("++++ avatar path:" + avatarPath);
                    params.put("profile_image", new File(avatarPath));
                }
            } else {
                if (prefs.getString("avatarPath", "").length() > 0) {
                    System.out.println("++++ avatar path:" + avatarPath);
                    params.put("profile_image", new File(avatarPath));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        params.put("device_token", regId);
        params.put("color", emotiColor);
        params.put("inserted_type", AppConfig.inserted_type);
        params.put("device_type", "0");
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostRegisterByNumberCallComplete, WSMethods.BASE_URL + WSMethods.REGISTRATION_POST, params);

    }

    /**
     * API parsing of registration using number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostRegisterByNumberCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    prefs1.edit().putString("userId", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("photoUrl", USER_DETAILS.getString("profile_image")).commit();
                    prefs1.edit().putString("name", USER_DETAILS.getString("user_name")).commit();
                    prefs1.edit().putString("firstName", USER_DETAILS.getString("first_name")).commit();
                    prefs1.edit().putString("lastName", USER_DETAILS.getString("last_name")).commit();
                    prefs1.edit().putString("color", USER_DETAILS.getString("color")).commit();
                    prefs1.edit().putString("mood", USER_DETAILS.optString("mood")).commit();
//                    prefs1.edit().putString("phonenum", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("gcm_regid", USER_DETAILS.getString("device_token_id")).commit();
//                    prefs1.edit().putString("deviceid", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("loginstate", "true").commit();
                    prefs1.edit().putString("notification", USER_DETAILS.optString("notification")).commit();
//                    prefs1.edit().putString("type", "phone");
                    goToHomeScreen();
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void goToHomeScreen() {
        AppConfig.USER_DETAILS = null;
        Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
        nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(nextIntent);
        finish();
    }

    /**
     * social registration
     */
    private void registerSocial() {
        RequestParams params = new RequestParams();
        params.put("social_id", AppConfig.social_id);
        params.put("user_name", userName);
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        try {
            if (avatarPath.length() > 0 && !avatarPath.contains("http"))
                params.put("profile-url", new File(avatarPath));
            else
                params.put("profile-url", avatarPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        params.put("device_token", regId);
        params.put("color", emotiColor);
        params.put("inserted_type", AppConfig.inserted_type);
        params.put("device_type", "0");
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostRegisterSocialCallComplete, WSMethods.BASE_URL + WSMethods.SOCIAL_REGISTRATION_POST, params);
    }

    /**
     * API parsing for social registration
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostRegisterSocialCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) { // User login successfully
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    prefs1.edit().putString("userId", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("photoUrl", USER_DETAILS.getString("profile_image")).commit();
                    prefs1.edit().putString("name", USER_DETAILS.getString("user_name")).commit();
                    prefs1.edit().putString("firstName", USER_DETAILS.getString("first_name")).commit();
                    prefs1.edit().putString("lastName", USER_DETAILS.getString("last_name")).commit();
                    prefs1.edit().putString("color", USER_DETAILS.getString("color")).commit();
                    prefs1.edit().putString("mood", USER_DETAILS.optString("mood")).commit();
//                    prefs1.edit().putString("phonenum", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("gcm_regid", USER_DETAILS.getString("device_token_id")).commit();
//                    prefs1.edit().putString("deviceid", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("loginstate", "true").commit();
                    prefs1.edit().putString("notification", USER_DETAILS.optString("notification")).commit();
//                    prefs1.edit().putString("type", "phone");
//
                    Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(nextIntent);
                    finish();
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * generate random number and send to user
     */
    private void sendSMS() {
        final int randomPIN = (int) (Math.random() * 9000) + 1000;
        prefs1.edit().putString("code", String.valueOf(randomPIN)).commit();
        OkHttpClient client = new OkHttpClient();
        String url = "https://api.twilio.com/2010-04-01/Accounts/ACb81b2a66810873a4d2a0823192c9ca77/Messages.json";
        String base64EncodedCredentials = "Basic " +
                Base64.encodeToString(("ACb81b2a66810873a4d2a0823192c9ca77" + ":"
                        + "8471b24341fa35e3b81ac0c0abf9fe8b").getBytes(), Base64.NO_WRAP);
        //TODO hiral's credentials
//        String url = "https://api.twilio.com/2010-04-01/Accounts/ACa833ae73e456f57bbebe3348574a0293/SMS/Messages";
//        String base64EncodedCredentials = "Basic " +
//                Base64.encodeToString(("ACa833ae73e456f57bbebe3348574a0293" + ":"
//                        + "909e53cb6f459830f40236e98bf3b0b8").getBytes(), Base64.NO_WRAP);

        RequestBody body = new FormBody.Builder()
                .add("From", "+447481339104") // Hiral: 17606992417
                .add("To", "+" + phoneNum)
                .add("Body", "OTP for registration with Talkee: " + randomPIN)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .header("Authorization", base64EncodedCredentials)
                .build();
        try {
            Response response = client.newCall(request).execute();
            Log.d("TAG", "++++++ sendSms: " + response.body().string());
            //TODO check status here if needed
            Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
            nextIntent.putExtra("phone", phoneNum);
            nextIntent.putExtra("isLogin", false);
            startActivity(nextIntent);
            finish();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", "++++ error:: " + e.getMessage());
        }
    }

//    /**
//     * twilio call
//     */
//    class RetrieveFeedTask extends AsyncTask<String, Void, Void> {
//
//        private int randomPIN = 0;
//        private Exception exception;
//
//        public RetrieveFeedTask(int randomPIN) {
//            System.out.println("++++ randomPIN:" + randomPIN);
//            this.randomPIN = randomPIN;
//        }
//
//        protected Void doInBackground(String... urls) {
//            System.out.println("++++ doInBackground:" + randomPIN);
//            HttpClient httpclient = new DefaultHttpClient();
//
//            HttpPost httppost = new HttpPost(
//                    "https://api.twilio.com/2010-04-01/Accounts/AC50481bed2da0300d9faab375b3ad414f/SMS/Messages");
//            String base64EncodedCredentials = "Basic "
//                    + Base64.encodeToString(
//                    ("AC50481bed2da0300d9faab375b3ad414f" + ":" + "3f734405654764a45d2fe30fa44c6a15").getBytes(),
//                    Base64.NO_WRAP);
//
//            httppost.setHeader("Authorization", base64EncodedCredentials);
//            try {
//                System.out.println("++++ doInBackground try:" + randomPIN);
//                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//                nameValuePairs.add(new BasicNameValuePair("From", "+447481339104"));
//                nameValuePairs.add(new BasicNameValuePair("To", phoneNum));
//                nameValuePairs.add(new BasicNameValuePair("Body", "OTP for registration with Talkee: " + randomPIN));
//
//                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//                // Execute HTTP Post Request
//                HttpResponse response = httpclient.execute(httppost);
//                HttpEntity entity = response.getEntity();
//                System.out.println("++++ doInBackground done:" + randomPIN);
//                System.out.println("+++++ Entity post is: " + EntityUtils.toString(entity));
//            } catch (ClientProtocolException e) {
//                System.out.println("+++++ Error: " + e.getMessage());
//            } catch (IOException e) {
//                System.out.println("+++++ Error: " + e.getMessage());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void feed) {
//            Intent verifyActivity = new Intent(AllDoneActivity.this, RegisterCodeActivity.class);
//            verifyActivity.putExtra("phonenum", phoneNum);
//            verifyActivity.putExtra("username", userName);
//            verifyActivity.putExtra("gcmid", regId);
//            verifyActivity.putExtra("deviceid", deviceId);
//            startActivity(verifyActivity);
//            finish();
//        }
//    }

//    private void register () {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/login.php")
//                .setParameter("act", "register")
//                .setParameter("phonenum", phoneNum)
//                .setParameter("deviceid", deviceId)
//                .setParameter("gcm_regid", regId)
//                .setParameter("username", userName)
//                .setParameter("type", "phone")
//                .setParameter("color", emotiColor);
//        String access_url = builder.toString();
//
//        Log.e("Super", "register = " + access_url);
//
//        startLoading();
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            String req_result = response.toString();
//                            Log.e("Super", "register result = " + response);
//
//                            JSONObject data;
//
//                            endLoading();
//                            String status = response.getString("status");
//                            if (status.equals("failed")) {
//                                Toast.makeText(getApplicationContext(), "You couldn't register with this number", Toast.LENGTH_SHORT).show();
//                                finish();
//
//                                return;
//                            }else if (status.equals("failed_registered")){
//                                Toast.makeText(getApplicationContext(), "This Number is already registered", Toast.LENGTH_SHORT).show();
//                                finish();
//                                return;
//                            }else if (status.equals("failed_otherdevice")){
//                                Toast.makeText(getApplicationContext(), "This Number is already registered with other Device", Toast.LENGTH_SHORT).show();
//                                finish();
//                                return;
//                            }else if (status.equals("SENT")){
//                                Intent verifyActivity = new Intent(AllDoneActivity.this, RegisterCodeActivity.class);
//                                verifyActivity.putExtra("phonenum", phoneNum);
//                                verifyActivity.putExtra("username", userName);
//                                verifyActivity.putExtra("gcmid", regId);
//                                verifyActivity.putExtra("deviceid", deviceId);
//                                startActivity(verifyActivity);
//                                finish();
//                            }else {
//                                data = response.getJSONObject("data");
//                                MyApplication.currentUser.initialize();
//                                MyApplication.currentUser.setUser(data);
//                                if (android.text.TextUtils.isEmpty(MyApplication.currentUser.phonenum))
//                                    prefs1.edit().putString("phonenum", MyApplication.currentUser.name).commit();
//                                else
//                                    prefs1.edit().putString("phonenum", MyApplication.currentUser.phonenum).commit();
//
//                                prefs1.edit().putString("userId", String.valueOf(MyApplication.currentUser.id)).commit();
//                                prefs1.edit().putString("photoUrl", MyApplication.currentUser.photo_url).commit();
//                                prefs1.edit().putString("name", MyApplication.currentUser.name).commit();
//                                prefs1.edit().putString("color", MyApplication.currentUser.emoticol).commit();
//                                prefs1.edit().putString("mood", MyApplication.currentUser.mood).commit();
//                                prefs1.edit().putString("gcm_regid", MyApplication.currentUser.gcm_id).commit();
//                                prefs1.edit().putString("deviceid", MyApplication.currentUser.device_id).commit();
//                                prefs1.edit().putString("loginstate", "true").commit();
//                                prefs1.edit().putString("type", "phone").commit();
//                                Log.e("Super", "444444444444444");
//                                finish();
//                                Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
//                                startActivity(nextIntent);
//                            }
//                        } catch (Exception e) {
//                            Log.e("Super", "errrr");
//                            e.printStackTrace();
//                        }
//                        endLoading();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }
}