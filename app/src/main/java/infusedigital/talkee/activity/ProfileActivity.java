package infusedigital.talkee.activity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.transition.Fade;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import infusedigital.talkee.R;
import infusedigital.talkee.fragment.TabProfileFragment;
import infusedigital.talkee.utilities.AppConfig;

public class ProfileActivity extends BaseActivity {

    private ImageView stackBtn;
    private RelativeLayout profileTitle;
    private TextView profileTitleText/*, profile_edit*/;
    int[] themeColorArray, alphaThemeColorArray;
    private int currentTheme;
    SharedPreferences prefs = null;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View contentView = getLayoutInflater().inflate(R.layout.activity_profile, mFrameContent);

//        Transition fade = new Fade();
//        fade.excludeTarget(android.R.id.statusBarBackground, true);
//        fade.excludeTarget(android.R.id.navigationBarBackground, true);
//        getWindow().setExitTransition(fade);
//        getWindow().setEnterTransition(fade);

        themeColorArray = getResources().getIntArray(R.array.themecolor);
        alphaThemeColorArray = getResources().getIntArray(R.array.alphathemecolor);
        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        profileTitle = (RelativeLayout) contentView.findViewById(R.id.profileTitle);
        profileTitleText = (TextView) contentView.findViewById(R.id.profile_title);
        profileTitleText.setTypeface(MyApplication.boldFont);

        currentTheme = prefs.getInt("currentThemeNum", 0);
        int themecolor = themeColorArray[currentTheme];
//        profileTitle.setBackgroundColor(themecolor);

        main.setSelected(false);
//        channel.setSelected(false);
        addfriend.setSelected(false);
        set.setSelected(true);

        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
        }

        stackBtn = (ImageView) contentView.findViewById(R.id.profile_stackbtn);
        stackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

//        profile_edit = (TextView) contentView.findViewById(R.id.profile_edit);
//        profile_edit.setTypeface(MyApplication.boldFont);
        //TODO removed setting click
        /*profileTitleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profileTitleText.getTag().equals("edit")) {
                    AppConfig.isEditProf = true;
                    profileTitleText.setTag("save");
//                    profileTitleText.setText("Save");
                    TabProfileFragment.editAction(true);
                } else if (profileTitleText.getTag().equals("save")) {
                    AppConfig.isEditProf = false;
                    profileTitleText.setTag("edit");
//                    profileTitleText.setText("Edit");
                    TabProfileFragment.saveSettings();
                }
            }
        });*/
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        changeStatusBarColor(getResources().getColor(R.color.statusbar));
    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }
}