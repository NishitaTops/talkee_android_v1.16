package infusedigital.talkee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import infusedigital.talkee.R;

public class ChannelActivity extends BaseActivity {

    private ImageView stackBtn;
    private TextView title;

    private RelativeLayout channelToolbar;
    int[] themeColorArray, alphaThemeColorArray;
    private int currentTheme;
    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View contentView = getLayoutInflater().inflate(R.layout.activity_channel, mFrameContent);
        themeColorArray = getResources().getIntArray(R.array.themecolor);
        alphaThemeColorArray = getResources().getIntArray(R.array.alphathemecolor);
        prefs = getApplicationContext().getSharedPreferences("UserInfo",MODE_PRIVATE);
        channelToolbar = (RelativeLayout)contentView.findViewById(R.id.channeltoolbar);

        currentTheme = prefs.getInt("currentThemeNum", 0);
        int themecolor = themeColorArray[currentTheme];
        channelToolbar.setBackgroundColor(themecolor);

        main.setSelected(false);
//        channel.setSelected(true);
        addfriend.setSelected(false);
        set.setSelected(false);

        changeStatusBarColor(getResources().getColor(R.color.statusbar));

        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
        }


        stackBtn = (ImageView) findViewById(R.id.channel_stackbtn);

        stackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

        title = (TextView)findViewById(R.id.channel_title);
        title.setTypeface(MyApplication.regularFont);

    }

    public void changeStatusBarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }
}
