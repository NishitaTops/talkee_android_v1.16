package infusedigital.talkee.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.RequestParams;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import cz.msebera.android.httpclient.client.utils.URIBuilder;
import infusedigital.talkee.R;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.facebookSignIn.FacebookHelper;
import infusedigital.talkee.facebookSignIn.FacebookResponse;
import infusedigital.talkee.facebookSignIn.FacebookUser;
import infusedigital.talkee.model.TwitterUser;
import infusedigital.talkee.twitterSignIn.TwitterHelper;
import infusedigital.talkee.twitterSignIn.TwitterResponse;
import infusedigital.talkee.utilities.AlertDialogManager;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;

public class LoginViewActivity extends AppCompatActivity implements FacebookResponse, TwitterResponse {

    LinearLayout loginWithNum;
    ImageView cancel_btn;
    LinearLayout fb_login, twt_login;
    TextView fbTitle, twTitle;
//    public static CallbackManager callbackmanager;

    String req_result = "";
    private ProgressDialog loadingDialog;
    AlertDialogManager alert = new AlertDialogManager();

    String fb_photo_uri;
    String gcm_reg_id = "";

    SharedPreferences prefs = null;
    private String deviceId;

    String fbName, fbPhoto;
    //    private TwitterLoginButton loginButton;
    private TwitterHelper mTwitterHelper;
    public FacebookHelper mFbHelper;
    Context context;
//    private CallbackManager fb_callbackManager;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent nextIntent = new Intent(getApplicationContext(), MainActivity.class);
        nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(nextIntent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        fb_callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login_view);
        context = LoginViewActivity.this;

        cancel_btn = (ImageView) findViewById(R.id.singup_cancel_btn);
        loginWithNum = (LinearLayout) findViewById(R.id.login_with_phone);

        Utility.clearState(context);
        changeStatusBarColor(getResources().getColor(R.color.whiteColor));
        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        gcm_reg_id = prefs.getString("gcm_regid", "null");
        if (gcm_reg_id.equalsIgnoreCase("null"))
            gcm_reg_id = FirebaseInstanceId.getInstance().getToken();

        deviceId = prefs.getString("deviceid", "null");

        fb_login = (LinearLayout) findViewById(R.id.fb_layout);
        twt_login = (LinearLayout) findViewById(R.id.twt_layout);

//        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
//        loginButton.setCallback(new Callback<TwitterSession>() {
//            @Override
//            public void success(Result<TwitterSession> result) {
//                // The TwitterSession is also available through:
//                // Twitter.ge\tInstance().core.getSessionManager().getActiveSession()
//                TwitterSession session = result.data;
//                // TODO: Remove toast and use the TwitterSession's userID
//                // with your app's user model
//
//                String msg = "@" + session.getUserName() + " logged in! (#" + session.getUserId() + ")";
//
//                Twitter.getApiClient(session).getAccountService().verifyCredentials(true, false, new Callback<User>() {
//                    @Override
//                    public void success(Result<User> userResult) {
//                        System.out.println("+++++ twitter success:" + userResult.data.toString() + "::" + userResult.data.screenName);
//                        TwitterUser user = new TwitterUser();
//                        user.name = userResult.data.name;
//                        user.email = userResult.data.email;
//                        user.description = userResult.data.description;
//                        user.pictureUrl = userResult.data.profileImageUrl;
//                        user.bannerUrl = userResult.data.profileBannerUrl;
//                        user.language = userResult.data.lang;
//                        user.id = userResult.data.id;
//
//                        AppConfig.social_id = String.valueOf(user.id);
//                        AppConfig.social_profile_image = user.pictureUrl;
//                        if (AppConfig.social_profile_image.contains("_normal"))
//                            AppConfig.social_profile_image = AppConfig.social_profile_image.replace("_normal", "");
//                        AppConfig.social_name = user.name;
//                        AppConfig.social_username = userResult.data.screenName;
//
//                        socialVerification(AppConfig.social_id, AppConfig.inserted_type);
//                    }
//
//                    @Override
//                    public void failure(TwitterException exception) {
//                        Toast.makeText(context, "Connection failure error:" + exception.getMessage(), Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//
//            @Override
//            public void failure(TwitterException exception) {
//                Toast.makeText(context, "Connection error:" + exception.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });

        mTwitterHelper = new TwitterHelper(R.string.twitter_api_key,
                R.string.twitter_secrate_key, this, this);
        mFbHelper = new FacebookHelper(this, "id,name,email,gender,birthday,picture,cover", this);//username

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loginWithNum.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppConfig.inserted_type = "1";
                Intent nextIntent = new Intent(getApplicationContext(), LoginWithNumberActivity.class);
                startActivity(nextIntent);
                finish();
            }
        });

        fb_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppConfig.inserted_type = "3";
//                onFblogin();
                mFbHelper.performSignIn(LoginViewActivity.this);
            }
        });


        twt_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppConfig.inserted_type = "4";
//                loginButton.performClick();
                mTwitterHelper.performSignIn();
            }
        });

        fbTitle = (TextView) findViewById(R.id.login_with_fb);
        twTitle = (TextView) findViewById(R.id.login_with_tw);

        fbTitle.setTypeface(MyApplication.semiboldFont);
        twTitle.setTypeface(MyApplication.semiboldFont);
    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

//    private void onFblogin() {
//        callbackmanager = CallbackManager.Factory.create();
//
//        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));
//
//        LoginManager.getInstance().registerCallback(callbackmanager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        System.out.println("+++++ Success FacebookCallback");
//
////                        GraphRequest request = GraphRequest.newMeRequest(
////                                loginResult.getAccessToken(),
////                                new GraphRequest.GraphJSONObjectCallback() {
////                                    @Override
////                                    public void onCompleted(
////                                            JSONObject object,
////                                            GraphResponse response) {
////                                        System.out.println("+++++ Success FacebookCallback:" + response);
////                                        // Application code
////                                        Log.v("LoginActivity", response.toString());
////                                    }
////                                });
////                        Bundle parameters = new Bundle();
////                        parameters.putString("fields", "id,name,email,gender,birthday");
////                        request.setParameters(parameters);
////                        request.executeAsync();
//                        GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//                            @Override
//                            public void onCompleted(JSONObject object, GraphResponse response) {
//                                if (response.getError() != null) {
//                                    System.out.println("Error");
//                                } else {
//                                    System.out.println("+++++ Success FacebookCallback:" + response);
//                                    try {
//                                        String jsonresult = String.valueOf(object);
//                                        System.out.println("JSON Result" + jsonresult);
//
//                                        String str_id = object.getString("id");
//                                        AppConfig.social_id = str_id;
//                                        fb_photo_uri = "https://graph.facebook.com/" + str_id + "/picture";
//
//                                        fbName = object.getString("name");
//                                        AppConfig.social_name = fbName;
//                                        AppConfig.social_profile_image = fb_photo_uri;
//                                        socialVerification(str_id, AppConfig.inserted_type);
////                                        loginWithFB (fbName, fb_photo_uri);
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }
//                        }).executeAsync();
//                    }
//
//                    @Override
//                    public void onCancel() {
//                    }
//
//                    @Override
//                    public void onError(FacebookException error) {
//                    }
//                });
//    }

    /**
     * Social verification
     *
     * @param str_id
     * @param type   facebook[3] or twitter[4]
     */
    private void socialVerification(String str_id, String type) {
        RequestParams params = new RequestParams();
//        if (gcm_reg_id.length() == 0 || gcm_reg_id.equalsIgnoreCase("null"))
        gcm_reg_id = FirebaseInstanceId.getInstance().getToken();
        params.put("device_token", gcm_reg_id);
        params.put("social_id", str_id);
        params.put("inserted_type", type);
        params.put("device_type", "0");
        startLoading();
        System.out.println("++++++++++++ params:" + params + ":: Token:" + gcm_reg_id);
        new MyLoopJPost(context, "", onLoopJPostSocialVerificationCallComplete, WSMethods.BASE_URL + WSMethods.SOCIAL_VERIFICATION_POST, params);
    }

    /**
     * API parsing of check status of social Verification
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSocialVerificationCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) { // social exist

                    clearPrefs();
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    prefs.edit().putString("userId", USER_DETAILS.getString("user_id")).commit();
                    prefs.edit().putString("photoUrl", USER_DETAILS.getString("profile_image")).commit();
                    prefs.edit().putString("name", USER_DETAILS.getString("user_name")).commit();
                    prefs.edit().putString("firstName", USER_DETAILS.getString("first_name")).commit();
                    prefs.edit().putString("lastName", USER_DETAILS.getString("last_name")).commit();
                    prefs.edit().putString("color", USER_DETAILS.getString("color")).commit();
                    prefs.edit().putString("mood", USER_DETAILS.optString("mood")).commit();
                    prefs.edit().putString("phonenum", USER_DETAILS.optString("mobile_number")).commit();
                    prefs.edit().putString("gcm_regid", USER_DETAILS.getString("device_token_id")).commit();
//                    prefs.edit().putString("deviceid", USER_DETAILS.getString("user_id")).commit();
                    prefs.edit().putString("loginstate", "true").commit();
                    prefs.edit().putString("notification", USER_DETAILS.optString("notification")).commit();
//                    prefs.edit().putString("type", "phone");

                    Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(nextIntent);
                    finish();
                } else {
                    Intent nextIntent = new Intent(getApplicationContext(), ReigsterWithPhoneActivity.class);
                    startActivity(nextIntent);
                    finish();

//                    Intent mIntent = new Intent(context, EnterUserNameActivity.class);
//                    mIntent.putExtra("name", AppConfig.social_name);
//                    mIntent.putExtra("profile_image", AppConfig.social_profile_image);
//                    mIntent.putExtra("username", AppConfig.social_username);
//                    startActivity(mIntent);
//                    finish();

//                    loginWithFB(fbName, fb_photo_uri);
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void clearPrefs() {
        prefs.edit().putString("userId", "").commit();
        prefs.edit().putString("photoUrl", "").commit();
        prefs.edit().putString("name", "").commit();
        prefs.edit().putString("firstName", "").commit();
        prefs.edit().putString("lastName", "").commit();
        prefs.edit().putString("color", "").commit();
        prefs.edit().putString("mood", "").commit();
        prefs.edit().putString("phonenum", "").commit();
        prefs.edit().putString("gcm_regid", "").commit();
//                    prefs.edit().putString("deviceid", USER_DETAILS.getString("user_id")).commit();
        prefs.edit().putString("loginstate", "false").commit();
        prefs.edit().putString("notification", "").commit();
    }

    private void loginWithFB(String fbName, String fb_photo_uri) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (callbackmanager != null) {
//            callbackmanager.onActivityResult(requestCode, resultCode, data);
        mFbHelper.onActivityResult(requestCode, resultCode, data);
//        } else {
        mTwitterHelper.onActivityResult(requestCode, resultCode, data);
//            loginButton.onActivityResult(requestCode, resultCode, data);
//        }
    }

//    private void loginWithFB(String username, String photoURL) {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/login.php")
//                .setParameter("act", "login")
//                .setParameter("phonenum", username)
//                .setParameter("deviceid", deviceId)
//                .setParameter("photourl", photoURL)
//                .setParameter("gcm_regid", gcm_reg_id)
//                .setParameter("type", "social");
//
//        String access_url = builder.toString();
//        Log.e("Super", "loginwithfb = " + access_url);
//
//        startLoading();
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        endLoading();
//
//                        try {
//                            req_result = response.toString();
//                            JSONObject data;
//
//                            endLoading();
//                            String status = response.getString("status");
//                            if (status.equals("failed")) {
//                                alert.showAlertDialog(LoginViewActivity.this,
//                                        "Login Error",
//                                        "You couldn't login with this number", false);
//                                return;
//                            }
//
//                            data = response.getJSONObject("data");
//                            MyApplication.currentUser.initialize();
//                            MyApplication.currentUser.setUser(data);
//
//                            prefs.edit().putString("userId", String.valueOf(MyApplication.currentUser.id)).commit();
//                            prefs.edit().putString("photoUrl", MyApplication.currentUser.photo_url).commit();
//                            prefs.edit().putString("name", MyApplication.currentUser.name).commit();
//                            prefs.edit().putString("color", MyApplication.currentUser.emoticol).commit();
//                            prefs.edit().putString("mood", MyApplication.currentUser.mood).commit();
//                            prefs.edit().putString("phonenum", MyApplication.currentUser.phonenum).commit();
//                            prefs.edit().putString("gcm_regid", MyApplication.currentUser.gcm_id).commit();
//                            prefs.edit().putString("deviceid", MyApplication.currentUser.device_id).commit();
//                            prefs.edit().putString("loginstate", "true").commit();
//                            prefs.edit().putString("type", "social").commit();
//
//                            Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
//                            startActivity(nextIntent);
//
//                            finish();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                    loadingDialog.hide();
                }
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    @Override
    public void onTwitterError() {

    }

    @Override
    public void onTwitterSignIn(@NonNull String userId, @NonNull String userName) {

    }

    @Override
    public void onTwitterProfileReceived(infusedigital.talkee.twitterSignIn.TwitterUser user) {

        AppConfig.social_id = String.valueOf(user.id);
        AppConfig.social_profile_image = user.pictureUrl;
        if (AppConfig.social_profile_image.contains("_normal"))
            AppConfig.social_profile_image = AppConfig.social_profile_image.replace("_normal", "");
        AppConfig.social_name = user.name;
        AppConfig.social_username = user.userName;

        socialVerification(AppConfig.social_id, AppConfig.inserted_type);
    }

    @Override
    public void onFbSignInFail() {

    }

    @Override
    public void onFbSignInSuccess() {

    }

    @Override
    public void onFbProfileReceived(FacebookUser facebookUser) {
        try {
//            try {
//                JSONObject jsonObject = new JSONObject(facebookUser.profilePic);
//                System.out.println("img url:" + jsonObject.getJSONObject("data").getString("url"));
//                fb_photo_uri = jsonObject.getJSONObject("data").getString("url");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
            fb_photo_uri = "https://graph.facebook.com/" + facebookUser.facebookID + "/picture?type=large";
            fbName = facebookUser.name;
            AppConfig.social_id = facebookUser.facebookID;
            AppConfig.social_name = fbName;
            AppConfig.social_profile_image = fb_photo_uri;
            socialVerification(facebookUser.facebookID, AppConfig.inserted_type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
