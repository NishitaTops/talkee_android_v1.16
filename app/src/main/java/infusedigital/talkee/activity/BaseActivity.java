package infusedigital.talkee.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

//import com.testfairy.TestFairy;

import infusedigital.talkee.R;
import infusedigital.talkee.utilities.AppConfig;

public class BaseActivity extends AppCompatActivity {

    public DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    public Toolbar mToolbar;
    public FrameLayout mFrameContent;

    private ImageView navHide;
    public ImageView main, addfriend, set, addressbook;//, channel
    public int index = 1;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        context = BaseActivity.this;
//        TestFairy.begin(this, "64b593feb9ec77ee5209919097663b501adeb533");

        mFrameContent = (FrameLayout) findViewById(R.id.content_frame);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);

                mFrameContent.setTranslationX(slideOffset * drawerView.getWidth());
                drawerLayout.bringChildToFront(drawerView);
                drawerLayout.requestLayout();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                InputMethodManager inputMethodmanager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodmanager.hideSoftInputFromInputMethod(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        NavigationView nView = (NavigationView) findViewById(R.id.nav_view);

        main = (ImageView) findViewById(R.id.mainicon);
//        channel = (ImageView) findViewById(R.id.channelicon);
        addfriend = (ImageView) findViewById(R.id.addfriendicon);
        set = (ImageView) findViewById(R.id.seticon);
        addressbook = (ImageView) findViewById(R.id.addressbookicon);

        addressbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConfig.isEditProf = false;
                finish();
                Intent nextIntent = new Intent(getApplicationContext(), AddressBookActivityNew.class);
                nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                if (AppConfig.isAddFriend) {
                    AppConfig.isAddFriend = false;
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(nextIntent);
            }
        });

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConfig.isEditProf = false;
                finish();
                Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
                nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                if (AppConfig.isAddFriend) {
                    AppConfig.isAddFriend = false;
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(nextIntent);
            }
        });

//        channel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//                Intent nextIntent = new Intent(getApplicationContext(), ChannelActivity.class);
//                startActivity(nextIntent);
//            }
//        });

        addfriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppConfig.isEditProf = false;
                finish();
                Intent nextIntent = new Intent(getApplicationContext(), AddFriendActivity.class);
                nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                if (AppConfig.isAddFriend) {
                    AppConfig.isAddFriend = false;
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(nextIntent);
//                overridePendingTransition(R.anim.in_slide_right_left, R.anim.out_slide_right_left);
            }
        });

        navHide = (ImageView) findViewById(R.id.nav_hide);
        navHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConfig.isEditProf = false;
                main.setSelected(false);
//                channel.setSelected(false);
                addfriend.setSelected(false);
                set.setSelected(true);
                finish();
                Intent nextIntent = new Intent(getApplicationContext(), ProfileActivity.class);
                nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(nextIntent);
            }
        });
    }

//    private void startAddressBookActivity() {
//        Intent addressIntent = new Intent(this, AddressBookActivity.class);
//        startActivity(addressIntent);
////        overridePendingTransition(R.anim.in_slide_right_left, R.anim.out_slide_right_left);
//    }

    @Override
    public void onBackPressed() {
        if (AppConfig.isAddFriend && addfriend.isSelected()) {
            finish();
        }
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
