package infusedigital.talkee.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.Base64;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import infusedigital.talkee.R;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.AddressBookData;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.WSMethods;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

public class RegisterCodeActivity extends AppCompatActivity /*implements OTPListener*/ {

    TextView resubmitBtn, sendBtn, tvTitle, tv_title_verification;
    public static EditText codeText;

    String gcmId, username;
    String phoneNum = "", userName, avatarPath, emotiColor, deviceId, regId, firstName, lastName;
    SharedPreferences prefs = null, prefs1;
    private ProgressDialog loadingDialog;
    Context context;
    private boolean isLogin, isRegister;
    private int randomPIN;
    ImageView back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register_code);
        context = RegisterCodeActivity.this;

        prefs = getApplicationContext().getSharedPreferences("RegisterUserInfo", MODE_PRIVATE);
        prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
//        prefs = getApplicationContext().getSharedPreferences("UserInfo",MODE_PRIVATE);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        resubmitBtn = (TextView) findViewById(R.id.resubmit);
        sendBtn = (TextView) findViewById(R.id.sendcode);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tv_title_verification = (TextView) findViewById(R.id.tv_title_verification);
        tv_title_verification.setTypeface(MyApplication.boldFont);
        codeText = (EditText) findViewById(R.id.verifycode);
        codeText.setBackgroundColor(getResources().getColor(R.color.darkPurple));

        phoneNum = getIntent().getStringExtra("phonenum");
        username = getIntent().getStringExtra("username");
        gcmId = getIntent().getStringExtra("gcmid");
        deviceId = getIntent().getStringExtra("deviceid");
        if (getIntent().hasExtra("isLogin") && getIntent().getBooleanExtra("isLogin", false)) {
            isLogin = getIntent().getBooleanExtra("isLogin", false);
            AppConfig.isLogin = isLogin;
//            sendSMS();
        }

        if (getIntent().hasExtra("isRegister") && getIntent().getBooleanExtra("isRegister", false)) {
            isRegister = getIntent().getBooleanExtra("isRegister", false);
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        OtpReader.bind(this, "+447481339104");

        if (getIntent().hasExtra("phone")) {
            tvTitle.setText(Html.fromHtml("We just sent a verification code to <font color='#35eb94'>+" + getIntent().getStringExtra("phone") + "</font>, please enter it below"));
        }

//        if (getIntent().hasExtra("sendSms")) {
//            Toast.makeText(context, "Verification code has been sent to +" + getIntent().getStringExtra("phone") + ". ", Toast.LENGTH_LONG).show();
//            phoneNum = getIntent().getStringExtra("phone");
//            prefs.edit().putString("phonenum", getIntent().getStringExtra("phone")).commit();
//            sendSMS();
//        }

        phoneNum = prefs.getString("phonenum", "null");
        userName = prefs.getString("registerUserName", "null");
        firstName = prefs.getString("registerFirstName", "null");
        lastName = prefs.getString("registerLastName", "null");
        avatarPath = prefs.getString("avatarPath", "null");
        emotiColor = prefs.getString("registerColor", "#2edadc");
        deviceId = prefs1.getString("deviceid", "null");
        regId = prefs1.getString("gcm_regid", "null");
        if (regId.equalsIgnoreCase("null"))
            regId = FirebaseInstanceId.getInstance().getToken();

        resubmitBtn.setText(Html.fromHtml("<u>RESEND CODE</u>"));
        resubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                requestNewCode();
                Toast.makeText(context, "Verification code has been sent again to +" + getIntent().getStringExtra("phone") + ". ", Toast.LENGTH_LONG).show();
                sendSMS();
            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmVerifyCode();
            }
        });
        Toast.makeText(context, "Verification code has been sent to +" + getIntent().getStringExtra("phone") + ". ", Toast.LENGTH_LONG).show();
        registerReceiver(broadcastReceiver, new IntentFilter("talkee.otpreader"));
//        sendSMS();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            String message = b.getString("otp");
            codeText.setText(message);
            confirmVerifyCode();
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent;
        if (AppConfig.isLogin) {
//        if (getIntent().getBooleanExtra("isLogin", false)) {
            mIntent = new Intent(getApplicationContext(), LoginViewActivity.class);
        } else {
            mIntent = new Intent(getApplicationContext(), RegisterActivity.class);
        }
        startActivity(mIntent);
        finish();
    }

    /**
     * generate random number and send to user
     */
    private void sendSMS() {
        randomPIN = (int) (Math.random() * 9000) + 1000;
        AppConfig.OTP = String.valueOf(randomPIN);
        prefs1.edit().putString("code", String.valueOf(randomPIN)).commit();

        final OkHttpClient client = new OkHttpClient();
        String url = "https://api.twilio.com/2010-04-01/Accounts/ACb81b2a66810873a4d2a0823192c9ca77/Messages.json";
        String base64EncodedCredentials = "Basic " +
                Base64.encodeToString(("ACb81b2a66810873a4d2a0823192c9ca77" + ":"
                        + "8471b24341fa35e3b81ac0c0abf9fe8b").getBytes(), Base64.NO_WRAP);
        //TODO hiral's credentials
//        String url = "https://api.twilio.com/2010-04-01/Accounts/ACa833ae73e456f57bbebe3348574a0293/SMS/Messages";
//        String base64EncodedCredentials = "Basic " +
//                Base64.encodeToString(("ACa833ae73e456f57bbebe3348574a0293" + ":"
//                        + "909e53cb6f459830f40236e98bf3b0b8").getBytes(), Base64.NO_WRAP);

        RequestBody body;
//        if (phoneNum.trim().length() > 0) {
        body = new FormBody.Builder()
                .add("From", "+447481339104") // Hiral: 17606992417
                .add("To", "+" + phoneNum)
                .add("Body", "OTP for registration with Talkee: " + randomPIN)
                .build();
//        } else {
//            body = new FormBody.Builder()
//                    .add("From", "+447481339104") // Hiral: 17606992417
//                    .add("To", "+" + getIntent().getStringExtra("phone"))
//                    .add("Body", "OTP for registration with Talkee: " + randomPIN)
//                    .build();
//        }

        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .header("Authorization", base64EncodedCredentials)
                .build();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    Log.d("TAG", "++++++ sendSms: " + response.body().string());
                    //TODO check status here if needed
//            Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
//            startActivity(nextIntent);
//            finish();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("TAG", "++++ error:: " + e.getMessage());
                }
            }
        }).start();

    }
//    private void requestNewCode() {
//        URIBuilder builder = new URIBuilder();
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/login.php")
//                .setParameter("act", "resendverifycode")
//                .setParameter("phonenum", phoneNum);
//
//        String access_url = builder.toString();
//        Log.e("Super", "resent code " + access_url);
//
//        startLoading();
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            endLoading();
//                            codeText.setText("");
//                            Toast.makeText(RegisterCodeActivity.this, "Verify code is sent to your phone", Toast.LENGTH_SHORT).show();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        endLoading();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

    /**
     * confirm verification code
     */
    public void confirmVerifyCode() {
        String verifyCode = codeText.getText().toString();
        System.out.println("++++++++++++++ pref:" + randomPIN + "::" + verifyCode + "::" + prefs1.getString("code", ""));
//        if (TextUtils.isEmpty(verifyCode)) {
//            Toast.makeText(this, "Please insert verify code", Toast.LENGTH_SHORT).show();
//            return;
////        } else if (verifyCode.equals(AppConfig.OTP)) {
//        } else if (verifyCode.equals(prefs1.getString("code", ""))) {
//            prefs = getSharedPreferences("RegisterUserInfo", MODE_PRIVATE);
//            prefs1 = getSharedPreferences("UserInfo", MODE_PRIVATE);
//            if (AppConfig.isLogin) {
////            if (getIntent().getBooleanExtra("isLogin", false)) {
////                setValues();
////                goToHomeScreen();
//                Intent nextIntent = new Intent(getApplicationContext(), EnterUserNameActivity.class);
//                startActivity(nextIntent);
//            } else {
//                register();
//            }
//        } else {
//            Toast.makeText(this, "Verification code is incorrect.", Toast.LENGTH_SHORT).show();
//        }

        if (TextUtils.isEmpty(verifyCode)) {
            Toast.makeText(this, "Please insert verify code", Toast.LENGTH_SHORT).show();
            return;
        } else if (verifyCode.equals(prefs1.getString("code", ""))) {
            if (isLogin) {
                setValues();
                goToHomeScreen();
            } else if (isRegister) {
                prefs.edit().putString("phonenum", phoneNum).commit();
                new GetContactsTask().execute();
                Intent nextIntent = new Intent(getApplicationContext(), EnterUserNameActivity.class);
                nextIntent.putExtra("name", AppConfig.social_name);
                nextIntent.putExtra("profile_image", AppConfig.social_profile_image);
                nextIntent.putExtra("username", AppConfig.social_username);
//                nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(nextIntent);
            } else {
                register();
            }
        } else {
            Toast.makeText(this, "Verification code is incorrect.", Toast.LENGTH_SHORT).show();
        }

//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/login.php")
//                .setParameter("act", "registerinfo")
//                .setParameter("code", verifyCode)
//                .setParameter("phonenum", phoneNum)
//                .setParameter("deviceid", deviceId)
//                .setParameter("gcm_regid", gcmId)
//                .setParameter("username", username);
//
//        String access_url = builder.toString();
//        Log.e("Super", "login with number = " + access_url);
//
//        startLoading();
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            endLoading();
//
//                            String status = response.getString("status");
//                            if (status.equals("failed")) {
//                                Log.e("Super", "status is failed");
//                                Toast.makeText(RegisterCodeActivity.this, "You can't login with this number", Toast.LENGTH_SHORT).show();
//                                return;
//                            }
//
//                            if (status.equals("invalid")) {
//                                Log.e("Super", "status is invalid");
//                                Toast.makeText(RegisterCodeActivity.this, "Verify code is invalid, Check again", Toast.LENGTH_SHORT).show();
//                                return;
//                            }
//
//                            JSONObject data = response.getJSONObject("data");
//                            MyApplication.currentUser.setUser(data);
//                            Log.e("Super", "status is success");
//
//                            if (android.text.TextUtils.isEmpty(phoneNum))
//                                prefs.edit().putString("phonenum", MyApplication.currentUser.name).commit();
//                            else
//                                prefs.edit().putString("phonenum", MyApplication.currentUser.phonenum).commit();
//
//                            prefs.edit().putString("userId", String.valueOf(MyApplication.currentUser.id)).commit();
//                            prefs.edit().putString("photoUrl", MyApplication.currentUser.photo_url).commit();
//                            prefs.edit().putString("name", MyApplication.currentUser.name).commit();
//                            prefs.edit().putString("color", MyApplication.currentUser.emoticol).commit();
//                            prefs.edit().putString("mood", MyApplication.currentUser.mood).commit();
//                            prefs.edit().putString("type", "phone");
//
//                            finish();
//                            Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
//                            startActivity(nextIntent);
//                        } catch (Exception e) {
//                            Log.e("Super", "status is error");
//                            e.printStackTrace();
//                        }
//                        endLoading();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
    }

    /**
     * save login values
     */
    private void setValues() {
        if (AppConfig.USER_DETAILS != null) {
            try {
                prefs1.edit().putString("userId", AppConfig.USER_DETAILS.getString("user_id")).commit();
                prefs1.edit().putString("photoUrl", AppConfig.USER_DETAILS.getString("profile_image")).commit();
                prefs1.edit().putString("name", AppConfig.USER_DETAILS.getString("user_name")).commit();
                prefs1.edit().putString("firstName", AppConfig.USER_DETAILS.getString("first_name")).commit();
                prefs1.edit().putString("lastName", AppConfig.USER_DETAILS.getString("last_name")).commit();
                prefs1.edit().putString("color", AppConfig.USER_DETAILS.getString("color")).commit();
                prefs1.edit().putString("mood", AppConfig.USER_DETAILS.optString("mood")).commit();
                prefs1.edit().putString("phonenum", AppConfig.USER_DETAILS.optString("phonenum")).commit();
                prefs1.edit().putString("gcm_regid", AppConfig.USER_DETAILS.getString("device_token_id")).commit();
//                    prefs1.edit().putString("deviceid", AppConfig.USER_DETAILS.getString("user_id")).commit();
                prefs1.edit().putString("loginstate", "true").commit();
                prefs1.edit().putString("notification", AppConfig.USER_DETAILS.optString("notification")).commit();
//                    prefs1.edit().putString("type", "phone");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void goToHomeScreen() {
        AppConfig.USER_DETAILS = null;
        Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
        nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(nextIntent);
        finish();
    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    /**
     * register device by phone number
     */
    private void register() {
        RequestParams params = new RequestParams();
        params.put("mobile_number", phoneNum);
        params.put("user_name", userName);
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        try {
            if (avatarPath != null) {
                if (avatarPath.length() > 0) {
                    System.out.println("++++ avatar path:" + avatarPath);
                    params.put("profile_image", new File(avatarPath));
                }
            } else {
                if (prefs.getString("avatarPath", "").length() > 0) {
                    System.out.println("++++ avatar path:" + avatarPath);
                    params.put("profile_image", new File(avatarPath));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        params.put("device_token", regId);
        params.put("color", emotiColor);
        params.put("inserted_type", AppConfig.inserted_type);
        params.put("device_type", "0");
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostRegisterByNumberCallComplete, WSMethods.BASE_URL + WSMethods.REGISTRATION_POST, params);
    }

    /**
     * API parsing of registration using number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostRegisterByNumberCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    prefs1.edit().putString("userId", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("photoUrl", USER_DETAILS.getString("profile_image")).commit();
                    prefs1.edit().putString("name", USER_DETAILS.getString("user_name")).commit();
                    prefs1.edit().putString("firstName", USER_DETAILS.getString("first_name")).commit();
                    prefs1.edit().putString("lastName", USER_DETAILS.getString("last_name")).commit();
                    prefs1.edit().putString("color", USER_DETAILS.getString("color")).commit();
                    prefs1.edit().putString("mood", USER_DETAILS.optString("mood")).commit();
                    prefs1.edit().putString("phonenum", USER_DETAILS.optString("mobile_number")).commit();
                    prefs1.edit().putString("gcm_regid", USER_DETAILS.getString("device_token_id")).commit();
//                    prefs1.edit().putString("deviceid", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("loginstate", "true").commit();
                    prefs1.edit().putString("notification", USER_DETAILS.optString("notification")).commit();
//                    prefs1.edit().putString("type", "phone");
                    goToHomeScreen();
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

//    @Override
//    public void otpReceived(String messageText) {
////        Toast.makeText(this, "got sms:" + messageText, Toast.LENGTH_LONG).show();
//        codeText.setText(messageText);
//        confirmVerifyCode();
//    }

    class GetContactsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
//            mProgressBar.setVisibility(View.VISIBLE);
//            startLoading();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getAddressBookInfos();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            endLoading();
//            syncContacts();
//            mProgressBar.setVisibility(View.GONE);
//            System.out.println("++++++++++++ addressBookDatas:" + addressBookDatas.size());
//            if (addressBookDatas.size() > 0) {
//                adapter.setData(addressBookDatas);
//                adapter.notifyDataSetChanged();
//                tvEmptyView.setVisibility(View.GONE);
//            } else {
//                tvEmptyView.setVisibility(View.VISIBLE);
//                contactListView.setEmptyView(tvEmptyView);
//            }
        }
    }

    private void getAddressBookInfos() {
        AppConfig.addressBookDatas.clear();

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String PHOTO_URI = ContactsContract.CommonDataKinds.Phone.PHOTO_URI;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String PHONE_TYPE = ContactsContract.CommonDataKinds.Phone.TYPE;
        int PHONE_TYPEMOBILE = ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Uri photo_uri;
//                Bitmap photoBmp;
                String phone = null;
                String name = "";

                String id = cursor.getString(cursor.getColumnIndex(_ID));
                name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                String url = cursor.getString(cursor.getColumnIndex(PHOTO_URI));
                if (url != null)
                    photo_uri = Uri.parse(url);
                else
                    photo_uri = null;

                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER))) > 0) {
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, new String[]{PHONE_NUMBER},
                            PHONE_CONTACT_ID + " = ? AND " + PHONE_TYPE + " = " + PHONE_TYPEMOBILE, new String[]{id}, null);

                    while (phoneCursor.moveToNext()) {
                        phone = phoneCursor.getString(phoneCursor.getColumnIndex(PHONE_NUMBER));
                        if (phone.trim().length() > 0)
                            phone = getOnlyDigits(phone);
                        System.out.println("++++++++ phone in register code activity:" + name + "::" + phone + ":::" + getOnlyDigits(phone));
                        AppConfig.arrAllContacts.add(getOnlyDigits(phone));
                    }

//                    if ((phone != null || phone.equals("null")) && phone.trim().length() > 0) {
//                        addressBookDatas.add(new AddressBookData(photo_uri, name, getOnlyDigits(phone), false));
//                    } else {
                    AppConfig.addressBookDatas.add(new AddressBookData(photo_uri, name, phone, false));
//                    }
//                    System.out.println("++++++++ phone:" + name + "::" + phone);

                    phoneCursor.close();
                }

                //TODO for sord array as alphabetical order
                Collections.sort(AppConfig.addressBookDatas, new Comparator<AddressBookData>() {
                    public int compare(AddressBookData p1, AddressBookData p2) {
                        return p1.getName().compareTo(p2.getName());
                    }
                });
            }
        }
    }

    /**
     * Get all numbers
     *
     * @param s
     * @return
     */
    public static String getOnlyDigits(String s) {
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }
}