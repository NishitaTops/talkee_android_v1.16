package infusedigital.talkee.activity;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.client.utils.URIBuilder;
import infusedigital.talkee.R;
import infusedigital.talkee.adapters.EmojiAdapter;
import infusedigital.talkee.adapters.MessageListViewAdapterNew;
import infusedigital.talkee.animation.CircleAngleAnimation;
import infusedigital.talkee.animation.CirclePlayAngleAnimation;
import infusedigital.talkee.animation.Circle_Play;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.AllMessages;
import infusedigital.talkee.datamodel.NotificationListData;
import infusedigital.talkee.helper.DBProvider;
import infusedigital.talkee.model.User;
import infusedigital.talkee.utilities.AlertDialogManager;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;

public class MessageScreen extends AppCompatActivity implements Runnable, SensorEventListener {

    static MessageScreen instance;
    public static String sender_id, user_id;
    private String friendAvatar = "", currentAvatar = "", friendUserName = "", currentUserName = "";
    private ImageView imgBtnPlay;
    private int mPosition = 0;
    private Object mPauseLock;
    private boolean isPlay = false;
    private boolean isValid;
    AlertDialogManager alert = new AlertDialogManager();
    private boolean isFirst = true;
    int currentPosition = 0;
    public static boolean isCompleted = false;
    private TextView tvStatusRecording;


    private Tracker mTracker;

    private String outputFile = null;

    NotificationListData friendInfo;

    //    String friendId;
    String userId;

    ImageView backBtn, recordBtn;
    ListView messageView;

    public int maxPlaybackWidthwithDP = 0;
    MessageListViewAdapterNew adapter;

    ArrayList<AllMessages> messageDatas;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    static public MediaRecorder myAudioRecorder;
    private ProgressDialog loadingDialog;
    private boolean recordingState = false;

    ValueAnimator anim;
    private MediaPlayer mPlayer = null;

    private TimerTask timerTask;
    private Timer timer;
    final Handler progressHandler = new Handler();

    ImageView emojiIcon;
    LinearLayout emojiLayout;
    GridView emojiGridView;
    ImageView emojiCancel;

    EmojiAdapter emojiAdapter;
    TypedArray emojiArray;
    Animation bottomTop, topBottom;
    MessageScreen mActivity;
    ProgressBar mPlayProgressBar;
    boolean mPaused = false;
    boolean mFinished = false;

    private SensorManager mSensorManager;
    private Sensor mSensor;
    private AudioManager mAudioManager;
    Context mContext;
    Circle_Play circle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_message_screen);
        mActivity = MessageScreen.this;
        mContext = MessageScreen.this;
        //get maxium pixcel
        int screenDp = getScreenDp();
        maxPlaybackWidthwithDP = screenDp - 160;
        AppConfig.deviceWidth = getWindowManager().getDefaultDisplay().getWidth();

        circle = (Circle_Play) findViewById(R.id.circle);

        setProximitySensor();

        backBtn = (ImageView) findViewById(R.id.messageview_backbtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        user_id = AppConfig.getUserId(MessageScreen.this);
        recordBtn = (ImageView) findViewById(R.id.messageview_recordbtn);

        recordBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (recordingState)
                            return true;

                        recordMessage();

                        break;
                    case MotionEvent.ACTION_UP:
                        if (!recordingState)
                            return true;

                        recordStop();
                        break;
                }
                return true;
            }
        });

        messageView = (ListView) findViewById(R.id.messagelistview);
        messageView.setDivider(null);

//        friendInfo = TabViewActivity.getInstance().friendInfo;
//        Log.e("Super", "friendinfo = " + friendInfo.group_id);
        userId = String.valueOf(MyApplication.currentUser.id);

        messageDatas = new ArrayList<>();
        adapter = new MessageListViewAdapterNew(this, messageDatas);
        messageView.setAdapter(adapter);

        tvStatusRecording = (TextView) findViewById(R.id.tvStatusRecording);
        tvStatusRecording.setTypeface(MyApplication.semiboldFont);

//        messageView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                imgBtnPlay = (ImageView) view.findViewById(R.id.playbtn);
//            }
//        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filters
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getExtras().getString("message");

                    if (message.equals("receiveR")) {
                    } else if (message.equals("receiveM")) {
//                        getNewMessages();
                    } else if (message.equals("updatestatus")) {
                    } else {
                    }
                }
            }
        };

        //TODO visible from layout for show emoji
        emojiIcon = (ImageView) findViewById(R.id.emoticon);
        emojiIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEmojis();
            }
        });

        emojiLayout = (LinearLayout) findViewById(R.id.emojilayout);

        emojiGridView = (GridView) findViewById(R.id.emogigridview);
        emojiCancel = (ImageView) findViewById(R.id.emoji_cancel);

        emojiArray = getResources().obtainTypedArray(R.array.emojies);
        emojiAdapter = new EmojiAdapter(MessageScreen.this, emojiArray);
        emojiGridView.setAdapter(emojiAdapter);

        emojiCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideEmojis();

            }
        });

        bottomTop = AnimationUtils.loadAnimation(MessageScreen.this, R.anim.slide_in_up);
        bottomTop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Log.e("Super", "show emoji");

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                emojiLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        topBottom = AnimationUtils.loadAnimation(MessageScreen.this, R.anim.slide_in_up_reverse);
        topBottom.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                emojiLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mTracker = MyApplication.getInstance().getDefaultTracker();
        instance = this;

        if (getIntent().hasExtra("sender_id")) {
            friendAvatar = getIntent().getStringExtra("friendAvatar");
            sender_id = getIntent().getStringExtra("sender_id");
            AppConfig.sender_id = sender_id;
            currentAvatar = getIntent().getStringExtra("currentAvatar");
            friendUserName = getIntent().getStringExtra("friendUserName");
            currentUserName = getIntent().getStringExtra("currentUserName");
            System.out.println("++++++++++++++ details get:sender_id:" + sender_id + " friendAvatar:" + friendAvatar
                    + " currentAvatar:" + currentAvatar + " friendUserName:" + friendUserName + " currentUserName:" + currentUserName);
            getAllMessages(true);
        }

        registerReceiver(broadcastReceiver, new IntentFilter("talkee.response"));
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            boolean isMessage = b.getBoolean("response");
            if (isMessage) {
                getAllMessages(false);
            }
        }
    };

    @Override
    public void onBackPressed() {
//        Intent returnIntent = new Intent();
//        setResult(Activity.RESULT_OK, returnIntent);
        Intent mIntent = new Intent(MessageScreen.this, TabViewActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(mIntent);
        finish();
//        super.onBackPressed();
    }

    public void setGridHeight() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) emojiLayout.getLayoutParams();
        params.height = dip2px(getScreenDp() * 3 / 7f) + dip2px(50);
        emojiLayout.setLayoutParams(params);
    }

    private void showEmojis() {
        Log.e("Super", "show emoji");
//        emojiLayout.startAnimation(bottomTop);
        emojiLayout.setVisibility(View.VISIBLE);
    }

    private void hideEmojis() {
//        emojiLayout.startAnimation(topBottom);
        emojiLayout.setVisibility(View.GONE);
    }

    private void recordMessage() {
        if (!isPlay) {
            recordingState = true;
            recordBtn.setImageDrawable(getResources().getDrawable(R.drawable.recordgreen));
            outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.m4a";
//            circle.setVisibility(View.VISIBLE);
////            circle.setColor(Color.parseColor("#000000"));
//            circle.setColor(getResources().getColor(R.color.colorAccent));
////            CircleAngleAnimation.recordingProgress(circle, 10500);
//            CirclePlayAngleAnimation.recordingProgress(circle, 10500);

            tvStatusRecording.setVisibility(View.VISIBLE);
            record_start();
        }
    }

    private void recordStop() {
        recordingState = false;
        recordBtn.setImageDrawable(getResources().getDrawable(R.drawable.recordblack));
        tvStatusRecording.setVisibility(View.GONE);
//        circle.setVisibility(View.GONE);
//        CirclePlayAngleAnimation.progressAnim.cancel();
//        CircleAngleAnimation.cancelProgressAnim();
        record_stop();
    }

    private void record_start() {
        playSound("record");
        Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (myAudioRecorder != null) {
                    try {
                        myAudioRecorder.stop();
//                        myAudioRecorder.reset();
                        myAudioRecorder.release();
                        myAudioRecorder = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                myAudioRecorder = new MediaRecorder();
                myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
                myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                myAudioRecorder.setOutputFile(outputFile);

                try {
                    myAudioRecorder.prepare();
                    myAudioRecorder.start();

                    anim = ValueAnimator.ofFloat(0, 1).setDuration(10000);
                    anim.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            recordStop();
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });

                    anim.start();

                } catch (Exception e) {
                    recordingState = false;
                    recordBtn.setImageDrawable(getResources().getDrawable(R.drawable.recordblack));
                    e.printStackTrace();
                }
            }
        };

        mHandler.sendEmptyMessageDelayed(0, 300);
    }

    @Override
    protected void onStart() {
        AppConfig.isMessageScreen = true;
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        AppConfig.isMessageScreen = false;
        super.onDestroy();
    }

    public void record_stop() {
        if (anim != null) {
            anim.removeAllListeners();
            anim.cancel();
        }

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                try {

                    if (myAudioRecorder == null)
                        return;
                    myAudioRecorder.stop();
                    myAudioRecorder.release();
                    myAudioRecorder = null;

                    Handler mHandler = new Handler();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
                            boolean valid = false;
                            try {
                                MediaPlayer mpPlay = new MediaPlayer();
                                mpPlay.setDataSource(path);
                                mpPlay.prepare();
                                int duration = mpPlay.getDuration();
                                System.out.println("++++++++++++++++ duration:" + duration);
                                if (duration < 1000) {
                                    alert.showAlertDialog(MessageScreen.this,
                                            "Recording Error",
                                            "Please record again. You must record more than 1 sec.", false);
                                } else {
                                    valid = true;
                                }
                                if (valid) {
                                    sendAudio();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);
                    //send message to friend
//                    send_voice_message();
//                    sendAudio();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.sendEmptyMessageDelayed(1, 200);
    }

    public void pauseMessage(ProgressBar mProgressBar, String url, final int pos) {
        if (mPlayer != null)
            mPlayer.pause();
        synchronized (mPauseLock) {
            mPaused = true;
        }
    }

    public void playMessage(ProgressBar mProgressBar, String url, final int pos) {
//        System.gc();
        if (mPlayer != null && isPlay) {
            if (mPosition == pos) {
                System.out.println("+++++++ playMessage mPlayer pause:" + url);
                mPlayer.seekTo(currentPosition);
                mPlayer.start();
                synchronized (mPauseLock) {
                    mPaused = false;
                    mPauseLock.notifyAll();
                }
            } else {
                mPlayer.stop();
                mPlayer.release();
                System.out.println("++++ is already play");
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (i != pos)
                        messageDatas.get(i).setPlaying(false);
                }
//            messageDatas.get(mPosition).setProgress(0);
//            messageDatas.get(mPosition).setPlaying(false);
                adapter.setData(messageDatas);
                adapter.notifyDataSetChanged();
                mPlayer = null;
                mPaused = false;
                mFinished = true;
            }
        }
        mPosition = pos;
        System.out.println("+++++++ playMessage:" + url);
        if (mPlayer == null) {
            isCompleted = false;
            isPlay = true;
            mFinished = false;
            System.out.println("+++++++ playMessage mPlayer if:" + url);
//            if (Utility.fileExist(MyApplication.getInstance().getMessageDir() +
//                    url.substring(url.lastIndexOf("/") + 1))) {
            if (isDownloaded(url)) {
                mPlayer = MediaPlayer.create(mActivity, Uri.parse(MyApplication.getInstance().getMessageDir() +
                        url.substring(url.lastIndexOf("/") + 1)));
                System.out.println("++++++++++++ play URI:" + MyApplication.getInstance().getMessageDir() +
                        url.substring(url.lastIndexOf("/") + 1));
            } else {
//                new DownloadTask(url, user_id, sender_id).execute();
                mPlayer = MediaPlayer.create(mActivity, Uri.parse(url));
            }
//            mPlayer = MediaPlayer.create(mActivity, Uri.parse(url));
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.start();
            mPlayProgressBar = mProgressBar;
            mPlayProgressBar.setProgress(0);
            mPlayProgressBar.setMax(mPlayer.getDuration());
            if (mPlayProgressBar != null)
                System.out.println("++++++ mPlayProgressBar");
            new Thread(this).start();
            mPauseLock = new Object();
        }
//        else {
//            System.out.println("+++++++ playMessage mPlayer else:" + url);
//            mPlayer.seekTo(currentPosition);
//            mPlayer.start();
//            synchronized (mPauseLock) {
//                mPaused = false;
//                mPauseLock.notifyAll();
//            }
//        }

//        startTimer(pos);
//        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                mPlayer.start();
//                startTimer(pos);
//            }
//        });
//        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                stopTimer();
//            }
//        });

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                System.out.println("+++++++ audio completed");
                isCompleted = true;
                for (int i = 0; i < messageDatas.size(); i++) {
                    messageDatas.get(i).setProgress(0);
                    messageDatas.get(i).setPlaying(false);
                }
//                messageDatas.get(pos).setPlaying(false);
//                messageDatas.get(pos).setProgress(0);
                adapter.setData(arrMessageDatas);
                adapter.notifyDataSetChanged();
                mPlayer.stop();
                mPlayer.release();
                mPlayer = null;
                mPaused = false;
                mFinished = true;
                isPlay = false;
            }
        });

//        String filepath;
//        filepath = messageDatas.get(pos).messagePath;
//        if (friendInfo.group_id == -1) {
//            Log.e("Super", "filepath = " + filepath);
//        }else{
//            filepath = MyApplication.getInstance().getGroupMessageDir() + friendInfo.group_id + File.separator + messageDatas.get(pos).user.id +
//                    File.separator + url + ".3gp";
//            Log.e("Super", "group filepath = " + filepath);
//        }
//        Log.e("Super", "play path = " + filepath);
//        if (mPlayer != null) {
//            mPlayer.stop();
//            mPlayer.release();
//            mPlayer = null;
//        }
//        try {
//            mPlayer.setDataSource(filepath);
//            mPlayer.prepare();
//            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mp) {
//                    mPlayer.start();
//                    startTimer(pos);
//                }
//            });
//
//            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    stopTimer();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void playSound(String name) {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/" + name);

            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int dip2px(float dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, getResources().getDisplayMetrics());
    }

    public int px2dip(float pxValue) {
        return (int) (pxValue / getResources().getDisplayMetrics().density + 0.5f);
    }

    private int getScreenDp() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;

        int dp = px2dip(width);
        return dp;
    }

    private void printDB() {
        Cursor cursor;
        cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, null, null, null);
        while (cursor.moveToNext()) {
            Log.e("Super", "DBINFo = " + cursor.getString(cursor.getColumnIndex(DBProvider.FRIEND_ID)) + "  " + cursor.getString(cursor.getColumnIndex(DBProvider.GROUP_ID)) +
                    "  " + cursor.getString(cursor.getColumnIndex(DBProvider.STATE)) + "  " + cursor.getString(cursor.getColumnIndex(DBProvider.MESSAGE_URL)));
        }
    }

//    private void getMessageDataFromDB() {
//
//        if (messageDatas != null && messageDatas.size() > 0) {
//            messageDatas.clear();
//        }
//
//        Cursor cursor;
//
//        if (friendInfo.group_id == -1) {
//            cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.FRIEND_ID + "=? AND " + DBProvider.GROUP_ID + "=" + "-1", new String[]{String.valueOf(friendInfo.friend.id)}, null);
//            while (cursor.moveToNext()) {
//                addnewMessageToView(cursor);
//            }
//        } else {
//            cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.GROUP_ID + "=?", new String[]{String.valueOf(friendInfo.group_id)}, null);
//            while (cursor.moveToNext()) {
//                addnewMessageToView(cursor);
//            }
//        }
//
//        adapter.setData(messageDatas);
//        adapter.notifyDataSetChanged();
//
//        messageView.clearFocus();
//        messageView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                messageView.setSelection(0);
//            }
//        }, 300);
//    }

    private String getPlayTime(String messagePath) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        File messageFile = new File(messagePath);

        if (messageFile != null && messageFile.exists()) {
            String duration = null;

            try {
                retriever.setDataSource(messagePath);
                duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
//            int result = Integer.parseInt(duration) / 1000;
            return duration;
        } else {
            return null;
        }
    }

    private void deleteEarlierData(String createdTime) {
        Cursor cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.FILE_PATH + "=?", new String[]{createdTime}, null);

        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(DBProvider._ID));
            String groupid = cursor.getString(cursor.getColumnIndex(DBProvider.GROUP_ID));
            String friendid = cursor.getString(cursor.getColumnIndex(DBProvider.FRIEND_ID));

            String message_path = "";

            if (groupid.equals("-1")) {
                message_path = MyApplication.getInstance().getMessageDir() + friendid + File.separator + createdTime + ".m4a";
            } else {
                message_path = MyApplication.getInstance().getGroupMessageDir() + groupid + File.separator + friendid +
                        File.separator + createdTime + ".m4a";
            }

            File file = new File(message_path);

            if (file != null && file.exists()) {
                file.delete();
            }

            getContentResolver().delete(DBProvider.CONTENT_URI, DBProvider._ID + "=?", new String[]{String.valueOf(id)});
        }
    }

//    private void addnewMessageToView(Cursor cursor) {
//
//        String state = cursor.getString(cursor.getColumnIndex(DBProvider.STATE));
//        String message_path, total_length = "", user_type;
//        String microseconds = cursor.getString(cursor.getColumnIndex(DBProvider.FILE_PATH));
//
//        String groupid = cursor.getString(cursor.getColumnIndex(DBProvider.GROUP_ID));
//        String friendid = cursor.getString(cursor.getColumnIndex(DBProvider.FRIEND_ID));
//
//        String messageurl = cursor.getString(cursor.getColumnIndex(DBProvider.MESSAGE_URL));
//
//        if (!TextUtils.isEmpty(messageurl) && !messageurl.contains("upload")) {
//            //this is emoji
//            message_path = messageurl;
//        } else {
//            if (groupid.equals("-1")) {
//                message_path = MyApplication.getInstance().getMessageDir() + friendid + File.separator + microseconds + ".3gp";
//            } else
//                message_path = MyApplication.getInstance().getGroupMessageDir() + groupid + File.separator + friendid + File.separator + microseconds + ".3gp";
//
//            total_length = getPlayTime(message_path);
//
//            if (total_length == null) {
//                int id = cursor.getInt(cursor.getColumnIndex(DBProvider._ID));
//                getContentResolver().delete(DBProvider.CONTENT_URI, DBProvider._ID + "=?", new String[]{String.valueOf(id)});
//                return;
//            }
//        }
//
//        if (state.equals("send")) {
//            //this is messsage what user sent
//            user_type = "user";
//            if (messageDatas.size() == 10) {
//                deleteEarlierData(messageDatas.get(messageDatas.size() - 1).createdTime);
//                messageDatas.remove(messageDatas.size() - 1);
//            }
//
//            if (!TextUtils.isEmpty(messageurl) && !messageurl.contains("upload")) {
//                messageDatas.add(0, new MessageData(MyApplication.currentUser, message_path, microseconds, "", user_type, 0, true));
//            } else
//                messageDatas.add(0, new MessageData(MyApplication.currentUser, message_path, microseconds, total_length, user_type, 0, false));
//        } else {
//
//            if (messageDatas.size() == 10) {
//                deleteEarlierData(messageDatas.get(messageDatas.size() - 1).createdTime);
//                messageDatas.remove(messageDatas.size() - 1);
//            }
//            //this is message what user received: friend sent
//            user_type = "friend";
//
//            if (!TextUtils.isEmpty(messageurl) && !messageurl.contains("upload")) {
//                if (groupid.equals("-1")) {
//                    messageDatas.add(0, new MessageData(friendInfo.friend, message_path, microseconds, "", user_type, 0, true));
//                } else {
//                    for (int j = 0; j < friendInfo.groupMembers.size(); j++) {
//                        User groupuser = friendInfo.groupMembers.get(j);
//                        if (friendid.equals(String.valueOf(groupuser.id))) {
//                            messageDatas.add(0, new MessageData(groupuser, message_path, microseconds, "", user_type, 0, true));
//                            break;
//                        }
//                    }
//                }
//            } else {
//                if (groupid.equals("-1")) {
//                    messageDatas.add(0, new MessageData(friendInfo.friend, message_path, microseconds, total_length, user_type, 0, false));
//                } else {
//                    for (int j = 0; j < friendInfo.groupMembers.size(); j++) {
//                        User groupuser = friendInfo.groupMembers.get(j);
//                        if (friendid.equals(String.valueOf(groupuser.id))) {
//                            messageDatas.add(0, new MessageData(groupuser, message_path, microseconds, total_length, user_type, 0, false));
//                            break;
//                        }
//                    }
//                }
//            }
//        }
//    }

//    public void getNewMessages() {
//        if (friendInfo.group_id == -1) {
//            URIBuilder builder = new URIBuilder();
//
//            builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
//                    .setParameter("act", "getfriendmessages")
//                    .setParameter("user_id", userId)
//                    .setParameter("friend_id", String.valueOf(friendInfo.friend.id));
//
//            String access_url = builder.toString();
//
//            JsonObjectRequest req = new JsonObjectRequest(access_url,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                JSONArray data;
//
//                                data = response.getJSONArray("data");
//
//                                for (int i = 0; i < data.length(); i++) {
//                                    String message_url = data.getString(i);
//
//                                    if (!message_url.contains("upload")) {
//                                        // this is for emonji
//                                        addReceivedEmoji(String.valueOf(friendInfo.friend.id), "-1", message_url);
//                                        continue;
//                                    }
//
//                                    //download and save to db
//                                    if (!isDownloaded(message_url)) {
//                                        //Download message and save db.
//                                        new DownloadTask(message_url, String.valueOf(friendInfo.friend.id), "-1").execute();
//                                    }
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                }
//            });
//
//            MyApplication.getInstance().addToRequestQueue(req);
//        } else {
//            URIBuilder builder = new URIBuilder();
//
//            builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/group.php")
//                    .setParameter("act", "getgroupmessages")
//                    .setParameter("user_id", userId)
//                    .setParameter("group_id", String.valueOf(friendInfo.group_id));
//
//            String access_url = builder.toString();
//
//            JsonObjectRequest req = new JsonObjectRequest(access_url,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                JSONArray data;
//                                JSONArray senduser;
//
//                                data = response.getJSONArray("data");
//                                senduser = response.getJSONArray("senduser");
//
//                                for (int i = 0; i < data.length(); i++) {
//                                    String message_url = data.getString(i);
//                                    String friendid = senduser.getString(i);
//
//                                    if (!message_url.contains("upload")) {
//                                        addReceivedEmoji(friendid, String.valueOf(friendInfo.group_id), message_url);
//                                        continue;
//                                    }
//
//                                    //download and save to db
//                                    if (!isDownloaded(message_url)) {
//                                        //Download message and save db.
//                                        new DownloadTask(message_url, friendid, String.valueOf(friendInfo.group_id)).execute();
//                                    }
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                }
//            });
//
//            MyApplication.getInstance().addToRequestQueue(req);
//        }
//    }

    private String getMessageurlForEmoji(String filename, int position) {
        String result = "";
        result = filename + ":" + position;
        return result;
    }

//    public void sendEmoji(final int position) {
//
//        mTracker.send(new HitBuilders.EventBuilder()
//                .setCategory("Action")
//                .setAction("Message")
//                .build());
//
//        addSendEmoji(position);
//        hideEmojis();
//    }

//    private void addSendEmoji(int position) {
//        ContentValues values = new ContentValues();
//
//        String friendID = "";
//        if (friendInfo.group_id == -1)
//            friendID = String.valueOf(friendInfo.friend.id);
//        else
//            friendID = String.valueOf(MyApplication.currentUser.id);
//
//        String filename = String.valueOf(System.currentTimeMillis());
//
//        //set message_url to determine emoji position
//        String messageUrl = getMessageurlForEmoji(filename, position);
//
//        values.put(DBProvider.FRIEND_ID, friendID);
//        values.put(DBProvider.MESSAGE_URL, messageUrl);
//        values.put(DBProvider.GROUP_ID, friendInfo.group_id);
//        values.put(DBProvider.STATE, "send");
//        values.put(DBProvider.FILE_PATH, filename);
//
//        getContentResolver().insert(DBProvider.CONTENT_URI, values);
//
//        Cursor cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.FILE_PATH + "=?", new String[]{filename}, null);
//
//        if (cursor.moveToNext())
//            addnewMessageToView(cursor);
//
//        //reset chat list view
//        adapter.setData(messageDatas);
//        adapter.notifyDataSetChanged();
//
//        messageView.clearFocus();
//        messageView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                messageView.setSelection(0);
//            }
//        }, 200);
//
//        //send emoji to server
//        if (friendInfo.group_id == -1) {
//            sendEmojitoFriend(friendInfo.friend, messageUrl);
//        } else {
//            sendEmojitoGroup(friendInfo.group_id, messageUrl);
//        }
//    }

    private void sendEmojitoFriend(User friend, String messageUrl) {

        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/send_message.php")
                .setParameter("userId", String.valueOf(MyApplication.currentUser.id))
                .setParameter("regId", friend.gcm_id)
                .setParameter("friendId", String.valueOf(friend.id))
                .setParameter("requestType", "5")
                .setParameter("messageurl", messageUrl);

        String access_url = builder.toString();
        Log.e("Super", "emoji to friend access url = " + access_url);

        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

    private void sendEmojitoGroup(int groupid, String message_url) {
        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/group.php")
                .setParameter("act", "sendemoji")
                .setParameter("user_id", String.valueOf(MyApplication.currentUser.id))
                .setParameter("group_id", String.valueOf(groupid))
                .setParameter("messageurl", message_url);

        String access_url = builder.toString();
        Log.e("Super", "emoji to group access url = " + access_url);
        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

//    private void addReceivedEmoji(String friendId, String groupId, String message_url) {
//        ContentValues values = new ContentValues();
//        values.put(DBProvider.FRIEND_ID, friendId);
//        values.put(DBProvider.GROUP_ID, groupId);
//        values.put(DBProvider.MESSAGE_URL, message_url);
//        values.put(DBProvider.STATE, "receive");
//        values.put(DBProvider.FILE_PATH, String.valueOf(System.currentTimeMillis()));
//
//        getContentResolver().insert(DBProvider.CONTENT_URI, values);
//        Cursor cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{message_url}, null);
//
//        if (cursor.moveToNext())
//            addnewMessageToView(cursor);
//
//        //reset chat list view
//        adapter.setData(messageDatas);
//        adapter.notifyDataSetChanged();
//
//        messageView.clearFocus();
//        messageView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                messageView.setSelection(0);
//            }
//        }, 300);
//
//        //set as read state
//        setReadEmoji(friendId, message_url);
//    }

    private void setReadMessage(String message_url) {
        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
                .setParameter("act", "setreadmessage")
                .setParameter("user_id", userId)
                .setParameter("friend_id", String.valueOf(getFriendId(message_url)))
                .setParameter("message_url", message_url);

        String access_url = builder.toString();
        Log.e("Super", "set as read message = " + access_url);

        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

//    class DownloadTask extends AsyncTask<Void, Void, Boolean> {
//        private String url;
//        private String friend_Id;
//        private String group_id;
//        private String filename;
//
//        public DownloadTask(String url, String id, String groupId) {
//            this.url = url;
//            friend_Id = id;
//            group_id = groupId;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            startLoading();
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... arg0) {
//            filename = String.valueOf(System.currentTimeMillis());
//            boolean result = DownloadFile(Config.server + url, friend_Id, group_id, filename + ".3gp");
//            return result;
//        }
//
//        @Override
//        protected void onPostExecute(Boolean aVoid) {
//            super.onPostExecute(aVoid);
//
//            endLoading();
//
//            if (aVoid) {
//                //insert info to DB
//                ContentValues values = new ContentValues();
//                values.put(DBProvider.FRIEND_ID, friend_Id);
//                values.put(DBProvider.GROUP_ID, group_id);
//                values.put(DBProvider.MESSAGE_URL, url);
//                values.put(DBProvider.STATE, "receive");
//                values.put(DBProvider.FILE_PATH, filename);
//
//                getContentResolver().insert(DBProvider.CONTENT_URI, values);
//
//                Cursor cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{url}, null);
//
//                if (cursor.moveToNext())
//                    addnewMessageToView(cursor);
//
//
//                //reset chat list view
//                adapter.setData(messageDatas);
//                adapter.notifyDataSetChanged();
//
//                messageView.clearFocus();
//                messageView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        messageView.setSelection(0);
//                    }
//                }, 300);
//
//                //set as read state
//                setReadMessage(url);
//            }
//        }
//    }

    private void setReadEmoji(String friendId, String messageurl) {
        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
                .setParameter("act", "setreadmessage")
                .setParameter("user_id", String.valueOf(MyApplication.currentUser.id))
                .setParameter("friend_id", friendId)
                .setParameter("message_url", messageurl);

        String access_url = builder.toString();

        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

    public boolean DownloadFile(String fileURL, String friendid, String groupid, String fileName) {
        try {
////            String RootDir = Environment.getExternalStorageDirectory() + File.separator;
//            String RootDir = getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator + friendid;
//
//            File RootFile = new File(RootDir);
//
//            if (!RootFile.exists())
//                RootFile.mkdir();

            String RootDir;
            File RootFile;

            if (groupid.equals("-1")) {
                RootDir = MyApplication.getInstance().getMessageDir() + friendid;
                RootFile = new File(RootDir);

                if (!RootFile.exists())
                    RootFile.mkdir();
            } else {
                RootDir = MyApplication.getInstance().getGroupMessageDir() + groupid;
                RootFile = new File(RootDir);

                if (!RootFile.exists())
                    RootFile.mkdir();

                String subDir = RootDir + File.separator + friendid;
                RootFile = new File(subDir);
                if (!RootFile.exists())
                    RootFile.mkdir();
            }

            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            FileOutputStream f = new FileOutputStream(new File(RootFile, fileName));

            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;

            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    private void sendAudio() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.m4a";
        boolean valid = false;

        MediaPlayer mp = new MediaPlayer(); //.create(SendViewActivity.this, Uri.parse(path));
        try {
            mp.setDataSource(path);
            mp.prepare();
            int duration = mp.getDuration();
            mp.release();
            if (duration < 2) {
                Toast.makeText(this, "Please record again. You must record more than 1 sec.", Toast.LENGTH_SHORT).show();
            } else {
                valid = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//                if (valid) {
//                    for (int i = 0; i < listData.size(); i++) {
//                        if (listData.get(i).isSelected) {
//                            SearchUserData data = listData.get(i);
//                            send_voice_message(data);
//                        }
//                    }
//                }
//        if (valid) {
//            if (isValid) {
//                isValid = false;
        sendAudioMessage();
//            } else {
//                isValid = false;
//                alert.showAlertDialog(MessageScreen.this,
//                        "Recording Error",
//                        "Please record again. You must record more than 1 sec.", false);
//            }
//        }
    }

    /**
     * send audio
     */
    private void sendAudioMessage() {
        ArrayList<String> userIds = new ArrayList<String>();
        userIds.add(sender_id);

        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(mActivity));
        params.put("reciver_id", userIds.toString());
        try {
            params.put("audio", new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.m4a"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (isFirst) {
            isFirst = false;
            startLoading();
        }
        isLoading = true;
        new MyLoopJPost(mActivity, "", onLoopJPostSendAudioMessageCallComplete, WSMethods.BASE_URL + WSMethods.SEND_MESSAGE_POST, params);
    }

    private boolean isLoading = false;
    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSendAudioMessageCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
                    getAllMessages(false);
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(mContext);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

//    private void send_voice_message() {
//
//        String resourcePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp";
//        File file = new File(resourcePath);
//        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "recording.3gp";
//        boolean valid = false;
//
//        MediaPlayer mp = new MediaPlayer(); //.create(SendViewActivity.this, Uri.parse(path));
//        try {
//            mp.setDataSource(path);
//            mp.prepare();
//            int duration = mp.getDuration();
//            mp.release();
//            if (duration < 2) {
//                Toast.makeText(this, "Please record again. You must record more than 1 sec.", Toast.LENGTH_SHORT).show();
//            } else {
//                valid = true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (!valid)
//            return;
//
//        mTracker.send(new HitBuilders.EventBuilder()
//                .setCategory("Action")
//                .setAction("Message")
//                .build());
//
//        if (friendInfo.group_id == -1) {
//            String serverUri = "http://gettalkee.com/talkee/send_message.php?";
//
//            Log.e("Super", "send_voice_message");
//
//            new UploadFileAsync(this, serverUri + "regId=" + friendInfo.friend.gcm_id + "&userId=" + MyApplication.currentUser.id + "&friendId=" +
//                    friendInfo.friend.id + "&requestType=2",
//                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp", false).execute("");
//        } else {
//            String serverUri = "http://gettalkee.com/talkee/group.php?";
//            new UploadFileAsync(this, serverUri + "act=" + "sendmessage" + "&user_id=" + MyApplication.currentUser.id + "&group_id=" + friendInfo.group_id,
//                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp", false).execute("");
//        }
//
//        new CopyTask(Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp").execute();
//
//
////
////        String serverUri = "http://gettalkee.com/talkee/send_message.php?";
////
////        Log.d ("******", Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp");
////
////        new UploadFileAsync(serverUri + "regId=" + reg_ids + "&userId=" + userId + "&friendId=" + friendid + "&requestType=2",
////                Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp").execute("");
////
////        new CopyTask(Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp", friendid).execute();
//    }

//    class CopyTask extends AsyncTask<Void, Void, Boolean> {
//        private String src_path;
//        //        private String friend_Id;
//        private String filename;
//
//        public CopyTask(String path) {
//            this.src_path = path;
////            friend_Id = friendid;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            startLoading();
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... arg0) {
//            filename = String.valueOf(System.currentTimeMillis());
//            File src = new File(src_path);
//
//            String directory;
//
//            if (friendInfo.group_id == -1) {
//                directory = MyApplication.getInstance().getMessageDir() + friendInfo.friend.id;
//            } else {
//                directory = MyApplication.getInstance().getGroupMessageDir() + friendInfo.group_id + File.separator + MyApplication.currentUser.id;
//            }
////            String friendDirectory = getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator + friendId;
//            File file = new File(directory);
////           Log.e("Super", "frienddirectory = " + friendDirectory);
//
//            if (!file.exists())
//                file.mkdir();
//
////            String destpath = getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator + friendId +
////                    File.separator + filename + ".3gp";
//            String destpath = directory + File.separator + filename + ".3gp";
////            String destpath = getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator + friend_Id +
////                    File.separator + filename + ".3gp";
//            File dest = new File(destpath);
//            boolean result = CopyFile(src, dest);
//            return result;
//        }
//
//        @Override
//        protected void onPostExecute(Boolean aVoid) {
//            super.onPostExecute(aVoid);
//
//            endLoading();
//
//            if (aVoid) {
//                ContentValues values = new ContentValues();
//
//                String friendID = "";
//                if (friendInfo.group_id == -1)
//                    friendID = String.valueOf(friendInfo.friend.id);
//                else
//                    friendID = String.valueOf(MyApplication.currentUser.id);
//
//                values.put(DBProvider.FRIEND_ID, friendID);
//                values.put(DBProvider.MESSAGE_URL, "");
//                values.put(DBProvider.GROUP_ID, friendInfo.group_id);
//                values.put(DBProvider.STATE, "send");
//                values.put(DBProvider.FILE_PATH, filename);
//
//                getContentResolver().insert(DBProvider.CONTENT_URI, values);
//
//                Cursor cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.FILE_PATH + "=?", new String[]{filename}, null);
//
//                if (cursor.moveToNext())
//                    addnewMessageToView(cursor);
//
//
//                //reset chat list view
//                adapter.setData(messageDatas);
//                adapter.notifyDataSetChanged();
//
//                messageView.clearFocus();
//                messageView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        messageView.setSelection(0);
//                    }
//                }, 300);
//            }
//        }
//    }

    private boolean CopyFile(File src, File dest) {
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private int getFriendId(String messageurl) {
        Cursor cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{messageurl}, null);
        int result;

        if (cursor.moveToNext()) {
            result = cursor.getInt(cursor.getColumnIndex(DBProvider.FRIEND_ID));
        } else
            result = 0;

        return result;
    }

    private void initializeTimerTask(final int pos) {
        timerTask = new TimerTask() {
            @Override
            public void run() {
                progressHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mPlayer != null) {
                            int mCurrentPosition = mPlayer.getCurrentPosition();
                            messageDatas.get(pos).setProgress(mCurrentPosition);

                            adapter.setData(messageDatas);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        };
    }

    private void startTimer(int pos) {
        stopTimer();
        timer = new Timer();
        initializeTimerTask(pos);
        timer.schedule(timerTask, 0, 100);
    }

    private void stopTimer() {
        for (int i = 0; i < messageDatas.size(); i++) {
            messageDatas.get(i).setProgress(0);
        }
        adapter.setData(messageDatas);
        adapter.notifyDataSetChanged();

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_FASTEST);
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        //get message data from db and set adapter again.
        emojiLayout.setVisibility(View.GONE);
//        printDB();
//        getMessageDataFromDB();
//        getNewMessages();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTimer();
        mAudioManager.setMode(AudioManager.MODE_NORMAL);
        try {
            mSensorManager.unregisterListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("+++++++++ onPause");
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;

            messageDatas.get(mPosition).setProgress(0);
            messageDatas.get(mPosition).setPlaying(false);
            adapter.setData(messageDatas);
            adapter.notifyDataSetChanged();
            mPaused = false;
            mFinished = true;
            isPlay = false;
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.in_slide_left_right, R.anim.fade_out);
    }

    /**
     * API call for getting all messages from the server
     *
     * @param showProgress
     */
    public void getAllMessages(boolean showProgress) {
        RequestParams params = new RequestParams();
        params.put("user_id", user_id);
        params.put("sender_id", sender_id);
        if (showProgress)
            startLoading();
        new MyLoopJPost(MessageScreen.this, "", onLoopJPostGetAllMessagesCallComplete, WSMethods.BASE_URL + WSMethods.FRIENDS_MESSAGE_LIST_POST, params);
    }

    private ArrayList<AllMessages> arrMessageDatas;
    /**
     * API parsing of all messages of the user
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostGetAllMessagesCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
//                if (swipeContainer.isRefreshing())
//                    swipeContainer.setRefreshing(false);

                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    JSONArray MESSAGE_THREAD = jobj.getJSONArray("MESSAGE_THREAD");
                    if (messageDatas.size() > 0)
                        messageDatas.clear();

//                    String friendID = "";
                    for (int i = 0; i < MESSAGE_THREAD.length(); i++) {
                        JSONObject obj = MESSAGE_THREAD.getJSONObject(i);
                        messageDatas.add(new AllMessages(obj.getString("read_status"),
                                obj.getString("audoi_msg"),
                                obj.getString("duration"),
                                obj.getString("AI"),
                                obj.getString("friendId"),
                                obj.getString("userId"),
                                friendAvatar,
                                currentAvatar,
                                friendUserName,
                                currentUserName,
                                0,
                                false));

//                        if (user_id != obj.getString("userId"))
//                            friendID = obj.getString("userId");
//                        else
//                            friendID = obj.getString("friendId");
                        //TODO check in db if not available will download the audio
                        if (!DBProvider.isExists(obj.getString("audoi_msg")))
                            new DownloadTask(obj.getString("audoi_msg"), obj.getString("userId"), obj.getString("friendId")).execute();
                    }

                    if (messageDatas.size() > 0) {
                        arrMessageDatas = messageDatas;
                        adapter.setData(messageDatas);
                        adapter.notifyDataSetChanged();
                        getAllAudioOfUser(mContext, messageDatas, sender_id); // get all record for delete older
                    }

                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(mContext);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Get all records for delete other audios
     *
     * @param messageDatas
     * @param friendID
     */
    private void getAllAudioOfUser(Context mContext, ArrayList<AllMessages> messageDatas, String friendID) {
        ArrayList<String> mArray = DBProvider.getAllAudioOfUser(mContext, friendID);
        if (mArray.size() > 0) {
            for (int i = 0; i < mArray.size(); i++) {
                if (messageDatas.contains(mArray.get(i))) {
                    System.out.println("+++++++++++++++++ contains:" + mArray.get(i));
                    DBProvider.deleteRowAndFile(mContext, mArray.get(i));
                }
            }
        }
    }

    @Override
    public void run() {
        if (mFinished) return;
        while (!mFinished) {
            // Do stuff.
            int total = 0;
            if (mPlayer != null) {
                try {
                    total = mPlayer.getDuration();
                    while (mPlayer != null && currentPosition < total) {
                        try {
                            Thread.sleep(200);
                            currentPosition = mPlayer.getCurrentPosition();
                        } catch (InterruptedException e) {
                            return;
                        } catch (Exception e) {
                            return;
                        }
                        messageDatas.get(mPosition).setProgress(currentPosition);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.setData(messageDatas);
                                adapter.notifyDataSetChanged();
                            }
                        });
//                System.out.println("++++++ progress_green:" + currentPosition);
//            if (mPlayProgressBar != null) {
//                System.out.println("++++++ progress_green in :" + currentPosition);
//                mPlayProgressBar.setProgress(currentPosition);
//            }
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.setData(messageDatas);
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }
            synchronized (mPauseLock) {
                while (mPaused) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.setData(messageDatas);
                            adapter.notifyDataSetChanged();
                        }
                    });

                    try {
                        mPauseLock.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    private void setProximitySensor() {
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        mAudioManager.setMode(AudioManager.MODE_NORMAL);
//        mAudioManager.setSpeakerphoneOn(true);
        if (mSensor == null) {
            Toast.makeText(this, "No Proximity Sensor!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
//        Log.e("distance", "++++++++++++++++++++ " + String.valueOf(sensorEvent.values[0]));
//        Log.e("MaximumRange", "++++++++++++++++++++ " + String.valueOf(mSensor.getMaximumRange()));

        if (mAudioManager.isWiredHeadsetOn()) {
//            Log.e("distance", "++++++++++++++++++++ " + "isWiredHeadsetOn : true");
        } else {
//            Log.e("distance", "++++++++++++++++++++ " + "isWiredHeadsetOn : false");
            if (sensorEvent.values[0] < mSensor.getMaximumRange()) {
                mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
                mAudioManager.setSpeakerphoneOn(false);
//                Log.e("distance", "++++++++++++++++++++ " + "setSpeakerphoneOn : false");
            } else {
                mAudioManager.setMode(AudioManager.MODE_NORMAL);
                mAudioManager.setSpeakerphoneOn(true);
//                Log.e("distance", "++++++++++++++++++++ " + "setSpeakerphoneOn : true");
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    /**
     * For downloading the audio and save in database.
     */
    class DownloadTask extends AsyncTask<Void, Void, Boolean> {
        private String downloadURL;
        private String userId;
        private String friendId;

        public DownloadTask(String downloadURL, String userId, String friendId) {
            this.downloadURL = downloadURL;
            this.userId = userId;
            this.friendId = friendId;
            System.out.println("++++++++++++ going to download:" + downloadURL + " --- " + userId + " --- " + friendId);
        }

        @Override
        protected void onPreExecute() {
//            startLoading();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            boolean result = DownloadFile(downloadURL);
            return result;
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);

//            endLoading();

            if (aVoid) {
                System.out.println("+++++++++++++++ download competed:" + aVoid);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String millisInString = dateFormat.format(new Date());

                //insert info to DB
                ContentValues values = new ContentValues();
                values.put(DBProvider.FRIEND_ID, friendId);
                values.put(DBProvider.USER_ID, userId);
                values.put(DBProvider.MESSAGE_URL, downloadURL);
                values.put(DBProvider.FILENAME, downloadURL.substring(downloadURL.lastIndexOf("/") + 1));
                values.put(DBProvider.CREATED, millisInString);

                System.out.println("+++++++++++++++ download competed: url:" + downloadURL + "\n filename:" +
                        downloadURL.substring(downloadURL.lastIndexOf("/") + 1) + "\ntime:" + millisInString +
                        "\nfriendId:" + friendId + "\nuserId:" + userId);

//                if (!DBProvider.isExists(downloadURL))
                getContentResolver().insert(DBProvider.CONTENT_URI, values);

                System.out.println("++++++++++++++ isDownloaded:" + downloadURL + "::::" + isDownloaded(downloadURL));
//                Cursor cursor = context.getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{url}, null);
//                if (cursor.moveToNext())
//                    addnewMessageToView(cursor);
            }
        }
    }

    private boolean isDownloaded(String messageurl) {
        Cursor cursor = getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{messageurl}, null);

        if (cursor.moveToNext()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean DownloadFile(String fileURL) {
        try {
            String RootDir;
            File RootFile;

            RootDir = MyApplication.getInstance().getMessageDir();
            RootFile = new File(RootDir);

            if (!RootFile.exists())
                RootFile.mkdir();

            String subDir = RootDir + File.separator;
            RootFile = new File(subDir);
            if (!RootFile.exists())
                RootFile.mkdir();

            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            FileOutputStream f = new FileOutputStream(new File(RootFile, fileURL.substring(fileURL.lastIndexOf("/") + 1)));

            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;

            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}