package infusedigital.talkee.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.loopj.android.http.Base64;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import infusedigital.talkee.R;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Iso2Phone;
import infusedigital.talkee.utilities.WSMethods;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ReigsterWithPhoneActivity extends AppCompatActivity {

    private LinearLayout registerWithPhone, register_skip;
    private ImageView back;
    private SharedPreferences prefs = null;
    private EditText phoneNum;
    private TextView registerBtnTitle, tvTerms, tv_title;
    private CountryCodePicker CCP;
    Context context;
    private ProgressDialog loadingDialog;
    private String countryphoneNumber;
    private String countryCode;
    private boolean isApiCalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_reigster_with_phone);
        prefs = getApplicationContext().getSharedPreferences("RegisterUserInfo", MODE_PRIVATE);
        context = ReigsterWithPhoneActivity.this;

        register_skip = (LinearLayout) findViewById(R.id.register_skip);
        registerWithPhone = (LinearLayout) findViewById(R.id.register_with_phone_num);

        phoneNum = (EditText) findViewById(R.id.registerphonenumberTxt);
        phoneNum.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                return false;
            }
        });

        CCP = (CountryCodePicker) findViewById(R.id.ccp);

        TelephonyManager telMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String simContryiso = telMgr.getSimCountryIso();
        String indicative = Iso2Phone.getPhone(simContryiso);

        System.out.println("++++++++++ indicative" + indicative);
        if (indicative != null) {
            int code = Integer.parseInt(indicative.replace("+", ""));
            CCP.setDefaultCountryUsingPhoneCode(code);
            CCP.setCountryForPhoneCode(code);
        }

//        TelephonyManager tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
//        String countryCodeValue = tm.getNetworkCountryIso();
//        System.out.println("++++++++++++ countryCodeValue:" + countryCodeValue);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setTypeface(MyApplication.boldFont);
        if (AppConfig.inserted_type.equals("1")) {//Phone
            tv_title.setText("Enter your number");
            register_skip.setVisibility(View.GONE);
        } else { //FB/Twitter
            tv_title.setText("Find friends using number");
            register_skip.setVisibility(View.VISIBLE);
        }

        tvTerms = (TextView) findViewById(R.id.tvTerms);
        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(context, TermsAndConditionsAct.class);
                startActivity(mIntent);
            }
        });

        register_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextIntent = new Intent(getApplicationContext(), EnterUserNameActivity.class);
                nextIntent.putExtra("name", AppConfig.social_name);
                nextIntent.putExtra("profile_image", AppConfig.social_profile_image);
                nextIntent.putExtra("username", AppConfig.social_username);
                startActivity(nextIntent);
            }
        });

        registerWithPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = phoneNum.getText().toString();
                if (TextUtils.isEmpty(phoneNumber)) {
                    Toast.makeText(context, "Please enter mobile number.", Toast.LENGTH_LONG).show();
                    return;
                }

                countryCode = CCP.getSelectedCountryCode();
                countryphoneNumber = countryCode + phoneNumber;
                prefs.edit().putString("phonenum", countryphoneNumber).commit();
                if (AppConfig.inserted_type.equals("1") ||
                        AppConfig.inserted_type.equals("3") ||
                        AppConfig.inserted_type.equals("4")) {
                    if (!isApiCalled)
                        checkNumber(countryphoneNumber);
                } else {
                    final Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                sendSMS();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }
            }
        });

        back = (ImageView) findViewById(R.id.register_back_btn);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReigsterWithPhoneActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // set font type, show input pad.
        registerBtnTitle = (TextView) findViewById(R.id.registerbtntitle);
        registerBtnTitle.setTypeface(MyApplication.semiboldFont);
        phoneNum.setTypeface(MyApplication.lightFont);
        CCP.setTypeFace(MyApplication.semiboldFont);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ReigsterWithPhoneActivity.this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * API call for check the number is exist or not
     *
     * @param countryphoneNumber
     */
    private void checkNumber(String countryphoneNumber) {
        isApiCalled = true;
        RequestParams params = new RequestParams();
        params.put("number", countryphoneNumber);
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostCheckNumberCallComplete, WSMethods.BASE_URL + WSMethods.CHECK_NUMBER_POST, params);
    }

    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostCheckNumberCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                isApiCalled = false;
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) { // number is not exist
                    final Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                sendSMS();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
//                    Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
//                    nextIntent.putExtra("phone", countryphoneNumber);
//                    nextIntent.putExtra("isLogin", false);
//                    startActivity(nextIntent);

//                    Intent nextIntent = new Intent(getApplicationContext(), EnterUserNameActivity.class);
//                    startActivity(nextIntent);

////                    finish();
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * generate random number and send to user
     */
    private void sendSMS() {
        final int randomPIN = (int) (Math.random() * 9000) + 1000;
        SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        prefs1.edit().putString("code", String.valueOf(randomPIN)).commit();
        OkHttpClient client = new OkHttpClient();
        String url = "https://api.twilio.com/2010-04-01/Accounts/ACb81b2a66810873a4d2a0823192c9ca77/Messages.json";
        String base64EncodedCredentials = "Basic " +
                Base64.encodeToString(("ACb81b2a66810873a4d2a0823192c9ca77" + ":"
                        + "8471b24341fa35e3b81ac0c0abf9fe8b").getBytes(), Base64.NO_WRAP);

        RequestBody body = new FormBody.Builder()
                .add("From", "+447481339104") // Hiral: 17606992417
                .add("To", "+" + countryphoneNumber)
                .add("Body", "OTP for registration with Talkee: " + randomPIN)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .header("Authorization", base64EncodedCredentials)
                .build();
        try {
            Response response = client.newCall(request).execute();
            Log.d("TAG", "++++++ sendSms: " + response.body().string());
            //TODO check status here if needed
            Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
            nextIntent.putExtra("phone", countryphoneNumber);
            nextIntent.putExtra("isRegister", true);
            startActivity(nextIntent);
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", "++++ error:: " + e.getMessage());
        }
    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }
}