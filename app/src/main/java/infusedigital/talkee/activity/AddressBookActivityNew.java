package infusedigital.talkee.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.transition.Fade;
import android.transition.Transition;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import infusedigital.talkee.R;
import infusedigital.talkee.adapters.AddressBookFriendsAdapter;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.FriendsData;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;

public class AddressBookActivityNew extends BaseActivity {

    SharedPreferences prefs = null;
    private ProgressDialog loadingDialog;
    private ImageView stackBtn;
    private RelativeLayout toolbarlayout;
    int[] themeColorArray, alphaThemeColorArray;

    private int currentTheme;

    private TextView addressbookTitle, tvShare;
//    private ArrayList<AddressBookData> addressBookDatas;

    public static final String SMS_SENT_ACTION = "com.andriodgifts.gift.SMS_SENT_ACTION";
    public static final String SMS_DELIVERED_ACTION = "com.andriodgifts.gift.SMS_DELIVERED_ACTION";
    //    private AddressBookListAdapter adapter;
//    private ListView contactListView;
    RecyclerView recycler_view_friends;
    TextView tvEmptyView;
    ProgressBar mProgressBar;
    int page = 1;
    private boolean isLoading;
    ArrayList<FriendsData> arrFriendsList;
    private LinearLayoutManager linearLayoutManager;
    private boolean isFirst = true;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View contentView = getLayoutInflater().inflate(R.layout.activity_address_book_old, mFrameContent);

//        Transition fade = new Fade();
//        fade.excludeTarget(android.R.id.statusBarBackground, true);
//        fade.excludeTarget(android.R.id.navigationBarBackground, true);
//        getWindow().setExitTransition(fade);
//        getWindow().setEnterTransition(fade);

        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        arrFriendsList = new ArrayList<>();

        themeColorArray = getResources().getIntArray(R.array.themecolor);
        alphaThemeColorArray = getResources().getIntArray(R.array.alphathemecolor);

        toolbarlayout = (RelativeLayout) contentView.findViewById(R.id.toolbarlayout);
        tvEmptyView = (TextView) contentView.findViewById(R.id.tvEmptyView);
        mProgressBar = (ProgressBar) contentView.findViewById(R.id.mProgressBar);

        currentTheme = prefs.getInt("currentThemeNum", 0);
        int themecolor = themeColorArray[currentTheme];
        toolbarlayout.setBackgroundColor(themecolor);

        main.setSelected(false);
//        channel.setSelected(false);
        addfriend.setSelected(false);
        addressbook.setSelected(true);
        set.setSelected(false);

        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
        }

        stackBtn = (ImageView) findViewById(R.id.stackicon);
        stackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

        addressbookTitle = (TextView) contentView.findViewById(R.id.addressbook_title);
        addressbookTitle.setTypeface(MyApplication.boldFont);

        tvShare = (TextView) contentView.findViewById(R.id.tvShare);
        tvShare.setTypeface(MyApplication.boldFont);
        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String detaileBody = getResources().getString(R.string.invitestring).toString();
//                String urlBody = getResources().getString(R.string.appurl).toString();
                String text = detaileBody /*+ " " + urlBody*/;
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.sharevia)));
            }
        });

        changeStatusBarColor(getResources().getColor(R.color.statusbar));

        recycler_view_friends = (RecyclerView) contentView.findViewById(R.id.recycler_view_friends);
        linearLayoutManager = new LinearLayoutManager(this);
        recycler_view_friends.setLayoutManager(linearLayoutManager);

//        recycler_view_friends.addOnScrollListener(new RecyclerViewScrollListener() {
//
//            @Override
//            public void onScrollUp() {
//
//            }
//
//            @Override
//            public void onScrollDown() {
//
//            }
//
//            @Override
//            public void onLoadMore() {
//                if (!isLoading) {
//                    isLoading = true;
//                    page++;
//                    getListOfFriends();
//                }
//            }
//        });

//        contactListView = (ListView) contentView.findViewById(R.id.contactlistview);
//        contactListView.setDivider(null);
        //TODO for phone book contacts
//        addressBookDatas = new ArrayList<>();
//        adapter = new AddressBookListAdapter(AddressBookActivityNew.this, addressBookDatas);
//        contactListView.setAdapter(adapter);

//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startLoading();
//                getAddressBookInfos();
//            }
//        }, 1500);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = null;
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        message = "Message Sent Successfully !";
                        Log.e("Super", "Message result = " + message);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        message = "Error.";
                        Log.e("Super", "Message result = " + message);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        message = "Error: No service.";
                        Log.e("Super", "Message result = " + message);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        message = "Error: Null PDU.";
                        Log.e("Super", "Message result = " + message);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        message = "Error: Radio off.";
                        Log.e("Super", "Message result = " + message);
                        break;

                }
            }
        }, new IntentFilter(SMS_SENT_ACTION));

        getListOfFriends(); //TODO open for Milestone 3
    }

    /**
     * Get all friends of the user
     */
    private void getListOfFriends() {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("paging", page);
        if (isFirst)
            startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostFriendRequestsCallComplete, WSMethods.BASE_URL + WSMethods.LIST_OF_FRIENDS_POST, params);
    }

    //    private RequestFriendListAdapter adapterFriendsReq;
    private AddressBookFriendsAdapter adapterAddressBookFriends;
    /**
     * API parsing of friends of the user
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostFriendRequestsCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    JSONArray jArrFriendsList = jobj.getJSONArray("USER_DETAILS");
                    arrFriendsList.clear();
                    for (int i = 0; i < jArrFriendsList.length(); i++) {
                        JSONObject obj = jArrFriendsList.getJSONObject(i);
                        arrFriendsList.add(new FriendsData(obj.getString("userid"),
                                obj.getString("name"),
                                obj.getString("last_name"),
                                obj.getString("username"),
                                obj.getString("profile_url")));
                    }
                    if (arrFriendsList.size() > 0) {
                        adapterAddressBookFriends = new AddressBookFriendsAdapter(AddressBookActivityNew.this, arrFriendsList);
                        recycler_view_friends.setAdapter(adapterAddressBookFriends);
//                        tvEmptyView.setVisibility(View.GONE);
                    } else {
//                        tvEmptyView.setVisibility(View.VISIBLE);
                    }
                } else {
                    recycler_view_friends.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                    if (jobj.optString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                    {"FLAG":false,"RESULT":"Failure","MESSAGE":"User requests not avaliable"}
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Call of unFriend req
     */
    public void unFriendCall(String friend_id) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("friend_id", friend_id);
        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostUnFriendCallComplete, WSMethods.BASE_URL + WSMethods.UNFRIEND_POST, params);
    }

    /**
     * API parsing of unFriend req
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostUnFriendCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    getListOfFriends();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Call of unFriend req
     */
    public void blockTheFriend(String friend_id) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("friend_id", friend_id);
        params.put("block_flag", "1");
        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostBlockFriendCallComplete, WSMethods.BASE_URL + WSMethods.BLOCK_UNBLOCK_POST, params);
    }

    /**
     * API parsing of unFriend req
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostBlockFriendCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    getListOfFriends();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onPostResume() {
        super.onPostResume();
        changeStatusBarColor(getResources().getColor(R.color.statusbar));
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

//    public void sendInvite(int position) {
//
//        String phoneNum = addressBookDatas.get(position).phonenum;
//        //Check if the phoneNumber is empty
//        if (phoneNum.isEmpty()) {
//            Toast.makeText(getApplicationContext(), "Please Enter a Phone Number", Toast.LENGTH_SHORT).show();
//        } else {
//            sendSMS(phoneNum);//, smsBody);
//        }
//
//        addressBookDatas.get(position).isSent = true;
//        adapter.setData(addressBookDatas);
//        adapter.notifyDataSetChanged();
//    }

    private void sendSMS(String phoneNumber) {//}, String smsMessage){
        SmsManager sms = SmsManager.getDefault();
        String detaileBody = getResources().getString(R.string.invitestring).toString();
//        String urlBody = getResources().getString(R.string.appurl).toString();
        String urlBody = "";

        List<String> messages = sms.divideMessage(detaileBody);
        for (String message : messages) {
            sms.sendTextMessage(phoneNumber, null, message, PendingIntent.getBroadcast(
                    this, 0, new Intent(SMS_SENT_ACTION), 0), PendingIntent.getBroadcast(this, 0, new Intent(SMS_DELIVERED_ACTION), 0));
        }

        sms.sendTextMessage(phoneNumber, null, urlBody, PendingIntent.getBroadcast(
                this, 0, new Intent(SMS_SENT_ACTION), 0), PendingIntent.getBroadcast(this, 0, new Intent(SMS_DELIVERED_ACTION), 0));
    }

//    private void getAddressBookInfos() {
//        addressBookDatas.clear();
//
//        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
//        String _ID = ContactsContract.Contacts._ID;
//        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
//        String PHOTO_URI = ContactsContract.CommonDataKinds.Phone.PHOTO_URI;
//        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
//
//        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//        String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
//        String PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
//        String PHONE_TYPE = ContactsContract.CommonDataKinds.Phone.TYPE;
//        int PHONE_TYPEMOBILE = ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;
//
//        ContentResolver contentResolver = getContentResolver();
//        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
//
//        if (cursor.getCount() > 0) {
//            while (cursor.moveToNext()) {
//                Uri photo_uri;
////                Bitmap photoBmp;
//                String phone = null;
//                String name = "";
//
//                String id = cursor.getString(cursor.getColumnIndex(_ID));
//                name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
//                String url = cursor.getString(cursor.getColumnIndex(PHOTO_URI));
//                if (url != null)
//                    photo_uri = Uri.parse(url);
//                else
//                    photo_uri = null;
//
//                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER))) > 0) {
//                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, new String[]{PHONE_NUMBER},
//                            PHONE_CONTACT_ID + " = ? AND " + PHONE_TYPE + " = " + PHONE_TYPEMOBILE, new String[]{id}, null);
//
//                    while (phoneCursor.moveToNext()) {
//                        phone = phoneCursor.getString(phoneCursor.getColumnIndex(PHONE_NUMBER));
//                    }
//
//                    addressBookDatas.add(new AddressBookData(photo_uri, name, phone, false));
//
//                    phoneCursor.close();
//                }
//
////                if (photo_uri != null){
////                    try{
////                        photoBmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(),Uri.parse(photo_uri));
////                    }catch (FileNotFoundException e){
////                        e.printStackTrace();
////                    }catch (IOException e){
////                        e.printStackTrace();
////                    }
////                }
//            }
//
//            adapter.setData(addressBookDatas);
//            adapter.notifyDataSetChanged();
//            endLoading();
//            tvEmptyView.setVisibility(View.GONE);
//            mProgressBar.setVisibility(View.GONE);
//        } else {
//            tvEmptyView.setVisibility(View.VISIBLE);
//        }
//    }
}