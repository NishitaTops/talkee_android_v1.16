package infusedigital.talkee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import infusedigital.talkee.R;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.utilities.WSMethods;

public class EnterUserNameActivity extends AppCompatActivity {

    private SharedPreferences prefs = null;

    private LinearLayout next;
    private EditText userNameEdit, firstNameEdit, lastNameEdit;
    private TextView nextView;
    private ImageView back_btn;
    private String profile_image, name, username;
    private TextView tvTitleDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_enter_user_name);

        prefs = getApplicationContext().getSharedPreferences("RegisterUserInfo", MODE_PRIVATE);

        next = (LinearLayout) findViewById(R.id.register_username_next);
        tvTitleDetails = (TextView) findViewById(R.id.tvTitleDetails);
        userNameEdit = (EditText) findViewById(R.id.register_username);
        firstNameEdit = (EditText) findViewById(R.id.register_firstname);
        lastNameEdit = (EditText) findViewById(R.id.register_lastname);

        tvTitleDetails.setTypeface(MyApplication.lightFont);
        userNameEdit.setTypeface(MyApplication.lightFont);
        firstNameEdit.setTypeface(MyApplication.lightFont);
        lastNameEdit.setTypeface(MyApplication.lightFont);

        userNameEdit.setBackgroundColor(getResources().getColor(R.color.darkPurple));
        firstNameEdit.setBackgroundColor(getResources().getColor(R.color.darkPurple));
        lastNameEdit.setBackgroundColor(getResources().getColor(R.color.darkPurple));

        back_btn = (ImageView) findViewById(R.id.back_btn);
        nextView = (TextView) findViewById(R.id.nextbtn);
        nextView.setTypeface(MyApplication.semiboldFont);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (getIntent().getExtras() != null) {
            name = getIntent().getStringExtra("name");
            profile_image = getIntent().getStringExtra("profile_image");
            username = getIntent().getStringExtra("username");
            if (name.length() > 0) {
                if (name.contains(" ")) {
                    String[] mName = name.split(" ");
                    firstNameEdit.setText(mName[0]);
                    lastNameEdit.setText(mName[1]);
                } else {
                    firstNameEdit.setText(name);
                }
            }
            if (username.toString().length() > 0) {
                userNameEdit.setMaxEms(username.length());
                userNameEdit.setText(username);
//                userNameEdit.setEnabled(false);
            }
        }


        if (prefs.getString("registerUserName", "").length() > 0) {
            userNameEdit.setText(prefs.getString("registerUserName", ""));
        }
        if (prefs.getString("registerFirstName", "").length() > 0) {
            firstNameEdit.setText(prefs.getString("registerFirstName", ""));
        }
        if (prefs.getString("registerLastName", "").length() > 0) {
            lastNameEdit.setText(prefs.getString("registerLastName", ""));
        }

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = userNameEdit.getText().toString().trim();
                String firstName = firstNameEdit.getText().toString().trim();
                String lastName = lastNameEdit.getText().toString().trim();

                if (userName.length() == 0 || firstName.length() == 0 || lastName.length() == 0) {
                    Toast.makeText(EnterUserNameActivity.this, "Please enter all details.", Toast.LENGTH_LONG).show();
                    return;
                } else if (userName.length() < 5) {
                    Toast.makeText(EnterUserNameActivity.this, "User name must be at least 5 characters.", Toast.LENGTH_LONG).show();
                    return;
                } else if (firstName.length() > 10 || lastName.length() > 10) {
                    Toast.makeText(EnterUserNameActivity.this, "FirstName and LastName should not be more than 10 characters.", Toast.LENGTH_LONG).show();
                    return;
                }

                prefs.edit().putString("registerUserName", userName).commit();
                prefs.edit().putString("registerFirstName", firstName).commit();
                prefs.edit().putString("registerLastName", lastName).commit();

                checkUserName(userName);
            }
        });
    }

    /**
     * check username api call
     *
     * @param username
     */
    private void checkUserName(String username) {
        RequestParams params = new RequestParams();
        params.put("user_name", username);
        new MyLoopJPost(EnterUserNameActivity.this, "", onLoopJPostLogoutCallComplete, WSMethods.BASE_URL + WSMethods.CHECK_USER_NAME_POST, params);
    }

    /**
     * API parsing of check username
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostLogoutCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            JSONObject jobj = null;
            try {
                jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");

                if (flag.equalsIgnoreCase("true")) {
                    Intent nextIntent = new Intent(getApplicationContext(), ChooseAvatarActivity.class);
                    nextIntent.putExtra("profile_image", profile_image);
                    startActivity(nextIntent);
                    finish();
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(EnterUserNameActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}
