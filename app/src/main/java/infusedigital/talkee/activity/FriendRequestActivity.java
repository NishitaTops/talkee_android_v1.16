package infusedigital.talkee.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import infusedigital.talkee.R;
import infusedigital.talkee.adapters.MsgFriendsListAdapter;
import infusedigital.talkee.adapters.RequestFriendListAdapter;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.FriendRequestsData;
import infusedigital.talkee.datamodel.FriendsData;
import infusedigital.talkee.datamodel.MsgFriendsData;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;

import static infusedigital.talkee.R.id.recycler_view_friends;

public class FriendRequestActivity extends AppCompatActivity {

    static final String SERVER_URL = "gettalkee.com";

    private ImageView backBtn;
    private TextView friendRequestTitleView;
    private ListView friendRequestView;
    //    private RequestFriendListAdapter adapter;
    //    ArrayList<User> requestListData;
    ArrayList<FriendRequestsData> requestListData;
    private ProgressDialog loadingDialog;
    int page = 1;
    private boolean isLoading;
    Context context;
    private ArrayList<FriendRequestsData> arrAllFriendRequestList;
    private TextView emptyElement;
    private String friendUsername;
    private boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_friend_request);
        context = FriendRequestActivity.this;

        emptyElement = (TextView) findViewById(R.id.emptyElement);
        backBtn = (ImageView) findViewById(R.id.friendview_back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        friendRequestTitleView = (TextView) findViewById(R.id.friendrequest_title);
        friendRequestTitleView.setTypeface(MyApplication.boldFont);

        arrAllFriendRequestList = new ArrayList<>();

        friendRequestView = (ListView) findViewById(R.id.request_listview);
        friendRequestView.setDivider(null);

        getRequestsList();
//        requestListData = TabViewActivity.getInstance().friendRequestData;
//        adapter = new RequestFriendListAdapter(this, requestListData);
//        friendRequestView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
    }

    /**
     * Get all request from server
     */
    private void getRequestsList() {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
//        params.put("user_id", "240");
        params.put("paging", page);
        if (isFirst)
            startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostFriendRequestsCallComplete, WSMethods.BASE_URL + WSMethods.LIST_REQUESTS_POST, params);
    }

    private boolean hasNext;
    private RequestFriendListAdapter adapterFriendsReq;
    /**
     * API parsing of search friends
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostFriendRequestsCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
//                hasNext = Boolean.parseBoolean(jobj.getString("IS_NEXT"));
                if (flag.equalsIgnoreCase("true")) {
                    JSONArray arrFriendsList = jobj.getJSONArray("Friend_list");
                    arrAllFriendRequestList.clear();
                    for (int i = 0; i < arrFriendsList.length(); i++) {
                        JSONObject obj = arrFriendsList.getJSONObject(i);
                        arrAllFriendRequestList.add(new FriendRequestsData(obj.getString("userid"),
                                obj.getString("name"),
                                obj.getString("username"),
                                obj.getString("profile_url")));
                    }
                    if (arrAllFriendRequestList.size() > 0) {
                        adapterFriendsReq = new RequestFriendListAdapter(FriendRequestActivity.this, arrAllFriendRequestList);
                        friendRequestView.setAdapter(adapterFriendsReq);
                        adapterFriendsReq.notifyDataSetChanged();
                        emptyElement.setVisibility(View.GONE);
                    } else {
                        emptyElement.setVisibility(View.VISIBLE);
                        friendRequestView.setEmptyView(emptyElement);
                    }
                } else {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                    {"FLAG":false,"RESULT":"Failure","MESSAGE":"User requests not avaliable"}
                    emptyElement.setVisibility(View.VISIBLE);
//                    friendRequestView.setEmptyView(emptyElement);
                    friendRequestView.setVisibility(View.GONE);

                    if (jobj.optString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Accept/Decline the request
     *
     * @param data
     */
    public void actionOnRequest(final FriendRequestsData data, String status) {
        friendUsername = data.getName();
        RequestParams params = new RequestParams();
//        params.put("user_id", userId);
//                                params.put("user_id", "240");
        params.put("user_id", AppConfig.getUserId(context));
        params.put("requestflag", status);
        params.put("sender_id", data.getUserid());
        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostSendFriendReqCallComplete, WSMethods.BASE_URL + WSMethods.ACCEPT_DEC_REQ_POST, params);
    }

    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSendFriendReqCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, "You are now friend with " + friendUsername + ".", Toast.LENGTH_LONG).show();
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    getRequestsList();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Call of unFriend req
     */
    public void blockTheFriend(String friend_id) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("friend_id", friend_id);
        params.put("block_flag", "1");
        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostBlockFriendCallComplete, WSMethods.BASE_URL + WSMethods.BLOCK_UNBLOCK_POST, params);
    }

    /**
     * API parsing of unFriend req
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostBlockFriendCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    getRequestsList();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

//    public void acceptRequest (final User data){
//
//        startLoading();
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(SERVER_URL).setPath("/talkee/send_message.php")
//                .setParameter("regId", data.gcm_id)
//                .setParameter("userId", String.valueOf(MyApplication.currentUser.id))
//                .setParameter("friendId", Integer.toString(data.id))
//                .setParameter("requestType", "1");
//
//        String access_url = builder.toString();
//        Log.e("Super", "friend accept request = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            requestListData.remove(data);
//                            if (requestListData.size() == 0){
//                                finish();
//                            }else {
//                                adapter.setData(requestListData);
//                                adapter.notifyDataSetChanged();
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        endLoading();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

//    public void declineRequest (final User data){
//
//        startLoading();
//
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(SERVER_URL).setPath("/talkee/send_message.php")
//                .setParameter("regId", data.gcm_id)
//                .setParameter("userId", String.valueOf(MyApplication.currentUser.id))
//                .setParameter("friendId", Integer.toString(data.id))
//                .setParameter("requestType", "4");
//
//        String access_url = builder.toString();
//
//        Log.e("Super", "request decline = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            requestListData.remove(data);
//
//                            if (requestListData.size() == 0){
//                                finish();
//                            }else{
//                                adapter.setData(requestListData);
//                                adapter.notifyDataSetChanged();
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        endLoading();
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }


    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent(FriendRequestActivity.this, TabViewActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(mIntent);
        finish();
//        Intent returnIntent = new Intent();
//        setResult(Activity.RESULT_OK, returnIntent);
//        finish();
//        super.onBackPressed();
    }

//    @Override
//    public void onBackPressed() {
//        Intent returnIntent = new Intent();
//        setResult(Activity.RESULT_OK, returnIntent);
//        finish();
//    }
}