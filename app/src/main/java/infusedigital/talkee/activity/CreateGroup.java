package infusedigital.talkee.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import cz.msebera.android.httpclient.client.utils.URIBuilder;
import infusedigital.talkee.R;
import infusedigital.talkee.adapters.CreateGroupAdapter;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.datamodel.SearchUserData;
import infusedigital.talkee.model.User;

public class CreateGroup extends AppCompatActivity {

    RelativeLayout createGroupToolView;
    private TextView createGroupTitleView;
    private EditText searchbox;
    private ImageView backBtn, searchIcon, searchCancel;
    private ListView friendListView;
    private TextView createGroupBtn;

    private ProgressDialog loadingDialog;
    int[] themeColorArray, alphaThemeColorArray;
    private int currentTheme;

    SharedPreferences prefs = null;

    private ArrayList<SearchUserData> friendList = new ArrayList<>();
    private ArrayList<SearchUserData> listData = new ArrayList<>();
    private ArrayList checked_position = new ArrayList<>();

    private CreateGroupAdapter adapter;

    HashMap<String, String> itemData = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_create_group);

        createGroupToolView = (RelativeLayout) findViewById(R.id.creategroup_toolbar);
        backBtn = (ImageView) findViewById(R.id.creategroup_back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        searchIcon = (ImageView) findViewById(R.id.creategroup_search_icon);
        searchCancel = (ImageView) findViewById(R.id.creategroup_search_cancel_btn);

        createGroupTitleView = (TextView) findViewById(R.id.creategroup_title);
        createGroupTitleView.setTypeface(MyApplication.boldFont);

        searchbox = (EditText) findViewById(R.id.creategroup_search_querybox);
        friendListView = (ListView) findViewById(R.id.creategroup_friend_list);
        friendListView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
        friendListView.setDivider(null);

        createGroupBtn = (TextView) findViewById(R.id.creategroup_confirm_btn);
        createGroupBtn.setVisibility(View.GONE);
        createGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGroup();
            }
        });

        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        themeColorArray = getResources().getIntArray(R.array.themecolor);
        alphaThemeColorArray = getResources().getIntArray(R.array.alphathemecolor);


        currentTheme = prefs.getInt("currentThemeNum", 0);
        final int themecolor = themeColorArray[currentTheme];
        createGroupToolView.setBackgroundColor(themecolor);
        createGroupBtn.setBackgroundColor(themecolor);

        adapter = new CreateGroupAdapter(CreateGroup.this, friendList);
        friendListView.setAdapter(adapter);

        friendListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                SearchUserData data = (SearchUserData) listData.get(position);

                if (data.isSelected) {
                    friendListView.setItemChecked(position, false);
                    data.isSelected = false;
                } else {
                    friendListView.setItemChecked(position, true);
                    data.isSelected = true;
                }

                adapter.notifyDataSetChanged();


//                TextView name, phone;
//                name = (TextView) view.findViewById(R.id.receiver_name);
//                phone = (TextView)view.findViewById(R.id.send_profile_phone);
//
//                if (checked_position.contains(position)) {
//                    friendListView.setItemChecked(position, false);
//
//                    int start = data.name.length() - 1;
//                    String plus = data.name.substring(start);
//
//                    deselectIcon.setVisibility(View.GONE);
//
//                    for ( String key : itemData.keySet() ) {
//                        if (key.equals(String.valueOf (position))) {
//                            data.photo_url = itemData.get(key);
//                            break;
//                        }
//                    }
//
//                    for (int i = 0; i < checked_position.size(); i++) {
//                        int value = (int)checked_position.get(i);
//                        if (value == position)
//                            checked_position.remove(i);
//                    }
//                } else {
//                    friendListView.setItemChecked(position, true);
//                    checked_position.add(position);
//                    itemData.put(String.valueOf(position), data.photo_url);
//                    int start = data.name.length() - 1;
//                    String plus = data.name.substring(start);
//
//                    if (plus.equals("+")) {
//                        data.name = data.name.substring(0, start - 1);
//                        Log.d("plus", data.name);
//                    }
//
//                    data.photo_url = "tmp";//Uri.parse("R.drawable.avatar_blank").toString();
//
////                    deselectIcon.setVisibility(View.VISIBLE);
//                }
//
//                adapter.notifyDataSetChanged ();
//                friendListView.clearFocus();
//                friendListView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        friendListView.setSelection(position);
//                    }
//                }, 100);

                if (checkCreateGroup()) {
                    createGroupBtn.setBackgroundColor(themecolor);
//                    createGroupBtn.setTextColor(Color.parseColor("#ffffff"));
                    createGroupBtn.setVisibility(View.VISIBLE);
                } else {
//                    createGroupBtn.setBackgroundColor(Color.parseColor("#000000"));
//                    createGroupBtn.setTextColor(Color.parseColor("#ffffff"));
                    createGroupBtn.setVisibility(View.GONE);
                }

//                if (checked_position.size() > 0) {
////                    send_btn.setBackgroundColor(Color.parseColor("#750cc7"));
//                    createGroupBtn.setBackgroundColor(themecolor);
//                    createGroupBtn.setTextColor(Color.parseColor("#ffffff"));
////                    name.setTextColor(Color.parseColor("#35eb93"));
////                    phone.setTextColor(Color.parseColor("#35eb93"));
//                    x.setVisibility(View.VISIBLE);
//                    createGroupBtn.setVisibility(View.VISIBLE);
//                } else {
//                    createGroupBtn.setBackgroundColor(Color.parseColor("#000000"));
//                    createGroupBtn.setTextColor(Color.parseColor("#ffffff"));
////                    name.setTextColor(Color.parseColor("#000000"));
////                    phone.setTextColor(Color.parseColor("#3a3a3a"));
//                    x.setVisibility(View.GONE);
//                    createGroupBtn.setVisibility(View.GONE);
//                }
            }
        });

    }

    private String getMembers() {
        String result = String.valueOf(MyApplication.currentUser.id);
        int j = 0;

        for (int i = 0; i < friendList.size(); i++) {

            String id = String.valueOf(friendList.get(i).friend.id);
            if (friendList.get(i).isSelected) {
                result = result + "," + id;
                j++;
            }
        }

        if (j == 0)
            return "";

        return result;
    }

    private void createGroup() {
        String members = getMembers();
        if (TextUtils.isEmpty(members))
            return;

        startLoading();

        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/group.php")
                .setParameter("act", "creategroup")
                .setParameter("groupmembers", members);

        String access_url = builder.toString();

        Log.e("Super", "create grup  = " + access_url);

        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString("status").equals("SUCCESS")) {
                                endLoading();
                                return;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        endLoading();
                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                endLoading();
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

    private boolean checkCreateGroup() {
        int checkednum = 0;

        if (friendList != null && friendList.size() > 0) {
            for (int i = 0; i < friendList.size(); i++) {
                if (friendList.get(i).isSelected)
                    checkednum++;
            }
        }

        if (checkednum > 0)
            return true;
        else
            return false;
    }

    private void getFriends() {
        startLoading();

        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
                .setParameter("act", "getfriends")
                .setParameter("user_id", String.valueOf(MyApplication.currentUser.id));
        String access_url = builder.toString();

        Log.e("Super", "get grup friend = " + access_url);

        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString("status").equals("SUCCESS"))
                                return;

                            JSONArray jsonData = response.getJSONArray("friend");

                            for (int i = 0; i < jsonData.length(); i++) {

                                JSONObject jresponse = jsonData.getJSONObject(i);

                                User friend = new User();
                                friend.setUser(jresponse);

                                friendList.add(new SearchUserData(friend, false, -1, null));
                            }

                            Collections.sort(friendList, new CustomComparator());
//                            listData.clear();
                            listData = friendList;
                            adapter.setData(friendList);
                            adapter.notifyDataSetChanged();
                            Log.e("Super", "friendlist111111 = " + friendList.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        endLoading();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                endLoading();
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getFriends();
        hideSoftKeyboard();

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSearch(searchbox.getText().toString());
            }
        });

        searchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchbox.setText("");
            }
        });

        searchbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                callSearch(searchbox.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void callSearch(String query) {
        if (TextUtils.isEmpty(query)) {
            adapter.setData(friendList);
            Log.e("Super", "friendlist = " + friendList.size());
//            listData.clear();
            listData = friendList;
            adapter.notifyDataSetChanged();
            return;
        }

        ArrayList newList = new ArrayList();

        for (int i = 0; i < friendList.size(); i++) {
            SearchUserData data = friendList.get(i);
            if (data.friend.name.contains(query)) {
                newList.add(data);
            }
        }

        adapter.setData(newList);
//        listData.clear();
        listData = newList;
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.in_slide_left_right, R.anim.fade_out);
    }

    public class CustomComparator implements Comparator<SearchUserData> {
        @Override
        public int compare(SearchUserData lhs, SearchUserData rhs) {
            return lhs.friend.name.compareTo(rhs.friend.name);
        }
    }

}
