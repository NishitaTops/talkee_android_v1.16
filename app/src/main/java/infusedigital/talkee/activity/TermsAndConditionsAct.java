package infusedigital.talkee.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import infusedigital.talkee.R;

/**
 * Created by Tops on 10/18/2016.
 */

public class TermsAndConditionsAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFormat(PixelFormat.TRANSLUCENT);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_terms_conditions);

        ImageView ivBackTerms = (ImageView) findViewById(R.id.ivBackTerms);
        ivBackTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView title_terms = (TextView) findViewById(R.id.title_terms);
        title_terms.setTypeface(MyApplication.boldFont);

        final ProgressDialog pd = new ProgressDialog(TermsAndConditionsAct.this);
        pd.setMessage("Loading...");
        pd.show();
        WebView webview = (WebView) findViewById(R.id.wvTermsConditions);
        webview.loadUrl("https://www.iubenda.com/privacy-policy/7931128");
        webview.setClickable(false);
        webview.cancelLongPress();
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pd.dismiss();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                pd.dismiss();
            }
        });
    }
}
