package infusedigital.talkee.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import infusedigital.talkee.R;

public class ExpandFriendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expand_friend);
    }
}
