package infusedigital.talkee.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gcm.GCMRegistrar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.loopj.android.http.Base64;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import infusedigital.talkee.R;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.utilities.AlertDialogManager;

import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Iso2Phone;
import infusedigital.talkee.utilities.WSMethods;
import infusedigital.talkee.utilities.WakeLocker;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginWithNumberActivity extends AppCompatActivity {

    // Asyntask
    AsyncTask<Void, Void, Void> mRegisterTask;

    SharedPreferences prefs = null;

    ImageView login_back;
    LinearLayout login_with_num;
    EditText phonenum;
    TextView tvTerms;
//    static final String SERVER_URL = "192.168.1.139";

    String req_result = "";
    private ProgressDialog loadingDialog;
    AlertDialogManager alert = new AlertDialogManager();

    private String gcm_reg_id = "";
    private String deviceId;

    String number;
    Context context;

    CountryCodePicker ccp;
    private TextView loginBtnTitle;
    RelativeLayout relMain;
    private TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_login_with_number);
        context = LoginWithNumberActivity.this;

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        }

        prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        gcm_reg_id = prefs.getString("gcm_regid", "null");
        if (gcm_reg_id.equalsIgnoreCase("null"))
            gcm_reg_id = FirebaseInstanceId.getInstance().getToken();
        deviceId = prefs.getString("deviceid", "null");

        changeStatusBarColor(getResources().getColor(R.color.whiteColor));

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setTypeface(MyApplication.boldFont);

        phonenum = (EditText) findViewById(R.id.phonenumberTxt);
        phonenum.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });
        tvTerms = (TextView) findViewById(R.id.tvTerms);
        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(LoginWithNumberActivity.this, TermsAndConditionsAct.class);
                startActivity(mIntent);
            }
        });

        phonenum.requestFocus();

        ccp = (CountryCodePicker) findViewById(R.id.ccp);

        login_back = (ImageView) findViewById(R.id.login_back_btn);
        login_with_num = (LinearLayout) findViewById(R.id.login_with_phone_num);

        login_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });

        login_with_num.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!isApiCalled)
                    loginWithNumber();
            }
        });

        //set font type, show input pad
        loginBtnTitle = (TextView) findViewById(R.id.loginbtntitle);
        loginBtnTitle.setTypeface(MyApplication.semiboldFont);

        phonenum.setTypeface(MyApplication.lightFont);
        ccp.setTypeFace(MyApplication.semiboldFont);

        TelephonyManager telMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String simContryiso = telMgr.getSimCountryIso();
        String indicative = Iso2Phone.getPhone(simContryiso);

        if (indicative != null) {
            int code = Integer.parseInt(indicative.replace("+", ""));
            ccp.setDefaultCountryUsingPhoneCode(code);
            ccp.setCountryForPhoneCode(code);
        }


//        relMain= (RelativeLayout) findViewById(R.id.relMain);
//        relMain.setFocusable(true);
//        TelephonyManager tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
//        String countryCodeValue = tm.getNetworkCountryIso();
//        System.out.println("++++++++++++ countryCodeValue:" + countryCodeValue);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent nextIntent = new Intent(getApplicationContext(), LoginViewActivity.class);
        startActivity(nextIntent);
        finish();
    }

    /**
     * Login with number
     */
    private void loginWithNumber() {
        number = phonenum.getText().toString();
        if (android.text.TextUtils.isEmpty(number)) {
            Toast.makeText(context, "Please enter mobile number.", Toast.LENGTH_LONG).show();
            return;
        }

        String countryCode = ccp.getSelectedCountryCode();
        number = countryCode + number;
        RequestParams params = new RequestParams();
        params.put("number", number);
        params.put("device_token", gcm_reg_id);
        params.put("inserted_type", "1");
        params.put("device_type", "0");
        startLoading();
        isApiCalled = true;
        new MyLoopJPost(context, "", onLoopJPostLoginByNumberCallComplete, WSMethods.BASE_URL + WSMethods.LOGIN_POST, params);
//        URIBuilder builder = new URIBuilder();
//        number = phonenum.getText().toString();
//
//        if (android.text.TextUtils.isEmpty(number)) return;
//
//        String countryCode = ccp.getSelectedCountryCode();
//        number = countryCode + number;
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/login.php")
//                .setParameter("act", "login")
//                .setParameter("phonenum", number)
//                .setParameter("deviceid", deviceId)
//                .setParameter("gcm_regid", gcm_reg_id)
//                .setParameter("type", "phone");
//
//        String access_url = builder.toString();
//        Log.e("Super", "login with number = " + access_url);
//
//        startLoading();
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            endLoading();
//
//                            String status = response.getString("status");
//                            if (status.equals("failed")) {
//                                Log.e("Super", "sent failed");
//                                alert.showAlertDialog(LoginWithNumberActivity.this,
//                                        "Login Error",
//                                        "You couldn't login with this number", false);
//                                return;
//                            }
//
//                            if (status.equals("SENT")) {
//                                Intent verifyActivity = new Intent(LoginWithNumberActivity.this, RegisterCodeActivity.class);
//                                verifyActivity.putExtra("phonenum", number);
//                                verifyActivity.putExtra("username", number);
//                                verifyActivity.putExtra("gcmid", gcm_reg_id);
//                                verifyActivity.putExtra("deviceid", deviceId);
//                                startActivity(verifyActivity);
//                                Log.e("Super", "sent code");
//                                finish();
//                                return;
//                            }
//
//                            JSONObject data = response.getJSONObject("data");
//                            MyApplication.currentUser.setUser(data);
//
//                            if (android.text.TextUtils.isEmpty(number))
//                                prefs.edit().putString("phonenum", MyApplication.currentUser.name).commit();
//                            else
//                                prefs.edit().putString("phonenum", MyApplication.currentUser.phonenum).commit();
//
//                            prefs.edit().putString("userId", String.valueOf(MyApplication.currentUser.id)).commit();
//                            prefs.edit().putString("photoUrl", MyApplication.currentUser.photo_url).commit();
//                            prefs.edit().putString("name", MyApplication.currentUser.name).commit();
//                            prefs.edit().putString("color", MyApplication.currentUser.emoticol).commit();
//                            prefs.edit().putString("mood", MyApplication.currentUser.mood).commit();
//                            prefs.edit().putString("phonenum", MyApplication.currentUser.phonenum).commit();
//                            prefs.edit().putString("gcm_regid", MyApplication.currentUser.gcm_id).commit();
//                            prefs.edit().putString("deviceid", MyApplication.currentUser.device_id).commit();
//                            prefs.edit().putString("loginstate", "true").commit();
//                            prefs.edit().putString("type", "phone");
//                            Log.e("Super", "sent success");
//
//                            finish();
//                            Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
//                            startActivity(nextIntent);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        endLoading();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("Super", "sent errpr");
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
    }

    private boolean isApiCalled = false;
    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostLoginByNumberCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                isApiCalled = false;
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    AppConfig.USER_DETAILS = USER_DETAILS;
                    Toast.makeText(context, "Please wait.", Toast.LENGTH_LONG).show();

//                    Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
//                    nextIntent.putExtra("phone", number);
//                    nextIntent.putExtra("isLogin", true);
//                    startActivity(nextIntent);
//                    finish();

                    sendSMS();

//                    Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
//                    nextIntent.putExtra("phone", number);
//                    nextIntent.putExtra("isLogin", true);
//                    nextIntent.putExtra("sendSms", true);
//                    startActivity(nextIntent);
//                    finish();
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * generate random number and send to user
     */
    private void sendSMS() {
        final int randomPIN = (int) (Math.random() * 9000) + 1000;
        SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        prefs1.edit().putString("code", String.valueOf(randomPIN)).commit();

        OkHttpClient client = new OkHttpClient();
        String url = "https://api.twilio.com/2010-04-01/Accounts/ACb81b2a66810873a4d2a0823192c9ca77/Messages.json";
        String base64EncodedCredentials = "Basic " +
                Base64.encodeToString(("ACb81b2a66810873a4d2a0823192c9ca77" + ":"
                        + "8471b24341fa35e3b81ac0c0abf9fe8b").getBytes(), Base64.NO_WRAP);
        //TODO hiral's credentials
//        String url = "https://api.twilio.com/2010-04-01/Accounts/ACa833ae73e456f57bbebe3348574a0293/SMS/Messages";
//        String base64EncodedCredentials = "Basic " +
//                Base64.encodeToString(("ACa833ae73e456f57bbebe3348574a0293" + ":"
//                        + "909e53cb6f459830f40236e98bf3b0b8").getBytes(), Base64.NO_WRAP);

        RequestBody body = new FormBody.Builder()
                .add("From", "+447481339104") // Hiral: 17606992417
                .add("To", "+" + number)
                .add("Body", "OTP for registration with Talkee: " + randomPIN)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .header("Authorization", base64EncodedCredentials)
                .build();
        try {
            Response response = client.newCall(request).execute();
            Log.d("TAG", "++++++ sendSms: " + response.body().string());
            //TODO check status here if needed
            Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
            nextIntent.putExtra("phone", number);
            nextIntent.putExtra("isLogin", true);
            startActivity(nextIntent);
//            finish();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", "++++ error:: " + e.getMessage());
        }
    }

//    /**
//     * generate random number and send to user
//     */
//    private void sendSMS() {
//        final int randomPIN = (int) (Math.random() * 9000) + 1000;
//        SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
//        prefs1.edit().putString("code", String.valueOf(randomPIN)).commit();
//        OkHttpClient client = new OkHttpClient();
//        String url = "https://api.twilio.com/2010-04-01/Accounts/ACb81b2a66810873a4d2a0823192c9ca77/Messages.json";
//        String base64EncodedCredentials = "Basic " +
//                Base64.encodeToString(("ACb81b2a66810873a4d2a0823192c9ca77" + ":"
//                        + "8471b24341fa35e3b81ac0c0abf9fe8b").getBytes(), Base64.NO_WRAP);
//        //TODO hiral's credentials
////        String url = "https://api.twilio.com/2010-04-01/Accounts/ACa833ae73e456f57bbebe3348574a0293/SMS/Messages";
////        String base64EncodedCredentials = "Basic " +
////                Base64.encodeToString(("ACa833ae73e456f57bbebe3348574a0293" + ":"
////                        + "909e53cb6f459830f40236e98bf3b0b8").getBytes(), Base64.NO_WRAP);
//
//        RequestBody body = new FormBody.Builder()
//                .add("From", "+447481339104") // Hiral: 17606992417
//                .add("To", "+" + number)
//                .add("Body", "OTP for registration with Talkee: " + randomPIN)
//                .build();
//
//        Request request = new Request.Builder()
//                .url(url)
//                .post(body)
//                .header("Authorization", base64EncodedCredentials)
//                .build();
//        try {
//            Response response = client.newCall(request).execute();
//            Log.d("TAG", "++++++ sendSms: " + response.body().string());
//            //TODO check status here if needed
//            Intent nextIntent = new Intent(getApplicationContext(), RegisterCodeActivity.class);
//            nextIntent.putExtra("phone", number);
//            nextIntent.putExtra("isLogin", true);
//            startActivity(nextIntent);
//            finish();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.d("TAG", "++++ error:: " + e.getMessage());
//        }
//    }

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    /**
     * Receiving push messages
     */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString("message");
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());
            Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
            WakeLocker.release();
        }
    };

    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
//            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {

        }
        super.onDestroy();
    }
}