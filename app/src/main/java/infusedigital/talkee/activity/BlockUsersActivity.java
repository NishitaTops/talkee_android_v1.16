package infusedigital.talkee.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import infusedigital.talkee.R;
import infusedigital.talkee.adapters.BlockedUserListAdapter;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.AddFriend;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;

public class BlockUsersActivity extends AppCompatActivity {

    static final String SERVER_URL = "gettalkee.com";

    private ImageView backBtn;
    private TextView blockUserTitleView;
    private ListView blockedUserView;
    private ProgressDialog loadingDialog;
    int page = 1;
    private boolean isLoading, isFirst = true;
    Context context;
    private ArrayList<AddFriend> arrAllBlockedUsersList;
    private TextView emptyElement;
    private String friendUsername;
    RelativeLayout mToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_block_users);
        context = BlockUsersActivity.this;

        mToolBar = (RelativeLayout) findViewById(R.id.mToolBar);
        emptyElement = (TextView) findViewById(R.id.emptyElement);
        backBtn = (ImageView) findViewById(R.id.friendview_back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        blockUserTitleView = (TextView) findViewById(R.id.friendrequest_title);
        blockUserTitleView.setTypeface(MyApplication.boldFont);

        int[] themeColorArray = getResources().getIntArray(R.array.themecolor);
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);
        int currentTheme = prefs.getInt("currentThemeNum", 0);
        int themecolor = themeColorArray[currentTheme];
//        mToolBar.setBackgroundColor(themecolor);

        blockedUserView = (ListView) findViewById(R.id.block_user_listview);
        blockedUserView.setDivider(null);

        arrAllBlockedUsersList = new ArrayList<>();

        getBlockedUserList(); //TODO open for Milestone 3
    }

    /**
     * Get all blocked user list
     */
    private void getBlockedUserList() {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        if (isFirst)
            startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostBlockUserListCallComplete, WSMethods.BASE_URL + WSMethods.BLOCK_USER_LIST_POST, params);
    }

    private boolean hasNext;
    private BlockedUserListAdapter adapterBlockedUsers;
    /**
     * API parsing of block user list
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostBlockUserListCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
//                hasNext = Boolean.parseBoolean(jobj.getString("IS_NEXT"));
                if (flag.equalsIgnoreCase("true")) {
                    JSONArray arrBlockedList = jobj.getJSONArray("BLOCK_USER");
                    arrAllBlockedUsersList.clear();
                    for (int i = 0; i < arrBlockedList.length(); i++) {
                        JSONObject obj = arrBlockedList.getJSONObject(i);
                        arrAllBlockedUsersList.add(new AddFriend(obj.getString("userid"),
                                obj.getString("name"),
                                obj.getString("last_name"),
                                obj.getString("username"),
                                obj.getString("profile_url")));
                    }
                    if (arrAllBlockedUsersList.size() > 0) {
                        adapterBlockedUsers = new BlockedUserListAdapter(BlockUsersActivity.this, arrAllBlockedUsersList);
                        blockedUserView.setAdapter(adapterBlockedUsers);
//                        emptyElement.setVisibility(View.GONE);
                    } else {
//                        emptyElement.setVisibility(View.VISIBLE);
//                        blockedUserView.setEmptyView(emptyElement);
                    }
                } else {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                    {"FLAG":false,"RESULT":"Failure","MESSAGE":"User requests not avaliable"}
                    blockedUserView.setVisibility(View.GONE);
                    emptyElement.setVisibility(View.VISIBLE);
//                    blockedUserView.setEmptyView(emptyElement);

                    if (jobj.optString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Call of unBlock req
     */
    public void unBlockTheFriend(String friend_id) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("friend_id", friend_id);
        params.put("block_flag", "2");
        startLoading();
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostBlockFriendCallComplete, WSMethods.BASE_URL + WSMethods.BLOCK_UNBLOCK_POST, params);
    }

    /**
     * API parsing of unBlock req
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostBlockFriendCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    getBlockedUserList();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

//    /**
//     * Get all blocked user list call
//     *
//     * @param data
//     */
//    public void actionOnRequest(final FriendRequestsData data, String status) {
//        friendUsername = data.getName();
//        RequestParams params = new RequestParams();
////        params.put("user_id", userId);
////                                params.put("user_id", "240");
//        params.put("user_id", AppConfig.getUserId(context));
//        params.put("requestflag", status);
//        params.put("sender_id", data.getUserid());
//        startLoading();
//        isLoading = true;
//        new MyLoopJPost(context, "", onLoopJPostBlockUserListCallComplete, WSMethods.BASE_URL + WSMethods.ACCEPT_DEC_REQ_POST, params);
//    }
//
//    /**
//     * API parsing of block user list
//     */
//    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostBlockUserListCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {
//
//        @Override
//        public void response(String result) {
//            try {
//                endLoading();
//                JSONObject jobj = new JSONObject(result);
//                String flag = jobj.getString("FLAG");
//                if (flag.equalsIgnoreCase("true")) {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, "You are now friend with " + friendUsername + ".", Toast.LENGTH_LONG).show();
//                    getBlockedUserList();
//                } else {
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                }
//                isLoading = false;
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    };

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }
}