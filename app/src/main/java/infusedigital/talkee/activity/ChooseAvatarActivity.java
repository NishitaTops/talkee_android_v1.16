package infusedigital.talkee.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.WSMethods;

public class ChooseAvatarActivity extends AppCompatActivity {

    private CircleImageView avatar_image;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private LinearLayout back, next;
    private SharedPreferences prefs, prefs1;
    private TextView nextTitle, backTitle;
    String profile_image = "";
    Context context;
    Bitmap thumbnail = null;
    private ProgressDialog loadingDialog;
    String phoneNum, userName, avatarPath, emotiColor, deviceId, regId, firstName, lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_choose_avatar);
        context = ChooseAvatarActivity.this;
        prefs = getApplicationContext().getSharedPreferences("RegisterUserInfo", MODE_PRIVATE);

        requestMultiplePermissions();

        avatar_image = (CircleImageView) findViewById(R.id.register_avatar_photo);
        if (getIntent().getExtras() != null && getIntent().hasExtra("profile_image")) {
            profile_image = getIntent().getStringExtra("profile_image");
            if (profile_image != null && profile_image.length() > 0) {
                Picasso.with(context).load(profile_image).placeholder(R.drawable.ic_appicon).into(avatar_image);
                prefs.edit().putString("avatarPath", profile_image).commit();
            }
        }

        if (AppConfig.social_profile_image.toString().length() > 0) {
            Picasso.with(context).load(AppConfig.social_profile_image).placeholder(R.drawable.ic_appicon).into(avatar_image);
            prefs.edit().putString("avatarPath", AppConfig.social_profile_image).commit();
        }

        avatar_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        back = (LinearLayout) findViewById(R.id.register_avatar_back);
        next = (LinearLayout) findViewById(R.id.register_avatar_next);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseAvatarActivity.this, EnterUserNameActivity.class);
                startActivity(intent);
                finish();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent nextIntent = new Intent(getApplicationContext(), AllDoneActivity.class);
//                startActivity(nextIntent);
//                finish();
                AllDone();
            }
        });

        backTitle = (TextView) findViewById(R.id.backbtntitle);
        nextTitle = (TextView) findViewById(R.id.nextbtntitle);

        backTitle.setTypeface(MyApplication.semiboldFont);
        nextTitle.setTypeface(MyApplication.semiboldFont);
    }

    private void requestMultiplePermissions() {
        String cameraPermission = android.Manifest.permission.CAMERA;
        String recordAudioPermission = android.Manifest.permission.RECORD_AUDIO;
        String wakeLockPermission = android.Manifest.permission.WAKE_LOCK;
        String writeExternalStoragePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStoragePermission = Manifest.permission.READ_EXTERNAL_STORAGE;
        String readPhoneStatePermission = Manifest.permission.READ_PHONE_STATE;
        String readContactsPermission = Manifest.permission.READ_CONTACTS;
        String sendSmsPermission = Manifest.permission.SEND_SMS;
        String internetPermission = Manifest.permission.INTERNET;
        String accessNetWorkPermission = Manifest.permission.ACCESS_NETWORK_STATE;

        int hasCameraPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, cameraPermission);
        int hasRecordAudioPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, recordAudioPermission);
        int hasWakeLockPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, wakeLockPermission);
        int hasWriteExternalPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, writeExternalStoragePermission);
        int hasReadExternalPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, readExternalStoragePermission);
        int hasReadPhoneStatePermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, readPhoneStatePermission);
        int hasReadContactsPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, readContactsPermission);
        int hasSendSmsPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, sendSmsPermission);
        int hasInternetPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, internetPermission);
        int hasAccessNetworkPermission = ContextCompat.checkSelfPermission(ChooseAvatarActivity.this, accessNetWorkPermission);

        List<String> permissions = new ArrayList<String>();
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(cameraPermission);
        }
        if (hasRecordAudioPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(recordAudioPermission);
        }
        if (hasWakeLockPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(wakeLockPermission);
        }
        if (hasWriteExternalPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(writeExternalStoragePermission);
        }
        if (hasReadExternalPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(readExternalStoragePermission);
        }
        if (hasReadPhoneStatePermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(readPhoneStatePermission);
        }
        if (hasReadContactsPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(readContactsPermission);
        }
        if (hasSendSmsPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(sendSmsPermission);
        }

        if (hasInternetPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(internetPermission);
        }

        if (hasAccessNetworkPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(accessNetWorkPermission);
        }

        if (!permissions.isEmpty()) {
            String[] params = permissions.toArray(new String[permissions.size()]);
            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(params, 1);
            }
        } /*else {
            initData();
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length == permissions.length) {

                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        return;
                }
//                initData();
            } else {
                Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhotoIntent, SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("aaaa", "ccc" + ": " + destination.getPath());

        prefs.edit().putString("avatarPath", destination.getPath()).commit();

//        avatar_image.setImageBitmap(Bitmap.createScaledBitmap(thumbnail, 512, 512, false));
        avatar_image.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    public void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        avatar_image.setImageBitmap(Bitmap.createScaledBitmap(bm, 512, 512, false));
        avatar_image.setImageBitmap(thumbnail);

        String path = getFilePathFromContentUri(data.getData(), this.getContentResolver());

        prefs.edit().putString("avatarPath", path).commit();
    }

    private static String getFilePathFromContentUri(Uri selectedVideoUri,
                                                    ContentResolver contentResolver) {
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA};

        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (thumbnail != null && !thumbnail.isRecycled()) {
            thumbnail.recycle();
            thumbnail = null;
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // TODO ALLDONE activiy actions from here

    public void AllDone() {
        prefs = getApplicationContext().getSharedPreferences("RegisterUserInfo", MODE_PRIVATE);
        prefs1 = getApplicationContext().getSharedPreferences("UserInfo", MODE_PRIVATE);

        phoneNum = prefs.getString("phonenum", "");
        userName = prefs.getString("registerUserName", "null");
        firstName = prefs.getString("registerFirstName", "null");
        lastName = prefs.getString("registerLastName", "null");
        avatarPath = prefs.getString("avatarPath", "null");
        emotiColor = prefs.getString("registerColor", "#2edadc");
        deviceId = prefs1.getString("deviceid", "null");
        regId = prefs1.getString("gcm_regid", "null");
        if (regId.equalsIgnoreCase("null"))
            regId = FirebaseInstanceId.getInstance().getToken();

        if (AppConfig.inserted_type.equals("1")) {
            registerByNumber();
        } else {
            registerSocial();
        }
    }

    private void registerByNumber() {
        RequestParams params = new RequestParams();
        params.put("mobile_number", phoneNum);
        params.put("user_name", userName);
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        try {
            if (avatarPath != null) {
                if (avatarPath.length() > 0) {
                    System.out.println("++++ avatar path:" + avatarPath);
                    params.put("profile_image", new File(avatarPath));
                }
            } else {
                if (prefs.getString("avatarPath", "").length() > 0) {
                    System.out.println("++++ avatar path:" + avatarPath);
                    params.put("profile_image", new File(avatarPath));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        params.put("device_token", regId);
        params.put("color", emotiColor);
        params.put("inserted_type", AppConfig.inserted_type);
        params.put("device_type", "0");
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostRegisterByNumberCallComplete, WSMethods.BASE_URL + WSMethods.REGISTRATION_POST, params);

    }

    /**
     * API parsing of registration using number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostRegisterByNumberCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    AppConfig.userID = USER_DETAILS.getString("user_id");
                    prefs1.edit().putString("userId", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("photoUrl", USER_DETAILS.getString("profile_image")).commit();
                    prefs1.edit().putString("name", USER_DETAILS.getString("user_name")).commit();
                    prefs1.edit().putString("firstName", USER_DETAILS.getString("first_name")).commit();
                    prefs1.edit().putString("lastName", USER_DETAILS.getString("last_name")).commit();
                    prefs1.edit().putString("color", USER_DETAILS.getString("color")).commit();
                    prefs1.edit().putString("mood", USER_DETAILS.optString("mood")).commit();
                    prefs1.edit().putString("phonenum", USER_DETAILS.optString("mobile_number")).commit();
                    prefs1.edit().putString("gcm_regid", USER_DETAILS.getString("device_token_id")).commit();
//                    prefs1.edit().putString("deviceid", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("loginstate", "true").commit();
                    prefs1.edit().putString("notification", USER_DETAILS.optString("notification")).commit();
//                    prefs1.edit().putString("type", "phone");
                    goToHomeScreen();
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void goToHomeScreen() {
        AppConfig.USER_DETAILS = null;
//        if (phoneNum.length() > 0) {
        Intent nextIntent = new Intent(getApplicationContext(), AddFriendsFromContacts.class);
        nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(nextIntent);
//        } else {
//            Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
//            nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(nextIntent);
//            finish();
//        }
    }

    /**
     * social registration
     */
    private void registerSocial() {
        RequestParams params = new RequestParams();
        params.put("phonenum", phoneNum); //TODO if need to add remove commen
        params.put("social_id", AppConfig.social_id);
        params.put("user_name", userName);
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        try {
            if (avatarPath.length() > 0 && !avatarPath.contains("http"))
                params.put("profile-url", new File(avatarPath));
            else
                params.put("profile-url", avatarPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        params.put("device_token", regId);
        params.put("color", emotiColor);
        params.put("inserted_type", AppConfig.inserted_type);
        params.put("device_type", "0");
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostRegisterSocialCallComplete, WSMethods.BASE_URL + WSMethods.SOCIAL_REGISTRATION_POST, params);
    }

    /**
     * API parsing for social registration
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostRegisterSocialCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) { // User login successfully
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    AppConfig.userID = USER_DETAILS.getString("user_id");
                    prefs1.edit().putString("userId", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("photoUrl", USER_DETAILS.getString("profile_image")).commit();
                    prefs1.edit().putString("name", USER_DETAILS.getString("user_name")).commit();
                    prefs1.edit().putString("firstName", USER_DETAILS.getString("first_name")).commit();
                    prefs1.edit().putString("lastName", USER_DETAILS.getString("last_name")).commit();
                    prefs1.edit().putString("color", USER_DETAILS.getString("color")).commit();
                    prefs1.edit().putString("mood", USER_DETAILS.optString("mood")).commit();
                    prefs1.edit().putString("phonenum", USER_DETAILS.optString("phonenum")).commit();
                    prefs1.edit().putString("gcm_regid", USER_DETAILS.getString("device_token_id")).commit();
//                    prefs1.edit().putString("deviceid", USER_DETAILS.getString("user_id")).commit();
                    prefs1.edit().putString("loginstate", "true").commit();
                    prefs1.edit().putString("notification", USER_DETAILS.optString("notification")).commit();
//                    prefs1.edit().putString("type", "phone");
//
                    goToHomeScreen();
//                    Intent nextIntent = new Intent(getApplicationContext(), TabViewActivity.class);
//                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(nextIntent);
//                    finish();
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }
}