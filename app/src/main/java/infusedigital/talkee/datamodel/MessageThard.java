package infusedigital.talkee.datamodel;

import java.util.ArrayList;

/**
 * Created by Tops on 11/9/2016.
 */

public class MessageThard {
    String userId;
    String name;
    String username;
    String emoticol;
    String mood;
    String image_url;
    String msg_count;
    ArrayList<MessageURLs> msg_thrade;

    public MessageThard(String userId, String name, String username, String emoticol, String mood, String image_url, String msg_count, ArrayList<MessageURLs> msg_thrade) {
        this.userId = userId;
        this.name = name;
        this.username = username;
        this.emoticol = emoticol;
        this.mood = mood;
        this.image_url = image_url;
        this.msg_count = msg_count;
        this.msg_thrade = msg_thrade;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmoticol() {
        return emoticol;
    }

    public void setEmoticol(String emoticol) {
        this.emoticol = emoticol;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getMsg_count() {
        return msg_count;
    }

    public void setMsg_count(String msg_count) {
        this.msg_count = msg_count;
    }

    public ArrayList<MessageURLs> getMsg_thrade() {
        return msg_thrade;
    }

    public void setMsg_thrade(ArrayList<MessageURLs> msg_thrade) {
        this.msg_thrade = msg_thrade;
    }
}