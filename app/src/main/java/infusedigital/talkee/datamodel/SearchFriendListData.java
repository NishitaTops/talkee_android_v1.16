package infusedigital.talkee.datamodel;

import infusedigital.talkee.model.User;

/**
 * Created by bluesky on 5/10/2016.
 */
public class SearchFriendListData {
    public User friend;
    public boolean btn_friend, btn_sent, btn_receive;

    public SearchFriendListData(User user, boolean isfriend, boolean sent, boolean receive){
        friend = user;
        btn_friend = isfriend;
        btn_sent = sent;
        btn_receive = receive;
    }
}
