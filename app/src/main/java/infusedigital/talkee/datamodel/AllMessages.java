package infusedigital.talkee.datamodel;

/**
 * Created by Tops on 11/15/2016.
 */

public class AllMessages {

    String read_status;
    String audoi_msg;
    String duration;
    String AI;
    String friendId;
    String userId;
    String friendAvatar;
    String currentAvatar;
    String friendUserName;
    String currentUserName;
    int progress;
    boolean isPlaying;

    public AllMessages(String read_status, String audoi_msg, String duration, String AI, String friendId, String userId, String friendAvatar, String currentAvatar, String friendUserName, String currentUserName, int progress, boolean isPlaying) {
        this.read_status = read_status;
        this.audoi_msg = audoi_msg;
        this.duration = duration;
        this.AI = AI;
        this.friendId = friendId;
        this.userId = userId;
        this.friendAvatar = friendAvatar;
        this.currentAvatar = currentAvatar;
        this.friendUserName = friendUserName;
        this.currentUserName = currentUserName;
        this.progress = progress;
        this.isPlaying = isPlaying;
    }

    public String getRead_status() {
        return read_status;
    }

    public void setRead_status(String read_status) {
        this.read_status = read_status;
    }

    public String getAudoi_msg() {
        return audoi_msg;
    }

    public void setAudoi_msg(String audoi_msg) {
        this.audoi_msg = audoi_msg;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAI() {
        return AI;
    }

    public void setAI(String AI) {
        this.AI = AI;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFriendAvatar() {
        return friendAvatar;
    }

    public void setFriendAvatar(String friendAvatar) {
        this.friendAvatar = friendAvatar;
    }

    public String getCurrentAvatar() {
        return currentAvatar;
    }

    public void setCurrentAvatar(String currentAvatar) {
        this.currentAvatar = currentAvatar;
    }

    public String getFriendUserName() {
        return friendUserName;
    }

    public void setFriendUserName(String friendUserName) {
        this.friendUserName = friendUserName;
    }

    public String getCurrentUserName() {
        return currentUserName;
    }

    public void setCurrentUserName(String currentUserName) {
        this.currentUserName = currentUserName;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }
}