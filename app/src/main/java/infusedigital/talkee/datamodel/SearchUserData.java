package infusedigital.talkee.datamodel;

import java.util.ArrayList;

import infusedigital.talkee.model.User;

/**
 * Created by Super on 8/31/2016.
 */
public class SearchUserData {
    public User friend;
    public boolean isSelected;
    public int group_id;
    public ArrayList<User> groupMembers;

    public SearchUserData(User user, boolean isselected, int groupid, ArrayList<User> members){
        friend = user;
        isSelected = isselected;
        group_id = groupid;
        groupMembers = members;
    }
}
