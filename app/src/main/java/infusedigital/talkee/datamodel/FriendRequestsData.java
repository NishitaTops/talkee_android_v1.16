package infusedigital.talkee.datamodel;

/**
 * Created by Tops on 10/24/2016.
 */

public class FriendRequestsData {

    String userid;
    String name;
    String username;
    String profile_url;

    public FriendRequestsData(String userid, String name, String username, String profile_url) {
        this.userid = userid;
        this.name = name;
        this.username = username;
        this.profile_url = profile_url;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }
}