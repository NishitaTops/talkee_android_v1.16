package infusedigital.talkee.datamodel;

import java.util.ArrayList;

/**
 * Created by Tops on 10/20/2016.
 */

public class Message {
    String sender_id;
    String messagecount;
    String name;
    String last_name;
    String username;
    String mood;
    String image;
    ArrayList<MessageURLs> message_url;

    public Message(String sender_id, String messagecount, String name, String last_name, String username, String mood, String image, ArrayList<MessageURLs> message_url) {
        this.sender_id = sender_id;
        this.messagecount = messagecount;
        this.name = name;
        this.last_name = last_name;
        this.username = username;
        this.mood = mood;
        this.image = image;
        this.message_url = message_url;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getMessagecount() {
        return messagecount;
    }

    public void setMessagecount(String messagecount) {
        this.messagecount = messagecount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<MessageURLs> getMessage_url() {
        return message_url;
    }

    public void setMessage_url(ArrayList<MessageURLs> message_url) {
        this.message_url = message_url;
    }
}