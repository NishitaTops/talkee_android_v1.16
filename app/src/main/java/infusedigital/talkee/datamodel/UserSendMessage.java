package infusedigital.talkee.datamodel;

/**
 * Created by Tops on 10/20/2016.
 */

public class UserSendMessage {

    String userId;
    String AI;
    String photo_url;
    String mood;
    String name;
    String username;

    public UserSendMessage(String userId, String AI, String photo_url, String mood, String name, String username) {
        this.userId = userId;
        this.AI = AI;
        this.photo_url = photo_url;
        this.mood = mood;
        this.name = name;
        this.username = username;
    }

    public UserSendMessage(String userId, String AI, String photo_url, String mood) {
        this.userId = userId;
        this.AI = AI;
        this.photo_url = photo_url;
        this.mood = mood;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAI() {
        return AI;
    }

    public void setAI(String AI) {
        this.AI = AI;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}