package infusedigital.talkee.datamodel;

import java.util.ArrayList;

import infusedigital.talkee.model.User;

/**
 * Created by bluesky on 5/10/2016.
 */
public class NotificationListData {
    public User friend;
    public int notification_count;
    public ArrayList<String> message_list;

    //0: messages, 1:isFriendRequest, 2:isGroupRequest
    public int requestType;
    public ArrayList<User> requestListData;

    public int group_id;
    public ArrayList<User> groupMembers;

    public NotificationListData(User user, int notificationcount, ArrayList<String> messages, int requesttype, ArrayList<User> requests,
                                int groupId, ArrayList<User> groupUsers){
        friend = user;
        notification_count = notificationcount;
        message_list = messages;
        requestType = requesttype;
        requestListData = requests;
        group_id = groupId;
        groupMembers = groupUsers;
    }
}
