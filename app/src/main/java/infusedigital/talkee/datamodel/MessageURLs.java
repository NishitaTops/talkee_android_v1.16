package infusedigital.talkee.datamodel;

/**
 * Created by Tops on 10/20/2016.
 */

public class MessageURLs {
    String msg_url;
    String read_state;
    String AI;

    public MessageURLs(String msg_url, String read_state, String AI) {
        this.msg_url = msg_url;
        this.read_state = read_state;
        this.AI = AI;
    }

    public String getMsg_url() {
        return msg_url;
    }

    public void setMsg_url(String msg_url) {
        this.msg_url = msg_url;
    }

    public String getRead_state() {
        return read_state;
    }

    public void setRead_state(String read_state) {
        this.read_state = read_state;
    }

    public String getAI() {
        return AI;
    }

    public void setAI(String AI) {
        this.AI = AI;
    }
}
