package infusedigital.talkee.datamodel;

/**
 * Created by Tops on 10/21/2016.
 */
public class FriendsData {
    String userid;
    String name;
    String last_name;
    String username;
    String profile_url;
    boolean isSelected;

    public FriendsData(String userid, String name, String last_name, String username, String profile_url, boolean isSelected) {
        this.userid = userid;
        this.name = name;
        this.last_name = last_name;
        this.username = username;
        this.profile_url = profile_url;
        this.isSelected = isSelected;
    }

    public FriendsData(String userid, String name, String last_name, String username, String profile_url) {
        this.userid = userid;
        this.name = name;
        this.last_name = last_name;
        this.username = username;
        this.profile_url = profile_url;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}