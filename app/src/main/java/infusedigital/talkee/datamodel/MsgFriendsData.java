package infusedigital.talkee.datamodel;

/**
 * Created by Tops on 10/21/2016.
 */
public class MsgFriendsData {
    String friends_id;
    String name;
    String last_name;
    String username;
    String phonenum;
    String photo_url;
    boolean isSelected;

    public MsgFriendsData(String friends_id, String name, String last_name, String username, String phonenum, String photo_url, boolean isSelected) {
        this.friends_id = friends_id;
        this.name = name;
        this.last_name = last_name;
        this.username = username;
        this.phonenum = phonenum;
        this.photo_url = photo_url;
        this.isSelected = isSelected;
    }

    public String getFriends_id() {
        return friends_id;
    }

    public void setFriends_id(String friends_id) {
        this.friends_id = friends_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}