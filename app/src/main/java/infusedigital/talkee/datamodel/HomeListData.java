package infusedigital.talkee.datamodel;

public class HomeListData {
    //0: message_thard, 1:user_send_msg
    String dataType;
    String user_requests;
    Message message_thard;
    UserSendMessage user_send_msg;

    public HomeListData(String dataType, String user_requests, Message message_thard, UserSendMessage user_send_msg) {
        this.dataType = dataType;
        this.user_requests = user_requests;
        this.message_thard = message_thard;
        this.user_send_msg = user_send_msg;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Message getMessage_thard() {
        return message_thard;
    }

    public void setMessage_thard(Message message_thard) {
        this.message_thard = message_thard;
    }

    public UserSendMessage getUser_send_msg() {
        return user_send_msg;
    }

    public void setUser_send_msg(UserSendMessage user_send_msg) {
        this.user_send_msg = user_send_msg;
    }

    public String getUser_requests() {
        return user_requests;
    }

    public void setUser_requests(String user_requests) {
        this.user_requests = user_requests;
    }
}