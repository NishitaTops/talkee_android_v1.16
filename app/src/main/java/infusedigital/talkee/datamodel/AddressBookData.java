package infusedigital.talkee.datamodel;

import android.net.Uri;

import infusedigital.talkee.model.User;

/**
 * Created by Super on 8/25/2016.
 */
public class AddressBookData {

    public Uri photoUri;
    public String name;
    public String phonenum;
    public boolean isSent;
    // for Talkee users
    public String userId;
    public String username;
    public String photo_url;
    public String isfollow;

    public AddressBookData(Uri addressPhotoUri, String addressName, String addressNum, boolean addressisSent) {
        photoUri = addressPhotoUri;
        name = addressName;
        phonenum = addressNum;
        isSent = addressisSent;
    }

    public AddressBookData(Uri photoUri, String name, String phonenum, boolean isSent, String userId, String username, String photo_url, String isfollow) {
        this.photoUri = photoUri;
        this.name = name;
        this.phonenum = phonenum;
        this.isSent = isSent;
        this.userId = userId;
        this.username = username;
        this.photo_url = photo_url;
        this.isfollow = isfollow;
    }

    public AddressBookData() {

    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getIsfollow() {
        return isfollow;
    }

    public void setIsfollow(String isfollow) {
        this.isfollow = isfollow;
    }
}
