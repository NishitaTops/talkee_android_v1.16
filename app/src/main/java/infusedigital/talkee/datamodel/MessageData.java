package infusedigital.talkee.datamodel;

import java.util.ArrayList;

import infusedigital.talkee.model.User;

/**
 * Created by Super on 8/25/2016.
 */
public class MessageData {

    public User user;
    public String messagePath;
    public String createdTime;
    public String totalLength;
    public String userType;
    public int progress;
    public boolean isemoji;


    public MessageData(User user1, String message_path, String created_time,  String total_length, String user_type, int progress1, boolean isEmoji){
        user = user1;
        messagePath = message_path;
        createdTime = created_time;
        totalLength = total_length;
        userType = user_type;
        progress = progress1;

        isemoji = isEmoji;
    }
}
