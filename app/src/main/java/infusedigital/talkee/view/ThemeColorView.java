package infusedigital.talkee.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import infusedigital.talkee.R;

/**
 * Created by Super on 8/6/2016.
 */
public class ThemeColorView extends View {

    public ThemeColorView(Context context) {
        super(context);
        initPaint();
    }

    public ThemeColorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public ThemeColorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    boolean selectstate = false;
    Paint mPaint, mAlphaPaint;

    private float mDensity;

    private void initPaint(){
        mDensity = getResources().getDisplayMetrics().density;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);

        mAlphaPaint = new Paint();
        mAlphaPaint.setAntiAlias(true);
        mAlphaPaint.setStyle(Paint.Style.FILL);
    }

    public void setSelectState(boolean state){
        this.selectstate = state;
    }

    public boolean getSelectState (){
        return selectstate;
    }

    public  void setColor (int color, int alphacolor){
        mPaint.setColor(color);
        mAlphaPaint.setColor(alphacolor);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float mRadius = Math.min(getWidth(), getHeight()) / 2;
        float centerY = getHeight() / 2;
        float centerX = getWidth() / 2;

        if (selectstate){
            canvas.drawCircle(centerX, centerY, mRadius, mAlphaPaint);
            canvas.drawCircle(centerX, centerY, mRadius - dip2px(3), mPaint);
        }else{
            canvas.drawCircle(centerX, centerY, mRadius, mPaint);
        }
    }

    private int dip2px (float dpValue){
        return (int)(dpValue *mDensity + 0.5f);
    }
}
