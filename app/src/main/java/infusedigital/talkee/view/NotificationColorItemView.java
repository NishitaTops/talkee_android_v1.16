package infusedigital.talkee.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import infusedigital.talkee.R;

/**
 * Created by gstream on 7/9/2016.
 */
public class NotificationColorItemView extends View {

    boolean selectstate = false;
    int color;
    float mRadius;
    Paint selectedPaint, originPaint, whitePaint;

    private float mDensity;

    public NotificationColorItemView(Context context) {
        super(context);
        initPaint();
    }

    public NotificationColorItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public NotificationColorItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    private void initPaint() {
        mDensity = getResources().getDisplayMetrics().density;
        selectedPaint = new Paint();
        selectedPaint.setAntiAlias(true);
        selectedPaint.setStyle(Paint.Style.FILL);
        selectedPaint.setColor(Color.parseColor("#17D5E3"));
//        selectedPaint.setColor(Color.parseColor("#35eb94"));
//        selectedPaint.setColor(Color.parseColor("#16D5DF")); // OLD
//        selectedPaint.setColor(Color.parseColor("#fbfbfb"));

        whitePaint = new Paint();
        whitePaint.setAntiAlias(true);
        whitePaint.setStyle(Paint.Style.FILL);
        whitePaint.setColor(Color.parseColor("#fbfbfb"));
//        whitePaint.setColor(Color.parseColor("#fbfbfb"));
    }

    public void setColor(int color) {
        this.color = color;
        selectedPaint.setColor(color);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float mRadius = getWidth() / 3;

        canvas.drawRoundRect(new RectF(0, 0, getWidth(), getHeight()), (int) mRadius, (int) mRadius, whitePaint);
        canvas.drawRoundRect(new RectF(dip2px(3), dip2px(3), getWidth() - dip2px(3), getHeight() - dip2px(3)), ((getWidth() - dip2px(3)) / 3), ((getWidth() - dip2px(3)) / 3), selectedPaint);
    }

    private int dip2px(float dpValue) {
        return (int) (dpValue * mDensity + 0.5f);
    }
}
