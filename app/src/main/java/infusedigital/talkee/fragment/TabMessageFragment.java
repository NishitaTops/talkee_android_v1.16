package infusedigital.talkee.fragment;

import android.animation.AnimatorSet;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import infusedigital.talkee.R;
import infusedigital.talkee.activity.AddFriendActivity;
import infusedigital.talkee.activity.FriendRequestActivity;
import infusedigital.talkee.activity.MessageScreen;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.activity.TabViewActivity;
import infusedigital.talkee.adapters.HomeAdapter;
import infusedigital.talkee.animation.CirclePlayAngleAnimation;
import infusedigital.talkee.animation.Circle_Play;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.datamodel.MessageThard;
import infusedigital.talkee.datamodel.MessageURLs;
import infusedigital.talkee.helper.DBProvider;
import infusedigital.talkee.utilities.AlertDialogManager;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;
import infusedigital.talkee.view.NotificationColorItemView;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by gstream on 5/23/2016.
 */
public class TabMessageFragment extends Fragment implements AdapterView.OnItemClickListener, SensorEventListener {
    private static final String ARG_POSITION = "position";

    static public GridView grid_received/*, grid_sent*/;
    TextView tvTitleReceiveMsg, tvTitleSentMsg, tvEmptyReceivedView, tvEmptySentView;

    private ProgressDialog loadingDialog;
    ProgressBar mProgressBar;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    SharedPreferences prefs = null;
//    ArrayList message_list;

    String userId;

    int notification_count = 0;

    TabViewActivity activity;

    View rootView;
    private MediaPlayer mp;
    private boolean isMsgPlaying = false;

    ImageView anim_pause/*, playBG*/;
    AnimatorSet prepareAnimator;
    AnimatorSet completeAnimator;
    Context context;

    //    ArrayList<HomeListData> mArraySentMsg = new ArrayList<>();
//    ArrayList<HomeListData> mArrayReceivedMsg = new ArrayList<>();
    ArrayList<MessageThard> mArrayAllData = new ArrayList<>();
    //    private SwipeRefreshLayout swipeContainer;
    private AlertDialogManager alert;
    private long mDuration;
    boolean isFirst = true, isFirstPlay = true;
    int playStatus = 0;
    private String strFriendId, strAI;
    private TextView tv_notification_count;
    NotificationColorItemView notification_color;
    private RelativeLayout default_state;
    private boolean isStatusCall = false;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private AudioManager mAudioManager;
    private int page = 1;
    private boolean hasNext = false;
    private boolean isLoading;
//    private int latPos = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_tab_message, container, false);
        ViewCompat.setElevation(rootView, 50);

        System.out.println("++++++++ Tab called" + MyApplication.getInstance().getMessageDir());
        context = getActivity();
        activity = (TabViewActivity) getActivity();

        prefs = activity.getSharedPreferences("UserInfo", activity.MODE_PRIVATE);
        userId = prefs.getString("userId", "null");

        tvEmptyReceivedView = (TextView) rootView.findViewById(R.id.tvEmptyReceivedView);
        tvEmptySentView = (TextView) rootView.findViewById(R.id.tvEmptySentView);
        tvTitleSentMsg = (TextView) rootView.findViewById(R.id.tvTitleSentMsg);
        tvTitleReceiveMsg = (TextView) rootView.findViewById(R.id.tvTitleReceiveMsg);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.mProgressBarLoad);

        AppConfig.userID = AppConfig.getUserId(context);
        setProximitySensor();
//        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                isFirstPlay = true;
//                getDetails();
//            }
//        });

        grid_received = (GridView) rootView.findViewById(R.id.grid_received);
//        grid_sent = (GridView) rootView.findViewById(R.id.grid_sent);
//        message_list = new ArrayList();
        mArrayAllData = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            grid_received.setNestedScrollingEnabled(true);
        }

//        contactSync();
        getDetails();

        grid_received.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                System.out.println("++++++ setOnItemClickListener:" + position);
                if (!isStatusCall) {

                }
                if (mArrayAllData.get(position).getName().equalsIgnoreCase("ADD FRIENDS")) {
                    AppConfig.isEditProf = false;
                    Intent nextIntent = new Intent(context, AddFriendActivity.class);
//                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(nextIntent);
                } else if (mArrayAllData.get(position).getName().equalsIgnoreCase("Requests")) {
                    Intent requestIntent = new Intent(activity, FriendRequestActivity.class);
//                    mActivity.startActivity(requestIntent);
                    startActivityForResult(requestIntent, 1);
                } else if (Integer.parseInt(mArrayAllData.get(position).getMsg_count()) > 0 /*&&
                        mArrayAllData.get(position).getMsg_thrade().size() > 0*/) {

                    notification_color = (NotificationColorItemView) view.findViewById(R.id.notification_color);
                    default_state = (RelativeLayout) view.findViewById(R.id.default_status);
                    tv_notification_count = (TextView) view.findViewById(R.id.notification_count);
                    anim_pause = (ImageView) view.findViewById(R.id.notify_pause_btn);
//                    playBG = (ImageView) view.findViewById(R.id.playBG);
                    int audioPos = mArrayAllData.get(position).getMsg_thrade().size() - 1;
                    String url = mArrayAllData.get(position).getMsg_thrade().get(audioPos).getMsg_url();
                    Circle_Play anim_cir = (Circle_Play) view.findViewById(R.id.play_message_circle);
                    anim_cir.setColor(getResources().getColor(R.color.rec_play_stroke));

                    if (mp == null && isFirstPlay && playStatus == 0) {
                        tv_notification_count.setVisibility(View.GONE);
                        notification_color.setVisibility(View.GONE);
                        default_state.setVisibility(View.GONE);
//                        playBG.setVisibility(View.VISIBLE);
                        System.out.println("++++++ audio start");
                        System.out.println("++++++ setOnItemClickListener:" + position + ":" + mArrayAllData.get(position).getMsg_count()
                                + ":" + mArrayAllData.get(position).getMsg_thrade().get(audioPos).getMsg_url());

                        strFriendId = mArrayAllData.get(position).getUserId().toString();
                        strAI = mArrayAllData.get(position).getMsg_thrade().get(audioPos).getAI();
                        anim_pause.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_home_pause));
                        try {
//                            if (Utility.fileExist(MyApplication.getInstance().getMessageDir() +
//                                    url.substring(url.lastIndexOf("/") + 1))) {
                            if (isDownloaded(url)) {
                                mp = MediaPlayer.create(activity, Uri.parse(MyApplication.getInstance().getMessageDir() +
                                        url.substring(url.lastIndexOf("/") + 1)));
                                System.out.println("++++++++++++ play URI:" + MyApplication.getInstance().getMessageDir() +
                                        url.substring(url.lastIndexOf("/") + 1));
                            } else {
                                new DownloadTask(url, userId, mArrayAllData.get(position).getUserId()).execute();
                                mp = MediaPlayer.create(activity, Uri.parse(url));
                            }
                            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mediaPlayer) {
                                    System.out.println("+++++++ audio completed");
                                    tv_notification_count.setVisibility(View.VISIBLE);
                                    notification_color.setVisibility(View.VISIBLE);
                                    default_state.setVisibility(View.VISIBLE);
                                    mp = null;
                                    isFirstPlay = true;
                                    playStatus = 0;
                                    anim_pause.setVisibility(View.GONE);
//                                    playBG.setVisibility(View.GONE);
                                    CirclePlayAngleAnimation.cancelProgress();
                                    updateAudioStatus();
                                }
                            });
                            mDuration = mp.getDuration();
                            mp.start();
                            anim_pause.setVisibility(View.VISIBLE);
                            if (isFirstPlay) {
                                CirclePlayAngleAnimation.recordingProgress(anim_cir, mDuration);
                                isFirstPlay = false;
                                playStatus = 1;
                            } else {
                                CirclePlayAngleAnimation.pauseProgress();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (!isFirstPlay && mp != null && playStatus == 1) {
                        System.out.println("++++++ audio paused.");
                        anim_pause.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_home_play));
                        playStatus = 2;
                        mp.pause();
                        mDuration = mp.getDuration();
                        CirclePlayAngleAnimation.pauseProgress();
                    } else if (!isFirstPlay && mp != null && playStatus == 2) {
                        System.out.println("++++++ audio resumed.");
                        playStatus = 1;
                        anim_pause.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_home_pause));
                        mp.start();
                        mDuration = mp.getDuration();
                        CirclePlayAngleAnimation.resumeProgress();
                    }
                } else {
                    Intent chatIntent = new Intent(activity, MessageScreen.class);
                    chatIntent.putExtra("sender_id", mArrayAllData.get(position).getUserId());
                    chatIntent.putExtra("friendAvatar", mArrayAllData.get(position).getImage_url());
                    chatIntent.putExtra("currentAvatar", prefs.getString("photoUrl", ""));
                    chatIntent.putExtra("friendUserName", mArrayAllData.get(position).getUsername());
                    chatIntent.putExtra("currentUserName", prefs.getString("name", ""));
//                    startActivity(chatIntent);
                    startActivityForResult(chatIntent, 1);
                    activity.overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
                }
            }
        });

        grid_received.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                System.out.println("+++++++++++++++ mArrayAllData.get(position).getName():" + mArrayAllData.get(position).getName());
                if (!mArrayAllData.get(position).getName().equalsIgnoreCase("Requests") &&
                        !mArrayAllData.get(position).getName().equalsIgnoreCase("ADD FRIENDS")) {
                    LayoutInflater li = LayoutInflater.from(activity);
                    View promptsView = li.inflate(R.layout.block_popup, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                    alertDialogBuilder.setView(promptsView);
                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    final TextView tvUsername = (TextView) promptsView.findViewById(R.id.tvUsername);
                    final TextView tvUnFriend = (TextView) promptsView.findViewById(R.id.tvUnFriend);
                    final TextView tvBlock = (TextView) promptsView.findViewById(R.id.tvBlock);
                    final TextView tvCancel = (TextView) promptsView.findViewById(R.id.tvCancel);

                    tvUsername.setText(mArrayAllData.get(position).getUsername());

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });

                    tvUnFriend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            unFriendCall(mArrayAllData.get(position).getUserId());
                        }
                    });

                    tvBlock.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            blockTheFriend(mArrayAllData.get(position).getUserId());
                        }
                    });
                    alertDialogBuilder.setCancelable(false);
                    alertDialog.show();
                }
                return true;
            }
        });

        grid_received.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                int threshold = 1;
                int count = grid_received.getCount();
//                latPos = count;
                if (scrollState == SCROLL_STATE_IDLE) {
                    System.out.println("++++++++++++++ SCROLL_STATE_IDLE");
                    if (grid_received.getLastVisiblePosition() >= count - threshold) {
                        System.out.println("++++++++++++++ loading more data");
                        if (hasNext) {
                            if (!isLoading) {
                                isLoading = true;
                                page++;
                                mProgressBar.setVisibility(View.VISIBLE);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        getDetails();
                                    }
                                });

                            }
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                System.out.println("+++++++++++++++++ lastInScreen:" + lastInScreen);
//                if ((lastInScreen == totalItemCount) && hasNext) {
//                    if (!isLoading) {
//                        isLoading = true;
//                        page++;
//                        mProgressBar.setVisibility(View.VISIBLE);
////                        latPos = grid_received.getLastVisiblePosition();
//                        getDetails();
//                    }
//                }
            }
        });

//        grid_received.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
//                if (scrollState == SCROLL_STATE_IDLE) {
//                    if (grid_received.getLastVisiblePosition() >= grid_received.getCount() - 1 - 100) {
//                        //load more list items:
//                        page++;
//                        getDetails();
//                    }
//                }
//            }
//
//            @Override
//            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
//
//            }
//
//        });

//        grid_received.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                NotificationListData data = (NotificationListData) activity.notificationList.get(position);
//
//                if (data.notification_count < 1) {
//                    // go to chat screen
//                    activity.friendInfo = data;
//                    Intent chatIntent = new Intent(activity, MessageScreen.class);
//                    startActivity(chatIntent);
//                    activity.overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
//                } else {
//                    if (data.requestType == 2) {
//                        // this is group request
//                        if (isMsgPlaying) return;
//
//                        message_list = data.message_list;
//                        for (int i = 0; i < data.message_list.size(); i++) {
//                            Log.e("Super", "message list = " + data.message_list.get(i));
//                        }
//                        Circle_Play anim_cir = (Circle_Play) view.findViewById(R.id.play_message_circle);
//                        anim_cir.setColor(getResources().getColor(R.color.rec_play_stroke));
//
//                        anim_pause = (ImageView) view.findViewById(R.id.notify_pause_btn);
//
//                        anim_pause.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                if (mp != null) {
//                                    mp.stop();
//                                    mp.release();
//                                    mp = null;
//                                }
//
//                                if (prepareAnimator.isRunning()) {
//                                    prepareAnimator.removeAllListeners();
//                                    prepareAnimator.cancel();
//                                    prepareAnimator = null;
//                                }
//                                CirclePlayAngleAnimation.cancelProgress();
//                                grid_received.setAdapter(new GridListAdapter(activity, activity.notificationList));
//                                isMsgPlaying = false;
//                            }
//                        });
//
//                        playMessage((String) message_list.get(0), view, data);
//
//                    } else if (data.requestType == 1) {
//                        // this is friend request
//                        Intent requestIntent = new Intent(activity, FriendRequestActivity.class);
//                        startActivity(requestIntent);
//                    } else {
//                        if (isMsgPlaying) return;
//
//                        message_list = data.message_list;
//
//                        Circle_Play anim_cir = (Circle_Play) view.findViewById(R.id.play_message_circle);
//                        anim_cir.setColor(getResources().getColor(R.color.rec_play_stroke));
//
//                        anim_pause = (ImageView) view.findViewById(R.id.notify_pause_btn);
//
//                        anim_pause.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                if (mp != null) {
//                                    mp.stop();
//                                    mp.release();
//                                    mp = null;
//                                }
//
//                                if (prepareAnimator.isRunning()) {
//                                    prepareAnimator.removeAllListeners();
//                                    prepareAnimator.cancel();
//                                    prepareAnimator = null;
//                                }
//                                CirclePlayAngleAnimation.cancelProgress();
//                                grid_received.setAdapter(new GridListAdapter(activity, activity.notificationList));
//                                isMsgPlaying = false;
//                            }
//                        });
//
//                        playMessage((String) message_list.get(0), view, data);
//                    }
//                }
//            }
//        });

//        grid_received.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//
//                final NotificationListData data = (NotificationListData) activity.notificationList.get(position);
//                if (data.requestType == 1)
//                    return true;
//
//                LayoutInflater li = LayoutInflater.from(getActivity());
//                View promptsView = li.inflate(R.layout.removeprompts, null);
//
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//
//                // set prompts.xml to alertdialog builder
//                alertDialogBuilder.setView(promptsView);
//                TextView dialogtitle = (TextView) promptsView.findViewById(R.id.deletedialogtitle);
//                if (data.group_id == -1) {
//                    dialogtitle.setText("Are you sure to Delete Friend ?");
//                } else {
//                    dialogtitle.setText("Are you sure to Delete Group ?");
//                }
//
//                // set dialog message
//                alertDialogBuilder
//                        .setCancelable(false)
//                        .setPositiveButton("OK",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        if (data.group_id == -1)
//                                            DeleteFriends(userId, data);
//                                        else
//                                            DeleteGroup(userId, data);
//                                    }
//                                })
//                        .setNegativeButton("Cancel",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.cancel();
//                                    }
//                                });
//
//                // create alert dialog
//                AlertDialog alertDialog = alertDialogBuilder.create();
//                // show it
//                alertDialog.show();
//                return true;
//            }
//        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filters
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getExtras().getString("message");

                    if (message.equals("receiveR")) {
                        Toast.makeText(activity, "You received new friend request.", Toast.LENGTH_LONG).show();
                        activity.friendRequestData.clear();
                        getVoiceMessages();
                    } else if (message.equals("receiveM")) {
                        Toast.makeText(activity, "You received a new message.", Toast.LENGTH_LONG).show();
                        getVoiceMessages();
                    } else if (message.equals("updatestatus")) {
                        getVoiceMessages();
                    } else {
                        Toast.makeText(activity, "Added new friend.", Toast.LENGTH_LONG).show();
                        activity.friendRequestData.clear();
                        getVoiceMessages();
                    }
                }
            }
        };

//        if (!DBProvider.isExists("http://gettalkee.com/talkee_app/uploads/message/Talkee-hello.m4a"))
//            new DownloadTask("http://gettalkee.com/talkee_app/uploads/message/Talkee-hello.m4a", userId, "700").execute();
//        else
//            System.out.println("++++++++++++++ exist 1");
//
//        if (!DBProvider.isExists("http://gettalkee.com/talkee_app/uploads/message/113ae5c-1481478554572.02.m4a"))
//            new DownloadTask("http://gettalkee.com/talkee_app/uploads/message/113ae5c-1481478554572.02.m4a", userId, "700").execute();
//        else
//            System.out.println("++++++++++++++ exist 2");
//
//        if (!DBProvider.isExists("http://gettalkee.com/talkee_app/uploads/message/d103e6c-1481787217146.56.m4a"))
//            new DownloadTask("http://gettalkee.com/talkee_app/uploads/message/d103e6c-1481787217146.56.m4a", userId, "700").execute();
//        else
//            System.out.println("++++++++++++++ exist 3");

        DBProvider.clearAllOlderAudios(context);
        return rootView;
    }

    private void contactSync() {
        ContentResolver cr = context.getContentResolver(); //Activity/Application android.content.Context
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            ArrayList<String> alContacts = new ArrayList<String>();
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        alContacts.add(contactNumber);
                        break;
                    }
                    pCur.close();
                }
            } while (cursor.moveToNext());

            if (alContacts.size() > 0) {
                JSONObject obj_json = new JSONObject();
                JSONArray obj_arr = new JSONArray();
                for (int i = 0; i < alContacts.size(); i++) {
                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("phonenum", alContacts.get(i));
                        obj_arr.put(obj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                System.out.println("+++++++++++++++ final array:" + obj_arr);
                try {
                    obj_json.put("sync", obj_arr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                RequestParams params = new RequestParams();
                params.put("sync", obj_json.toString());
                new MyLoopJPost(context, "", onLoopJPostContactSyncCallComplete, WSMethods.BASE_URL + WSMethods.SYNC_CONT_NUMBER_POST + userId, params);
            }
        }
    }

    /**
     * API parsing of contact sync
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostContactSyncCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    getDetails();
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }
//                    String message = jobj.getString("MESSAGE");
//                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * API call for getting details of home screen
     */
    public void getDetails() {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("paging", page);
//        params.put("user_id", "642");
        if (isFirst && !AppConfig.isRefresh) {
            isFirst = false;
            startLoading();
        }
        isLoading = true;
        new MyLoopJPost(context, "", onLoopJPostHomeScreenDetailsCallComplete, WSMethods.BASE_URL + WSMethods.HOME_THREAD_LIST_POST, params);
    }

    public static HomeAdapter mAdapter;
    /**
     * API parsing of get details of the home screen
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostHomeScreenDetailsCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
//                if (swipeContainer.isRefreshing())
//                    swipeContainer.setRefreshing(false);

                if (mProgressBar.getVisibility() == View.VISIBLE)
                    mProgressBar.setVisibility(View.GONE);

                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) { // social exist
                    if (jobj.getString("IS_NEXT").equalsIgnoreCase("true")) {
                        hasNext = true;
                    } else {
                        hasNext = false;
                    }

                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    JSONArray arrMessages = USER_DETAILS.getJSONArray("message_thard");
//                    JSONArray arrSendMessages = USER_DETAILS.getJSONArray("user_send_msg");
                    String countRequests = USER_DETAILS.getString("user_requests");
                    if (page == 1)
                        mArrayAllData.clear();
//                    mArrayAllData = new ArrayList<>();
                    if (Integer.parseInt(countRequests) > 0) {
//                        mArrayAllData.add(new HomeListData("2", countRequests, null, null));
                        mArrayAllData.add(new MessageThard("0", "Requests", "0", "0", "0", "0", countRequests, null));
                    }

//                    System.out.println("----------------------- size:"+arrMessages.length());
                    boolean noFriends = false;
                    if (arrMessages.length() > 0) {
                        for (int i = 0; i < arrMessages.length(); i++) {
                            JSONObject mData = arrMessages.getJSONObject(i);
                            ArrayList<MessageURLs> arrMsgUrl = new ArrayList<>();
                            JSONArray mMsgUrl = mData.getJSONArray("msg_thrade");
                            if (mMsgUrl.length() > 0) {
                                for (int j = 0; j < mMsgUrl.length(); j++) {
                                    arrMsgUrl.add(new MessageURLs(mMsgUrl.getJSONObject(j).getString("msg_url"),
                                            mMsgUrl.getJSONObject(j).getString("read_state"),
                                            mMsgUrl.getJSONObject(j).getString("AI")));

                                    //TODO check in db if not available will download the audio
                                    if (!DBProvider.isExists(mMsgUrl.getJSONObject(j).getString("msg_url")))
                                        new DownloadTask(mMsgUrl.getJSONObject(j).getString("msg_url"), userId, mData.getString("userId")).execute();
                                }
                            }
                            mArrayAllData.add(new MessageThard(mData.getString("userId"),
                                    mData.getString("name"),
                                    mData.getString("username"),
                                    mData.getString("emoticol"),
                                    mData.getString("mood"),
                                    mData.getString("image_url"),
                                    mData.getString("msg_count"),
                                    arrMsgUrl));
//                            if (mData.getString("username").equals("TalkeeIM")) {
//                                noFriends = true;
//                            } else {
//                                noFriends = false;
//                            }
                        }
                        if (arrMessages.length() > 1) {
                            noFriends = false;
                        } else if (arrMessages.length() == 1 && arrMessages.getJSONObject(0).getString("username").equals("TalkeeIM")) {
                            noFriends = true;
                        }
                    }

                    if (mArrayAllData.size() > 0) {
                        for (int i = 0; i < mArrayAllData.size(); i++) {
                            if (mArrayAllData.get(i).getName().equals("ADD FRIENDS"))
                                mArrayAllData.remove(i);
                        }
                        //TODO remove while not required permanently
                        mArrayAllData.add(new MessageThard("0", "ADD FRIENDS", "0", "0", "0", "0", "0", null));
                    }

                    //TODO remove while not required permanently
//                    mArrayAllData.add(new MessageThard("0", "ADD FRIENDS", "0", "0", "0", "0", "0", null));

//                    if (mArrayAllData.size() == 1 && mArrayAllData.get(0).getUsername().equals("TalkeeIM")) {
//                        noFriends = true;
//                    }

//                    if (mArrayAllData.size() > 0) {
//
////                        tvTitleReceiveMsg.setVisibility(View.VISIBLE);
//                        mAdapter = new HomeAdapter(activity, mArrayAllData);
//                        grid_received.setAdapter(mAdapter);
//                        Utility.setGridViewHeightBasedOnItems(grid_received, 3);
////                        setGridViewHeightBasedOnChildren(grid_received, 3);
//                        mAdapter.notifyDataSetChanged();
//                        System.out.println("++++++++ dat get");
//                        if (AppConfig.isRefresh) {
//                            grid_received.setAdapter(null);
//                            mAdapter = new HomeAdapter(activity, mArrayAllData);
//                            grid_received.setAdapter(mAdapter);
//                            System.out.println("++++++++ dat get refresh:" + AppConfig.isRefresh);
////                            FragmentTransaction ft = getFragmentManager().beginTransaction();
////                            ft.attach(TabMessageFragment.this).commit();
//                            AppConfig.isRefresh = false;
//                        }
//                    } else {
////                        grid_received.setMinimumHeight(200);
////                        grid_received.setEmptyView(tvEmptyReceivedView); // TODO open this for show message
////                        tvTitleReceiveMsg.setVisibility(View.GONE);
//                    }

                    if (mArrayAllData.size() > 0) {
                        if (AppConfig.isRefresh) {
                            grid_received.setAdapter(null);
                            mAdapter = new HomeAdapter(activity, mArrayAllData);
                            grid_received.setAdapter(mAdapter);
                            System.out.println("++++++++ dat get refresh:" + AppConfig.isRefresh);
//                            FragmentTransaction ft = getFragmentManager().beginTransaction();
//                            ft.attach(TabMessageFragment.this).commit();
                            AppConfig.isRefresh = false;
                        }

                        if (mAdapter != null) {
//                            System.out.println("++++++++++++++++++ mAdapter: not null");
                            mAdapter.notifyDataSetChanged();
                            grid_received.smoothScrollToPosition(mArrayAllData.size());
                        } else if (mAdapter == null) {
//                            System.out.println("++++++++++++++++++ mAdapter: null");
                            mAdapter = new HomeAdapter(activity, mArrayAllData);
                            grid_received.setAdapter(mAdapter);
                        }
                        Utility.setGridViewHeightBasedOnItems(grid_received, 3);
//                        grid_received.smoothScrollToPosition(latPos);
                    }
                } else {
                    if (page == 1) {
                        mArrayAllData.clear();
//                        latPos = 0;
                    }

                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }
                }
                isLoading = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * API call for update audio status
     */
    public void updateAudioStatus() {
        isStatusCall = true;
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("friend_id", strFriendId);
        params.put("AI", strAI);
//        if (isFirst) {
//            isFirst = false;
//            startLoading();
//        }
        new MyLoopJPost(context, "", onLoopJPostUpdateStatusCallComplete, WSMethods.BASE_URL + WSMethods.UPDATE_READ_STATUS_POST, params);
    }

    /**
     * API parsing of update status
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostUpdateStatusCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
//                endLoading();
//                if (swipeContainer.isRefreshing())
//                    swipeContainer.setRefreshing(false);

                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    AppConfig.isRefresh = true;
                    isStatusCall = false;
                    getDetails();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    public View getView() {
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

//    private void playMessage(String path, final View anim_view, MessageThard anim_data) {
//        new ProgressBack(path, anim_view, anim_data).execute();
//    }
//
//    class ProgressBack extends AsyncTask<Void, Void, Boolean> {
//        private String url;
//        View view;
//        MessageThard data;
//        boolean isDownloaded;
//        private String filename;
//
//        public ProgressBack(String url, View view, MessageThard data) {
//            this.url = url;
//            this.view = view;
//            this.data = data;
//            isDownloaded = isDownloaded(url);
//        }
//
//        @Override
//        protected void onPreExecute() {
//            if (!isDownloaded)
//                startLoading();
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... arg0) {
//            Log.e("Super", "download url = " + url);
//            if (!isDownloaded) {
//                filename = String.valueOf(System.currentTimeMillis());
//                return DownloadFile(Config.server + url, String.valueOf(data.friend.id), String.valueOf(data.group_id), filename + ".3gp");
//            }
//            return false;
//        }
//
//        @Override
//        protected void onPostExecute(Boolean aVoid) {
//            super.onPostExecute(aVoid);
//
//            if (!isDownloaded) {
//                endLoading();
//            }
//
//            if (aVoid) {
//                //insert info to DB
//                ContentValues values = new ContentValues();
//                values.put(DBProvider.FRIEND_ID, data.friend.id);
//                values.put(DBProvider.GROUP_ID, data.group_id);
//                values.put(DBProvider.MESSAGE_URL, url);
//                values.put(DBProvider.STATE, "receive");
//                values.put(DBProvider.FILE_PATH, filename);
//
//                activity.getContentResolver().insert(DBProvider.CONTENT_URI, values);
//            }
//
//            playVoice(url, view, data);
//        }
//    }

//    private void playMessage(String path, final View anim_view, final NotificationListData anim_data) {
//        new ProgressBack(path, anim_view, anim_data).execute();
//    }

//    private void playVoice(String messageurl, final View anim_view, final NotificationListData anim_data) {
//
//        try {
//            if (mp != null) {
//                mp.stop();
//                mp.release();
//                mp = null;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        mp = new MediaPlayer();
//
//        try {
//            String filename = getMessageUrl(messageurl);
//            String mediaURL;
//
//            if (anim_data.group_id == -1)
//                mediaURL = MyApplication.getInstance().getMessageDir() + String.valueOf(anim_data.friend.id) + File.separator + filename + ".3gp";
//            else {
//                int friend_id = getFriendId(messageurl);
//                mediaURL = MyApplication.getInstance().getGroupMessageDir() + String.valueOf(anim_data.group_id) + File.separator +
//                        String.valueOf(friend_id) + File.separator + filename + ".3gp";
//            }
//
//            mp.setDataSource(mediaURL);
//            mp.prepare();
//            isMsgPlaying = true;
//            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mp) {
//                    mp.start();
//
//                    long duration = mp.getDuration();
//
//                    CircleImageView background = (CircleImageView) anim_view.findViewById(R.id.notification_play_image);
//                    ImageView anim_pause = (ImageView) anim_view.findViewById(R.id.notify_pause_btn);
//                    Circle_Play anim_circle = (Circle_Play) anim_view.findViewById(R.id.play_message_circle);
//                    anim_circle.setColor(getResources().getColor(R.color.friend_rec_play_stroke));
//                    background.setVisibility(View.VISIBLE);
//                    anim_pause.setVisibility(View.VISIBLE);
//                    NotificationColorItemView notify_circle = (NotificationColorItemView) anim_view.findViewById(R.id.notification_color);
//                    TextView notify_count = (TextView) anim_view.findViewById(R.id.notification_count);
//                    notify_circle.setVisibility(View.GONE);
//                    notify_count.setVisibility(View.GONE);
//                    CirclePlayAngleAnimation.recordingProgress(anim_circle, duration);
//
//                    Animator playAnimX = ObjectAnimator.ofFloat(anim_view, "scaleX", 1f, 1.1f).setDuration(100);
//                    Animator playAnimY = ObjectAnimator.ofFloat(anim_view, "scaleY", 1f, 1.1f).setDuration(100);
//                    prepareAnimator = new AnimatorSet();
//                    prepareAnimator.playTogether(playAnimX, playAnimY);
//                    prepareAnimator.start();
//
//                    prepareAnimator.addListener(new Animator.AnimatorListener() {
//                        @Override
//                        public void onAnimationStart(Animator animation) {
//
//                        }
//
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//                        }
//
//                        @Override
//                        public void onAnimationCancel(Animator animation) {
//
//                        }
//
//                        @Override
//                        public void onAnimationRepeat(Animator animation) {
//
//                        }
//                    });
//                }
//            });
//
//
//            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    anim_data.notification_count--;
//                    setReadMessage(anim_data);
//                    try {
//                        mp.stop();
//                        mp.release();
//                        mp = null;
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    Animator playAnimX1 = ObjectAnimator.ofFloat(anim_view, "scaleX", 1.1f, 1f).setDuration(100);
//                    Animator playAnimY1 = ObjectAnimator.ofFloat(anim_view, "scaleY", 1.1f, 1f).setDuration(100);
//                    completeAnimator = new AnimatorSet();
//                    completeAnimator.playTogether(playAnimX1, playAnimY1);
//                    completeAnimator.start();
//                    isMsgPlaying = false;
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    private void getGroups() {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/group.php")
//                .setParameter("act", "getgroup")
//                .setParameter("user_id", userId);
//
//        final String access_url = builder.toString();
//
//        startLoading();
//
//        Log.e("Super", "get group message = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (!response.getString("status").equals("SUCCESS")) {
//                                endLoading();
//                                return;
//                            }
//
//                            JSONArray groupuserdata, groupiddata, message;
//
//                            groupuserdata = response.getJSONArray("groupusers");
//                            groupiddata = response.getJSONArray("groupidinfo");
//                            message = response.getJSONArray("messages");
//                            Log.e("Super", "message = " + message);
//
//
//                            for (int i = 0; i < groupiddata.length(); i++) {
//                                //get group_id
//                                int group_id = groupiddata.getInt(i);
//
//                                //create group_id folder as child of group folder
//                                String RootDir = activity.getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.GROUP_DIR +
//                                        File.separator + group_id;
//                                File RootFile = new File(RootDir);
//
//                                if (!RootFile.exists())
//                                    RootFile.mkdir();
//
//                                //get group members
//                                JSONArray groupusers = groupuserdata.getJSONArray(i);
//                                ArrayList<String> messages = new ArrayList<>();
//                                ArrayList<User> groupUserList = new ArrayList<>();
//                                for (int j = 0; j < groupusers.length(); j++) {
//                                    JSONObject jsonObject = groupusers.getJSONObject(j);
//                                    User groupuser = new User();
//                                    groupuser.setUser(jsonObject);
//                                    groupUserList.add(groupuser);
//
//                                    //create group user's folder under group_id folder
//                                    String groupuserDir = RootDir + File.separator + groupuser.id;
//                                    File groupuserFile = new File(groupuserDir);
//                                    if (!groupuserFile.exists())
//                                        groupuserFile.mkdir();
//
//                                    //get messages of each group users
//
//                                    for (int k = 0; k < message.length(); k++) {
//                                        JSONObject messageData = message.getJSONObject(k);
//                                        int user_id = messageData.getInt("user_id");
//                                        int groupid = messageData.getInt("group_id");
//                                        String message_url = messageData.getString("message_url");
//
//                                        if (groupuser.id == user_id && groupid == group_id) {
//
//                                            if (!message_url.contains("upload")) {
//                                                //this is for emoji
//                                                ContentValues values = new ContentValues();
//                                                values.put(DBProvider.FRIEND_ID, user_id);
//                                                values.put(DBProvider.GROUP_ID, groupid);
//                                                values.put(DBProvider.MESSAGE_URL, message_url);
//                                                values.put(DBProvider.STATE, "receive");
//                                                values.put(DBProvider.FILE_PATH, String.valueOf(System.currentTimeMillis()));
//
//                                                activity.getContentResolver().insert(DBProvider.CONTENT_URI, values);
//                                                setReadEmoji(user_id, message_url);
//                                                continue;
//                                            }
//
//                                            messages.add(message_url);
//
//                                            if (!isDownloaded(message_url)) {
//                                                new DownloadTask(message_url, String.valueOf(user_id), String.valueOf(group_id)).execute();
//                                            }
//                                        }
//                                    }
//                                }
//
//                                activity.notificationList.add(new NotificationListData(null, messages.size(), messages, 2, null, group_id, groupUserList));
//                            }
//
//                            grid_received.setAdapter(new GridListAdapter(activity, activity.notificationList));
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        endLoading();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

//    private void getRequests() {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
//                .setParameter("act", "getrequests")
//                .setParameter("user_id", userId);
//
//        final String access_url = builder.toString();
//        Log.e("Super", "get request = " + access_url);
//
//        startLoading();
//
//        final JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (!response.getString("status").equals("SUCCESS")) {
//                                endLoading();
//                                return;
//                            }
//
//                            JSONArray data;
//
//                            data = response.getJSONArray("data");
//
//                            activity.friendRequestData.clear();
//                            for (int i = 0; i < data.length(); i++) {
//                                JSONObject jsonData = data.getJSONObject(i);
//
//                                User friend = new User();
//                                friend.setUser(jsonData);
//
//                                activity.friendRequestData.add(friend);
//                            }
//
//                            if (data.length() > 0) {
//                                activity.notificationList.add(new NotificationListData(null, activity.friendRequestData.size(), null, 1, activity.friendRequestData, -1, null));
//                            }
//
//                            getGroups();
////                            grid_received.setAdapter(new GridListAdapter(activity, notificationList));
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        endLoading();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

    public void getVoiceMessages() {

//        MyApplication.getInstance().cancelPendingRequests(MyApplication.getInstance().TAG);
//
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
//                .setParameter("act", "getfriends")
//                .setParameter("user_id", userId);
//
//        String access_url = builder.toString();
//
//        startLoading();
//        activity.notificationList.clear();
//        Log.e("Super", "get voice message = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (!response.getString("status").equals("SUCCESS")) {
//                                endLoading();
//                                return;
//                            }
//
//                            JSONArray data, message;
//
//                            data = response.getJSONArray("friend");
//                            message = response.getJSONArray("message");
//
//                            for (int i = 0; i < data.length(); i++) {
//                                JSONObject jsonData = data.getJSONObject(i);
//
//                                User friend = new User();
//                                friend.setUser(jsonData);
//
//                                String RootDir = activity.getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator + friend.id;
//                                File RootFile = new File(RootDir);
//
//                                if (!RootFile.exists())
//                                    RootFile.mkdir();
//
//                                ArrayList<String> messages = new ArrayList<>();
//
//                                for (int k = 0; k < message.length(); k++) {
//                                    JSONObject messageData = message.getJSONObject(k);
//                                    int user_ID = messageData.getInt("user_id");
//                                    String message_url = messageData.getString("message_url");
//
//                                    if (friend.id == user_ID) {
//
//                                        if (!message_url.contains("upload")) {
//                                            ContentValues values = new ContentValues();
//                                            values.put(DBProvider.FRIEND_ID, user_ID);
//                                            values.put(DBProvider.GROUP_ID, "-1");
//                                            values.put(DBProvider.MESSAGE_URL, message_url);
//                                            values.put(DBProvider.STATE, "receive");
//                                            values.put(DBProvider.FILE_PATH, String.valueOf(System.currentTimeMillis()));
//
//                                            activity.getContentResolver().insert(DBProvider.CONTENT_URI, values);
//                                            setReadEmoji(user_ID, message_url);
//                                            continue;
//                                        }
//
//                                        messages.add(message_url);
//
//                                        if (!isDownloaded(message_url)) {
//                                            //Download message and save db.
//                                            new DownloadTask(message_url, String.valueOf(user_ID), "-1").execute();
//                                        }
//                                    }
//                                }
//
//                                activity.notificationList.add(new NotificationListData(friend, messages.size(), messages, 0, null, -1, null));
//                            }
//
//                            getRequests();
//
////                            grid_received.setAdapter(new GridListAdapter(activity, notificationList));
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        endLoading();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
    }

//    private void setReadMessage(NotificationListData data) {
//        URIBuilder builder = new URIBuilder();
//
//        int friendId = getFriendId(data.message_list.get(message_list.size() - 1));
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
//                .setParameter("act", "setreadmessage")
//                .setParameter("user_id", String.valueOf(MyApplication.currentUser.id))
//                .setParameter("friend_id", String.valueOf(friendId))
//                .setParameter("message_url", data.message_list.get(message_list.size() - 1));
//
//        String access_url = builder.toString();
//
//        startLoading();
//
//        Log.e("Super", "set read ,essage = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        endLoading();
//                        try {
//                            activity.notificationList.clear();
//                            getVoiceMessages();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

//    private void setReadEmoji(int friendId, String messageurl) {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
//                .setParameter("act", "setreadmessage")
//                .setParameter("user_id", String.valueOf(MyApplication.currentUser.id))
//                .setParameter("friend_id", String.valueOf(friendId))
//                .setParameter("message_url", messageurl);
//
//        String access_url = builder.toString();
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

    public void startLoading() {
//        mProgressBar.setVisibility(View.VISIBLE);
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(getActivity(), R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    public void endLoading() {
//        mProgressBar.setVisibility(View.GONE);
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(mRegistrationBroadcastReceiver);
        mAudioManager.setMode(AudioManager.MODE_NORMAL);
        try {
            mSensorManager.unregisterListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_FASTEST);

        LocalBroadcastManager.getInstance(activity).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        getVoiceMessages();
    }

//    public boolean DownloadFile(String fileURL, String friendid, String groupid, String fileName) {
//        try {
//            String RootDir;
//            File RootFile;
//
//            if (groupid.equals("-1")) {
//                RootDir = activity.getExternalFilesDir(null).getAbsolutePath() + File.separator + Config.MESSAGE_DIR + File.separator + friendid;
//                RootFile = new File(RootDir);
//
//                if (!RootFile.exists())
//                    RootFile.mkdir();
//            } else {
//                RootDir = MyApplication.getInstance().getGroupMessageDir() + groupid;
//                RootFile = new File(RootDir);
//
//                if (!RootFile.exists())
//                    RootFile.mkdir();
//
//                String subDir = RootDir + File.separator + friendid;
//                Log.e("Super", "super = " + subDir);
//                RootFile = new File(subDir);
//                if (!RootFile.exists())
//                    RootFile.mkdir();
//            }
//
//            URL u = new URL(fileURL);
//            HttpURLConnection c = (HttpURLConnection) u.openConnection();
//            c.setRequestMethod("GET");
//            c.setDoOutput(true);
//            c.connect();
//            FileOutputStream f = new FileOutputStream(new File(RootFile, fileName));
//
//            InputStream in = c.getInputStream();
//            byte[] buffer = new byte[1024];
//            int len1 = 0;
//
//            while ((len1 = in.read(buffer)) > 0) {
//                f.write(buffer, 0, len1);
//            }
//            f.close();
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }

//    class DownloadTask extends AsyncTask<Void, Void, Boolean> {
//        private String url;
//        private String friendId;
//        private String groupId;
//        private String filename;
//
//        public DownloadTask(String url, String id, String group_id) {
//            this.url = url;
//            friendId = id;
//            groupId = group_id;
//        }
//
//        @Override
//        protected void onPreExecute() {
//
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... arg0) {
//            filename = String.valueOf(System.currentTimeMillis());
//            boolean result = DownloadFile(Config.server + url, friendId, groupId, filename + ".3gp");
//            return result;
//        }
//
//        @Override
//        protected void onPostExecute(Boolean aVoid) {
//            super.onPostExecute(aVoid);
//
//            if (aVoid) {
//                //insert info to DB
//                ContentValues values = new ContentValues();
//                values.put(DBProvider.FRIEND_ID, friendId);
//                values.put(DBProvider.GROUP_ID, groupId);
//                values.put(DBProvider.MESSAGE_URL, url);
//                values.put(DBProvider.STATE, "receive");
//                values.put(DBProvider.FILE_PATH, filename);
//
//                activity.getContentResolver().insert(DBProvider.CONTENT_URI, values);
//            }
//        }
//    }

//    class ProgressBack extends AsyncTask<Void, Void, Boolean> {
//        private String url;
//        View view;
//        NotificationListData data;
//        boolean isDownloaded;
//        private String filename;
//
//        public ProgressBack(String url, View view, NotificationListData data) {
//            this.url = url;
//            this.view = view;
//            this.data = data;
//            isDownloaded = isDownloaded(url);
//        }
//
//        @Override
//        protected void onPreExecute() {
//            if (!isDownloaded)
//                startLoading();
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... arg0) {
//            Log.e("Super", "download url = " + url);
//            if (!isDownloaded) {
//                filename = String.valueOf(System.currentTimeMillis());
//
//                return DownloadFile(Config.server + url, String.valueOf(data.friend.id), String.valueOf(data.group_id), filename + ".3gp");
//
//            }
//            return false;
//        }
//
//        @Override
//        protected void onPostExecute(Boolean aVoid) {
//            super.onPostExecute(aVoid);
//
//            if (!isDownloaded) {
//                endLoading();
//            }
//
//            if (aVoid) {
//                //insert info to DB
//                ContentValues values = new ContentValues();
//                values.put(DBProvider.FRIEND_ID, data.friend.id);
//                values.put(DBProvider.GROUP_ID, data.group_id);
//                values.put(DBProvider.MESSAGE_URL, url);
//                values.put(DBProvider.STATE, "receive");
//                values.put(DBProvider.FILE_PATH, filename);
//
//                activity.getContentResolver().insert(DBProvider.CONTENT_URI, values);
//            }
//
//            playVoice(url, view, data);
//        }
//    }

//    private void DeleteGroup(String uId, final NotificationListData fData) {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/group.php")
//                .setParameter("act", "deletegroup")
//                .setParameter("user_id", uId)
//                .setParameter("group_id", String.valueOf(fData.group_id));
//
//        String access_url = builder.toString();
//
//        Log.e("Super", "delete friend = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            activity.notificationList.remove(fData);
//                            grid_received.setAdapter(new GridListAdapter(activity, activity.notificationList));
//
//                            //update DB, delete media folder
//                            updateDB(fData);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

//    private void DeleteFriends(String uId, final NotificationListData fData) {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/get.php")
//                .setParameter("act", "deletefriend")
//                .setParameter("user_id", uId)
//                .setParameter("friend_id", String.valueOf(fData.friend.id));
//
//        String access_url = builder.toString();
//
//        Log.e("Super", "delete friend = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            boolean result = activity.notificationList.remove(fData);
//                            grid_received.setAdapter(new GridListAdapter(activity, activity.notificationList));
//
//                            //update DB, delete media folder
//                            updateDB(fData);
//
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }

//    private void updateDB(NotificationListData data) throws IOException {
//        if (data.group_id == -1) {
//            String deletedPath = MyApplication.getInstance().getMessageDir() + data.friend.id;
//            File deletedDir = new File(deletedPath);
//
//            FileUtils.deleteDirectory(deletedDir);
//
////            if (deletedDir.isDirectory()) {
////                String[] children = deletedDir.list();
////                for (int i = 0; i < children.length; i++) {
////                    new File(deletedDir, children[i]).delete();
////                }
////            }
//
//            Cursor cursor = activity.getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.FRIEND_ID + "=?", new String[]{String.valueOf(data.friend.id)}, null);
//
//            while (cursor.moveToNext()) {
//                int id = cursor.getInt(cursor.getColumnIndex(DBProvider._ID));
//                activity.getContentResolver().delete(DBProvider.CONTENT_URI, DBProvider._ID + "=?", new String[]{String.valueOf(id)});
//            }
//        } else {
//            String deletedPath = MyApplication.getInstance().getGroupMessageDir() + data.group_id;
//            File deletedDir = new File(deletedPath);
//
//            FileUtils.deleteDirectory(deletedDir);
//
//            Cursor cursor = activity.getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.GROUP_ID + "=?", new String[]{String.valueOf(data.group_id)}, null);
//
//            while (cursor.moveToNext()) {
//                int id = cursor.getInt(cursor.getColumnIndex(DBProvider._ID));
//                activity.getContentResolver().delete(DBProvider.CONTENT_URI, DBProvider._ID + "=?", new String[]{String.valueOf(id)});
//            }
//        }
//    }

//    private boolean isDownloaded(String messageurl) {
//        Cursor cursor = activity.getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{messageurl}, null);
//
//        if (cursor.moveToNext()) {
//            return true;
//        } else
//            return false;
//    }

//    private String getMessageUrl(String messageurl) {
//        Cursor cursor = activity.getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{messageurl}, null);
//        String result;
//
//        if (cursor.moveToNext()) {
//            result = cursor.getString(cursor.getColumnIndex(DBProvider.FILE_PATH));
//        } else
//            result = "";
//
//        return result;
//    }

//    private int getFriendId(String messageurl) {
//        Cursor cursor = activity.getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{messageurl}, null);
//        int result;
//
//        if (cursor.moveToNext()) {
//            result = cursor.getInt(cursor.getColumnIndex(DBProvider.FRIEND_ID));
//        } else
//            result = 0;
//
//        return result;
//    }

    /**
     * Call of unBlock req
     */
    public void blockTheFriend(String friend_id) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("friend_id", friend_id);
        params.put("block_flag", "1");
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostBlockFriendCallComplete, WSMethods.BASE_URL + WSMethods.BLOCK_UNBLOCK_POST, params);
    }

    /**
     * API parsing of unBlock req
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostBlockFriendCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    activity.finish();
                    startActivity(getActivity().getIntent());
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Call of unFriend req
     */
    public void unFriendCall(String friend_id) {
        RequestParams params = new RequestParams();
        params.put("user_id", AppConfig.getUserId(context));
        params.put("friend_id", friend_id);
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostUnFriendCallComplete, WSMethods.BASE_URL + WSMethods.UNFRIEND_POST, params);
    }

    /**
     * API parsing of unFriend req
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostUnFriendCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    activity.finish();
                    startActivity(getActivity().getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        String message = jobj.getString("MESSAGE");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void setProximitySensor() {
        mSensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//        mAudioManager.setMode(AudioManager.STREAM_MUSIC);

        if (mSensor == null) {
            Toast.makeText(context, "No Proximity Sensor!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
//        Log.e("distance", "++++++++++++++++++++ " + String.valueOf(sensorEvent.values[0]));
//        Log.e("MaximumRange", "++++++++++++++++++++ " + String.valueOf(mSensor.getMaximumRange()));

        if (sensorEvent.values[0] < mSensor.getMaximumRange()) {
            mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            mAudioManager.setSpeakerphoneOn(false);
//            Log.e("distance", "++++++++++++++++++++ " + "setSpeakerphoneOn : false");
        } else {
            mAudioManager.setMode(AudioManager.MODE_NORMAL);
            mAudioManager.setSpeakerphoneOn(true);
//            Log.e("distance", "++++++++++++++++++++ " + "setSpeakerphoneOn : true");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    /**
     * For downloading the audio and save in database.
     */
    class DownloadTask extends AsyncTask<Void, Void, Boolean> {
        private String downloadURL;
        private String userId;
        private String friendId;

        public DownloadTask(String downloadURL, String userId, String friendId) {
            this.downloadURL = downloadURL;
            this.userId = userId;
            this.friendId = friendId;
            System.out.println("++++++++++++ going to download:" + downloadURL + " --- " + userId + " --- " + friendId);
        }

        @Override
        protected void onPreExecute() {
//            startLoading();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            boolean result = DownloadFile(downloadURL);
            return result;
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);

//            endLoading();

            if (aVoid) {
                System.out.println("+++++++++++++++ download competed:" + aVoid);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String millisInString = dateFormat.format(new Date());

                //insert info to DB
                ContentValues values = new ContentValues();
                values.put(DBProvider.FRIEND_ID, friendId);
                values.put(DBProvider.USER_ID, userId);
                values.put(DBProvider.MESSAGE_URL, downloadURL);
                values.put(DBProvider.FILENAME, downloadURL.substring(downloadURL.lastIndexOf("/") + 1));
                values.put(DBProvider.CREATED, millisInString);

                System.out.println("+++++++++++++++ download competed: url:" + downloadURL + "\n filename:" +
                        downloadURL.substring(downloadURL.lastIndexOf("/") + 1) + "\ntime:" + millisInString +
                        "\nfriendId:" + friendId + "\nuserId:" + userId);

//                if (!DBProvider.isExists(downloadURL))
                context.getContentResolver().insert(DBProvider.CONTENT_URI, values);

                System.out.println("++++++++++++++ isDownloaded:" + downloadURL + "::::" + isDownloaded(downloadURL));
//                Cursor cursor = context.getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{url}, null);
//                if (cursor.moveToNext())
//                    addnewMessageToView(cursor);
            }
        }
    }

    private boolean isDownloaded(String messageurl) {
        Cursor cursor = activity.getContentResolver().query(DBProvider.CONTENT_URI, null, DBProvider.MESSAGE_URL + "=?", new String[]{messageurl}, null);

        if (cursor.moveToNext()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean DownloadFile(String fileURL) {
        try {
            String RootDir;
            File RootFile;

            RootDir = MyApplication.getInstance().getMessageDir();
            RootFile = new File(RootDir);

            if (!RootFile.exists())
                RootFile.mkdir();

            String subDir = RootDir + File.separator;
            RootFile = new File(subDir);
            if (!RootFile.exists())
                RootFile.mkdir();

            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            FileOutputStream f = new FileOutputStream(new File(RootFile, fileURL.substring(fileURL.lastIndexOf("/") + 1)));

            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;

            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}