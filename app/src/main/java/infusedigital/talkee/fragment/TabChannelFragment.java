package infusedigital.talkee.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import infusedigital.talkee.R;

/**
 * Created by gstream on 5/23/2016.
 */
public class TabChannelFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String ARG_POSITION = "position";
    private int position;

    public static TabChannelFragment newInstance(int position) {
        TabChannelFragment f = new TabChannelFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_channel,
                container, false);
        ViewCompat.setElevation(rootView, 50);

        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
