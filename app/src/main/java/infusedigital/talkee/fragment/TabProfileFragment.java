package infusedigital.talkee.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.util.TextUtils;
import de.hdodenhof.circleimageview.CircleImageView;
import infusedigital.talkee.R;
import infusedigital.talkee.activity.AddressBookActivityNew;
import infusedigital.talkee.activity.BlockUsersActivity;
import infusedigital.talkee.activity.LoginViewActivity;
import infusedigital.talkee.activity.MainActivity;
import infusedigital.talkee.activity.MyApplication;
import infusedigital.talkee.adapters.ColorPickerAdapter;
import infusedigital.talkee.app.Config;
import infusedigital.talkee.client.MyLoopJPost;
import infusedigital.talkee.helper.DBProvider;
import infusedigital.talkee.utilities.AlertDialogManager;
import infusedigital.talkee.utilities.AppConfig;
import infusedigital.talkee.utilities.Utility;
import infusedigital.talkee.utilities.WSMethods;
import infusedigital.talkee.view.UserBlankAvatar;

/**
 * Created by gstream on 5/23/2016.
 */
public class TabProfileFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String ARG_POSITION = "position";
    private int position;

    private static String serverUri = "http://gettalkee.com/talkee/uploader.php?";

    private static ProgressDialog loadingDialog;
    AlertDialogManager alert = new AlertDialogManager();
    private String userChoosenTask;

    private String userName;
    //    private String new_name;
    private static String usermood;
    private String notification;
    private static String new_mood = "Available";
//    private String phoneNum;

    static SharedPreferences prefs = null, prefsRegInfo = null;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView add_photo_btn;
    static String photo_url = "";
    private static CircleImageView profile_image;
    private static UserBlankAvatar profile_image_blank;
    TextView profilename;
    public static TextView profileusername;
    static TextView profilemood;
    CircleImageView color_shape;
    static Context context;
    static Switch swNotification;

    public static String color;

    RelativeLayout colorBtn;
    static LinearLayout logoutBtn;
    static LinearLayout llBlockUserList;

    private float mDensity;

    private RelativeLayout current_status;
    private CircleImageView statusCol;
    ArrayList<String> colorLists = new ArrayList<String>() {{
        add("#bc51ba");
        add("#d85780");
        add("#a0dce2");
        add("#868686");
        add("#579cd8");
        add("#eb9722");
        add("#d56759");
        add("#e5d24a");
        add("#2be2e3");
        add("#b0d47e");
        add("#73dfab");
        add("#5c5db1");
    }};

    private GridView colorPickerGridView;
    private static String imagePath = "";
    private static String strFirstName = "";
    private static String strLastName = "";
    private String strStatus = "";
    private static LinearLayout llNotification;
    static Bitmap thumbnail = null;

    public static TabProfileFragment newInstance(int position) {
        TabProfileFragment f = new TabProfileFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_profile, container, false);
//        ViewCompat.setElevation(rootView, 0);

        prefs = getActivity().getSharedPreferences("UserInfo", getActivity().MODE_PRIVATE);
        prefsRegInfo = getActivity().getSharedPreferences("RegisterUserInfo", getActivity().MODE_PRIVATE);
        context = getActivity();
        mDensity = getResources().getDisplayMetrics().density;

        photo_url = prefs.getString("photoUrl", "null");
        System.out.println("+++++ photo_url:" + photo_url);
        userName = prefs.getString("name", "Anonymous");
        usermood = prefs.getString("mood", "Available");
        if (usermood.length() == 0)
            usermood = "Available";
        notification = prefs.getString("notification", "0");
        color = prefs.getString("color", "#27dbd9");
//        phoneNum = prefs.getString("phonenum", "null");

//        Log.e ("Super", "on create view photoUrl:" + photo_url);
//Log.e ("profile name ", userName);

        profile_image = (de.hdodenhof.circleimageview.CircleImageView) rootView.findViewById(R.id.profile_image);
        profile_image_blank = (UserBlankAvatar) rootView.findViewById(R.id.profile_image_blank);

        profilename = (TextView) rootView.findViewById(R.id.profile_name);
        profileusername = (TextView) rootView.findViewById(R.id.profile_username);
        profilemood = (TextView) rootView.findViewById(R.id.profile_mood);
        swNotification = (Switch) rootView.findViewById(R.id.notification_switch);
        current_status = (RelativeLayout) rootView.findViewById(R.id.default_status);
        statusCol = (CircleImageView) rootView.findViewById(R.id.statusCol);
        llNotification = (LinearLayout) rootView.findViewById(R.id.llNotification);

        if (notification.equals("1"))
            swNotification.setChecked(true);
        else
            swNotification.setChecked(false);

        swNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

//                if (!AppConfig.isEditProf) {
                if (isChecked)
                    setNotification(true);
                else
                    setNotification(false);
//                }
            }
        });
        MyApplication.currentUser.photo_url = prefs.getString("photoUrl", "");
        if (!TextUtils.isEmpty(MyApplication.currentUser.photo_url)) {
            Log.e("Super", "photo url profile - " + MyApplication.currentUser.photo_url);
            Log.e("Super", Config.server + MyApplication.currentUser.photo_url);
            profile_image.setVisibility(View.VISIBLE);
            profile_image_blank.setVisibility(View.GONE);
            if (MyApplication.currentUser.photo_url.contains("http")) {
                Picasso.with(getActivity()).load(MyApplication.currentUser.photo_url).placeholder(R.drawable.ic_appicon).fit().into(profile_image);
            } else {
                Picasso.with(getActivity()).load(Config.server + MyApplication.currentUser.photo_url).placeholder(R.drawable.ic_appicon).fit().into(profile_image);
            }
        } else {
            Picasso.with(getActivity()).load(R.drawable.ic_placeholder).placeholder(R.drawable.ic_appicon).fit().into(profile_image);
//            profile_image.setVisibility(View.GONE);
//            profile_image_blank.setVisibility(View.VISIBLE);
//            MyApplication.getInstance().setBlankAvatar(profile_image_blank, MyApplication.currentUser.name, true);
//            profile_image.setImageDrawable(getResources().getDrawable(R.drawable.avatar_blank));
        }

        profilename.setText(prefs.getString("name", ""));
        profilename.setTypeface(MyApplication.boldFont);

        profileusername.setText(prefs.getString("firstName", "") + " " + prefs.getString("lastName", ""));
        if (profileusername.getText().toString().trim().length() == 0)
            profileusername.setText("Add");
        profilemood.setText(usermood);
        if (!prefs.getString("photoUrl", "").equals("")) {
            Picasso.with(context).load(prefs.getString("photoUrl", "")).placeholder(R.drawable.ic_appicon).into(profile_image);
        } else {
            Picasso.with(context).load(R.drawable.ic_appicon).placeholder(R.drawable.ic_appicon).into(profile_image);
        }

//        color = MyApplication.currentUser.emoticol;//prefs.getString("color", "#27dbd9");

        add_photo_btn = (ImageView) rootView.findViewById(R.id.plus_profile_photo_btn);

        add_photo_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (AppConfig.isEditProf) {
                selectImage();
//                }
            }
        });

//        profilemood.setTextColor(Color.parseColor("#35eb93"));
        if (usermood.equals("Available")) {
            profilemood.setTextColor(Color.parseColor("#35eb93"));
            statusCol.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb93")));
        } else if (usermood.equals("Away")) {
            profilemood.setTextColor(Color.parseColor("#ffce0c"));
            statusCol.setImageDrawable(new ColorDrawable(Color.parseColor("#ffce0c")));
        } else {
            profilemood.setTextColor(Color.parseColor("#f52e62"));
            statusCol.setImageDrawable(new ColorDrawable(Color.parseColor("#f52e62")));
        }

        if (TextUtils.isEmpty(color)) color = "#27dbd9";

        if (TextUtils.isEmpty(MyApplication.currentUser.emoticol))
            color = "#27dbd9";

        color_shape = (de.hdodenhof.circleimageview.CircleImageView) rootView.findViewById(R.id.profile_color);

        color_shape.setImageDrawable(new ColorDrawable(Color.parseColor(color)));

        colorBtn = (RelativeLayout) rootView.findViewById(R.id.profile_color_btn);
        colorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(getActivity());
                RelativeLayout someLayout = (RelativeLayout) li.inflate(R.layout.colorpickergridlayout, null);
                AlertDialog.Builder colorAlert = new AlertDialog.Builder(getActivity(), R.style.ColorDialogTheme);
                final Dialog d = colorAlert.setView(someLayout).create();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(d.getWindow().getAttributes());
                lp.width = dip2px(280);
                lp.height = dip2px(320);

                d.show();
                d.getWindow().setAttributes(lp);

                colorPickerGridView = (GridView) someLayout.findViewById(R.id.colorPickerGrid);
                colorPickerGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String selectedColor = colorLists.get(position);
                        d.hide();
                        color_shape.setImageDrawable(new ColorDrawable(Color.parseColor(selectedColor)));

                        URIBuilder builder = new URIBuilder();

                        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/update_color.php")
                                .setParameter("userId", String.valueOf(MyApplication.currentUser.id))
                                .setParameter("color", selectedColor);
                        String access_url = builder.toString();

                        Log.e("Super", "profile color = " + access_url);

                        JsonObjectRequest req = new JsonObjectRequest(access_url,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        });

                        MyApplication.getInstance().addToRequestQueue(req);
                    }
                });
                colorPickerGridView.setAdapter(new ColorPickerAdapter(getActivity(), colorLists));
            }
        });

        llBlockUserList = (LinearLayout) rootView.findViewById(R.id.llBlockUserList);
        llBlockUserList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextIntent = new Intent(getActivity(), BlockUsersActivity.class);
                startActivity(nextIntent);
            }
        });

        logoutBtn = (LinearLayout) rootView.findViewById(R.id.logout_btn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        profilemood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (AppConfig.isEditProf) {
                showZenderDialog();
//                }
            }
        });

        profileusername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (AppConfig.isEditProf) {
                LayoutInflater li = LayoutInflater.from(getActivity());
                View promptsView = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        getActivity());

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final TextView mTitle = (TextView) promptsView.findViewById(R.id.textView1);
                final EditText mFirstName = (EditText) promptsView.findViewById(R.id.edtFirstName);
                final EditText mLastName = (EditText) promptsView.findViewById(R.id.edtLastName);
                final TextView tvOK = (TextView) promptsView.findViewById(R.id.tvOK);
                final TextView tvCancel = (TextView) promptsView.findViewById(R.id.tvCancel);

//                    mFirstName.setText(prefs.getString("firstName", ""));
//                    mLastName.setText(prefs.getString("lastName", ""));
                System.out.println("+++++++++ name:" + profileusername.getText().toString());
                if (!profileusername.getText().toString().equals("Add")) {
                    mFirstName.setText(profileusername.getText().toString().split(" ")[0]);
                    if (profileusername.getText().toString().trim().contains(" "))
                        mLastName.setText(profileusername.getText().toString().split(" ")[1]);
                    else
                        mLastName.setText("");
                } else {
                    mFirstName.setText("");
                    mLastName.setText("");
                }
//                mFirstName.setTypeface(MyApplication.lightFont);
//                mLastName.setTypeface(MyApplication.lightFont);
//                mTitle.setTypeface(MyApplication.lightFont);
                // set dialog message
                alertDialogBuilder
                        .setCancelable(false);
//                        .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
//                        .setNegativeButton(android.R.string.cancel, null);
//                            .setPositiveButton("OK",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            if (android.text.TextUtils.isEmpty(mFirstName.getText())) {
//                                                Toast.makeText(getActivity(), "Please enter first name.", Toast.LENGTH_SHORT).show();
//                                            } else if (android.text.TextUtils.isEmpty(mLastName.getText())) {
//                                                Toast.makeText(getActivity(), "Please enter last name", Toast.LENGTH_SHORT).show();
//                                            } else {
//                                                strFirstName = mFirstName.getText().toString();
//                                                strLastName = mLastName.getText().toString();
//                                                profileusername.setText(mFirstName.getText() + " " + mLastName.getText());
//                                                dialog.dismiss();
//                                            }
//                                        }
//                                    })
//                            .setNegativeButton("Cancel",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.dismiss();
//                                        }
//                                    });
                final AlertDialog alertDialog = alertDialogBuilder.create();
//                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
//                    @Override
//                    public void onShow(DialogInterface dialogInterface) {
//                        Button btnOk = ((AlertDialog) dialogInterface).getButton(AlertDialog.BUTTON_POSITIVE);
//                        btnOk.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                if (android.text.TextUtils.isEmpty(mFirstName.getText())) {
//                                    Toast.makeText(getActivity(), "Please enter first name.", Toast.LENGTH_SHORT).show();
//                                } else if (android.text.TextUtils.isEmpty(mLastName.getText())) {
//                                    Toast.makeText(getActivity(), "Please enter last name", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    strFirstName = mFirstName.getText().toString();
//                                    strLastName = mLastName.getText().toString();
//                                    profileusername.setText(mFirstName.getText() + " " + mLastName.getText());
//                                    alertDialog.dismiss();
//                                    saveSettings();
//                                }
//                            }
//                        });
//                        Button btnCancel = ((AlertDialog) dialogInterface).getButton(AlertDialog.BUTTON_NEGATIVE);
//                        btnCancel.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                alertDialog.dismiss();
//                            }
//                        });
//                    }
//                });
                alertDialog.show();
                tvOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (android.text.TextUtils.isEmpty(mFirstName.getText())) {
                            Toast.makeText(getActivity(), "Please enter first name.", Toast.LENGTH_SHORT).show();
                        } else if (android.text.TextUtils.isEmpty(mLastName.getText())) {
                            Toast.makeText(getActivity(), "Please enter last name", Toast.LENGTH_SHORT).show();
                        } else {
                            strFirstName = mFirstName.getText().toString();
                            strLastName = mLastName.getText().toString();
                            profileusername.setText(mFirstName.getText() + " " + mLastName.getText());
                            alertDialog.dismiss();
                            saveSettings(false);
                        }
                    }
                });

                tvCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
//                }
            }
        });

        strFirstName = prefs.getString("firstName", "");
        strLastName = prefs.getString("lastName", "");

        return rootView;
    }

    /**
     * set notification on off
     *
     * @param status
     */
    private void setNotification(boolean status) {
        RequestParams params = new RequestParams();
        params.put("userid", prefs.getString("userId", "null"));
        if (status)
            params.put("notification", "1");
        else
            params.put("notification", "0");
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostSetNotificationCallComplete, WSMethods.BASE_URL + WSMethods.NOTIFICATION_POST, params);
    }

    /**
     * API parsing of set notification on off
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSetNotificationCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) { // Notification on
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    prefs.edit().putString("notification", USER_DETAILS.optString("notification")).commit();
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        Toast.makeText(context, jobj.getString("MESSAGE"), Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * save settings API call
     */
    public static void saveSettings(boolean isShowProgress) {
        RequestParams params = new RequestParams();
        params.put("userid", prefs.getString("userId", "null"));

        if (imagePath.length() > 0) {
            if (imagePath.contains("http")) {
                params.put("profile_image", photo_url);
            } else {
                try {
                    params.put("profile_image", new File(imagePath));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } /*else {
            params.put("profile_image", "");
        }*/

        params.put("first_name", strFirstName);
        params.put("last_name", strLastName);
        params.put("mood", new_mood);
        params.put("color", color);

        if (swNotification.isChecked())
            params.put("notification", "1");
        else
            params.put("notification", "0");

        if (isShowProgress)
            startLoading();

        new MyLoopJPost(context, "", onLoopJPostSaveSettingsCallComplete, WSMethods.BASE_URL + WSMethods.SETTINGS_POST, params);
    }

    /**
     * API parsing of set notification on off
     */
    static MyLoopJPost.OnLoopJPostCallComplete onLoopJPostSaveSettingsCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                editAction(false); // Done save
                if (flag.equalsIgnoreCase("true")) { // Success save Setting
//                    Toast.makeText(context, jobj.getString("MESSAGE"), Toast.LENGTH_LONG).show();
                    JSONObject USER_DETAILS = jobj.getJSONObject("USER_DETAILS");
                    prefs.edit().putString("userId", USER_DETAILS.getString("user_id")).commit();
                    prefs.edit().putString("photoUrl", USER_DETAILS.getString("profile_image")).commit();
                    prefs.edit().putString("name", USER_DETAILS.getString("user_name")).commit();
                    prefs.edit().putString("firstName", USER_DETAILS.getString("first_name")).commit();
                    prefs.edit().putString("lastName", USER_DETAILS.getString("last_name")).commit();
                    prefs.edit().putString("color", USER_DETAILS.getString("color")).commit();
                    prefs.edit().putString("mood", USER_DETAILS.optString("mood")).commit();
//                    prefs.edit().putString("phonenum", USER_DETAILS.getString("user_id")).commit();
                    prefs.edit().putString("gcm_regid", USER_DETAILS.getString("device_token_id")).commit();
//                    prefs.edit().putString("deviceid", USER_DETAILS.getString("user_id")).commit();
                    prefs.edit().putString("loginstate", "true").commit();
                    prefs.edit().putString("notification", USER_DETAILS.optString("notification")).commit();
                    imagePath = "";
                    //Set Data
                    System.out.println("++++++++++++ stored value:" + prefs.getString("firstName", "") + " " + prefs.getString("lastName", ""));
                    profileusername.setText(prefs.getString("firstName", "") + " " + prefs.getString("lastName", ""));
                    usermood = prefs.getString("mood", "Available");
                    if (USER_DETAILS.getString("profile_image").toString().length() > 0) {
                        Picasso.with(context).load(USER_DETAILS.getString("profile_image"))
                                .placeholder(R.drawable.ic_appicon)
                                .into(profile_image);
                    }
                    if (thumbnail != null && !thumbnail.isRecycled()) {
                        thumbnail.recycle();
                        thumbnail = null;
                    }
                    if (usermood.length() == 0)
                        usermood = "Available";
                    profilemood.setText(usermood);
                } else {
                    if (jobj.getString(AppConfig.param_session).equals("0")) {
                        Utility.sessionExpireDialog(context);
                    } else {
                        Toast.makeText(context, jobj.getString("MESSAGE"), Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private void removePhotoImage() {
        profile_image.setVisibility(View.GONE);
        profile_image_blank.setVisibility(View.VISIBLE);
        MyApplication.getInstance().setBlankAvatar(profile_image_blank, MyApplication.currentUser.name, true);

        URIBuilder builder = new URIBuilder();

        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/update_photourl.php")
                .setParameter("userId", String.valueOf(MyApplication.currentUser.id));

        MyApplication.currentUser.photo_url = "";

        String access_url = builder.toString();

        Log.e("Super", "profile photo update = " + access_url);

        JsonObjectRequest req = new JsonObjectRequest(access_url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        MyApplication.getInstance().addToRequestQueue(req);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", /*"Remove Photo",*/ "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

//                if (items[item].equals("Remove Photo")) {
//                    imagePath = "";
//                    profile_image.setVisibility(View.GONE);
//                    profile_image_blank.setVisibility(View.VISIBLE);
//                    MyApplication.getInstance().setBlankAvatar(profile_image_blank, MyApplication.currentUser.name, true);
////                    removePhotoImage();
//                }

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhotoIntent, SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        profile_image.setVisibility(View.VISIBLE);
        profile_image_blank.setVisibility(View.GONE);
//        profile_image.setImageBitmap(Bitmap.createScaledBitmap(thumbnail, 512, 512, false));
        profile_image.setImageBitmap(thumbnail);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imagePath = destination.getPath();
        saveSettings(true);
//        Picasso.with(getActivity()).load(destination.getAbsolutePath()).placeholder(R.drawable.avatar_blank).fit().into(profile_image);

//        String server_uri = serverUri + "userId=" + MyApplication.currentUser.id;

//        new UploadFileAsync(getActivity(), server_uri, destination.getPath(), true).execute("");
    }

    @SuppressWarnings("deprecation")
    public void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        profile_image.setVisibility(View.VISIBLE);
        profile_image_blank.setVisibility(View.GONE);
//        profile_image.setImageBitmap(Bitmap.createScaledBitmap(bm, 512, 512, false));
        profile_image.setImageBitmap(thumbnail);

        String path = getFilePathFromContentUri(data.getData(), getActivity().getContentResolver());
        imagePath = path;
        saveSettings(true);
//        String server_uri = serverUri + "userId=" + MyApplication.currentUser.id;

//        new UploadFileAsync(getActivity(), server_uri, path, true).execute("");
    }

    private static String getFilePathFromContentUri(Uri selectedVideoUri,
                                                    ContentResolver contentResolver) {
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA};

        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();

//        URIBuilder builder = new URIBuilder();

        profileusername.setFocusable(false);
        profileusername.setFocusableInTouchMode(false);
        profilemood.setFocusable(false);
        profilemood.setFocusableInTouchMode(false);

//        new_name = profile_name.getText().toString();
        new_mood = profilemood.getText().toString();

        if (new_mood.isEmpty()) new_mood = "Available";

//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/update_name.php")
//                .setParameter("userId", String.valueOf(MyApplication.currentUser.id))
//                .setParameter("name", new_name)
//                .setParameter("mood", new_mood);

//        prefs.edit().putString("name", new_name).commit();
        prefs.edit().putString("mood", new_mood).commit();

//        MyApplication.currentUser.name = new_name;
        MyApplication.currentUser.mood = new_mood;

//        String access_url = builder.toString();
//
//        Log.e("Super", "profile update = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            String status = response.getString("status");
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("aaaab", error.toString());
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
    }

    private int dip2px(float dpValue) {
        return (int) (dpValue * mDensity + 0.5f);
    }

    private void logout() {
        RequestParams params = new RequestParams();
        params.put("user_id", prefs.getString("userId", "null"));
        startLoading();
        new MyLoopJPost(context, "", onLoopJPostLogoutCallComplete, WSMethods.BASE_URL + WSMethods.LOGOUT_POST, params);
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/login.php")
//                .setParameter("act", "logout")
//                .setParameter("phonenum", MyApplication.currentUser.phonenum);
//
//        String access_url = builder.toString();
//
//        startLoading();
//
//        Log.e("Super", "logout = " + access_url);
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        endLoading();
//                        try {
//                            prefs.edit().putString("userId", "0").commit();
//                            prefs.edit().putString("photoUrl", null).commit();
//                            prefs.edit().putString("name", null).commit();
//                            prefs.edit().putString("color", null).commit();
//                            prefs.edit().putString("mood", null).commit();
//                            prefs.edit().putString("phonenum", null).commit();
//                            prefs.edit().putString("loginstate", "false").commit();
//                            MyApplication.currentUser.initialize();
//
//                            getActivity().finish();
//                            Intent nextIntent = new Intent(getActivity(), LoginViewActivity.class);
//                            startActivity(nextIntent);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                endLoading();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
    }

    /**
     * API parsing of check status of number
     */
    MyLoopJPost.OnLoopJPostCallComplete onLoopJPostLogoutCallComplete = new MyLoopJPost.OnLoopJPostCallComplete() {

        @Override
        public void response(String result) {
            try {
                endLoading();
                JSONObject jobj = new JSONObject(result);
                String flag = jobj.getString("FLAG");
                if (flag.equalsIgnoreCase("true")) {
                    if (!AppConfig.getUserId(context).equals(null)) {
                        ArrayList<String> mArrayList = DBProvider.getAllAudioOfUser(context, AppConfig.getUserId(context));
                        for (int i = 0; i < mArrayList.size(); i++) {
                            DBProvider.deleteRowAndFile(context, mArrayList.get(i));
                        }
                    }

                    prefs.edit().putString("loginstate", "false").commit();
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.clear();
                    editor.apply();

                    SharedPreferences.Editor editor1 = prefsRegInfo.edit();
                    editor1.clear();
                    editor1.apply();

                    AppConfig.social_profile_image = "";
                    AppConfig.inserted_type = "1";
                    AppConfig.social_id = "";
                    AppConfig.social_name = "";
                    AppConfig.social_username = "";
                    AppConfig.social_profile_image = "";

                    getActivity().finish();
                    Intent nextIntent = new Intent(getActivity(), MainActivity.class);
                    nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(nextIntent);
                } else {
                    String message = jobj.getString("MESSAGE");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private static void startLoading() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(context, R.style.ProgressTheme);
            loadingDialog.setCancelable(false);
            loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            loadingDialog.show();
        }
    }

    private static void endLoading() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
                loadingDialog.hide();
            }
            loadingDialog = null;
        }
    }


    void showZenderDialog() {

        final Dialog mediaPicker = new Dialog(getActivity());
        mediaPicker.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Window window = mediaPicker.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        wlp.gravity = Gravity.BOTTOM | Gravity.FILL_HORIZONTAL;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.width = displaymetrics.widthPixels;
        window.setAttributes(wlp);

        mediaPicker.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mediaPicker.setContentView(R.layout.popup_profile_edit);

        TextView availble_Button, busy_Button, awayImage, cancelBtn;
        availble_Button = (TextView) mediaPicker.findViewById(R.id.viewAvailble);
        busy_Button = (TextView) mediaPicker.findViewById(R.id.viewBusy);
        awayImage = (TextView) mediaPicker.findViewById(R.id.viewAway);
        cancelBtn = (TextView) mediaPicker.findViewById(R.id.closeSBtn);

        availble_Button.setText("Available");
        busy_Button.setText("Busy");
        awayImage.setText("Away");

        if (profilemood.getText().toString().equals("Available")) {
            availble_Button.setTextColor(Color.parseColor("#35eb93"));
            busy_Button.setTextColor(Color.parseColor("#000000"));
            awayImage.setTextColor(Color.parseColor("#000000"));
        } else if (profilemood.getText().toString().equals("Busy")) {
            availble_Button.setTextColor(Color.parseColor("#000000"));
            busy_Button.setTextColor(Color.parseColor("#f52e62"));
            awayImage.setTextColor(Color.parseColor("#000000"));
        } else {
            availble_Button.setTextColor(Color.parseColor("#000000"));
            busy_Button.setTextColor(Color.parseColor("#000000"));
            awayImage.setTextColor(Color.parseColor("#ffce0c"));
        }

        availble_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_mood = "Available";
                profilemood.setText("Available");
                profilemood.setTextColor(Color.parseColor("#35eb93"));
                statusCol.setImageDrawable(new ColorDrawable(Color.parseColor("#35eb93")));

                mediaPicker.dismiss();
                saveSettings(false);
//                changeStatus(new_mood);
            }
        });

        busy_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new_mood = "Busy";
                profilemood.setText("Busy");
                profilemood.setTextColor(Color.parseColor("#f52e62"));

                statusCol.setImageDrawable(new ColorDrawable(Color.parseColor("#f52e62")));
                mediaPicker.dismiss();
                saveSettings(false);
//                changeStatus(new_mood);
            }
        });


        awayImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new_mood = "Away";
                profilemood.setText("Away");
                profilemood.setTextColor(Color.parseColor("#ffce0c"));

                statusCol.setImageDrawable(new ColorDrawable(Color.parseColor("#ffce0c")));
                mediaPicker.dismiss();
                saveSettings(false);
//                changeStatus(new_mood);
            }
        });


        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mediaPicker.dismiss();
            }
        });
        mediaPicker.show();

    }

    /**
     * edit action on
     *
     * @param status
     */
    public static void editAction(boolean status) {
        if (status) {
            llNotification.setVisibility(View.GONE);
            llBlockUserList.setVisibility(View.GONE);
            logoutBtn.setVisibility(View.GONE);
        } else {
            llNotification.setVisibility(View.VISIBLE);
            llBlockUserList.setVisibility(View.VISIBLE);
            logoutBtn.setVisibility(View.VISIBLE);
        }
    }

//    private void changeStatus(String status) {
//        URIBuilder builder = new URIBuilder();
//
//        builder.setScheme("http").setHost(Config.SERVER_URL).setPath("/talkee/send_message.php")
//                .setParameter("regId", "kk")
//                .setParameter("userId", String.valueOf(MyApplication.currentUser.id))
//                .setParameter("friendId", "1")
//                .setParameter("status", status)
//                .setParameter("requestType", "3");
//
//        String access_url = builder.toString();
//
//        Log.e("Super", "profile change states = " + access_url);
//
//        JsonObjectRequest req = new JsonObjectRequest(access_url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        try {
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(req);
//    }
}
